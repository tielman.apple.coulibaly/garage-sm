package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._TypeElementFicheControlRepository;

/**
 * Repository : TypeElementFicheControl.
 */
@Repository
public interface TypeElementFicheControlRepository extends JpaRepository<TypeElementFicheControl, Integer>, _TypeElementFicheControlRepository {
	/**
	 * Finds TypeElementFicheControl by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object TypeElementFicheControl whose id is equals to the given id. If
	 *         no TypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from TypeElementFicheControl e where e.id = :id and e.isDeleted = :isDeleted")
	TypeElementFicheControl findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds TypeElementFicheControl by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object TypeElementFicheControl whose libelle is equals to the given libelle. If
	 *         no TypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from TypeElementFicheControl e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	TypeElementFicheControl findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeElementFicheControl by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object TypeElementFicheControl whose code is equals to the given code. If
	 *         no TypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from TypeElementFicheControl e where e.code = :code and e.isDeleted = :isDeleted")
	TypeElementFicheControl findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeElementFicheControl by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object TypeElementFicheControl whose isDeleted is equals to the given isDeleted. If
	 *         no TypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from TypeElementFicheControl e where e.isDeleted = :isDeleted")
	List<TypeElementFicheControl> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeElementFicheControl by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object TypeElementFicheControl whose createdAt is equals to the given createdAt. If
	 *         no TypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from TypeElementFicheControl e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<TypeElementFicheControl> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeElementFicheControl by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object TypeElementFicheControl whose updatedAt is equals to the given updatedAt. If
	 *         no TypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from TypeElementFicheControl e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<TypeElementFicheControl> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeElementFicheControl by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object TypeElementFicheControl whose deletedAt is equals to the given deletedAt. If
	 *         no TypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from TypeElementFicheControl e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<TypeElementFicheControl> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeElementFicheControl by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object TypeElementFicheControl whose createdBy is equals to the given createdBy. If
	 *         no TypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from TypeElementFicheControl e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<TypeElementFicheControl> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeElementFicheControl by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object TypeElementFicheControl whose updatedBy is equals to the given updatedBy. If
	 *         no TypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from TypeElementFicheControl e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<TypeElementFicheControl> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeElementFicheControl by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object TypeElementFicheControl whose deletedBy is equals to the given deletedBy. If
	 *         no TypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from TypeElementFicheControl e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<TypeElementFicheControl> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of TypeElementFicheControl by using typeElementFicheControlDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of TypeElementFicheControl
	 * @throws DataAccessException,ParseException
	 */
	public default List<TypeElementFicheControl> getByCriteria(Request<TypeElementFicheControlDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from TypeElementFicheControl e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<TypeElementFicheControl> query = em.createQuery(req, TypeElementFicheControl.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of TypeElementFicheControl by using typeElementFicheControlDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of TypeElementFicheControl
	 *
	 */
	public default Long count(Request<TypeElementFicheControlDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from TypeElementFicheControl e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<TypeElementFicheControlDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		TypeElementFicheControlDto dto = request.getData() != null ? request.getData() : new TypeElementFicheControlDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (TypeElementFicheControlDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(TypeElementFicheControlDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
