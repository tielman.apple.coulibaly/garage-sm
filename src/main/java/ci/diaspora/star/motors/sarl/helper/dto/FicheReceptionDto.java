
/*
 * Java dto for entity table fiche_reception
 * Created on 2019-09-10 ( Time 09:02:06 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.SearchParam;
import ci.diaspora.star.motors.sarl.helper.dto.customize._FicheReceptionDto;

/**
 * DTO for table "fiche_reception"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class FicheReceptionDto extends _FicheReceptionDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    userId               ;
	/*
	 * 
	 */
    private Integer    vehiculeId           ;
	/*
	 * 
	 */
    private Integer    couleurVehiculeId    ;
	/*
	 * 
	 */
    private Integer    niveauCarburantId    ;
	/*
	 * 
	 */
    private String     numeroFiche          ;
	/*
	 * 
	 */
    private String     interlocuteur        ;
	/*
	 * 
	 */
    private String     nomDeposeur          ;
	/*
	 * 
	 */
    private String     nomRecepteur         ;
	/*
	 * 
	 */
    private String     kilometrage          ;
	/*
	 * 
	 */
    private String     autres               ;
	/*
	 * 
	 */
	private String     dateProchaineRevision ;
	/*
	 * 
	 */
	private String     dateReception        ;
	/*
	 * 
	 */
	private String     dateSortie           ;
	/*
	 * 
	 */
	private String     dateDepot            ;
	/*
	 * 
	 */
	private String     dateVisiteTechnique  ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private Boolean    isDeposed            ;
	/*
	 * 
	 */
    private Boolean    isReceved            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Integer    etatId               ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String userNom;
	private String userPrenom;
	private String userLogin;
	private String etatCode;
	private String etatLibelle;
	private String niveauCarburantLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  userIdParam           ;                     
	private SearchParam<Integer>  vehiculeIdParam       ;                     
	private SearchParam<Integer>  couleurVehiculeIdParam;                     
	private SearchParam<Integer>  niveauCarburantIdParam;                     
	private SearchParam<String>   numeroFicheParam      ;                     
	private SearchParam<String>   interlocuteurParam    ;                     
	private SearchParam<String>   nomDeposeurParam      ;                     
	private SearchParam<String>   nomRecepteurParam     ;                     
	private SearchParam<String>   kilometrageParam      ;                     
	private SearchParam<String>   autresParam           ;                     

		private SearchParam<String>   dateProchaineRevisionParam;                     

		private SearchParam<String>   dateReceptionParam    ;                     

		private SearchParam<String>   dateSortieParam       ;                     

		private SearchParam<String>   dateDepotParam        ;                     

		private SearchParam<String>   dateVisiteTechniqueParam;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Boolean>  isDeposedParam        ;                     
	private SearchParam<Boolean>  isRecevedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Integer>  etatIdParam           ;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomParam       ;                     
	private SearchParam<String>   userLoginParam        ;                     
	private SearchParam<String>   etatCodeParam         ;                     
	private SearchParam<String>   etatLibelleParam      ;                     
	private SearchParam<String>   niveauCarburantLibelleParam;                     
    /**
     * Default constructor
     */
    public FicheReceptionDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "userId" field value
     * @param userId
     */
	public void setUserId( Integer userId )
    {
        this.userId = userId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserId()
    {
        return this.userId;
    }

    /**
     * Set the "vehiculeId" field value
     * @param vehiculeId
     */
	public void setVehiculeId( Integer vehiculeId )
    {
        this.vehiculeId = vehiculeId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getVehiculeId()
    {
        return this.vehiculeId;
    }

    /**
     * Set the "couleurVehiculeId" field value
     * @param couleurVehiculeId
     */
	public void setCouleurVehiculeId( Integer couleurVehiculeId )
    {
        this.couleurVehiculeId = couleurVehiculeId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCouleurVehiculeId()
    {
        return this.couleurVehiculeId;
    }

    /**
     * Set the "niveauCarburantId" field value
     * @param niveauCarburantId
     */
	public void setNiveauCarburantId( Integer niveauCarburantId )
    {
        this.niveauCarburantId = niveauCarburantId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getNiveauCarburantId()
    {
        return this.niveauCarburantId;
    }

    /**
     * Set the "numeroFiche" field value
     * @param numeroFiche
     */
	public void setNumeroFiche( String numeroFiche )
    {
        this.numeroFiche = numeroFiche ;
    }
    /**
     * 
     * @return the field value
     */
	public String getNumeroFiche()
    {
        return this.numeroFiche;
    }

    /**
     * Set the "interlocuteur" field value
     * @param interlocuteur
     */
	public void setInterlocuteur( String interlocuteur )
    {
        this.interlocuteur = interlocuteur ;
    }
    /**
     * 
     * @return the field value
     */
	public String getInterlocuteur()
    {
        return this.interlocuteur;
    }

    /**
     * Set the "nomDeposeur" field value
     * @param nomDeposeur
     */
	public void setNomDeposeur( String nomDeposeur )
    {
        this.nomDeposeur = nomDeposeur ;
    }
    /**
     * 
     * @return the field value
     */
	public String getNomDeposeur()
    {
        return this.nomDeposeur;
    }

    /**
     * Set the "nomRecepteur" field value
     * @param nomRecepteur
     */
	public void setNomRecepteur( String nomRecepteur )
    {
        this.nomRecepteur = nomRecepteur ;
    }
    /**
     * 
     * @return the field value
     */
	public String getNomRecepteur()
    {
        return this.nomRecepteur;
    }

    /**
     * Set the "kilometrage" field value
     * @param kilometrage
     */
	public void setKilometrage( String kilometrage )
    {
        this.kilometrage = kilometrage ;
    }
    /**
     * 
     * @return the field value
     */
	public String getKilometrage()
    {
        return this.kilometrage;
    }

    /**
     * Set the "autres" field value
     * @param autres
     */
	public void setAutres( String autres )
    {
        this.autres = autres ;
    }
    /**
     * 
     * @return the field value
     */
	public String getAutres()
    {
        return this.autres;
    }

    /**
     * Set the "dateProchaineRevision" field value
     * @param dateProchaineRevision
     */
	public void setDateProchaineRevision( String dateProchaineRevision )
    {
        this.dateProchaineRevision = dateProchaineRevision ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateProchaineRevision()
    {
        return this.dateProchaineRevision;
    }

    /**
     * Set the "dateReception" field value
     * @param dateReception
     */
	public void setDateReception( String dateReception )
    {
        this.dateReception = dateReception ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateReception()
    {
        return this.dateReception;
    }

    /**
     * Set the "dateSortie" field value
     * @param dateSortie
     */
	public void setDateSortie( String dateSortie )
    {
        this.dateSortie = dateSortie ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateSortie()
    {
        return this.dateSortie;
    }

    /**
     * Set the "dateDepot" field value
     * @param dateDepot
     */
	public void setDateDepot( String dateDepot )
    {
        this.dateDepot = dateDepot ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateDepot()
    {
        return this.dateDepot;
    }

    /**
     * Set the "dateVisiteTechnique" field value
     * @param dateVisiteTechnique
     */
	public void setDateVisiteTechnique( String dateVisiteTechnique )
    {
        this.dateVisiteTechnique = dateVisiteTechnique ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateVisiteTechnique()
    {
        return this.dateVisiteTechnique;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "isDeposed" field value
     * @param isDeposed
     */
	public void setIsDeposed( Boolean isDeposed )
    {
        this.isDeposed = isDeposed ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeposed()
    {
        return this.isDeposed;
    }

    /**
     * Set the "isReceved" field value
     * @param isReceved
     */
	public void setIsReceved( Boolean isReceved )
    {
        this.isReceved = isReceved ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsReceved()
    {
        return this.isReceved;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }

    /**
     * Set the "etatId" field value
     * @param etatId
     */
	public void setEtatId( Integer etatId )
    {
        this.etatId = etatId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getEtatId()
    {
        return this.etatId;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getUserNom()
    {
        return this.userNom;
    }
	public void setUserNom(String userNom)
    {
        this.userNom = userNom;
    }

	public String getUserPrenom()
    {
        return this.userPrenom;
    }
	public void setUserPrenom(String userPrenom)
    {
        this.userPrenom = userPrenom;
    }

	public String getUserLogin()
    {
        return this.userLogin;
    }
	public void setUserLogin(String userLogin)
    {
        this.userLogin = userLogin;
    }

	public String getEtatCode()
    {
        return this.etatCode;
    }
	public void setEtatCode(String etatCode)
    {
        this.etatCode = etatCode;
    }

	public String getEtatLibelle()
    {
        return this.etatLibelle;
    }
	public void setEtatLibelle(String etatLibelle)
    {
        this.etatLibelle = etatLibelle;
    }

	public String getNiveauCarburantLibelle()
    {
        return this.niveauCarburantLibelle;
    }
	public void setNiveauCarburantLibelle(String niveauCarburantLibelle)
    {
        this.niveauCarburantLibelle = niveauCarburantLibelle;
    }


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "userIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserIdParam(){
		return this.userIdParam;
	}
	/**
     * Set the "userIdParam" field value
     * @param userIdParam
     */
    public void setUserIdParam( SearchParam<Integer> userIdParam ){
        this.userIdParam = userIdParam;
    }

	/**
     * Get the "vehiculeIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getVehiculeIdParam(){
		return this.vehiculeIdParam;
	}
	/**
     * Set the "vehiculeIdParam" field value
     * @param vehiculeIdParam
     */
    public void setVehiculeIdParam( SearchParam<Integer> vehiculeIdParam ){
        this.vehiculeIdParam = vehiculeIdParam;
    }

	/**
     * Get the "couleurVehiculeIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCouleurVehiculeIdParam(){
		return this.couleurVehiculeIdParam;
	}
	/**
     * Set the "couleurVehiculeIdParam" field value
     * @param couleurVehiculeIdParam
     */
    public void setCouleurVehiculeIdParam( SearchParam<Integer> couleurVehiculeIdParam ){
        this.couleurVehiculeIdParam = couleurVehiculeIdParam;
    }

	/**
     * Get the "niveauCarburantIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getNiveauCarburantIdParam(){
		return this.niveauCarburantIdParam;
	}
	/**
     * Set the "niveauCarburantIdParam" field value
     * @param niveauCarburantIdParam
     */
    public void setNiveauCarburantIdParam( SearchParam<Integer> niveauCarburantIdParam ){
        this.niveauCarburantIdParam = niveauCarburantIdParam;
    }

	/**
     * Get the "numeroFicheParam" field value
     * @return the field value
     */
	public SearchParam<String> getNumeroFicheParam(){
		return this.numeroFicheParam;
	}
	/**
     * Set the "numeroFicheParam" field value
     * @param numeroFicheParam
     */
    public void setNumeroFicheParam( SearchParam<String> numeroFicheParam ){
        this.numeroFicheParam = numeroFicheParam;
    }

	/**
     * Get the "interlocuteurParam" field value
     * @return the field value
     */
	public SearchParam<String> getInterlocuteurParam(){
		return this.interlocuteurParam;
	}
	/**
     * Set the "interlocuteurParam" field value
     * @param interlocuteurParam
     */
    public void setInterlocuteurParam( SearchParam<String> interlocuteurParam ){
        this.interlocuteurParam = interlocuteurParam;
    }

	/**
     * Get the "nomDeposeurParam" field value
     * @return the field value
     */
	public SearchParam<String> getNomDeposeurParam(){
		return this.nomDeposeurParam;
	}
	/**
     * Set the "nomDeposeurParam" field value
     * @param nomDeposeurParam
     */
    public void setNomDeposeurParam( SearchParam<String> nomDeposeurParam ){
        this.nomDeposeurParam = nomDeposeurParam;
    }

	/**
     * Get the "nomRecepteurParam" field value
     * @return the field value
     */
	public SearchParam<String> getNomRecepteurParam(){
		return this.nomRecepteurParam;
	}
	/**
     * Set the "nomRecepteurParam" field value
     * @param nomRecepteurParam
     */
    public void setNomRecepteurParam( SearchParam<String> nomRecepteurParam ){
        this.nomRecepteurParam = nomRecepteurParam;
    }

	/**
     * Get the "kilometrageParam" field value
     * @return the field value
     */
	public SearchParam<String> getKilometrageParam(){
		return this.kilometrageParam;
	}
	/**
     * Set the "kilometrageParam" field value
     * @param kilometrageParam
     */
    public void setKilometrageParam( SearchParam<String> kilometrageParam ){
        this.kilometrageParam = kilometrageParam;
    }

	/**
     * Get the "autresParam" field value
     * @return the field value
     */
	public SearchParam<String> getAutresParam(){
		return this.autresParam;
	}
	/**
     * Set the "autresParam" field value
     * @param autresParam
     */
    public void setAutresParam( SearchParam<String> autresParam ){
        this.autresParam = autresParam;
    }

	/**
     * Get the "dateProchaineRevisionParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateProchaineRevisionParam(){
		return this.dateProchaineRevisionParam;
	}
	/**
     * Set the "dateProchaineRevisionParam" field value
     * @param dateProchaineRevisionParam
     */
    public void setDateProchaineRevisionParam( SearchParam<String> dateProchaineRevisionParam ){
        this.dateProchaineRevisionParam = dateProchaineRevisionParam;
    }

	/**
     * Get the "dateReceptionParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateReceptionParam(){
		return this.dateReceptionParam;
	}
	/**
     * Set the "dateReceptionParam" field value
     * @param dateReceptionParam
     */
    public void setDateReceptionParam( SearchParam<String> dateReceptionParam ){
        this.dateReceptionParam = dateReceptionParam;
    }

	/**
     * Get the "dateSortieParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateSortieParam(){
		return this.dateSortieParam;
	}
	/**
     * Set the "dateSortieParam" field value
     * @param dateSortieParam
     */
    public void setDateSortieParam( SearchParam<String> dateSortieParam ){
        this.dateSortieParam = dateSortieParam;
    }

	/**
     * Get the "dateDepotParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateDepotParam(){
		return this.dateDepotParam;
	}
	/**
     * Set the "dateDepotParam" field value
     * @param dateDepotParam
     */
    public void setDateDepotParam( SearchParam<String> dateDepotParam ){
        this.dateDepotParam = dateDepotParam;
    }

	/**
     * Get the "dateVisiteTechniqueParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateVisiteTechniqueParam(){
		return this.dateVisiteTechniqueParam;
	}
	/**
     * Set the "dateVisiteTechniqueParam" field value
     * @param dateVisiteTechniqueParam
     */
    public void setDateVisiteTechniqueParam( SearchParam<String> dateVisiteTechniqueParam ){
        this.dateVisiteTechniqueParam = dateVisiteTechniqueParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "isDeposedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeposedParam(){
		return this.isDeposedParam;
	}
	/**
     * Set the "isDeposedParam" field value
     * @param isDeposedParam
     */
    public void setIsDeposedParam( SearchParam<Boolean> isDeposedParam ){
        this.isDeposedParam = isDeposedParam;
    }

	/**
     * Get the "isRecevedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsRecevedParam(){
		return this.isRecevedParam;
	}
	/**
     * Set the "isRecevedParam" field value
     * @param isRecevedParam
     */
    public void setIsRecevedParam( SearchParam<Boolean> isRecevedParam ){
        this.isRecevedParam = isRecevedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

	/**
     * Get the "etatIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getEtatIdParam(){
		return this.etatIdParam;
	}
	/**
     * Set the "etatIdParam" field value
     * @param etatIdParam
     */
    public void setEtatIdParam( SearchParam<Integer> etatIdParam ){
        this.etatIdParam = etatIdParam;
    }

		/**
     * Get the "userNomParam" field value
     * @return the field value
     */
	public SearchParam<String> getUserNomParam(){
		return this.userNomParam;
	}
	/**
     * Set the "userNomParam" field value
     * @param userNomParam
     */
    public void setUserNomParam( SearchParam<String> userNomParam ){
        this.userNomParam = userNomParam;
    }

		/**
     * Get the "userPrenomParam" field value
     * @return the field value
     */
	public SearchParam<String> getUserPrenomParam(){
		return this.userPrenomParam;
	}
	/**
     * Set the "userPrenomParam" field value
     * @param userPrenomParam
     */
    public void setUserPrenomParam( SearchParam<String> userPrenomParam ){
        this.userPrenomParam = userPrenomParam;
    }

		/**
     * Get the "userLoginParam" field value
     * @return the field value
     */
	public SearchParam<String> getUserLoginParam(){
		return this.userLoginParam;
	}
	/**
     * Set the "userLoginParam" field value
     * @param userLoginParam
     */
    public void setUserLoginParam( SearchParam<String> userLoginParam ){
        this.userLoginParam = userLoginParam;
    }

		/**
     * Get the "etatCodeParam" field value
     * @return the field value
     */
	public SearchParam<String> getEtatCodeParam(){
		return this.etatCodeParam;
	}
	/**
     * Set the "etatCodeParam" field value
     * @param etatCodeParam
     */
    public void setEtatCodeParam( SearchParam<String> etatCodeParam ){
        this.etatCodeParam = etatCodeParam;
    }

		/**
     * Get the "etatLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getEtatLibelleParam(){
		return this.etatLibelleParam;
	}
	/**
     * Set the "etatLibelleParam" field value
     * @param etatLibelleParam
     */
    public void setEtatLibelleParam( SearchParam<String> etatLibelleParam ){
        this.etatLibelleParam = etatLibelleParam;
    }

		/**
     * Get the "niveauCarburantLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getNiveauCarburantLibelleParam(){
		return this.niveauCarburantLibelleParam;
	}
	/**
     * Set the "niveauCarburantLibelleParam" field value
     * @param niveauCarburantLibelleParam
     */
    public void setNiveauCarburantLibelleParam( SearchParam<String> niveauCarburantLibelleParam ){
        this.niveauCarburantLibelleParam = niveauCarburantLibelleParam;
    }


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		FicheReceptionDto other = (FicheReceptionDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("couleurVehiculeId:"+couleurVehiculeId);
		sb.append("|");
		sb.append("numeroFiche:"+numeroFiche);
		sb.append("|");
		sb.append("interlocuteur:"+interlocuteur);
		sb.append("|");
		sb.append("nomDeposeur:"+nomDeposeur);
		sb.append("|");
		sb.append("nomRecepteur:"+nomRecepteur);
		sb.append("|");
		sb.append("kilometrage:"+kilometrage);
		sb.append("|");
		sb.append("autres:"+autres);
		sb.append("|");
		sb.append("dateProchaineRevision:"+dateProchaineRevision);
		sb.append("|");
		sb.append("dateReception:"+dateReception);
		sb.append("|");
		sb.append("dateSortie:"+dateSortie);
		sb.append("|");
		sb.append("dateDepot:"+dateDepot);
		sb.append("|");
		sb.append("dateVisiteTechnique:"+dateVisiteTechnique);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("isDeposed:"+isDeposed);
		sb.append("|");
		sb.append("isReceved:"+isReceved);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
        return sb.toString();
    }
}
