


/*
 * Java transformer for entity table piece_huile_utilise
 * Created on 2019-08-18 ( Time 17:15:14 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "piece_huile_utilise"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class PieceHuileUtiliseBusiness implements IBasicBusiness<Request<PieceHuileUtiliseDto>, Response<PieceHuileUtiliseDto>> {

	private Response<PieceHuileUtiliseDto> response;
	@Autowired
	private PieceHuileUtiliseRepository pieceHuileUtiliseRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private DesignationPieceHuileUtiliseRepository designationPieceHuileUtiliseRepository;
	@Autowired
	private OrdreReparationRepository ordreReparationRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public PieceHuileUtiliseBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create PieceHuileUtilise by using PieceHuileUtiliseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PieceHuileUtiliseDto> create(Request<PieceHuileUtiliseDto> request, Locale locale)  {
		slf4jLogger.info("----begin create PieceHuileUtilise-----");

		response = new Response<PieceHuileUtiliseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<PieceHuileUtilise> items = new ArrayList<PieceHuileUtilise>();

			for (PieceHuileUtiliseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("designationPieceHuileUtiliseId", dto.getDesignationPieceHuileUtiliseId());
				fieldsToVerify.put("ordreReparationId", dto.getOrdreReparationId());
				fieldsToVerify.put("quantite", dto.getQuantite());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				

				// Verify if pieceHuileUtilise to insert do not exist
				PieceHuileUtilise existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("pieceHuileUtilise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if designationPieceHuileUtilise exist
				DesignationPieceHuileUtilise existingDesignationPieceHuileUtilise = designationPieceHuileUtiliseRepository.findById(dto.getDesignationPieceHuileUtiliseId(), false);
				if (existingDesignationPieceHuileUtilise == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("designationPieceHuileUtilise -> " + dto.getDesignationPieceHuileUtiliseId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if ordreReparation exist
				OrdreReparation existingOrdreReparation = ordreReparationRepository.findById(dto.getOrdreReparationId(), false);
				if (existingOrdreReparation == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getOrdreReparationId(), locale));
					response.setHasError(true);
					return response;
				}
				
				existingEntity = pieceHuileUtiliseRepository.findPieceHuileUtiliseByOrdreReparationIdAndDesignationPieceHuileUtiliseId(dto.getOrdreReparationId(), dto.getDesignationPieceHuileUtiliseId(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("pieceHuileUtilise -> " + dto.getOrdreReparationId() +" et " + dto.getDesignationPieceHuileUtiliseId(), locale));
					response.setHasError(true);
					return response;
				}
				
				if (items.stream().anyMatch(a->a.getDesignationPieceHuileUtilise().getId().equals(dto.getDesignationPieceHuileUtiliseId()) && a.getOrdreReparation().getId().equals(dto.getOrdreReparationId()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du ordreReparationId '" + dto.getOrdreReparationId()+"' et du designationPieceHuileUtiliseId '" + dto.getDesignationPieceHuileUtiliseId() +"' pour les pieceHuileUtilise", locale));
					response.setHasError(true);
					return response;
				}
				PieceHuileUtilise entityToSave = null;
				entityToSave = PieceHuileUtiliseTransformer.INSTANCE.toEntity(dto, existingDesignationPieceHuileUtilise, existingOrdreReparation);
				entityToSave.setIsDeleted(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<PieceHuileUtilise> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = pieceHuileUtiliseRepository.save((Iterable<PieceHuileUtilise>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("pieceHuileUtilise", locale));
					response.setHasError(true);
					return response;
				}
				List<PieceHuileUtiliseDto> itemsDto = new ArrayList<PieceHuileUtiliseDto>();
				for (PieceHuileUtilise entity : itemsSaved) {
					PieceHuileUtiliseDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create PieceHuileUtilise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update PieceHuileUtilise by using PieceHuileUtiliseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PieceHuileUtiliseDto> update(Request<PieceHuileUtiliseDto> request, Locale locale)  {
		slf4jLogger.info("----begin update PieceHuileUtilise-----");

		response = new Response<PieceHuileUtiliseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<PieceHuileUtilise> items = new ArrayList<PieceHuileUtilise>();

			for (PieceHuileUtiliseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				
				// Verifier si la pieceHuileUtilise existe
				PieceHuileUtilise entityToSave = null;
				entityToSave = pieceHuileUtiliseRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pieceHuileUtilise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if designationPieceHuileUtilise exist
				if (dto.getDesignationPieceHuileUtiliseId() != null && dto.getDesignationPieceHuileUtiliseId() > 0){
					DesignationPieceHuileUtilise existingDesignationPieceHuileUtilise = designationPieceHuileUtiliseRepository.findById(dto.getDesignationPieceHuileUtiliseId(), false);
					if (existingDesignationPieceHuileUtilise == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("designationPieceHuileUtilise -> " + dto.getDesignationPieceHuileUtiliseId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setDesignationPieceHuileUtilise(existingDesignationPieceHuileUtilise);
				}
				// Verify if ordreReparation exist
				if (dto.getOrdreReparationId() != null && dto.getOrdreReparationId() > 0){
					OrdreReparation existingOrdreReparation = ordreReparationRepository.findById(dto.getOrdreReparationId(), false);
					if (existingOrdreReparation == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getOrdreReparationId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setOrdreReparation(existingOrdreReparation);
				}
				
				
				PieceHuileUtilise existingEntity = pieceHuileUtiliseRepository.findPieceHuileUtiliseByOrdreReparationIdAndDesignationPieceHuileUtiliseId(dto.getOrdreReparationId(), dto.getDesignationPieceHuileUtiliseId(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSaveId)) {
					response.setStatus(functionalError.DATA_EXIST("pieceHuileUtilise -> " + dto.getOrdreReparationId() +" et " + dto.getDesignationPieceHuileUtiliseId(), locale));
					response.setHasError(true);
					return response;
				}
				
				if (items.stream().anyMatch(a->a.getDesignationPieceHuileUtilise().getId().equals(dto.getDesignationPieceHuileUtiliseId()) && a.getOrdreReparation().getId().equals(dto.getOrdreReparationId()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du ordreReparationId '" + dto.getOrdreReparationId()+"' et du designationPieceHuileUtiliseId '" + dto.getDesignationPieceHuileUtiliseId() +"' pour les pieceHuileUtilise", locale));
					response.setHasError(true);
					return response;
				}
				if (dto.getQuantite() != null && dto.getQuantite() > 0) {
					entityToSave.setQuantite(dto.getQuantite());
				}
				
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<PieceHuileUtilise> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = pieceHuileUtiliseRepository.save((Iterable<PieceHuileUtilise>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("pieceHuileUtilise", locale));
					response.setHasError(true);
					return response;
				}
				List<PieceHuileUtiliseDto> itemsDto = new ArrayList<PieceHuileUtiliseDto>();
				for (PieceHuileUtilise entity : itemsSaved) {
					PieceHuileUtiliseDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update PieceHuileUtilise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete PieceHuileUtilise by using PieceHuileUtiliseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PieceHuileUtiliseDto> delete(Request<PieceHuileUtiliseDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete PieceHuileUtilise-----");

		response = new Response<PieceHuileUtiliseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<PieceHuileUtilise> items = new ArrayList<PieceHuileUtilise>();

			for (PieceHuileUtiliseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				
				// Verifier si la pieceHuileUtilise existe
				PieceHuileUtilise existingEntity = null;
				existingEntity = pieceHuileUtiliseRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pieceHuileUtilise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				pieceHuileUtiliseRepository.save((Iterable<PieceHuileUtilise>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete PieceHuileUtilise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete PieceHuileUtilise by using PieceHuileUtiliseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<PieceHuileUtiliseDto> forceDelete(Request<PieceHuileUtiliseDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete PieceHuileUtilise-----");

		response = new Response<PieceHuileUtiliseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<PieceHuileUtilise> items = new ArrayList<PieceHuileUtilise>();

			for (PieceHuileUtiliseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la pieceHuileUtilise existe
				PieceHuileUtilise existingEntity = null;
				existingEntity = pieceHuileUtiliseRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pieceHuileUtilise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				pieceHuileUtiliseRepository.save((Iterable<PieceHuileUtilise>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete PieceHuileUtilise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get PieceHuileUtilise by using PieceHuileUtiliseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<PieceHuileUtiliseDto> getByCriteria(Request<PieceHuileUtiliseDto> request, Locale locale) {
		slf4jLogger.info("----begin get PieceHuileUtilise-----");

		response = new Response<PieceHuileUtiliseDto>();

		try {
			List<PieceHuileUtilise> items = null;
			items = pieceHuileUtiliseRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<PieceHuileUtiliseDto> itemsDto = new ArrayList<PieceHuileUtiliseDto>();
				for (PieceHuileUtilise entity : items) {
					PieceHuileUtiliseDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(pieceHuileUtiliseRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("pieceHuileUtilise", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get PieceHuileUtilise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full PieceHuileUtiliseDto by using PieceHuileUtilise as object.
	 *
	 * @param entity, locale
	 * @return PieceHuileUtiliseDto
	 *
	 */
	private PieceHuileUtiliseDto getFullInfos(PieceHuileUtilise entity, Integer size, Locale locale) throws Exception {
		PieceHuileUtiliseDto dto = PieceHuileUtiliseTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
