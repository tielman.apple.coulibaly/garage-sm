
/*
 * Java dto for entity table fiche_preparation_devis
 * Created on 2019-08-18 ( Time 17:15:11 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._FichePreparationDevisDto;

/**
 * DTO for table "fiche_preparation_devis"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class FichePreparationDevisDto extends _FichePreparationDevisDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    ordreReparationId    ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Integer    etatId               ;
	/*
	 * 
	 */
    private Integer    userVerificateurId   ;
	/*
	 * 
	 */
    private Integer    userValideurId       ;
	/*
	 * 
	 */
    private Boolean    isValidedByVerificateur ;
	/*
	 * 
	 */
    private Boolean    isValidedByValideur  ;
	/*
	 * 
	 */
	private String     dateValidationVerificateur ;
	/*
	 * 
	 */
	private String     dateValidationValideur ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String userValideurNom;
	private String userValideurPrenom;
	private String userValideurLogin;
	private String userVerificateurNom;
	private String userVerificateurPrenom;
	private String userVerificateurLogin;
	private String etatCode;
	private String etatLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  ordreReparationIdParam;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Integer>  etatIdParam           ;                     
	private SearchParam<Integer>  userVerificateurIdParam;                     
	private SearchParam<Integer>  userValideurIdParam   ;                     
	private SearchParam<Boolean>  isValidedByVerificateurParam;                     
	private SearchParam<Boolean>  isValidedByValideurParam;                     

		private SearchParam<String>   dateValidationVerificateurParam;                     

		private SearchParam<String>   dateValidationValideurParam;                     
	private SearchParam<String>   userValideurNomParam          ;                     
	private SearchParam<String>   userValideurPrenomParam       ;                     
	private SearchParam<String>   userValideurLoginParam        ;                     
	private SearchParam<String>   userVerificateurNomParam          ;                     
	private SearchParam<String>   userVerificateurPrenomParam       ;                     
	private SearchParam<String>   userVerificateurLoginParam        ;                     
	private SearchParam<String>   etatCodeParam         ;                     
	private SearchParam<String>   etatLibelleParam      ;                     
    /**
     * Default constructor
     */
    public FichePreparationDevisDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "ordreReparationId" field value
     * @param ordreReparationId
     */
	public void setOrdreReparationId( Integer ordreReparationId )
    {
        this.ordreReparationId = ordreReparationId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getOrdreReparationId()
    {
        return this.ordreReparationId;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }

    /**
     * Set the "etatId" field value
     * @param etatId
     */
	public void setEtatId( Integer etatId )
    {
        this.etatId = etatId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getEtatId()
    {
        return this.etatId;
    }

    /**
     * Set the "userVerificateurId" field value
     * @param userVerificateurId
     */
	public void setUserVerificateurId( Integer userVerificateurId )
    {
        this.userVerificateurId = userVerificateurId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserVerificateurId()
    {
        return this.userVerificateurId;
    }

    /**
     * Set the "userValideurId" field value
     * @param userValideurId
     */
	public void setUserValideurId( Integer userValideurId )
    {
        this.userValideurId = userValideurId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserValideurId()
    {
        return this.userValideurId;
    }

    /**
     * Set the "isValidedByVerificateur" field value
     * @param isValidedByVerificateur
     */
	public void setIsValidedByVerificateur( Boolean isValidedByVerificateur )
    {
        this.isValidedByVerificateur = isValidedByVerificateur ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByVerificateur()
    {
        return this.isValidedByVerificateur;
    }

    /**
     * Set the "isValidedByValideur" field value
     * @param isValidedByValideur
     */
	public void setIsValidedByValideur( Boolean isValidedByValideur )
    {
        this.isValidedByValideur = isValidedByValideur ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByValideur()
    {
        return this.isValidedByValideur;
    }

    /**
     * Set the "dateValidationVerificateur" field value
     * @param dateValidationVerificateur
     */
	public void setDateValidationVerificateur( String dateValidationVerificateur )
    {
        this.dateValidationVerificateur = dateValidationVerificateur ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateValidationVerificateur()
    {
        return this.dateValidationVerificateur;
    }

    /**
     * Set the "dateValidationValideur" field value
     * @param dateValidationValideur
     */
	public void setDateValidationValideur( String dateValidationValideur )
    {
        this.dateValidationValideur = dateValidationValideur ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateValidationValideur()
    {
        return this.dateValidationValideur;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	
	public String getEtatCode()
    {
        return this.etatCode;
    }
	public void setEtatCode(String etatCode)
    {
        this.etatCode = etatCode;
    }

	public String getEtatLibelle()
    {
        return this.etatLibelle;
    }
	public void setEtatLibelle(String etatLibelle)
    {
        this.etatLibelle = etatLibelle;
    }


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "ordreReparationIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getOrdreReparationIdParam(){
		return this.ordreReparationIdParam;
	}
	/**
     * Set the "ordreReparationIdParam" field value
     * @param ordreReparationIdParam
     */
    public void setOrdreReparationIdParam( SearchParam<Integer> ordreReparationIdParam ){
        this.ordreReparationIdParam = ordreReparationIdParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

	/**
     * Get the "etatIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getEtatIdParam(){
		return this.etatIdParam;
	}
	/**
     * Set the "etatIdParam" field value
     * @param etatIdParam
     */
    public void setEtatIdParam( SearchParam<Integer> etatIdParam ){
        this.etatIdParam = etatIdParam;
    }

	/**
     * Get the "userVerificateurIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserVerificateurIdParam(){
		return this.userVerificateurIdParam;
	}
	/**
     * Set the "userVerificateurIdParam" field value
     * @param userVerificateurIdParam
     */
    public void setUserVerificateurIdParam( SearchParam<Integer> userVerificateurIdParam ){
        this.userVerificateurIdParam = userVerificateurIdParam;
    }

	/**
     * Get the "userValideurIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserValideurIdParam(){
		return this.userValideurIdParam;
	}
	/**
     * Set the "userValideurIdParam" field value
     * @param userValideurIdParam
     */
    public void setUserValideurIdParam( SearchParam<Integer> userValideurIdParam ){
        this.userValideurIdParam = userValideurIdParam;
    }

	/**
     * Get the "isValidedByVerificateurParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByVerificateurParam(){
		return this.isValidedByVerificateurParam;
	}
	/**
     * Set the "isValidedByVerificateurParam" field value
     * @param isValidedByVerificateurParam
     */
    public void setIsValidedByVerificateurParam( SearchParam<Boolean> isValidedByVerificateurParam ){
        this.isValidedByVerificateurParam = isValidedByVerificateurParam;
    }

	/**
     * Get the "isValidedByValideurParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByValideurParam(){
		return this.isValidedByValideurParam;
	}
	/**
     * Set the "isValidedByValideurParam" field value
     * @param isValidedByValideurParam
     */
    public void setIsValidedByValideurParam( SearchParam<Boolean> isValidedByValideurParam ){
        this.isValidedByValideurParam = isValidedByValideurParam;
    }

	/**
     * Get the "dateValidationVerificateurParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateValidationVerificateurParam(){
		return this.dateValidationVerificateurParam;
	}
	/**
     * Set the "dateValidationVerificateurParam" field value
     * @param dateValidationVerificateurParam
     */
    public void setDateValidationVerificateurParam( SearchParam<String> dateValidationVerificateurParam ){
        this.dateValidationVerificateurParam = dateValidationVerificateurParam;
    }

	/**
     * Get the "dateValidationValideurParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateValidationValideurParam(){
		return this.dateValidationValideurParam;
	}
	/**
     * Set the "dateValidationValideurParam" field value
     * @param dateValidationValideurParam
     */
    public void setDateValidationValideurParam( SearchParam<String> dateValidationValideurParam ){
        this.dateValidationValideurParam = dateValidationValideurParam;
    }

	

		/**
     * Get the "etatCodeParam" field value
     * @return the field value
     */
	public SearchParam<String> getEtatCodeParam(){
		return this.etatCodeParam;
	}
	/**
     * Set the "etatCodeParam" field value
     * @param etatCodeParam
     */
    public void setEtatCodeParam( SearchParam<String> etatCodeParam ){
        this.etatCodeParam = etatCodeParam;
    }

		/**
     * Get the "etatLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getEtatLibelleParam(){
		return this.etatLibelleParam;
	}
	/**
     * Set the "etatLibelleParam" field value
     * @param etatLibelleParam
     */
    public void setEtatLibelleParam( SearchParam<String> etatLibelleParam ){
        this.etatLibelleParam = etatLibelleParam;
    }


	public String getUserValideurNom() {
		return userValideurNom;
	}

	public void setUserValideurNom(String userValideurNom) {
		this.userValideurNom = userValideurNom;
	}

	public String getUserValideurPrenom() {
		return userValideurPrenom;
	}

	public void setUserValideurPrenom(String userValideurPrenom) {
		this.userValideurPrenom = userValideurPrenom;
	}

	public String getUserValideurLogin() {
		return userValideurLogin;
	}

	public void setUserValideurLogin(String userValideurLogin) {
		this.userValideurLogin = userValideurLogin;
	}

	public String getUserVerificateurNom() {
		return userVerificateurNom;
	}

	public void setUserVerificateurNom(String userVerificateurNom) {
		this.userVerificateurNom = userVerificateurNom;
	}

	public String getUserVerificateurPrenom() {
		return userVerificateurPrenom;
	}

	public void setUserVerificateurPrenom(String userVerificateurPrenom) {
		this.userVerificateurPrenom = userVerificateurPrenom;
	}

	public String getUserVerificateurLogin() {
		return userVerificateurLogin;
	}

	public void setUserVerificateurLogin(String userVerificateurLogin) {
		this.userVerificateurLogin = userVerificateurLogin;
	}

	public SearchParam<String> getUserValideurNomParam() {
		return userValideurNomParam;
	}

	public void setUserValideurNomParam(SearchParam<String> userValideurNomParam) {
		this.userValideurNomParam = userValideurNomParam;
	}

	public SearchParam<String> getUserValideurPrenomParam() {
		return userValideurPrenomParam;
	}

	public void setUserValideurPrenomParam(SearchParam<String> userValideurPrenomParam) {
		this.userValideurPrenomParam = userValideurPrenomParam;
	}

	public SearchParam<String> getUserValideurLoginParam() {
		return userValideurLoginParam;
	}

	public void setUserValideurLoginParam(SearchParam<String> userValideurLoginParam) {
		this.userValideurLoginParam = userValideurLoginParam;
	}

	public SearchParam<String> getUserVerificateurNomParam() {
		return userVerificateurNomParam;
	}

	public void setUserVerificateurNomParam(SearchParam<String> userVerificateurNomParam) {
		this.userVerificateurNomParam = userVerificateurNomParam;
	}

	public SearchParam<String> getUserVerificateurPrenomParam() {
		return userVerificateurPrenomParam;
	}

	public void setUserVerificateurPrenomParam(SearchParam<String> userVerificateurPrenomParam) {
		this.userVerificateurPrenomParam = userVerificateurPrenomParam;
	}

	public SearchParam<String> getUserVerificateurLoginParam() {
		return userVerificateurLoginParam;
	}

	public void setUserVerificateurLoginParam(SearchParam<String> userVerificateurLoginParam) {
		this.userVerificateurLoginParam = userVerificateurLoginParam;
	}

	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		FichePreparationDevisDto other = (FichePreparationDevisDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
		sb.append("|");
		sb.append("isValidedByVerificateur:"+isValidedByVerificateur);
		sb.append("|");
		sb.append("isValidedByValideur:"+isValidedByValideur);
		sb.append("|");
		sb.append("dateValidationVerificateur:"+dateValidationVerificateur);
		sb.append("|");
		sb.append("dateValidationValideur:"+dateValidationValideur);
        return sb.toString();
    }
}
