package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._PanneDeclareeOrdreReparationRepository;

/**
 * Repository : PanneDeclareeOrdreReparation.
 */
@Repository
public interface PanneDeclareeOrdreReparationRepository extends JpaRepository<PanneDeclareeOrdreReparation, Integer>, _PanneDeclareeOrdreReparationRepository {
	/**
	 * Finds PanneDeclareeOrdreReparation by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object PanneDeclareeOrdreReparation whose id is equals to the given id. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.id = :id and e.isDeleted = :isDeleted")
	PanneDeclareeOrdreReparation findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PanneDeclareeOrdreReparation by using observations as a search criteria.
	 *
	 * @param observations
	 * @return An Object PanneDeclareeOrdreReparation whose observations is equals to the given observations. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.observations = :observations and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByObservations(@Param("observations")String observations, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PanneDeclareeOrdreReparation by using diagnosticAtelier as a search criteria.
	 *
	 * @param diagnosticAtelier
	 * @return An Object PanneDeclareeOrdreReparation whose diagnosticAtelier is equals to the given diagnosticAtelier. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.diagnosticAtelier = :diagnosticAtelier and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByDiagnosticAtelier(@Param("diagnosticAtelier")String diagnosticAtelier, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PanneDeclareeOrdreReparation by using isDeclaredByClient as a search criteria.
	 *
	 * @param isDeclaredByClient
	 * @return An Object PanneDeclareeOrdreReparation whose isDeclaredByClient is equals to the given isDeclaredByClient. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.isDeclaredByClient = :isDeclaredByClient and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByIsDeclaredByClientX(@Param("isDeclaredByClient")Boolean isDeclaredByClient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PanneDeclareeOrdreReparation by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object PanneDeclareeOrdreReparation whose isDeleted is equals to the given isDeleted. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PanneDeclareeOrdreReparation by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object PanneDeclareeOrdreReparation whose createdAt is equals to the given createdAt. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PanneDeclareeOrdreReparation by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object PanneDeclareeOrdreReparation whose updatedAt is equals to the given updatedAt. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PanneDeclareeOrdreReparation by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object PanneDeclareeOrdreReparation whose deletedAt is equals to the given deletedAt. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PanneDeclareeOrdreReparation by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object PanneDeclareeOrdreReparation whose createdBy is equals to the given createdBy. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PanneDeclareeOrdreReparation by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object PanneDeclareeOrdreReparation whose updatedBy is equals to the given updatedBy. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PanneDeclareeOrdreReparation by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object PanneDeclareeOrdreReparation whose deletedBy is equals to the given deletedBy. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PanneDeclareeOrdreReparation by using ordreReparationId as a search criteria.
	 *
	 * @param ordreReparationId
	 * @return A list of Object PanneDeclareeOrdreReparation whose ordreReparationId is equals to the given ordreReparationId. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.ordreReparation.id = :ordreReparationId and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByOrdreReparationId(@Param("ordreReparationId")Integer ordreReparationId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PanneDeclareeOrdreReparation by using ordreReparationId as a search criteria.
   *
   * @param ordreReparationId
   * @return An Object PanneDeclareeOrdreReparation whose ordreReparationId is equals to the given ordreReparationId. If
   *         no PanneDeclareeOrdreReparation is found, this method returns null.
   */
  @Query("select e from PanneDeclareeOrdreReparation e where e.ordreReparation.id = :ordreReparationId and e.isDeleted = :isDeleted")
  PanneDeclareeOrdreReparation findPanneDeclareeOrdreReparationByOrdreReparationId(@Param("ordreReparationId")Integer ordreReparationId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds PanneDeclareeOrdreReparation by using panneDeclareeId as a search criteria.
	 *
	 * @param panneDeclareeId
	 * @return A list of Object PanneDeclareeOrdreReparation whose panneDeclareeId is equals to the given panneDeclareeId. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.panneDeclaree.id = :panneDeclareeId and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByPanneDeclareeId(@Param("panneDeclareeId")Integer panneDeclareeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PanneDeclareeOrdreReparation by using panneDeclareeId as a search criteria.
   *
   * @param panneDeclareeId
   * @return An Object PanneDeclareeOrdreReparation whose panneDeclareeId is equals to the given panneDeclareeId. If
   *         no PanneDeclareeOrdreReparation is found, this method returns null.
   */
  @Query("select e from PanneDeclareeOrdreReparation e where e.panneDeclaree.id = :panneDeclareeId and e.isDeleted = :isDeleted")
  PanneDeclareeOrdreReparation findPanneDeclareeOrdreReparationByPanneDeclareeId(@Param("panneDeclareeId")Integer panneDeclareeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of PanneDeclareeOrdreReparation by using panneDeclareeOrdreReparationDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of PanneDeclareeOrdreReparation
	 * @throws DataAccessException,ParseException
	 */
	public default List<PanneDeclareeOrdreReparation> getByCriteria(Request<PanneDeclareeOrdreReparationDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from PanneDeclareeOrdreReparation e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<PanneDeclareeOrdreReparation> query = em.createQuery(req, PanneDeclareeOrdreReparation.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of PanneDeclareeOrdreReparation by using panneDeclareeOrdreReparationDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of PanneDeclareeOrdreReparation
	 *
	 */
	public default Long count(Request<PanneDeclareeOrdreReparationDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from PanneDeclareeOrdreReparation e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PanneDeclareeOrdreReparationDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		PanneDeclareeOrdreReparationDto dto = request.getData() != null ? request.getData() : new PanneDeclareeOrdreReparationDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PanneDeclareeOrdreReparationDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PanneDeclareeOrdreReparationDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getObservations())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("observations", dto.getObservations(), "e.observations", "String", dto.getObservationsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDiagnosticAtelier())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("diagnosticAtelier", dto.getDiagnosticAtelier(), "e.diagnosticAtelier", "String", dto.getDiagnosticAtelierParam(), param, index, locale));
			}
			if (dto.getIsDeclaredByClient()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeclaredByClient", dto.getIsDeclaredByClient(), "e.isDeclaredByClient", "Boolean", dto.getIsDeclaredByClientParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getOrdreReparationId()!= null && dto.getOrdreReparationId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ordreReparationId", dto.getOrdreReparationId(), "e.ordreReparation.id", "Integer", dto.getOrdreReparationIdParam(), param, index, locale));
			}
			if (dto.getPanneDeclareeId()!= null && dto.getPanneDeclareeId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("panneDeclareeId", dto.getPanneDeclareeId(), "e.panneDeclaree.id", "Integer", dto.getPanneDeclareeIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPanneDeclareeLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("panneDeclareeLibelle", dto.getPanneDeclareeLibelle(), "e.panneDeclaree.libelle", "String", dto.getPanneDeclareeLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
