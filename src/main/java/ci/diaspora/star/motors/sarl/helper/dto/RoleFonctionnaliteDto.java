
/*
 * Java dto for entity table role_fonctionnalite
 * Created on 2019-08-18 ( Time 17:15:15 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._RoleFonctionnaliteDto;

/**
 * DTO for table "role_fonctionnalite"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class RoleFonctionnaliteDto extends _RoleFonctionnaliteDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    fonctionnaliteId     ;
	/*
	 * 
	 */
    private Integer    roleId               ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String roleCode;
	private String roleLibelle;
	private String fonctionnaliteCode;
	private String fonctionnaliteLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  fonctionnaliteIdParam ;                     
	private SearchParam<Integer>  roleIdParam           ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<String>   roleCodeParam         ;                     
	private SearchParam<String>   roleLibelleParam      ;                     
	private SearchParam<String>   fonctionnaliteCodeParam;                     
	private SearchParam<String>   fonctionnaliteLibelleParam;                     
    /**
     * Default constructor
     */
    public RoleFonctionnaliteDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "fonctionnaliteId" field value
     * @param fonctionnaliteId
     */
	public void setFonctionnaliteId( Integer fonctionnaliteId )
    {
        this.fonctionnaliteId = fonctionnaliteId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getFonctionnaliteId()
    {
        return this.fonctionnaliteId;
    }

    /**
     * Set the "roleId" field value
     * @param roleId
     */
	public void setRoleId( Integer roleId )
    {
        this.roleId = roleId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getRoleId()
    {
        return this.roleId;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getRoleCode()
    {
        return this.roleCode;
    }
	public void setRoleCode(String roleCode)
    {
        this.roleCode = roleCode;
    }

	public String getRoleLibelle()
    {
        return this.roleLibelle;
    }
	public void setRoleLibelle(String roleLibelle)
    {
        this.roleLibelle = roleLibelle;
    }

	public String getFonctionnaliteCode()
    {
        return this.fonctionnaliteCode;
    }
	public void setFonctionnaliteCode(String fonctionnaliteCode)
    {
        this.fonctionnaliteCode = fonctionnaliteCode;
    }

	public String getFonctionnaliteLibelle()
    {
        return this.fonctionnaliteLibelle;
    }
	public void setFonctionnaliteLibelle(String fonctionnaliteLibelle)
    {
        this.fonctionnaliteLibelle = fonctionnaliteLibelle;
    }


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "fonctionnaliteIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getFonctionnaliteIdParam(){
		return this.fonctionnaliteIdParam;
	}
	/**
     * Set the "fonctionnaliteIdParam" field value
     * @param fonctionnaliteIdParam
     */
    public void setFonctionnaliteIdParam( SearchParam<Integer> fonctionnaliteIdParam ){
        this.fonctionnaliteIdParam = fonctionnaliteIdParam;
    }

	/**
     * Get the "roleIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getRoleIdParam(){
		return this.roleIdParam;
	}
	/**
     * Set the "roleIdParam" field value
     * @param roleIdParam
     */
    public void setRoleIdParam( SearchParam<Integer> roleIdParam ){
        this.roleIdParam = roleIdParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

		/**
     * Get the "roleCodeParam" field value
     * @return the field value
     */
	public SearchParam<String> getRoleCodeParam(){
		return this.roleCodeParam;
	}
	/**
     * Set the "roleCodeParam" field value
     * @param roleCodeParam
     */
    public void setRoleCodeParam( SearchParam<String> roleCodeParam ){
        this.roleCodeParam = roleCodeParam;
    }

		/**
     * Get the "roleLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getRoleLibelleParam(){
		return this.roleLibelleParam;
	}
	/**
     * Set the "roleLibelleParam" field value
     * @param roleLibelleParam
     */
    public void setRoleLibelleParam( SearchParam<String> roleLibelleParam ){
        this.roleLibelleParam = roleLibelleParam;
    }

		/**
     * Get the "fonctionnaliteCodeParam" field value
     * @return the field value
     */
	public SearchParam<String> getFonctionnaliteCodeParam(){
		return this.fonctionnaliteCodeParam;
	}
	/**
     * Set the "fonctionnaliteCodeParam" field value
     * @param fonctionnaliteCodeParam
     */
    public void setFonctionnaliteCodeParam( SearchParam<String> fonctionnaliteCodeParam ){
        this.fonctionnaliteCodeParam = fonctionnaliteCodeParam;
    }

		/**
     * Get the "fonctionnaliteLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getFonctionnaliteLibelleParam(){
		return this.fonctionnaliteLibelleParam;
	}
	/**
     * Set the "fonctionnaliteLibelleParam" field value
     * @param fonctionnaliteLibelleParam
     */
    public void setFonctionnaliteLibelleParam( SearchParam<String> fonctionnaliteLibelleParam ){
        this.fonctionnaliteLibelleParam = fonctionnaliteLibelleParam;
    }


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		RoleFonctionnaliteDto other = (RoleFonctionnaliteDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
        return sb.toString();
    }
}
