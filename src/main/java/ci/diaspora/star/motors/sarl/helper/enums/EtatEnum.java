package ci.diaspora.star.motors.sarl.helper.enums;

public class EtatEnum {
	public static final String	BROUILLON	= "BROUILLON";
	public static final String	ENTRANT	= "ENTRANT";
	public static final String	TERMINER	= "TERMINER";
	
}
