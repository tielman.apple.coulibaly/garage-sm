


/*
 * Java transformer for entity table element_etat_vehicule_fiche_reception
 * Created on 2019-08-18 ( Time 17:15:09 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "element_etat_vehicule_fiche_reception"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class ElementEtatVehiculeFicheReceptionBusiness implements IBasicBusiness<Request<ElementEtatVehiculeFicheReceptionDto>, Response<ElementEtatVehiculeFicheReceptionDto>> {

	private Response<ElementEtatVehiculeFicheReceptionDto> response;
	@Autowired
	private ElementEtatVehiculeFicheReceptionRepository elementEtatVehiculeFicheReceptionRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FicheReceptionRepository ficheReceptionRepository;
	@Autowired
	private ElementEtatVehiculeRepository elementEtatVehiculeRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public ElementEtatVehiculeFicheReceptionBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create ElementEtatVehiculeFicheReception by using ElementEtatVehiculeFicheReceptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ElementEtatVehiculeFicheReceptionDto> create(Request<ElementEtatVehiculeFicheReceptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin create ElementEtatVehiculeFicheReception-----");

		response = new Response<ElementEtatVehiculeFicheReceptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ElementEtatVehiculeFicheReception> items = new ArrayList<ElementEtatVehiculeFicheReception>();

			for (ElementEtatVehiculeFicheReceptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("elementEtatVehiculeId", dto.getElementEtatVehiculeId());
				fieldsToVerify.put("ficheReceptionId", dto.getFicheReceptionId());
				fieldsToVerify.put("isGood", dto.getIsGood());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if elementEtatVehiculeFicheReception to insert do not exist
				ElementEtatVehiculeFicheReception existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("elementEtatVehiculeFicheReception -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if ficheReception exist
				FicheReception existingFicheReception = ficheReceptionRepository.findById(dto.getFicheReceptionId(), false);
				if (existingFicheReception == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getFicheReceptionId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if elementEtatVehicule exist
				ElementEtatVehicule existingElementEtatVehicule = elementEtatVehiculeRepository.findById(dto.getElementEtatVehiculeId(), false);
				if (existingElementEtatVehicule == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("elementEtatVehicule -> " + dto.getElementEtatVehiculeId(), locale));
					response.setHasError(true);
					return response;
				}
				ElementEtatVehiculeFicheReception entityToSave = null;
				entityToSave = ElementEtatVehiculeFicheReceptionTransformer.INSTANCE.toEntity(dto, existingFicheReception, existingElementEtatVehicule);
				entityToSave.setIsDeleted(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ElementEtatVehiculeFicheReception> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = elementEtatVehiculeFicheReceptionRepository.save((Iterable<ElementEtatVehiculeFicheReception>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("elementEtatVehiculeFicheReception", locale));
					response.setHasError(true);
					return response;
				}
				List<ElementEtatVehiculeFicheReceptionDto> itemsDto = new ArrayList<ElementEtatVehiculeFicheReceptionDto>();
				for (ElementEtatVehiculeFicheReception entity : itemsSaved) {
					ElementEtatVehiculeFicheReceptionDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create ElementEtatVehiculeFicheReception-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ElementEtatVehiculeFicheReception by using ElementEtatVehiculeFicheReceptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ElementEtatVehiculeFicheReceptionDto> update(Request<ElementEtatVehiculeFicheReceptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin update ElementEtatVehiculeFicheReception-----");

		response = new Response<ElementEtatVehiculeFicheReceptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ElementEtatVehiculeFicheReception> items = new ArrayList<ElementEtatVehiculeFicheReception>();

			for (ElementEtatVehiculeFicheReceptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la elementEtatVehiculeFicheReception existe
				ElementEtatVehiculeFicheReception entityToSave = null;
				entityToSave = elementEtatVehiculeFicheReceptionRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("elementEtatVehiculeFicheReception -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if ficheReception exist
				if (dto.getFicheReceptionId() != null && dto.getFicheReceptionId() > 0){
					FicheReception existingFicheReception = ficheReceptionRepository.findById(dto.getFicheReceptionId(), false);
					if (existingFicheReception == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getFicheReceptionId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setFicheReception(existingFicheReception);
				}
				// Verify if elementEtatVehicule exist
				if (dto.getElementEtatVehiculeId() != null && dto.getElementEtatVehiculeId() > 0){
					ElementEtatVehicule existingElementEtatVehicule = elementEtatVehiculeRepository.findById(dto.getElementEtatVehiculeId(), false);
					if (existingElementEtatVehicule == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("elementEtatVehicule -> " + dto.getElementEtatVehiculeId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setElementEtatVehicule(existingElementEtatVehicule);
				}
				if (dto.getIsGood() != null) {
					entityToSave.setIsGood(dto.getIsGood());
				}

				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ElementEtatVehiculeFicheReception> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = elementEtatVehiculeFicheReceptionRepository.save((Iterable<ElementEtatVehiculeFicheReception>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("elementEtatVehiculeFicheReception", locale));
					response.setHasError(true);
					return response;
				}
				List<ElementEtatVehiculeFicheReceptionDto> itemsDto = new ArrayList<ElementEtatVehiculeFicheReceptionDto>();
				for (ElementEtatVehiculeFicheReception entity : itemsSaved) {
					ElementEtatVehiculeFicheReceptionDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update ElementEtatVehiculeFicheReception-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete ElementEtatVehiculeFicheReception by using ElementEtatVehiculeFicheReceptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ElementEtatVehiculeFicheReceptionDto> delete(Request<ElementEtatVehiculeFicheReceptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete ElementEtatVehiculeFicheReception-----");

		response = new Response<ElementEtatVehiculeFicheReceptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ElementEtatVehiculeFicheReception> items = new ArrayList<ElementEtatVehiculeFicheReception>();

			for (ElementEtatVehiculeFicheReceptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la elementEtatVehiculeFicheReception existe
				ElementEtatVehiculeFicheReception existingEntity = null;
				existingEntity = elementEtatVehiculeFicheReceptionRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("elementEtatVehiculeFicheReception -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				elementEtatVehiculeFicheReceptionRepository.save((Iterable<ElementEtatVehiculeFicheReception>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete ElementEtatVehiculeFicheReception-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete ElementEtatVehiculeFicheReception by using ElementEtatVehiculeFicheReceptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<ElementEtatVehiculeFicheReceptionDto> forceDelete(Request<ElementEtatVehiculeFicheReceptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete ElementEtatVehiculeFicheReception-----");

		response = new Response<ElementEtatVehiculeFicheReceptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ElementEtatVehiculeFicheReception> items = new ArrayList<ElementEtatVehiculeFicheReception>();

			for (ElementEtatVehiculeFicheReceptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la elementEtatVehiculeFicheReception existe
				ElementEtatVehiculeFicheReception existingEntity = null;
				existingEntity = elementEtatVehiculeFicheReceptionRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("elementEtatVehiculeFicheReception -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				elementEtatVehiculeFicheReceptionRepository.save((Iterable<ElementEtatVehiculeFicheReception>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete ElementEtatVehiculeFicheReception-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get ElementEtatVehiculeFicheReception by using ElementEtatVehiculeFicheReceptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<ElementEtatVehiculeFicheReceptionDto> getByCriteria(Request<ElementEtatVehiculeFicheReceptionDto> request, Locale locale) {
		slf4jLogger.info("----begin get ElementEtatVehiculeFicheReception-----");

		response = new Response<ElementEtatVehiculeFicheReceptionDto>();

		try {
			List<ElementEtatVehiculeFicheReception> items = null;
			items = elementEtatVehiculeFicheReceptionRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<ElementEtatVehiculeFicheReceptionDto> itemsDto = new ArrayList<ElementEtatVehiculeFicheReceptionDto>();
				for (ElementEtatVehiculeFicheReception entity : items) {
					ElementEtatVehiculeFicheReceptionDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(elementEtatVehiculeFicheReceptionRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("elementEtatVehiculeFicheReception", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get ElementEtatVehiculeFicheReception-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ElementEtatVehiculeFicheReceptionDto by using ElementEtatVehiculeFicheReception as object.
	 *
	 * @param entity, locale
	 * @return ElementEtatVehiculeFicheReceptionDto
	 *
	 */
	private ElementEtatVehiculeFicheReceptionDto getFullInfos(ElementEtatVehiculeFicheReception entity, Integer size, Locale locale) throws Exception {
		ElementEtatVehiculeFicheReceptionDto dto = ElementEtatVehiculeFicheReceptionTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
