

/*
 * Java transformer for entity table client 
 * Created on 2019-08-22 ( Time 17:31:49 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "client"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface ClientTransformer {

	ClientTransformer INSTANCE = Mappers.getMapper(ClientTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.typeClient.id", target="typeClientId"),
		@Mapping(source="entity.typeClient.libelle", target="typeClientLibelle"),
	})
	ClientDto toDto(Client entity) throws ParseException;

    List<ClientDto> toDtos(List<Client> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.matricule", target="matricule"),
		@Mapping(source="dto.identificationProprietaire", target="identificationProprietaire"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.adresseGeographique", target="adresseGeographique"),
		@Mapping(source="dto.telephone", target="telephone"),
		@Mapping(source="dto.isOld", target="isOld"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="typeClient", target="typeClient"),
	})
    Client toEntity(ClientDto dto, TypeClient typeClient) throws ParseException;

    //List<Client> toEntities(List<ClientDto> dtos) throws ParseException;

}
