package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._FournisseurRepository;

/**
 * Repository : Fournisseur.
 */
@Repository
public interface FournisseurRepository extends JpaRepository<Fournisseur, Integer>, _FournisseurRepository {
	/**
	 * Finds Fournisseur by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Fournisseur whose id is equals to the given id. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.id = :id and e.isDeleted = :isDeleted")
	Fournisseur findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Fournisseur by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object Fournisseur whose nom is equals to the given nom. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<Fournisseur> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using prenom as a search criteria.
	 *
	 * @param prenom
	 * @return An Object Fournisseur whose prenom is equals to the given prenom. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.prenom = :prenom and e.isDeleted = :isDeleted")
	List<Fournisseur> findByPrenom(@Param("prenom")String prenom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using matricule as a search criteria.
	 *
	 * @param matricule
	 * @return An Object Fournisseur whose matricule is equals to the given matricule. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.matricule = :matricule and e.isDeleted = :isDeleted")
	List<Fournisseur> findByMatricule(@Param("matricule")String matricule, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using telephone as a search criteria.
	 *
	 * @param telephone
	 * @return An Object Fournisseur whose telephone is equals to the given telephone. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.telephone = :telephone and e.isDeleted = :isDeleted")
	List<Fournisseur> findByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using email as a search criteria.
	 *
	 * @param email
	 * @return An Object Fournisseur whose email is equals to the given email. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.email = :email and e.isDeleted = :isDeleted")
	Fournisseur findByEmail(@Param("email")String email, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Fournisseur whose isDeleted is equals to the given isDeleted. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.isDeleted = :isDeleted")
	List<Fournisseur> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Fournisseur whose createdAt is equals to the given createdAt. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Fournisseur> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Fournisseur whose updatedAt is equals to the given updatedAt. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Fournisseur> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Fournisseur whose deletedAt is equals to the given deletedAt. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Fournisseur> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Fournisseur whose createdBy is equals to the given createdBy. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Fournisseur> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Fournisseur whose updatedBy is equals to the given updatedBy. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Fournisseur> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Fournisseur by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Fournisseur whose deletedBy is equals to the given deletedBy. If
	 *         no Fournisseur is found, this method returns null.
	 */
	@Query("select e from Fournisseur e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Fournisseur> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of Fournisseur by using fournisseurDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Fournisseur
	 * @throws DataAccessException,ParseException
	 */
	public default List<Fournisseur> getByCriteria(Request<FournisseurDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Fournisseur e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Fournisseur> query = em.createQuery(req, Fournisseur.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Fournisseur by using fournisseurDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Fournisseur
	 *
	 */
	public default Long count(Request<FournisseurDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Fournisseur e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<FournisseurDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		FournisseurDto dto = request.getData() != null ? request.getData() : new FournisseurDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (FournisseurDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(FournisseurDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prenom", dto.getPrenom(), "e.prenom", "String", dto.getPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMatricule())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("matricule", dto.getMatricule(), "e.matricule", "String", dto.getMatriculeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephone())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephone", dto.getTelephone(), "e.telephone", "String", dto.getTelephoneParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmail())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("email", dto.getEmail(), "e.email", "String", dto.getEmailParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
