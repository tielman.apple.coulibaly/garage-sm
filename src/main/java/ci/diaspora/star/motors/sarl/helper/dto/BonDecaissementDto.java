
/*
 * Java dto for entity table bon_decaissement
 * Created on 2019-08-18 ( Time 17:15:05 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._BonDecaissementDto;

/**
 * DTO for table "bon_decaissement"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class BonDecaissementDto extends _BonDecaissementDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    devisId              ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Integer    etatId               ;
	/*
	 * 
	 */
    private Integer    userChefAtelierId    ;
	/*
	 * 
	 */
    private Integer    userResponsableTechniqueId ;
	/*
	 * 
	 */
    private Integer    userResponsableDecaissementId ;
	/*
	 * 
	 */
    private Integer    userResponsableAchatId ;
	/*
	 * 
	 */
    private Boolean    isValidedByChefAtelier ;
	/*
	 * 
	 */
    private Boolean    isValidedByResponsableTechnique ;
	/*
	 * 
	 */
    private Boolean    isValidedByResponsableDecaissement ;
	/*
	 * 
	 */
    private Boolean    isValidedByResponsableAchat ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
    
   
    
    
	private String userResponsableTechniqueNom;
	private String userResponsableTechniquePrenom;
	private String userResponsableTechniqueLogin;
	private String userResponsableDecaissementNom;
	private String userResponsableDecaissementPrenom;
	private String userResponsableDecaissementLogin;
	private String userChefAtelierNom;
	private String userChefAtelierPrenom;
	private String userChefAtelierLogin;
	private String etatCode;
	private String etatLibelle;
	private String userResponsableAchatNom;
	private String userResponsableAchatPrenom;
	private String userResponsableAchatLogin;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  devisIdParam          ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Integer>  etatIdParam           ;                     
	private SearchParam<Integer>  userChefAtelierIdParam;                     
	private SearchParam<Integer>  userResponsableTechniqueIdParam;                     
	private SearchParam<Integer>  userResponsableDecaissementIdParam;                     
	private SearchParam<Integer>  userResponsableAchatIdParam;                     
	private SearchParam<Boolean>  isValidedByChefAtelierParam;                     
	private SearchParam<Boolean>  isValidedByResponsableTechniqueParam;                     
	private SearchParam<Boolean>  isValidedByResponsableDecaissementParam;                     
	private SearchParam<Boolean>  isValidedByResponsableAchatParam;                     
	private SearchParam<String>   userResponsableAchatNomParam          ;                     
	private SearchParam<String>   userResponsableAchatPrenomParam       ;                     
	private SearchParam<String>   userResponsableAchatLoginParam        ;                     
	private SearchParam<String>   userChefAtelierNomParam          ;                     
	private SearchParam<String>   userChefAtelierPrenomParam       ;                     
	private SearchParam<String>   userChefAtelierLoginParam        ;                     
	private SearchParam<String>   userResponsableDecaissementNomParam          ;                     
	private SearchParam<String>   userResponsableDecaissementPrenomParam       ;                     
	private SearchParam<String>   userResponsableDecaissementLoginParam        ;                     
	private SearchParam<String>   etatCodeParam         ;                     
	private SearchParam<String>   etatLibelleParam      ;                     
	private SearchParam<String>   userResponsableTechniqueNomParam          ;                     
	private SearchParam<String>   userResponsableTechniquePrenomParam       ;                     
	private SearchParam<String>   userResponsableTechniqueLoginParam        ;                     
    /**
     * Default constructor
     */
    public BonDecaissementDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "devisId" field value
     * @param devisId
     */
	public void setDevisId( Integer devisId )
    {
        this.devisId = devisId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDevisId()
    {
        return this.devisId;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }

    /**
     * Set the "etatId" field value
     * @param etatId
     */
	public void setEtatId( Integer etatId )
    {
        this.etatId = etatId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getEtatId()
    {
        return this.etatId;
    }

    /**
     * Set the "userChefAtelierId" field value
     * @param userChefAtelierId
     */
	public void setUserChefAtelierId( Integer userChefAtelierId )
    {
        this.userChefAtelierId = userChefAtelierId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserChefAtelierId()
    {
        return this.userChefAtelierId;
    }

    /**
     * Set the "userResponsableTechniqueId" field value
     * @param userResponsableTechniqueId
     */
	public void setUserResponsableTechniqueId( Integer userResponsableTechniqueId )
    {
        this.userResponsableTechniqueId = userResponsableTechniqueId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserResponsableTechniqueId()
    {
        return this.userResponsableTechniqueId;
    }

    /**
     * Set the "userResponsableDecaissementId" field value
     * @param userResponsableDecaissementId
     */
	public void setUserResponsableDecaissementId( Integer userResponsableDecaissementId )
    {
        this.userResponsableDecaissementId = userResponsableDecaissementId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserResponsableDecaissementId()
    {
        return this.userResponsableDecaissementId;
    }

    /**
     * Set the "userResponsableAchatId" field value
     * @param userResponsableAchatId
     */
	public void setUserResponsableAchatId( Integer userResponsableAchatId )
    {
        this.userResponsableAchatId = userResponsableAchatId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserResponsableAchatId()
    {
        return this.userResponsableAchatId;
    }

    /**
     * Set the "isValidedByChefAtelier" field value
     * @param isValidedByChefAtelier
     */
	public void setIsValidedByChefAtelier( Boolean isValidedByChefAtelier )
    {
        this.isValidedByChefAtelier = isValidedByChefAtelier ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByChefAtelier()
    {
        return this.isValidedByChefAtelier;
    }

    /**
     * Set the "isValidedByResponsableTechnique" field value
     * @param isValidedByResponsableTechnique
     */
	public void setIsValidedByResponsableTechnique( Boolean isValidedByResponsableTechnique )
    {
        this.isValidedByResponsableTechnique = isValidedByResponsableTechnique ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByResponsableTechnique()
    {
        return this.isValidedByResponsableTechnique;
    }

    /**
     * Set the "isValidedByResponsableDecaissement" field value
     * @param isValidedByResponsableDecaissement
     */
	public void setIsValidedByResponsableDecaissement( Boolean isValidedByResponsableDecaissement )
    {
        this.isValidedByResponsableDecaissement = isValidedByResponsableDecaissement ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByResponsableDecaissement()
    {
        return this.isValidedByResponsableDecaissement;
    }

    /**
     * Set the "isValidedByResponsableAchat" field value
     * @param isValidedByResponsableAchat
     */
	public void setIsValidedByResponsableAchat( Boolean isValidedByResponsableAchat )
    {
        this.isValidedByResponsableAchat = isValidedByResponsableAchat ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByResponsableAchat()
    {
        return this.isValidedByResponsableAchat;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "devisIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDevisIdParam(){
		return this.devisIdParam;
	}
	/**
     * Set the "devisIdParam" field value
     * @param devisIdParam
     */
    public void setDevisIdParam( SearchParam<Integer> devisIdParam ){
        this.devisIdParam = devisIdParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

	/**
     * Get the "etatIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getEtatIdParam(){
		return this.etatIdParam;
	}
	/**
     * Set the "etatIdParam" field value
     * @param etatIdParam
     */
    public void setEtatIdParam( SearchParam<Integer> etatIdParam ){
        this.etatIdParam = etatIdParam;
    }

	/**
     * Get the "userChefAtelierIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserChefAtelierIdParam(){
		return this.userChefAtelierIdParam;
	}
	/**
     * Set the "userChefAtelierIdParam" field value
     * @param userChefAtelierIdParam
     */
    public void setUserChefAtelierIdParam( SearchParam<Integer> userChefAtelierIdParam ){
        this.userChefAtelierIdParam = userChefAtelierIdParam;
    }

	/**
     * Get the "userResponsableTechniqueIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserResponsableTechniqueIdParam(){
		return this.userResponsableTechniqueIdParam;
	}
	/**
     * Set the "userResponsableTechniqueIdParam" field value
     * @param userResponsableTechniqueIdParam
     */
    public void setUserResponsableTechniqueIdParam( SearchParam<Integer> userResponsableTechniqueIdParam ){
        this.userResponsableTechniqueIdParam = userResponsableTechniqueIdParam;
    }

	/**
     * Get the "userResponsableDecaissementIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserResponsableDecaissementIdParam(){
		return this.userResponsableDecaissementIdParam;
	}
	/**
     * Set the "userResponsableDecaissementIdParam" field value
     * @param userResponsableDecaissementIdParam
     */
    public void setUserResponsableDecaissementIdParam( SearchParam<Integer> userResponsableDecaissementIdParam ){
        this.userResponsableDecaissementIdParam = userResponsableDecaissementIdParam;
    }

	/**
     * Get the "userResponsableAchatIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserResponsableAchatIdParam(){
		return this.userResponsableAchatIdParam;
	}
	/**
     * Set the "userResponsableAchatIdParam" field value
     * @param userResponsableAchatIdParam
     */
    public void setUserResponsableAchatIdParam( SearchParam<Integer> userResponsableAchatIdParam ){
        this.userResponsableAchatIdParam = userResponsableAchatIdParam;
    }

	/**
     * Get the "isValidedByChefAtelierParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByChefAtelierParam(){
		return this.isValidedByChefAtelierParam;
	}
	/**
     * Set the "isValidedByChefAtelierParam" field value
     * @param isValidedByChefAtelierParam
     */
    public void setIsValidedByChefAtelierParam( SearchParam<Boolean> isValidedByChefAtelierParam ){
        this.isValidedByChefAtelierParam = isValidedByChefAtelierParam;
    }

	/**
     * Get the "isValidedByResponsableTechniqueParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByResponsableTechniqueParam(){
		return this.isValidedByResponsableTechniqueParam;
	}
	/**
     * Set the "isValidedByResponsableTechniqueParam" field value
     * @param isValidedByResponsableTechniqueParam
     */
    public void setIsValidedByResponsableTechniqueParam( SearchParam<Boolean> isValidedByResponsableTechniqueParam ){
        this.isValidedByResponsableTechniqueParam = isValidedByResponsableTechniqueParam;
    }

	/**
     * Get the "isValidedByResponsableDecaissementParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByResponsableDecaissementParam(){
		return this.isValidedByResponsableDecaissementParam;
	}
	/**
     * Set the "isValidedByResponsableDecaissementParam" field value
     * @param isValidedByResponsableDecaissementParam
     */
    public void setIsValidedByResponsableDecaissementParam( SearchParam<Boolean> isValidedByResponsableDecaissementParam ){
        this.isValidedByResponsableDecaissementParam = isValidedByResponsableDecaissementParam;
    }

	/**
     * Get the "isValidedByResponsableAchatParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByResponsableAchatParam(){
		return this.isValidedByResponsableAchatParam;
	}
	/**
     * Set the "isValidedByResponsableAchatParam" field value
     * @param isValidedByResponsableAchatParam
     */
    public void setIsValidedByResponsableAchatParam( SearchParam<Boolean> isValidedByResponsableAchatParam ){
        this.isValidedByResponsableAchatParam = isValidedByResponsableAchatParam;
    }

	
    
	public String getUserResponsableTechniqueNom() {
		return userResponsableTechniqueNom;
	}

	public void setUserResponsableTechniqueNom(String userResponsableTechniqueNom) {
		this.userResponsableTechniqueNom = userResponsableTechniqueNom;
	}

	public String getUserResponsableTechniquePrenom() {
		return userResponsableTechniquePrenom;
	}

	public void setUserResponsableTechniquePrenom(String userResponsableTechniquePrenom) {
		this.userResponsableTechniquePrenom = userResponsableTechniquePrenom;
	}

	public String getUserResponsableTechniqueLogin() {
		return userResponsableTechniqueLogin;
	}

	public void setUserResponsableTechniqueLogin(String userResponsableTechniqueLogin) {
		this.userResponsableTechniqueLogin = userResponsableTechniqueLogin;
	}

	public String getUserResponsableDecaissementNom() {
		return userResponsableDecaissementNom;
	}

	public void setUserResponsableDecaissementNom(String userResponsableDecaissementNom) {
		this.userResponsableDecaissementNom = userResponsableDecaissementNom;
	}

	public String getUserResponsableDecaissementPrenom() {
		return userResponsableDecaissementPrenom;
	}

	public void setUserResponsableDecaissementPrenom(String userResponsableDecaissementPrenom) {
		this.userResponsableDecaissementPrenom = userResponsableDecaissementPrenom;
	}

	public String getUserResponsableDecaissementLogin() {
		return userResponsableDecaissementLogin;
	}

	public void setUserResponsableDecaissementLogin(String userResponsableDecaissementLogin) {
		this.userResponsableDecaissementLogin = userResponsableDecaissementLogin;
	}

	public String getUserChefAtelierNom() {
		return userChefAtelierNom;
	}

	public void setUserChefAtelierNom(String userChefAtelierNom) {
		this.userChefAtelierNom = userChefAtelierNom;
	}

	public String getUserChefAtelierPrenom() {
		return userChefAtelierPrenom;
	}

	public void setUserChefAtelierPrenom(String userChefAtelierPrenom) {
		this.userChefAtelierPrenom = userChefAtelierPrenom;
	}

	public String getUserChefAtelierLogin() {
		return userChefAtelierLogin;
	}

	public void setUserChefAtelierLogin(String userChefAtelierLogin) {
		this.userChefAtelierLogin = userChefAtelierLogin;
	}

	public String getEtatCode() {
		return etatCode;
	}

	public void setEtatCode(String etatCode) {
		this.etatCode = etatCode;
	}

	public String getEtatLibelle() {
		return etatLibelle;
	}

	public void setEtatLibelle(String etatLibelle) {
		this.etatLibelle = etatLibelle;
	}

	public String getUserResponsableAchatNom() {
		return userResponsableAchatNom;
	}

	public void setUserResponsableAchatNom(String userResponsableAchatNom) {
		this.userResponsableAchatNom = userResponsableAchatNom;
	}

	public String getUserResponsableAchatPrenom() {
		return userResponsableAchatPrenom;
	}

	public void setUserResponsableAchatPrenom(String userResponsableAchatPrenom) {
		this.userResponsableAchatPrenom = userResponsableAchatPrenom;
	}

	public String getUserResponsableAchatLogin() {
		return userResponsableAchatLogin;
	}

	public void setUserResponsableAchatLogin(String userResponsableAchatLogin) {
		this.userResponsableAchatLogin = userResponsableAchatLogin;
	}

	
	public SearchParam<String> getEtatCodeParam() {
		return etatCodeParam;
	}

	public void setEtatCodeParam(SearchParam<String> etatCodeParam) {
		this.etatCodeParam = etatCodeParam;
	}

	public SearchParam<String> getEtatLibelleParam() {
		return etatLibelleParam;
	}

	public void setEtatLibelleParam(SearchParam<String> etatLibelleParam) {
		this.etatLibelleParam = etatLibelleParam;
	}

	

	public SearchParam<String> getUserResponsableAchatNomParam() {
		return userResponsableAchatNomParam;
	}

	public void setUserResponsableAchatNomParam(SearchParam<String> userResponsableAchatNomParam) {
		this.userResponsableAchatNomParam = userResponsableAchatNomParam;
	}

	public SearchParam<String> getUserResponsableAchatPrenomParam() {
		return userResponsableAchatPrenomParam;
	}

	public void setUserResponsableAchatPrenomParam(SearchParam<String> userResponsableAchatPrenomParam) {
		this.userResponsableAchatPrenomParam = userResponsableAchatPrenomParam;
	}

	public SearchParam<String> getUserResponsableAchatLoginParam() {
		return userResponsableAchatLoginParam;
	}

	public void setUserResponsableAchatLoginParam(SearchParam<String> userResponsableAchatLoginParam) {
		this.userResponsableAchatLoginParam = userResponsableAchatLoginParam;
	}

	public SearchParam<String> getUserChefAtelierNomParam() {
		return userChefAtelierNomParam;
	}

	public void setUserChefAtelierNomParam(SearchParam<String> userChefAtelierNomParam) {
		this.userChefAtelierNomParam = userChefAtelierNomParam;
	}

	public SearchParam<String> getUserChefAtelierPrenomParam() {
		return userChefAtelierPrenomParam;
	}

	public void setUserChefAtelierPrenomParam(SearchParam<String> userChefAtelierPrenomParam) {
		this.userChefAtelierPrenomParam = userChefAtelierPrenomParam;
	}

	public SearchParam<String> getUserChefAtelierLoginParam() {
		return userChefAtelierLoginParam;
	}

	public void setUserChefAtelierLoginParam(SearchParam<String> userChefAtelierLoginParam) {
		this.userChefAtelierLoginParam = userChefAtelierLoginParam;
	}

	public SearchParam<String> getUserResponsableDecaissementNomParam() {
		return userResponsableDecaissementNomParam;
	}

	public void setUserResponsableDecaissementNomParam(SearchParam<String> userResponsableDecaissementNomParam) {
		this.userResponsableDecaissementNomParam = userResponsableDecaissementNomParam;
	}

	public SearchParam<String> getUserResponsableDecaissementPrenomParam() {
		return userResponsableDecaissementPrenomParam;
	}

	public void setUserResponsableDecaissementPrenomParam(SearchParam<String> userResponsableDecaissementPrenomParam) {
		this.userResponsableDecaissementPrenomParam = userResponsableDecaissementPrenomParam;
	}

	public SearchParam<String> getUserResponsableDecaissementLoginParam() {
		return userResponsableDecaissementLoginParam;
	}

	public void setUserResponsableDecaissemenLogintParam(SearchParam<String> userResponsableDecaissementLoginParam) {
		this.userResponsableDecaissementLoginParam = userResponsableDecaissementLoginParam;
	}

	public SearchParam<String> getUserResponsableTechniqueNomParam() {
		return userResponsableTechniqueNomParam;
	}

	public void setUserResponsableTechniqueNomParam(SearchParam<String> userResponsableTechniqueNomParam) {
		this.userResponsableTechniqueNomParam = userResponsableTechniqueNomParam;
	}

	public SearchParam<String> getUserResponsableTechniquePrenomParam() {
		return userResponsableTechniquePrenomParam;
	}

	public void setUserResponsableTechniquePrenomParam(SearchParam<String> userResponsableTechniquePrenomParam) {
		this.userResponsableTechniquePrenomParam = userResponsableTechniquePrenomParam;
	}

	public SearchParam<String> getUserResponsableTechniqueLoginParam() {
		return userResponsableTechniqueLoginParam;
	}

	public void setUserResponsableTechniqueLoginParam(SearchParam<String> userResponsableTechniqueLoginParam) {
		this.userResponsableTechniqueLoginParam = userResponsableTechniqueLoginParam;
	}

	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		BonDecaissementDto other = (BonDecaissementDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
		sb.append("|");
		sb.append("isValidedByChefAtelier:"+isValidedByChefAtelier);
		sb.append("|");
		sb.append("isValidedByResponsableTechnique:"+isValidedByResponsableTechnique);
		sb.append("|");
		sb.append("isValidedByResponsableDecaissement:"+isValidedByResponsableDecaissement);
		sb.append("|");
		sb.append("isValidedByResponsableAchat:"+isValidedByResponsableAchat);
        return sb.toString();
    }
}
