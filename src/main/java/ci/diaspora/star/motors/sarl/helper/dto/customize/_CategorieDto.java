
/*
 * Java dto for entity table categorie 
 * Created on 2019-08-19 ( Time 10:29:45 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.dto.ElementEtatVehiculeDto;

/**
 * DTO customize for table "categorie"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _CategorieDto {
	
	List<ElementEtatVehiculeDto> datasElementEtatVehicule;

	public List<ElementEtatVehiculeDto> getDatasElementEtatVehicule() {
		return datasElementEtatVehicule;
	}

	public void setDatasElementEtatVehicule(List<ElementEtatVehiculeDto> datasElementEtatVehicule) {
		this.datasElementEtatVehicule = datasElementEtatVehicule;
	}
	
	

}
