


                                                                                                              /*
 * Java transformer for entity table facture
 * Created on 2019-08-18 ( Time 17:15:10 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "facture"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class FactureBusiness implements IBasicBusiness<Request<FactureDto>, Response<FactureDto>> {

  private Response<FactureDto> response;
  @Autowired
  private FactureRepository factureRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private UserRepository userDirectionRepository;
  @Autowired
  private EtatRepository etatRepository;
  @Autowired
  private UserRepository userResponsableTechniqueRepository;
  @Autowired
  private FicheReceptionRepository ficheReceptionRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                      

  public FactureBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create Facture by using FactureDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FactureDto> create(Request<FactureDto> request, Locale locale)  {
    slf4jLogger.info("----begin create Facture-----");

    response = new Response<FactureDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Facture> items = new ArrayList<Facture>();

      for (FactureDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("ficheReceptionId", dto.getFicheReceptionId());
        fieldsToVerify.put("userChefAtelierId", dto.getUserChefAtelierId());
        fieldsToVerify.put("userResponsableTechniqueId", dto.getUserResponsableTechniqueId());
        fieldsToVerify.put("userDirectionId", dto.getUserDirectionId());
        fieldsToVerify.put("isValidedByChefAtelier", dto.getIsValidedByChefAtelier());
        fieldsToVerify.put("isValidedByResponsableTechnique", dto.getIsValidedByResponsableTechnique());
        fieldsToVerify.put("isValidedByDirection", dto.getIsValidedByDirection());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        fieldsToVerify.put("etatId", dto.getEtatId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if facture to insert do not exist
        Facture existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("facture -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if userDirection exist
        User existingUserDirection = userDirectionRepository.findById(dto.getUserDirectionId(), false);
        if (existingUserDirection == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("userDirection -> " + dto.getUserDirectionId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if etat exist
        Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
        if (existingEtat == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if user exist
        User existingUser = userRepository.findById(dto.getUserChefAtelierId(), false);
        if (existingUser == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserChefAtelierId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if userResponsableTechnique exist
        User existingUserResponsableTechnique = userResponsableTechniqueRepository.findById(dto.getUserResponsableTechniqueId(), false);
        if (existingUserResponsableTechnique == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("userResponsableTechnique -> " + dto.getUserResponsableTechniqueId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if ficheReception exist
        FicheReception existingFicheReception = ficheReceptionRepository.findById(dto.getFicheReceptionId(), false);
        if (existingFicheReception == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getFicheReceptionId(), locale));
          response.setHasError(true);
          return response;
        }
        Facture entityToSave = null;
        entityToSave = FactureTransformer.INSTANCE.toEntity(dto, existingUserDirection, existingEtat, existingUser, existingUserResponsableTechnique, existingFicheReception);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<Facture> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = factureRepository.save((Iterable<Facture>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("facture", locale));
          response.setHasError(true);
          return response;
        }
        List<FactureDto> itemsDto = new ArrayList<FactureDto>();
        for (Facture entity : itemsSaved) {
          FactureDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create Facture-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update Facture by using FactureDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FactureDto> update(Request<FactureDto> request, Locale locale)  {
    slf4jLogger.info("----begin update Facture-----");

    response = new Response<FactureDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Facture> items = new ArrayList<Facture>();

      for (FactureDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la facture existe
        Facture entityToSave = null;
        entityToSave = factureRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("facture -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if userDirection exist
        if (dto.getUserDirectionId() != null && dto.getUserDirectionId() > 0){
          User existingUserDirection = userDirectionRepository.findById(dto.getUserDirectionId(), false);
          if (existingUserDirection == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("userDirection -> " + dto.getUserDirectionId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserDirection(existingUserDirection);
        }
        // Verify if etat exist
        if (dto.getEtatId() != null && dto.getEtatId() > 0){
          Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
          if (existingEtat == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setEtat(existingEtat);
        }
        // Verify if user exist
        if (dto.getUserChefAtelierId() != null && dto.getUserChefAtelierId() > 0){
          User existingUserChefAtelier = userRepository.findById(dto.getUserChefAtelierId(), false);
          if (existingUserChefAtelier == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserChefAtelierId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserChefAtelier(existingUserChefAtelier);
        }
        // Verify if userResponsableTechnique exist
        if (dto.getUserResponsableTechniqueId() != null && dto.getUserResponsableTechniqueId() > 0){
          User existingUserResponsableTechnique = userResponsableTechniqueRepository.findById(dto.getUserResponsableTechniqueId(), false);
          if (existingUserResponsableTechnique == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("userResponsableTechnique -> " + dto.getUserResponsableTechniqueId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserResponsableTechnique(existingUserResponsableTechnique);
        }
        // Verify if ficheReception exist
        if (dto.getFicheReceptionId() != null && dto.getFicheReceptionId() > 0){
          FicheReception existingFicheReception = ficheReceptionRepository.findById(dto.getFicheReceptionId(), false);
          if (existingFicheReception == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getFicheReceptionId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setFicheReception(existingFicheReception);
        }
        if (dto.getIsValidedByChefAtelier() != null) {
          entityToSave.setIsValidedByChefAtelier(dto.getIsValidedByChefAtelier());
        }
        if (dto.getIsValidedByResponsableTechnique() != null) {
          entityToSave.setIsValidedByResponsableTechnique(dto.getIsValidedByResponsableTechnique());
        }
        if (dto.getIsValidedByDirection() != null) {
          entityToSave.setIsValidedByDirection(dto.getIsValidedByDirection());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<Facture> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = factureRepository.save((Iterable<Facture>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("facture", locale));
          response.setHasError(true);
          return response;
        }
        List<FactureDto> itemsDto = new ArrayList<FactureDto>();
        for (Facture entity : itemsSaved) {
          FactureDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update Facture-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete Facture by using FactureDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FactureDto> delete(Request<FactureDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete Facture-----");

    response = new Response<FactureDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Facture> items = new ArrayList<Facture>();

      for (FactureDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la facture existe
        Facture existingEntity = null;
        existingEntity = factureRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("facture -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        factureRepository.save((Iterable<Facture>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete Facture-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete Facture by using FactureDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<FactureDto> forceDelete(Request<FactureDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete Facture-----");

    response = new Response<FactureDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Facture> items = new ArrayList<Facture>();

      for (FactureDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la facture existe
        Facture existingEntity = null;
          existingEntity = factureRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("facture -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                      

                                    existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          factureRepository.save((Iterable<Facture>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete Facture-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get Facture by using FactureDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<FactureDto> getByCriteria(Request<FactureDto> request, Locale locale) {
    slf4jLogger.info("----begin get Facture-----");

    response = new Response<FactureDto>();

    try {
      List<Facture> items = null;
      items = factureRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<FactureDto> itemsDto = new ArrayList<FactureDto>();
        for (Facture entity : items) {
          FactureDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(factureRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("facture", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get Facture-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full FactureDto by using Facture as object.
   *
   * @param entity, locale
   * @return FactureDto
   *
   */
  private FactureDto getFullInfos(Facture entity, Integer size, Locale locale) throws Exception {
    FactureDto dto = FactureTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
