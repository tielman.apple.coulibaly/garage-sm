

/*
 * Java transformer for entity table type_ordre_reparation 
 * Created on 2019-08-18 ( Time 17:15:16 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "type_ordre_reparation"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface TypeOrdreReparationTransformer {

	TypeOrdreReparationTransformer INSTANCE = Mappers.getMapper(TypeOrdreReparationTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
	})
	TypeOrdreReparationDto toDto(TypeOrdreReparation entity) throws ParseException;

    List<TypeOrdreReparationDto> toDtos(List<TypeOrdreReparation> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
	})
    TypeOrdreReparation toEntity(TypeOrdreReparationDto dto) throws ParseException;

    //List<TypeOrdreReparation> toEntities(List<TypeOrdreReparationDto> dtos) throws ParseException;

}
