


/*
 * Java transformer for entity table designation_element_fiche_controle
 * Created on 2019-08-19 ( Time 10:29:46 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "designation_element_fiche_controle"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class DesignationElementFicheControleBusiness implements IBasicBusiness<Request<DesignationElementFicheControleDto>, Response<DesignationElementFicheControleDto>> {

	private Response<DesignationElementFicheControleDto> response;
	@Autowired
	private DesignationElementFicheControleRepository designationElementFicheControleRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CategorieRepository categorieRepository;
	@Autowired
	private DesignationElementFicheControleFicheControlFinalRepository designationElementFicheControleFicheControlFinalRepository;
	@Autowired
	private DesignationElementFicheControleTypeElementFicheControlRepository designationElementFicheControleTypeElementFicheControlRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private DesignationElementFicheControleFicheControlFinalBusiness designationElementFicheControleFicheControlFinalBusiness;


	@Autowired
	private DesignationElementFicheControleTypeElementFicheControlBusiness designationElementFicheControleTypeElementFicheControlBusiness;



	public DesignationElementFicheControleBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create DesignationElementFicheControle by using DesignationElementFicheControleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<DesignationElementFicheControleDto> create(Request<DesignationElementFicheControleDto> request, Locale locale)  {
		slf4jLogger.info("----begin create DesignationElementFicheControle-----");

		response = new Response<DesignationElementFicheControleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DesignationElementFicheControle> items = new ArrayList<DesignationElementFicheControle>();

			for (DesignationElementFicheControleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("categorieId", dto.getCategorieId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				

				// Verify if designationElementFicheControle to insert do not exist
				DesignationElementFicheControle existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("designationElementFicheControle -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if categorie exist
				Categorie existingCategorie = categorieRepository.findById(dto.getCategorieId(), false);
				if (existingCategorie == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("categorie -> " + dto.getCategorieId(), locale));
					response.setHasError(true);
					return response;
				}
				
				existingEntity = designationElementFicheControleRepository.findByLibelleAndCategorieId(dto.getLibelle(), dto.getCategorieId(),  false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("designationElementFicheControle -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
//				existingEntity = designationElementFicheControleRepository.findByLibelle(dto.getLibelle(), false);
//				if (existingEntity != null) {
//					response.setStatus(functionalError.DATA_EXIST("designationElementFicheControle -> " + dto.getLibelle(), locale));
//					response.setHasError(true);
//					return response;
//				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && a.getCategorie().getId().equals(dto.getCategorieId()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les designationElementFicheControles", locale));
					response.setHasError(true);
					return response;
				}

				
				DesignationElementFicheControle entityToSave = null;
				entityToSave = DesignationElementFicheControleTransformer.INSTANCE.toEntity(dto, existingCategorie);
				entityToSave.setIsDeleted(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<DesignationElementFicheControle> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = designationElementFicheControleRepository.save((Iterable<DesignationElementFicheControle>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("designationElementFicheControle", locale));
					response.setHasError(true);
					return response;
				}
				List<DesignationElementFicheControleDto> itemsDto = new ArrayList<DesignationElementFicheControleDto>();
				for (DesignationElementFicheControle entity : itemsSaved) {
					DesignationElementFicheControleDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create DesignationElementFicheControle-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update DesignationElementFicheControle by using DesignationElementFicheControleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<DesignationElementFicheControleDto> update(Request<DesignationElementFicheControleDto> request, Locale locale)  {
		slf4jLogger.info("----begin update DesignationElementFicheControle-----");

		response = new Response<DesignationElementFicheControleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DesignationElementFicheControle> items = new ArrayList<DesignationElementFicheControle>();

			for (DesignationElementFicheControleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la designationElementFicheControle existe
				DesignationElementFicheControle entityToSave = null;
				entityToSave = designationElementFicheControleRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControle -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if categorie exist
				if (dto.getCategorieId() != null && dto.getCategorieId() > 0){
					Categorie existingCategorie = categorieRepository.findById(dto.getCategorieId(), false);
					if (existingCategorie == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("categorie -> " + dto.getCategorieId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCategorie(existingCategorie);
				}
				Integer categorieId = entityToSave.getCategorie().getId();
				if (Utilities.notBlank(dto.getLibelle())) {
					DesignationElementFicheControle existingEntity = designationElementFicheControleRepository.findByLibelleAndCategorieId(dto.getLibelle(), categorieId,  false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("designationElementFicheControle -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}

					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) 
							&& a.getCategorie().getId().equals(categorieId)
							&& !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les designationElementFicheControles", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
			
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<DesignationElementFicheControle> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = designationElementFicheControleRepository.save((Iterable<DesignationElementFicheControle>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("designationElementFicheControle", locale));
					response.setHasError(true);
					return response;
				}
				List<DesignationElementFicheControleDto> itemsDto = new ArrayList<DesignationElementFicheControleDto>();
				for (DesignationElementFicheControle entity : itemsSaved) {
					DesignationElementFicheControleDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update DesignationElementFicheControle-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete DesignationElementFicheControle by using DesignationElementFicheControleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<DesignationElementFicheControleDto> delete(Request<DesignationElementFicheControleDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete DesignationElementFicheControle-----");

		response = new Response<DesignationElementFicheControleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DesignationElementFicheControle> items = new ArrayList<DesignationElementFicheControle>();

			for (DesignationElementFicheControleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la designationElementFicheControle existe
				DesignationElementFicheControle existingEntity = null;
				existingEntity = designationElementFicheControleRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControle -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// designationElementFicheControleFicheControlFinal
				List<DesignationElementFicheControleFicheControlFinal> listOfDesignationElementFicheControleFicheControlFinal = designationElementFicheControleFicheControlFinalRepository.findByDesignationNiveauControlId(existingEntity.getId(), false);
				if (listOfDesignationElementFicheControleFicheControlFinal != null && !listOfDesignationElementFicheControleFicheControlFinal.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDesignationElementFicheControleFicheControlFinal.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// designationElementFicheControleTypeElementFicheControl
				List<DesignationElementFicheControleTypeElementFicheControl> listOfDesignationElementFicheControleTypeElementFicheControl = designationElementFicheControleTypeElementFicheControlRepository.findByDesignationNiveauControlId(existingEntity.getId(), false);
				if (listOfDesignationElementFicheControleTypeElementFicheControl != null && !listOfDesignationElementFicheControleTypeElementFicheControl.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDesignationElementFicheControleTypeElementFicheControl.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				designationElementFicheControleRepository.save((Iterable<DesignationElementFicheControle>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete DesignationElementFicheControle-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete DesignationElementFicheControle by using DesignationElementFicheControleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<DesignationElementFicheControleDto> forceDelete(Request<DesignationElementFicheControleDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete DesignationElementFicheControle-----");

		response = new Response<DesignationElementFicheControleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DesignationElementFicheControle> items = new ArrayList<DesignationElementFicheControle>();

			for (DesignationElementFicheControleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la designationElementFicheControle existe
				DesignationElementFicheControle existingEntity = null;
				existingEntity = designationElementFicheControleRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControle -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// designationElementFicheControleFicheControlFinal
				List<DesignationElementFicheControleFicheControlFinal> listOfDesignationElementFicheControleFicheControlFinal = designationElementFicheControleFicheControlFinalRepository.findByDesignationNiveauControlId(existingEntity.getId(), false);
				if (listOfDesignationElementFicheControleFicheControlFinal != null && !listOfDesignationElementFicheControleFicheControlFinal.isEmpty()){
					Request<DesignationElementFicheControleFicheControlFinalDto> deleteRequest = new Request<DesignationElementFicheControleFicheControlFinalDto>();
					deleteRequest.setDatas(DesignationElementFicheControleFicheControlFinalTransformer.INSTANCE.toDtos(listOfDesignationElementFicheControleFicheControlFinal));
					deleteRequest.setUser(request.getUser());
					Response<DesignationElementFicheControleFicheControlFinalDto> deleteResponse = designationElementFicheControleFicheControlFinalBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// designationElementFicheControleTypeElementFicheControl
				List<DesignationElementFicheControleTypeElementFicheControl> listOfDesignationElementFicheControleTypeElementFicheControl = designationElementFicheControleTypeElementFicheControlRepository.findByDesignationNiveauControlId(existingEntity.getId(), false);
				if (listOfDesignationElementFicheControleTypeElementFicheControl != null && !listOfDesignationElementFicheControleTypeElementFicheControl.isEmpty()){
					Request<DesignationElementFicheControleTypeElementFicheControlDto> deleteRequest = new Request<DesignationElementFicheControleTypeElementFicheControlDto>();
					deleteRequest.setDatas(DesignationElementFicheControleTypeElementFicheControlTransformer.INSTANCE.toDtos(listOfDesignationElementFicheControleTypeElementFicheControl));
					deleteRequest.setUser(request.getUser());
					Response<DesignationElementFicheControleTypeElementFicheControlDto> deleteResponse = designationElementFicheControleTypeElementFicheControlBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				designationElementFicheControleRepository.save((Iterable<DesignationElementFicheControle>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete DesignationElementFicheControle-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get DesignationElementFicheControle by using DesignationElementFicheControleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<DesignationElementFicheControleDto> getByCriteria(Request<DesignationElementFicheControleDto> request, Locale locale) {
		slf4jLogger.info("----begin get DesignationElementFicheControle-----");

		response = new Response<DesignationElementFicheControleDto>();

		try {
			List<DesignationElementFicheControle> items = null;
			items = designationElementFicheControleRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<DesignationElementFicheControleDto> itemsDto = new ArrayList<DesignationElementFicheControleDto>();
				for (DesignationElementFicheControle entity : items) {
					DesignationElementFicheControleDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(designationElementFicheControleRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("designationElementFicheControle", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get DesignationElementFicheControle-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full DesignationElementFicheControleDto by using DesignationElementFicheControle as object.
	 *
	 * @param entity, locale
	 * @return DesignationElementFicheControleDto
	 *
	 */
	private DesignationElementFicheControleDto getFullInfos(DesignationElementFicheControle entity, Integer size, Locale locale) throws Exception {
		DesignationElementFicheControleDto dto = DesignationElementFicheControleTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
