

/*
 * Java transformer for entity table fiche_reception 
 * Created on 2019-09-10 ( Time 09:02:06 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "fiche_reception"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface FicheReceptionTransformer {

	FicheReceptionTransformer INSTANCE = Mappers.getMapper(FicheReceptionTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateProchaineRevision", dateFormat="dd/MM/yyyy",target="dateProchaineRevision"),

		@Mapping(source="entity.dateReception", dateFormat="dd/MM/yyyy",target="dateReception"),

		@Mapping(source="entity.dateSortie", dateFormat="dd/MM/yyyy",target="dateSortie"),

		@Mapping(source="entity.dateDepot", dateFormat="dd/MM/yyyy",target="dateDepot"),

		@Mapping(source="entity.dateVisiteTechnique", dateFormat="dd/MM/yyyy",target="dateVisiteTechnique"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.user.id", target="userId"),
		@Mapping(source="entity.user.nom", target="userNom"),
		@Mapping(source="entity.user.prenom", target="userPrenom"),
		@Mapping(source="entity.user.login", target="userLogin"),
		@Mapping(source="entity.etat.id", target="etatId"),
		@Mapping(source="entity.etat.code", target="etatCode"),
		@Mapping(source="entity.etat.libelle", target="etatLibelle"),
		@Mapping(source="entity.niveauCarburant.id", target="niveauCarburantId"),
		@Mapping(source="entity.niveauCarburant.libelle", target="niveauCarburantLibelle"),
		@Mapping(source="entity.vehicule.id", target="vehiculeId"),
	})
	FicheReceptionDto toDto(FicheReception entity) throws ParseException;

    List<FicheReceptionDto> toDtos(List<FicheReception> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.couleurVehiculeId", target="couleurVehiculeId"),
		@Mapping(source="dto.numeroFiche", target="numeroFiche"),
		@Mapping(source="dto.interlocuteur", target="interlocuteur"),
		@Mapping(source="dto.nomDeposeur", target="nomDeposeur"),
		@Mapping(source="dto.nomRecepteur", target="nomRecepteur"),
		@Mapping(source="dto.kilometrage", target="kilometrage"),
		@Mapping(source="dto.autres", target="autres"),
		@Mapping(source="dto.dateProchaineRevision", dateFormat="dd/MM/yyyy",target="dateProchaineRevision"),
		@Mapping(source="dto.dateReception", dateFormat="dd/MM/yyyy",target="dateReception"),
		@Mapping(source="dto.dateSortie", dateFormat="dd/MM/yyyy",target="dateSortie"),
		@Mapping(source="dto.dateDepot", dateFormat="dd/MM/yyyy",target="dateDepot"),
		@Mapping(source="dto.dateVisiteTechnique", dateFormat="dd/MM/yyyy",target="dateVisiteTechnique"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.isDeposed", target="isDeposed"),
		@Mapping(source="dto.isReceved", target="isReceved"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="user", target="user"),
		@Mapping(source="etat", target="etat"),
		@Mapping(source="niveauCarburant", target="niveauCarburant"),
		@Mapping(source="vehicule", target="vehicule"),
	})
    FicheReception toEntity(FicheReceptionDto dto, User user, Etat etat, NiveauCarburant niveauCarburant, Vehicule vehicule) throws ParseException;

    //List<FicheReception> toEntities(List<FicheReceptionDto> dtos) throws ParseException;

}
