package ci.diaspora.star.motors.sarl.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;

/**
 * Repository customize : PanneDeclareeOrdreReparation.
 */
@Repository
public interface _PanneDeclareeOrdreReparationRepository {
	default List<String> _generateCriteria(PanneDeclareeOrdreReparationDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	
	/**
	 * Finds PanneDeclareeOrdreReparation by using isDeclaredByClient as a search criteria.
	 *
	 * @param isDeclaredByClient
	 * @return An Object PanneDeclareeOrdreReparation whose isDeclaredByClient is equals to the given isDeclaredByClient. If
	 *         no PanneDeclareeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from PanneDeclareeOrdreReparation e where e.isDeclaredByClient = :isDeclaredByClient and e.ordreReparation.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
	List<PanneDeclareeOrdreReparation> findByIsDeclaredByClientAndFicheReceptionId(@Param("ficheReceptionId") Integer ficheReceptionId, @Param("isDeclaredByClient")Boolean isDeclaredByClient, @Param("isDeleted")Boolean isDeleted);
	
}
