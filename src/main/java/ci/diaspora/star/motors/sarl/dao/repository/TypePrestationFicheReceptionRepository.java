package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._TypePrestationFicheReceptionRepository;

/**
 * Repository : TypePrestationFicheReception.
 */
@Repository
public interface TypePrestationFicheReceptionRepository extends JpaRepository<TypePrestationFicheReception, Integer>, _TypePrestationFicheReceptionRepository {
	/**
	 * Finds TypePrestationFicheReception by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object TypePrestationFicheReception whose id is equals to the given id. If
	 *         no TypePrestationFicheReception is found, this method returns null.
	 */
	@Query("select e from TypePrestationFicheReception e where e.id = :id and e.isDeleted = :isDeleted")
	TypePrestationFicheReception findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds TypePrestationFicheReception by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object TypePrestationFicheReception whose isDeleted is equals to the given isDeleted. If
	 *         no TypePrestationFicheReception is found, this method returns null.
	 */
	@Query("select e from TypePrestationFicheReception e where e.isDeleted = :isDeleted")
	List<TypePrestationFicheReception> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypePrestationFicheReception by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object TypePrestationFicheReception whose createdAt is equals to the given createdAt. If
	 *         no TypePrestationFicheReception is found, this method returns null.
	 */
	@Query("select e from TypePrestationFicheReception e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<TypePrestationFicheReception> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypePrestationFicheReception by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object TypePrestationFicheReception whose updatedAt is equals to the given updatedAt. If
	 *         no TypePrestationFicheReception is found, this method returns null.
	 */
	@Query("select e from TypePrestationFicheReception e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<TypePrestationFicheReception> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypePrestationFicheReception by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object TypePrestationFicheReception whose deletedAt is equals to the given deletedAt. If
	 *         no TypePrestationFicheReception is found, this method returns null.
	 */
	@Query("select e from TypePrestationFicheReception e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<TypePrestationFicheReception> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypePrestationFicheReception by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object TypePrestationFicheReception whose createdBy is equals to the given createdBy. If
	 *         no TypePrestationFicheReception is found, this method returns null.
	 */
	@Query("select e from TypePrestationFicheReception e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<TypePrestationFicheReception> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypePrestationFicheReception by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object TypePrestationFicheReception whose updatedBy is equals to the given updatedBy. If
	 *         no TypePrestationFicheReception is found, this method returns null.
	 */
	@Query("select e from TypePrestationFicheReception e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<TypePrestationFicheReception> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypePrestationFicheReception by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object TypePrestationFicheReception whose deletedBy is equals to the given deletedBy. If
	 *         no TypePrestationFicheReception is found, this method returns null.
	 */
	@Query("select e from TypePrestationFicheReception e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<TypePrestationFicheReception> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds TypePrestationFicheReception by using typePrestationId as a search criteria.
	 *
	 * @param typePrestationId
	 * @return A list of Object TypePrestationFicheReception whose typePrestationId is equals to the given typePrestationId. If
	 *         no TypePrestationFicheReception is found, this method returns null.
	 */
	@Query("select e from TypePrestationFicheReception e where e.typePrestation.id = :typePrestationId and e.isDeleted = :isDeleted")
	List<TypePrestationFicheReception> findByTypePrestationId(@Param("typePrestationId")Integer typePrestationId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one TypePrestationFicheReception by using typePrestationId as a search criteria.
   *
   * @param typePrestationId
   * @return An Object TypePrestationFicheReception whose typePrestationId is equals to the given typePrestationId. If
   *         no TypePrestationFicheReception is found, this method returns null.
   */
  @Query("select e from TypePrestationFicheReception e where e.typePrestation.id = :typePrestationId and e.isDeleted = :isDeleted")
  TypePrestationFicheReception findTypePrestationFicheReceptionByTypePrestationId(@Param("typePrestationId")Integer typePrestationId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds TypePrestationFicheReception by using ficheReceptionId as a search criteria.
	 *
	 * @param ficheReceptionId
	 * @return A list of Object TypePrestationFicheReception whose ficheReceptionId is equals to the given ficheReceptionId. If
	 *         no TypePrestationFicheReception is found, this method returns null.
	 */
	@Query("select e from TypePrestationFicheReception e where e.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
	List<TypePrestationFicheReception> findByFicheReceptionId(@Param("ficheReceptionId")Integer ficheReceptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one TypePrestationFicheReception by using ficheReceptionId as a search criteria.
   *
   * @param ficheReceptionId
   * @return An Object TypePrestationFicheReception whose ficheReceptionId is equals to the given ficheReceptionId. If
   *         no TypePrestationFicheReception is found, this method returns null.
   */
  @Query("select e from TypePrestationFicheReception e where e.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
  TypePrestationFicheReception findTypePrestationFicheReceptionByFicheReceptionId(@Param("ficheReceptionId")Integer ficheReceptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of TypePrestationFicheReception by using typePrestationFicheReceptionDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of TypePrestationFicheReception
	 * @throws DataAccessException,ParseException
	 */
	public default List<TypePrestationFicheReception> getByCriteria(Request<TypePrestationFicheReceptionDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from TypePrestationFicheReception e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<TypePrestationFicheReception> query = em.createQuery(req, TypePrestationFicheReception.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of TypePrestationFicheReception by using typePrestationFicheReceptionDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of TypePrestationFicheReception
	 *
	 */
	public default Long count(Request<TypePrestationFicheReceptionDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from TypePrestationFicheReception e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<TypePrestationFicheReceptionDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		TypePrestationFicheReceptionDto dto = request.getData() != null ? request.getData() : new TypePrestationFicheReceptionDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (TypePrestationFicheReceptionDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(TypePrestationFicheReceptionDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getTypePrestationId()!= null && dto.getTypePrestationId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typePrestationId", dto.getTypePrestationId(), "e.typePrestation.id", "Integer", dto.getTypePrestationIdParam(), param, index, locale));
			}
			if (dto.getFicheReceptionId()!= null && dto.getFicheReceptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ficheReceptionId", dto.getFicheReceptionId(), "e.ficheReception.id", "Integer", dto.getFicheReceptionIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypePrestationLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typePrestationLibelle", dto.getTypePrestationLibelle(), "e.typePrestation.libelle", "String", dto.getTypePrestationLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
