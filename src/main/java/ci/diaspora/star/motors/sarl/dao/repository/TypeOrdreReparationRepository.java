package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._TypeOrdreReparationRepository;

/**
 * Repository : TypeOrdreReparation.
 */
@Repository
public interface TypeOrdreReparationRepository extends JpaRepository<TypeOrdreReparation, Integer>, _TypeOrdreReparationRepository {
	/**
	 * Finds TypeOrdreReparation by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object TypeOrdreReparation whose id is equals to the given id. If
	 *         no TypeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from TypeOrdreReparation e where e.id = :id and e.isDeleted = :isDeleted")
	TypeOrdreReparation findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds TypeOrdreReparation by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object TypeOrdreReparation whose libelle is equals to the given libelle. If
	 *         no TypeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from TypeOrdreReparation e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	TypeOrdreReparation findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeOrdreReparation by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object TypeOrdreReparation whose isDeleted is equals to the given isDeleted. If
	 *         no TypeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from TypeOrdreReparation e where e.isDeleted = :isDeleted")
	List<TypeOrdreReparation> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeOrdreReparation by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object TypeOrdreReparation whose createdAt is equals to the given createdAt. If
	 *         no TypeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from TypeOrdreReparation e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<TypeOrdreReparation> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeOrdreReparation by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object TypeOrdreReparation whose updatedAt is equals to the given updatedAt. If
	 *         no TypeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from TypeOrdreReparation e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<TypeOrdreReparation> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeOrdreReparation by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object TypeOrdreReparation whose deletedAt is equals to the given deletedAt. If
	 *         no TypeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from TypeOrdreReparation e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<TypeOrdreReparation> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeOrdreReparation by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object TypeOrdreReparation whose createdBy is equals to the given createdBy. If
	 *         no TypeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from TypeOrdreReparation e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<TypeOrdreReparation> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeOrdreReparation by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object TypeOrdreReparation whose updatedBy is equals to the given updatedBy. If
	 *         no TypeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from TypeOrdreReparation e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<TypeOrdreReparation> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TypeOrdreReparation by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object TypeOrdreReparation whose deletedBy is equals to the given deletedBy. If
	 *         no TypeOrdreReparation is found, this method returns null.
	 */
	@Query("select e from TypeOrdreReparation e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<TypeOrdreReparation> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of TypeOrdreReparation by using typeOrdreReparationDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of TypeOrdreReparation
	 * @throws DataAccessException,ParseException
	 */
	public default List<TypeOrdreReparation> getByCriteria(Request<TypeOrdreReparationDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from TypeOrdreReparation e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<TypeOrdreReparation> query = em.createQuery(req, TypeOrdreReparation.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of TypeOrdreReparation by using typeOrdreReparationDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of TypeOrdreReparation
	 *
	 */
	public default Long count(Request<TypeOrdreReparationDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from TypeOrdreReparation e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<TypeOrdreReparationDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		TypeOrdreReparationDto dto = request.getData() != null ? request.getData() : new TypeOrdreReparationDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (TypeOrdreReparationDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(TypeOrdreReparationDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
