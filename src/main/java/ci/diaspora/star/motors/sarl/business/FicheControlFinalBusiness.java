


                                                                                                                                                  /*
 * Java transformer for entity table fiche_control_final
 * Created on 2019-08-18 ( Time 17:15:10 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "fiche_control_final"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class FicheControlFinalBusiness implements IBasicBusiness<Request<FicheControlFinalDto>, Response<FicheControlFinalDto>> {

  private Response<FicheControlFinalDto> response;
  @Autowired
  private FicheControlFinalRepository ficheControlFinalRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private UserRepository userControleurFinalRepository;
  @Autowired
  private DesignationElementFicheControleFicheControlFinalRepository designationElementFicheControleFicheControlFinalRepository;
  @Autowired
  private EtatRepository etatRepository;
  @Autowired
  private UserRepository userReceptionRepository;
  @Autowired
  private OrdreReparationRepository ordreReparationRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                        
  @Autowired
  private DesignationElementFicheControleFicheControlFinalBusiness designationElementFicheControleFicheControlFinalBusiness;

                    

  public FicheControlFinalBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create FicheControlFinal by using FicheControlFinalDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FicheControlFinalDto> create(Request<FicheControlFinalDto> request, Locale locale)  {
    slf4jLogger.info("----begin create FicheControlFinal-----");

    response = new Response<FicheControlFinalDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FicheControlFinal> items = new ArrayList<FicheControlFinal>();

      for (FicheControlFinalDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("ordreReparationId", dto.getOrdreReparationId());
        fieldsToVerify.put("userControleurFinalId", dto.getUserControleurFinalId());
        fieldsToVerify.put("userReceptionId", dto.getUserReceptionId());
        fieldsToVerify.put("userFacturationId", dto.getUserFacturationId());
        fieldsToVerify.put("isValidedByControleurFinal", dto.getIsValidedByControleurFinal());
        fieldsToVerify.put("isValidedByReception", dto.getIsValidedByReception());
        fieldsToVerify.put("isValidedByFacturation", dto.getIsValidedByFacturation());
        fieldsToVerify.put("observationsControleurFinal", dto.getObservationsControleurFinal());
        fieldsToVerify.put("observationsReception", dto.getObservationsReception());
        fieldsToVerify.put("observationsFacturation", dto.getObservationsFacturation());
        fieldsToVerify.put("dateValidationControleurFinal", dto.getDateValidationControleurFinal());
        fieldsToVerify.put("dateValidationReception", dto.getDateValidationReception());
        fieldsToVerify.put("dateValidationFacturation", dto.getDateValidationFacturation());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        fieldsToVerify.put("etatId", dto.getEtatId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if ficheControlFinal to insert do not exist
        FicheControlFinal existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("ficheControlFinal -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if userControleurFinal exist
        User existingUserControleurFinal = userControleurFinalRepository.findById(dto.getUserControleurFinalId(), false);
        if (existingUserControleurFinal == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("userControleurFinal -> " + dto.getUserControleurFinalId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if etat exist
        Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
        if (existingEtat == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if user exist
        User existingUser = userRepository.findById(dto.getUserFacturationId(), false);
        if (existingUser == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserFacturationId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if userReception exist
        User existingUserReception = userReceptionRepository.findById(dto.getUserReceptionId(), false);
        if (existingUserReception == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("userReception -> " + dto.getUserReceptionId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if ordreReparation exist
        OrdreReparation existingOrdreReparation = ordreReparationRepository.findById(dto.getOrdreReparationId(), false);
        if (existingOrdreReparation == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getOrdreReparationId(), locale));
          response.setHasError(true);
          return response;
        }
        FicheControlFinal entityToSave = null;
        entityToSave = FicheControlFinalTransformer.INSTANCE.toEntity(dto, existingUserControleurFinal, existingEtat, existingUser, existingUserReception, existingOrdreReparation);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<FicheControlFinal> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = ficheControlFinalRepository.save((Iterable<FicheControlFinal>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("ficheControlFinal", locale));
          response.setHasError(true);
          return response;
        }
        List<FicheControlFinalDto> itemsDto = new ArrayList<FicheControlFinalDto>();
        for (FicheControlFinal entity : itemsSaved) {
          FicheControlFinalDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create FicheControlFinal-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update FicheControlFinal by using FicheControlFinalDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FicheControlFinalDto> update(Request<FicheControlFinalDto> request, Locale locale)  {
    slf4jLogger.info("----begin update FicheControlFinal-----");

    response = new Response<FicheControlFinalDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FicheControlFinal> items = new ArrayList<FicheControlFinal>();

      for (FicheControlFinalDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la ficheControlFinal existe
        FicheControlFinal entityToSave = null;
        entityToSave = ficheControlFinalRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ficheControlFinal -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if userControleurFinal exist
        if (dto.getUserControleurFinalId() != null && dto.getUserControleurFinalId() > 0){
          User existingUserControleurFinal = userControleurFinalRepository.findById(dto.getUserControleurFinalId(), false);
          if (existingUserControleurFinal == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("userControleurFinal -> " + dto.getUserControleurFinalId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserControleurFinal(existingUserControleurFinal);
        }
        // Verify if etat exist
        if (dto.getEtatId() != null && dto.getEtatId() > 0){
          Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
          if (existingEtat == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setEtat(existingEtat);
        }
        // Verify if user exist
        if (dto.getUserFacturationId() != null && dto.getUserFacturationId() > 0){
          User existingUserFacturation = userRepository.findById(dto.getUserFacturationId(), false);
          if (existingUserFacturation == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserFacturationId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserFacturation(existingUserFacturation);
        }
        // Verify if userReception exist
        if (dto.getUserReceptionId() != null && dto.getUserReceptionId() > 0){
          User existingUserReception = userReceptionRepository.findById(dto.getUserReceptionId(), false);
          if (existingUserReception == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("userReception -> " + dto.getUserReceptionId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserReception(existingUserReception);
        }
        // Verify if ordreReparation exist
        if (dto.getOrdreReparationId() != null && dto.getOrdreReparationId() > 0){
          OrdreReparation existingOrdreReparation = ordreReparationRepository.findById(dto.getOrdreReparationId(), false);
          if (existingOrdreReparation == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getOrdreReparationId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setOrdreReparation(existingOrdreReparation);
        }
        if (dto.getIsValidedByControleurFinal() != null) {
          entityToSave.setIsValidedByControleurFinal(dto.getIsValidedByControleurFinal());
        }
        if (dto.getIsValidedByReception() != null) {
          entityToSave.setIsValidedByReception(dto.getIsValidedByReception());
        }
        if (dto.getIsValidedByFacturation() != null) {
          entityToSave.setIsValidedByFacturation(dto.getIsValidedByFacturation());
        }
        if (Utilities.notBlank(dto.getObservationsControleurFinal())) {
                  entityToSave.setObservationsControleurFinal(dto.getObservationsControleurFinal());
        }
        if (Utilities.notBlank(dto.getObservationsReception())) {
                  entityToSave.setObservationsReception(dto.getObservationsReception());
        }
        if (Utilities.notBlank(dto.getObservationsFacturation())) {
                  entityToSave.setObservationsFacturation(dto.getObservationsFacturation());
        }
        if (Utilities.notBlank(dto.getDateValidationControleurFinal())) {
          entityToSave.setDateValidationControleurFinal(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateValidationControleurFinal()));
        }
        if (Utilities.notBlank(dto.getDateValidationReception())) {
          entityToSave.setDateValidationReception(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateValidationReception()));
        }
        if (Utilities.notBlank(dto.getDateValidationFacturation())) {
          entityToSave.setDateValidationFacturation(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateValidationFacturation()));
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<FicheControlFinal> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = ficheControlFinalRepository.save((Iterable<FicheControlFinal>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("ficheControlFinal", locale));
          response.setHasError(true);
          return response;
        }
        List<FicheControlFinalDto> itemsDto = new ArrayList<FicheControlFinalDto>();
        for (FicheControlFinal entity : itemsSaved) {
          FicheControlFinalDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update FicheControlFinal-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete FicheControlFinal by using FicheControlFinalDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FicheControlFinalDto> delete(Request<FicheControlFinalDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete FicheControlFinal-----");

    response = new Response<FicheControlFinalDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FicheControlFinal> items = new ArrayList<FicheControlFinal>();

      for (FicheControlFinalDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la ficheControlFinal existe
        FicheControlFinal existingEntity = null;
        existingEntity = ficheControlFinalRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ficheControlFinal -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

        // designationElementFicheControleFicheControlFinal
        List<DesignationElementFicheControleFicheControlFinal> listOfDesignationElementFicheControleFicheControlFinal = designationElementFicheControleFicheControlFinalRepository.findByFicheControlFinalId(existingEntity.getId(), false);
        if (listOfDesignationElementFicheControleFicheControlFinal != null && !listOfDesignationElementFicheControleFicheControlFinal.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDesignationElementFicheControleFicheControlFinal.size() + ")", locale));
          response.setHasError(true);
          return response;
        }


        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        ficheControlFinalRepository.save((Iterable<FicheControlFinal>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete FicheControlFinal-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete FicheControlFinal by using FicheControlFinalDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<FicheControlFinalDto> forceDelete(Request<FicheControlFinalDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete FicheControlFinal-----");

    response = new Response<FicheControlFinalDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FicheControlFinal> items = new ArrayList<FicheControlFinal>();

      for (FicheControlFinalDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la ficheControlFinal existe
        FicheControlFinal existingEntity = null;
          existingEntity = ficheControlFinalRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ficheControlFinal -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                                // designationElementFicheControleFicheControlFinal
        List<DesignationElementFicheControleFicheControlFinal> listOfDesignationElementFicheControleFicheControlFinal = designationElementFicheControleFicheControlFinalRepository.findByFicheControlFinalId(existingEntity.getId(), false);
        if (listOfDesignationElementFicheControleFicheControlFinal != null && !listOfDesignationElementFicheControleFicheControlFinal.isEmpty()){
          Request<DesignationElementFicheControleFicheControlFinalDto> deleteRequest = new Request<DesignationElementFicheControleFicheControlFinalDto>();
          deleteRequest.setDatas(DesignationElementFicheControleFicheControlFinalTransformer.INSTANCE.toDtos(listOfDesignationElementFicheControleFicheControlFinal));
          deleteRequest.setUser(request.getUser());
          Response<DesignationElementFicheControleFicheControlFinalDto> deleteResponse = designationElementFicheControleFicheControlFinalBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
                    

                                                                                    existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          ficheControlFinalRepository.save((Iterable<FicheControlFinal>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete FicheControlFinal-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get FicheControlFinal by using FicheControlFinalDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<FicheControlFinalDto> getByCriteria(Request<FicheControlFinalDto> request, Locale locale) {
    slf4jLogger.info("----begin get FicheControlFinal-----");

    response = new Response<FicheControlFinalDto>();

    try {
      List<FicheControlFinal> items = null;
      items = ficheControlFinalRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<FicheControlFinalDto> itemsDto = new ArrayList<FicheControlFinalDto>();
        for (FicheControlFinal entity : items) {
          FicheControlFinalDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(ficheControlFinalRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("ficheControlFinal", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get FicheControlFinal-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full FicheControlFinalDto by using FicheControlFinal as object.
   *
   * @param entity, locale
   * @return FicheControlFinalDto
   *
   */
  private FicheControlFinalDto getFullInfos(FicheControlFinal entity, Integer size, Locale locale) throws Exception {
    FicheControlFinalDto dto = FicheControlFinalTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
