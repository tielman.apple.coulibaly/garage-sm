package ci.diaspora.star.motors.sarl.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.dao.entity.FicheReception;
import ci.diaspora.star.motors.sarl.helper.dto.FicheReceptionDto;

/**
 * Repository customize : FicheReception.
 */
@Repository
public interface _FicheReceptionRepository {
	default List<String> _generateCriteria(FicheReceptionDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds FicheReception by using numeroFiche as a search criteria.
	 *
	 * @param numeroFiche
	 * @return An Object FicheReception whose numeroFiche is equals to the given numeroFiche. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.numeroFiche = :numeroFiche and e.isDeleted = :isDeleted")
	FicheReception findFicheReceptionByNumeroFiche(@Param("numeroFiche")String numeroFiche, @Param("isDeleted")Boolean isDeleted);
	
}
