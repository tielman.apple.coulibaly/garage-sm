
/*
 * Java dto for entity table panne_declaree_ordre_reparation
 * Created on 2019-08-18 ( Time 17:15:13 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._PanneDeclareeOrdreReparationDto;

/**
 * DTO for table "panne_declaree_ordre_reparation"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class PanneDeclareeOrdreReparationDto extends _PanneDeclareeOrdreReparationDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    panneDeclareeId      ;
	/*
	 * 
	 */
    private Integer    ordreReparationId    ;
	/*
	 * 
	 */
    private String     observations         ;
	/*
	 * 
	 */
    private String     diagnosticAtelier    ;
	/*
	 * 
	 */
    private Boolean    isDeclaredByClient   ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String panneDeclareeLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  panneDeclareeIdParam  ;                     
	private SearchParam<Integer>  ordreReparationIdParam;                     
	private SearchParam<String>   observationsParam     ;                     
	private SearchParam<String>   diagnosticAtelierParam;                     
	private SearchParam<Boolean>  isDeclaredByClientParam;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<String>   panneDeclareeLibelleParam;                     
    /**
     * Default constructor
     */
    public PanneDeclareeOrdreReparationDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "panneDeclareeId" field value
     * @param panneDeclareeId
     */
	public void setPanneDeclareeId( Integer panneDeclareeId )
    {
        this.panneDeclareeId = panneDeclareeId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getPanneDeclareeId()
    {
        return this.panneDeclareeId;
    }

    /**
     * Set the "ordreReparationId" field value
     * @param ordreReparationId
     */
	public void setOrdreReparationId( Integer ordreReparationId )
    {
        this.ordreReparationId = ordreReparationId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getOrdreReparationId()
    {
        return this.ordreReparationId;
    }

    /**
     * Set the "observations" field value
     * @param observations
     */
	public void setObservations( String observations )
    {
        this.observations = observations ;
    }
    /**
     * 
     * @return the field value
     */
	public String getObservations()
    {
        return this.observations;
    }

    /**
     * Set the "diagnosticAtelier" field value
     * @param diagnosticAtelier
     */
	public void setDiagnosticAtelier( String diagnosticAtelier )
    {
        this.diagnosticAtelier = diagnosticAtelier ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDiagnosticAtelier()
    {
        return this.diagnosticAtelier;
    }

    /**
     * Set the "isDeclaredByClient" field value
     * @param isDeclaredByClient
     */
	public void setIsDeclaredByClient( Boolean isDeclaredByClient )
    {
        this.isDeclaredByClient = isDeclaredByClient ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeclaredByClient()
    {
        return this.isDeclaredByClient;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getPanneDeclareeLibelle()
    {
        return this.panneDeclareeLibelle;
    }
	public void setPanneDeclareeLibelle(String panneDeclareeLibelle)
    {
        this.panneDeclareeLibelle = panneDeclareeLibelle;
    }


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "panneDeclareeIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getPanneDeclareeIdParam(){
		return this.panneDeclareeIdParam;
	}
	/**
     * Set the "panneDeclareeIdParam" field value
     * @param panneDeclareeIdParam
     */
    public void setPanneDeclareeIdParam( SearchParam<Integer> panneDeclareeIdParam ){
        this.panneDeclareeIdParam = panneDeclareeIdParam;
    }

	/**
     * Get the "ordreReparationIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getOrdreReparationIdParam(){
		return this.ordreReparationIdParam;
	}
	/**
     * Set the "ordreReparationIdParam" field value
     * @param ordreReparationIdParam
     */
    public void setOrdreReparationIdParam( SearchParam<Integer> ordreReparationIdParam ){
        this.ordreReparationIdParam = ordreReparationIdParam;
    }

	/**
     * Get the "observationsParam" field value
     * @return the field value
     */
	public SearchParam<String> getObservationsParam(){
		return this.observationsParam;
	}
	/**
     * Set the "observationsParam" field value
     * @param observationsParam
     */
    public void setObservationsParam( SearchParam<String> observationsParam ){
        this.observationsParam = observationsParam;
    }

	/**
     * Get the "diagnosticAtelierParam" field value
     * @return the field value
     */
	public SearchParam<String> getDiagnosticAtelierParam(){
		return this.diagnosticAtelierParam;
	}
	/**
     * Set the "diagnosticAtelierParam" field value
     * @param diagnosticAtelierParam
     */
    public void setDiagnosticAtelierParam( SearchParam<String> diagnosticAtelierParam ){
        this.diagnosticAtelierParam = diagnosticAtelierParam;
    }

	/**
     * Get the "isDeclaredByClientParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeclaredByClientParam(){
		return this.isDeclaredByClientParam;
	}
	/**
     * Set the "isDeclaredByClientParam" field value
     * @param isDeclaredByClientParam
     */
    public void setIsDeclaredByClientParam( SearchParam<Boolean> isDeclaredByClientParam ){
        this.isDeclaredByClientParam = isDeclaredByClientParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

		/**
     * Get the "panneDeclareeLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getPanneDeclareeLibelleParam(){
		return this.panneDeclareeLibelleParam;
	}
	/**
     * Set the "panneDeclareeLibelleParam" field value
     * @param panneDeclareeLibelleParam
     */
    public void setPanneDeclareeLibelleParam( SearchParam<String> panneDeclareeLibelleParam ){
        this.panneDeclareeLibelleParam = panneDeclareeLibelleParam;
    }


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		PanneDeclareeOrdreReparationDto other = (PanneDeclareeOrdreReparationDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("observations:"+observations);
		sb.append("|");
		sb.append("diagnosticAtelier:"+diagnosticAtelier);
		sb.append("|");
		sb.append("isDeclaredByClient:"+isDeclaredByClient);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
        return sb.toString();
    }
}
