


                                                                          /*
 * Java transformer for entity table designation_element_fiche_controle_type_element_fiche_control
 * Created on 2019-08-18 ( Time 17:15:07 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "designation_element_fiche_controle_type_element_fiche_control"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class DesignationElementFicheControleTypeElementFicheControlBusiness implements IBasicBusiness<Request<DesignationElementFicheControleTypeElementFicheControlDto>, Response<DesignationElementFicheControleTypeElementFicheControlDto>> {

  private Response<DesignationElementFicheControleTypeElementFicheControlDto> response;
  @Autowired
  private DesignationElementFicheControleTypeElementFicheControlRepository designationElementFicheControleTypeElementFicheControlRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private TypeElementFicheControlRepository typeElementFicheControlRepository;
  @Autowired
  private DesignationElementFicheControleRepository designationElementFicheControleRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

          

  public DesignationElementFicheControleTypeElementFicheControlBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create DesignationElementFicheControleTypeElementFicheControl by using DesignationElementFicheControleTypeElementFicheControlDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<DesignationElementFicheControleTypeElementFicheControlDto> create(Request<DesignationElementFicheControleTypeElementFicheControlDto> request, Locale locale)  {
    slf4jLogger.info("----begin create DesignationElementFicheControleTypeElementFicheControl-----");

    response = new Response<DesignationElementFicheControleTypeElementFicheControlDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<DesignationElementFicheControleTypeElementFicheControl> items = new ArrayList<DesignationElementFicheControleTypeElementFicheControl>();

      for (DesignationElementFicheControleTypeElementFicheControlDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("designationNiveauControlId", dto.getDesignationNiveauControlId());
        fieldsToVerify.put("typeNiveauControlId", dto.getTypeNiveauControlId());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if designationElementFicheControleTypeElementFicheControl to insert do not exist
        DesignationElementFicheControleTypeElementFicheControl existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("designationElementFicheControleTypeElementFicheControl -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if typeElementFicheControl exist
        TypeElementFicheControl existingTypeElementFicheControl = typeElementFicheControlRepository.findById(dto.getTypeNiveauControlId(), false);
        if (existingTypeElementFicheControl == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("typeElementFicheControl -> " + dto.getTypeNiveauControlId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if designationElementFicheControle exist
        DesignationElementFicheControle existingDesignationElementFicheControle = designationElementFicheControleRepository.findById(dto.getDesignationNiveauControlId(), false);
        if (existingDesignationElementFicheControle == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControle -> " + dto.getDesignationNiveauControlId(), locale));
          response.setHasError(true);
          return response;
        }
        DesignationElementFicheControleTypeElementFicheControl entityToSave = null;
        entityToSave = DesignationElementFicheControleTypeElementFicheControlTransformer.INSTANCE.toEntity(dto, existingTypeElementFicheControl, existingDesignationElementFicheControle);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<DesignationElementFicheControleTypeElementFicheControl> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = designationElementFicheControleTypeElementFicheControlRepository.save((Iterable<DesignationElementFicheControleTypeElementFicheControl>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("designationElementFicheControleTypeElementFicheControl", locale));
          response.setHasError(true);
          return response;
        }
        List<DesignationElementFicheControleTypeElementFicheControlDto> itemsDto = new ArrayList<DesignationElementFicheControleTypeElementFicheControlDto>();
        for (DesignationElementFicheControleTypeElementFicheControl entity : itemsSaved) {
          DesignationElementFicheControleTypeElementFicheControlDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create DesignationElementFicheControleTypeElementFicheControl-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update DesignationElementFicheControleTypeElementFicheControl by using DesignationElementFicheControleTypeElementFicheControlDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<DesignationElementFicheControleTypeElementFicheControlDto> update(Request<DesignationElementFicheControleTypeElementFicheControlDto> request, Locale locale)  {
    slf4jLogger.info("----begin update DesignationElementFicheControleTypeElementFicheControl-----");

    response = new Response<DesignationElementFicheControleTypeElementFicheControlDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<DesignationElementFicheControleTypeElementFicheControl> items = new ArrayList<DesignationElementFicheControleTypeElementFicheControl>();

      for (DesignationElementFicheControleTypeElementFicheControlDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la designationElementFicheControleTypeElementFicheControl existe
        DesignationElementFicheControleTypeElementFicheControl entityToSave = null;
        entityToSave = designationElementFicheControleTypeElementFicheControlRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControleTypeElementFicheControl -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if typeElementFicheControl exist
        if (dto.getTypeNiveauControlId() != null && dto.getTypeNiveauControlId() > 0){
          TypeElementFicheControl existingTypeElementFicheControl = typeElementFicheControlRepository.findById(dto.getTypeNiveauControlId(), false);
          if (existingTypeElementFicheControl == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("typeElementFicheControl -> " + dto.getTypeNiveauControlId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setTypeElementFicheControl(existingTypeElementFicheControl);
        }
        // Verify if designationElementFicheControle exist
        if (dto.getDesignationNiveauControlId() != null && dto.getDesignationNiveauControlId() > 0){
          DesignationElementFicheControle existingDesignationElementFicheControle = designationElementFicheControleRepository.findById(dto.getDesignationNiveauControlId(), false);
          if (existingDesignationElementFicheControle == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControle -> " + dto.getDesignationNiveauControlId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setDesignationElementFicheControle(existingDesignationElementFicheControle);
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<DesignationElementFicheControleTypeElementFicheControl> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = designationElementFicheControleTypeElementFicheControlRepository.save((Iterable<DesignationElementFicheControleTypeElementFicheControl>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("designationElementFicheControleTypeElementFicheControl", locale));
          response.setHasError(true);
          return response;
        }
        List<DesignationElementFicheControleTypeElementFicheControlDto> itemsDto = new ArrayList<DesignationElementFicheControleTypeElementFicheControlDto>();
        for (DesignationElementFicheControleTypeElementFicheControl entity : itemsSaved) {
          DesignationElementFicheControleTypeElementFicheControlDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update DesignationElementFicheControleTypeElementFicheControl-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete DesignationElementFicheControleTypeElementFicheControl by using DesignationElementFicheControleTypeElementFicheControlDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<DesignationElementFicheControleTypeElementFicheControlDto> delete(Request<DesignationElementFicheControleTypeElementFicheControlDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete DesignationElementFicheControleTypeElementFicheControl-----");

    response = new Response<DesignationElementFicheControleTypeElementFicheControlDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<DesignationElementFicheControleTypeElementFicheControl> items = new ArrayList<DesignationElementFicheControleTypeElementFicheControl>();

      for (DesignationElementFicheControleTypeElementFicheControlDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la designationElementFicheControleTypeElementFicheControl existe
        DesignationElementFicheControleTypeElementFicheControl existingEntity = null;
        existingEntity = designationElementFicheControleTypeElementFicheControlRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControleTypeElementFicheControl -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        designationElementFicheControleTypeElementFicheControlRepository.save((Iterable<DesignationElementFicheControleTypeElementFicheControl>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete DesignationElementFicheControleTypeElementFicheControl-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete DesignationElementFicheControleTypeElementFicheControl by using DesignationElementFicheControleTypeElementFicheControlDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<DesignationElementFicheControleTypeElementFicheControlDto> forceDelete(Request<DesignationElementFicheControleTypeElementFicheControlDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete DesignationElementFicheControleTypeElementFicheControl-----");

    response = new Response<DesignationElementFicheControleTypeElementFicheControlDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<DesignationElementFicheControleTypeElementFicheControl> items = new ArrayList<DesignationElementFicheControleTypeElementFicheControl>();

      for (DesignationElementFicheControleTypeElementFicheControlDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la designationElementFicheControleTypeElementFicheControl existe
        DesignationElementFicheControleTypeElementFicheControl existingEntity = null;
          existingEntity = designationElementFicheControleTypeElementFicheControlRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControleTypeElementFicheControl -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

          

            existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          designationElementFicheControleTypeElementFicheControlRepository.save((Iterable<DesignationElementFicheControleTypeElementFicheControl>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete DesignationElementFicheControleTypeElementFicheControl-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get DesignationElementFicheControleTypeElementFicheControl by using DesignationElementFicheControleTypeElementFicheControlDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<DesignationElementFicheControleTypeElementFicheControlDto> getByCriteria(Request<DesignationElementFicheControleTypeElementFicheControlDto> request, Locale locale) {
    slf4jLogger.info("----begin get DesignationElementFicheControleTypeElementFicheControl-----");

    response = new Response<DesignationElementFicheControleTypeElementFicheControlDto>();

    try {
      List<DesignationElementFicheControleTypeElementFicheControl> items = null;
      items = designationElementFicheControleTypeElementFicheControlRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<DesignationElementFicheControleTypeElementFicheControlDto> itemsDto = new ArrayList<DesignationElementFicheControleTypeElementFicheControlDto>();
        for (DesignationElementFicheControleTypeElementFicheControl entity : items) {
          DesignationElementFicheControleTypeElementFicheControlDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(designationElementFicheControleTypeElementFicheControlRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("designationElementFicheControleTypeElementFicheControl", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get DesignationElementFicheControleTypeElementFicheControl-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full DesignationElementFicheControleTypeElementFicheControlDto by using DesignationElementFicheControleTypeElementFicheControl as object.
   *
   * @param entity, locale
   * @return DesignationElementFicheControleTypeElementFicheControlDto
   *
   */
  private DesignationElementFicheControleTypeElementFicheControlDto getFullInfos(DesignationElementFicheControleTypeElementFicheControl entity, Integer size, Locale locale) throws Exception {
    DesignationElementFicheControleTypeElementFicheControlDto dto = DesignationElementFicheControleTypeElementFicheControlTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
