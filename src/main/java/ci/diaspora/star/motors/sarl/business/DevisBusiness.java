


                                                                                                              /*
 * Java transformer for entity table devis
 * Created on 2019-08-18 ( Time 17:15:08 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "devis"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class DevisBusiness implements IBasicBusiness<Request<DevisDto>, Response<DevisDto>> {

  private Response<DevisDto> response;
  @Autowired
  private DevisRepository devisRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private BonDecaissementRepository bonDecaissementRepository;
  @Autowired
  private UserRepository userDirectionRepository;
  @Autowired
  private EtatRepository etatRepository;
  @Autowired
  private UserRepository userResponsableTechniqueRepository;
  @Autowired
  private FichePreparationDevisRepository fichePreparationDevisRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                                            
  @Autowired
  private BonDecaissementBusiness bonDecaissementBusiness;

                        

  public DevisBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create Devis by using DevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<DevisDto> create(Request<DevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin create Devis-----");

    response = new Response<DevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Devis> items = new ArrayList<Devis>();

      for (DevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("fichePreparationId", dto.getFichePreparationId());
        fieldsToVerify.put("userChefAtelierId", dto.getUserChefAtelierId());
        fieldsToVerify.put("userResponsableTechniqueId", dto.getUserResponsableTechniqueId());
        fieldsToVerify.put("userDirectionId", dto.getUserDirectionId());
        fieldsToVerify.put("isValidedByChefAtelier", dto.getIsValidedByChefAtelier());
        fieldsToVerify.put("isValidedByResponsableTechnique", dto.getIsValidedByResponsableTechnique());
        fieldsToVerify.put("isValidedByDirection", dto.getIsValidedByDirection());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        fieldsToVerify.put("etatId", dto.getEtatId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if devis to insert do not exist
        Devis existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("devis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if userDirection exist
        User existingUserDirection = userDirectionRepository.findById(dto.getUserDirectionId(), false);
        if (existingUserDirection == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("userDirection -> " + dto.getUserDirectionId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if etat exist
        Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
        if (existingEtat == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if user exist
        User existingUser = userRepository.findById(dto.getUserChefAtelierId(), false);
        if (existingUser == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserChefAtelierId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if userResponsableTechnique exist
        User existingUserResponsableTechnique = userResponsableTechniqueRepository.findById(dto.getUserResponsableTechniqueId(), false);
        if (existingUserResponsableTechnique == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("userResponsableTechnique -> " + dto.getUserResponsableTechniqueId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if fichePreparationDevis exist
        FichePreparationDevis existingFichePreparationDevis = fichePreparationDevisRepository.findById(dto.getFichePreparationId(), false);
        if (existingFichePreparationDevis == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("fichePreparationDevis -> " + dto.getFichePreparationId(), locale));
          response.setHasError(true);
          return response;
        }
        Devis entityToSave = null;
        entityToSave = DevisTransformer.INSTANCE.toEntity(dto, existingUserDirection, existingEtat, existingUser, existingUserResponsableTechnique, existingFichePreparationDevis);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<Devis> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = devisRepository.save((Iterable<Devis>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("devis", locale));
          response.setHasError(true);
          return response;
        }
        List<DevisDto> itemsDto = new ArrayList<DevisDto>();
        for (Devis entity : itemsSaved) {
          DevisDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create Devis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update Devis by using DevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<DevisDto> update(Request<DevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin update Devis-----");

    response = new Response<DevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Devis> items = new ArrayList<Devis>();

      for (DevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la devis existe
        Devis entityToSave = null;
        entityToSave = devisRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("devis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if userDirection exist
        if (dto.getUserDirectionId() != null && dto.getUserDirectionId() > 0){
          User existingUserDirection = userDirectionRepository.findById(dto.getUserDirectionId(), false);
          if (existingUserDirection == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("userDirection -> " + dto.getUserDirectionId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserDirection(existingUserDirection);
        }
        // Verify if etat exist
        if (dto.getEtatId() != null && dto.getEtatId() > 0){
          Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
          if (existingEtat == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setEtat(existingEtat);
        }
        // Verify if user exist
        if (dto.getUserChefAtelierId() != null && dto.getUserChefAtelierId() > 0){
          User existingUserChefAtelier = userRepository.findById(dto.getUserChefAtelierId(), false);
          if (existingUserChefAtelier == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserChefAtelierId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserChefAtelier(existingUserChefAtelier);
        }
        // Verify if userResponsableTechnique exist
        if (dto.getUserResponsableTechniqueId() != null && dto.getUserResponsableTechniqueId() > 0){
          User existingUserResponsableTechnique = userResponsableTechniqueRepository.findById(dto.getUserResponsableTechniqueId(), false);
          if (existingUserResponsableTechnique == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("userResponsableTechnique -> " + dto.getUserResponsableTechniqueId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserResponsableTechnique(existingUserResponsableTechnique);
        }
        // Verify if fichePreparationDevis exist
        if (dto.getFichePreparationId() != null && dto.getFichePreparationId() > 0){
          FichePreparationDevis existingFichePreparationDevis = fichePreparationDevisRepository.findById(dto.getFichePreparationId(), false);
          if (existingFichePreparationDevis == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("fichePreparationDevis -> " + dto.getFichePreparationId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setFichePreparationDevis(existingFichePreparationDevis);
        }
        if (dto.getIsValidedByChefAtelier() != null) {
          entityToSave.setIsValidedByChefAtelier(dto.getIsValidedByChefAtelier());
        }
        if (dto.getIsValidedByResponsableTechnique() != null) {
          entityToSave.setIsValidedByResponsableTechnique(dto.getIsValidedByResponsableTechnique());
        }
        if (dto.getIsValidedByDirection() != null) {
          entityToSave.setIsValidedByDirection(dto.getIsValidedByDirection());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<Devis> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = devisRepository.save((Iterable<Devis>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("devis", locale));
          response.setHasError(true);
          return response;
        }
        List<DevisDto> itemsDto = new ArrayList<DevisDto>();
        for (Devis entity : itemsSaved) {
          DevisDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update Devis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete Devis by using DevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<DevisDto> delete(Request<DevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete Devis-----");

    response = new Response<DevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Devis> items = new ArrayList<Devis>();

      for (DevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la devis existe
        Devis existingEntity = null;
        existingEntity = devisRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("devis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

        // bonDecaissement
        List<BonDecaissement> listOfBonDecaissement = bonDecaissementRepository.findByDevisId(existingEntity.getId(), false);
        if (listOfBonDecaissement != null && !listOfBonDecaissement.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfBonDecaissement.size() + ")", locale));
          response.setHasError(true);
          return response;
        }


        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        devisRepository.save((Iterable<Devis>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete Devis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete Devis by using DevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<DevisDto> forceDelete(Request<DevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete Devis-----");

    response = new Response<DevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Devis> items = new ArrayList<Devis>();

      for (DevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la devis existe
        Devis existingEntity = null;
          existingEntity = devisRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("devis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                                                    // bonDecaissement
        List<BonDecaissement> listOfBonDecaissement = bonDecaissementRepository.findByDevisId(existingEntity.getId(), false);
        if (listOfBonDecaissement != null && !listOfBonDecaissement.isEmpty()){
          Request<BonDecaissementDto> deleteRequest = new Request<BonDecaissementDto>();
          deleteRequest.setDatas(BonDecaissementTransformer.INSTANCE.toDtos(listOfBonDecaissement));
          deleteRequest.setUser(request.getUser());
          Response<BonDecaissementDto> deleteResponse = bonDecaissementBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
                        

                                    existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          devisRepository.save((Iterable<Devis>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete Devis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get Devis by using DevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<DevisDto> getByCriteria(Request<DevisDto> request, Locale locale) {
    slf4jLogger.info("----begin get Devis-----");

    response = new Response<DevisDto>();

    try {
      List<Devis> items = null;
      items = devisRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<DevisDto> itemsDto = new ArrayList<DevisDto>();
        for (Devis entity : items) {
          DevisDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(devisRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("devis", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get Devis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full DevisDto by using Devis as object.
   *
   * @param entity, locale
   * @return DevisDto
   *
   */
  private DevisDto getFullInfos(Devis entity, Integer size, Locale locale) throws Exception {
    DevisDto dto = DevisTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
