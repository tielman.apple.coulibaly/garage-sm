package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._BonDecaissementRepository;

/**
 * Repository : BonDecaissement.
 */
@Repository
public interface BonDecaissementRepository extends JpaRepository<BonDecaissement, Integer>, _BonDecaissementRepository {
	/**
	 * Finds BonDecaissement by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object BonDecaissement whose id is equals to the given id. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.id = :id and e.isDeleted = :isDeleted")
	BonDecaissement findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds BonDecaissement by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object BonDecaissement whose isDeleted is equals to the given isDeleted. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.isDeleted = :isDeleted")
	List<BonDecaissement> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonDecaissement by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object BonDecaissement whose createdAt is equals to the given createdAt. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonDecaissement by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object BonDecaissement whose updatedAt is equals to the given updatedAt. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonDecaissement by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object BonDecaissement whose deletedAt is equals to the given deletedAt. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonDecaissement by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object BonDecaissement whose createdBy is equals to the given createdBy. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonDecaissement by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object BonDecaissement whose updatedBy is equals to the given updatedBy. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonDecaissement by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object BonDecaissement whose deletedBy is equals to the given deletedBy. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonDecaissement by using isValidedByChefAtelier as a search criteria.
	 *
	 * @param isValidedByChefAtelier
	 * @return An Object BonDecaissement whose isValidedByChefAtelier is equals to the given isValidedByChefAtelier. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.isValidedByChefAtelier = :isValidedByChefAtelier and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByIsValidedByChefAtelier(@Param("isValidedByChefAtelier")Boolean isValidedByChefAtelier, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonDecaissement by using isValidedByResponsableTechnique as a search criteria.
	 *
	 * @param isValidedByResponsableTechnique
	 * @return An Object BonDecaissement whose isValidedByResponsableTechnique is equals to the given isValidedByResponsableTechnique. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.isValidedByResponsableTechnique = :isValidedByResponsableTechnique and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByIsValidedByResponsableTechnique(@Param("isValidedByResponsableTechnique")Boolean isValidedByResponsableTechnique, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonDecaissement by using isValidedByResponsableDecaissement as a search criteria.
	 *
	 * @param isValidedByResponsableDecaissement
	 * @return An Object BonDecaissement whose isValidedByResponsableDecaissement is equals to the given isValidedByResponsableDecaissement. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.isValidedByResponsableDecaissement = :isValidedByResponsableDecaissement and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByIsValidedByResponsableDecaissement(@Param("isValidedByResponsableDecaissement")Boolean isValidedByResponsableDecaissement, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonDecaissement by using isValidedByResponsableAchat as a search criteria.
	 *
	 * @param isValidedByResponsableAchat
	 * @return An Object BonDecaissement whose isValidedByResponsableAchat is equals to the given isValidedByResponsableAchat. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.isValidedByResponsableAchat = :isValidedByResponsableAchat and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByIsValidedByResponsableAchat(@Param("isValidedByResponsableAchat")Boolean isValidedByResponsableAchat, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds BonDecaissement by using userResponsableTechniqueId as a search criteria.
	 *
	 * @param userResponsableTechniqueId
	 * @return A list of Object BonDecaissement whose userResponsableTechniqueId is equals to the given userResponsableTechniqueId. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.userResponsableTechnique.id = :userResponsableTechniqueId and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByUserResponsableTechniqueId(@Param("userResponsableTechniqueId")Integer userResponsableTechniqueId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one BonDecaissement by using userResponsableTechniqueId as a search criteria.
   *
   * @param userResponsableTechniqueId
   * @return An Object BonDecaissement whose userResponsableTechniqueId is equals to the given userResponsableTechniqueId. If
   *         no BonDecaissement is found, this method returns null.
   */
  @Query("select e from BonDecaissement e where e.userResponsableTechnique.id = :userResponsableTechniqueId and e.isDeleted = :isDeleted")
  BonDecaissement findBonDecaissementByUserResponsableTechniqueId(@Param("userResponsableTechniqueId")Integer userResponsableTechniqueId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds BonDecaissement by using userChefAtelierId as a search criteria.
	 *
	 * @param userChefAtelierId
	 * @return A list of Object BonDecaissement whose userChefAtelierId is equals to the given userChefAtelierId. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.userChefAtelier. id = :userChefAtelierId and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByUserChefAtelierId(@Param("userChefAtelierId")Integer userChefAtelierId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one BonDecaissement by using userChefAtelierId as a search criteria.
   *
   * @param userChefAtelierId
   * @return An Object BonDecaissement whose userChefAtelierId is equals to the given userChefAtelierId. If
   *         no BonDecaissement is found, this method returns null.
   */
  @Query("select e from BonDecaissement e where e.userChefAtelier. id = :userChefAtelierId and e.isDeleted = :isDeleted")
  BonDecaissement findBonDecaissementByUserChefAtelierId(@Param("userChefAtelierId")Integer userChefAtelierId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds BonDecaissement by using userResponsableDecaissementId as a search criteria.
	 *
	 * @param userResponsableDecaissementId
	 * @return A list of Object BonDecaissement whose userResponsableDecaissementId is equals to the given userResponsableDecaissementId. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.userResponsableDecaissement.id = :userResponsableDecaissementId and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByUserResponsableDecaissementId(@Param("userResponsableDecaissementId")Integer userResponsableDecaissementId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one BonDecaissement by using userResponsableDecaissementId as a search criteria.
   *
   * @param userResponsableDecaissementId
   * @return An Object BonDecaissement whose userResponsableDecaissementId is equals to the given userResponsableDecaissementId. If
   *         no BonDecaissement is found, this method returns null.
   */
  @Query("select e from BonDecaissement e where e.userResponsableDecaissement.id = :userResponsableDecaissementId and e.isDeleted = :isDeleted")
  BonDecaissement findBonDecaissementByUserResponsableDecaissementId(@Param("userResponsableDecaissementId")Integer userResponsableDecaissementId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds BonDecaissement by using devisId as a search criteria.
	 *
	 * @param devisId
	 * @return A list of Object BonDecaissement whose devisId is equals to the given devisId. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.devis.id = :devisId and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByDevisId(@Param("devisId")Integer devisId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one BonDecaissement by using devisId as a search criteria.
   *
   * @param devisId
   * @return An Object BonDecaissement whose devisId is equals to the given devisId. If
   *         no BonDecaissement is found, this method returns null.
   */
  @Query("select e from BonDecaissement e where e.devis.id = :devisId and e.isDeleted = :isDeleted")
  BonDecaissement findBonDecaissementByDevisId(@Param("devisId")Integer devisId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds BonDecaissement by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object BonDecaissement whose etatId is equals to the given etatId. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one BonDecaissement by using etatId as a search criteria.
   *
   * @param etatId
   * @return An Object BonDecaissement whose etatId is equals to the given etatId. If
   *         no BonDecaissement is found, this method returns null.
   */
  @Query("select e from BonDecaissement e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
  BonDecaissement findBonDecaissementByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds BonDecaissement by using userResponsableAchatId as a search criteria.
	 *
	 * @param userResponsableAchatId
	 * @return A list of Object BonDecaissement whose userResponsableAchatId is equals to the given userResponsableAchatId. If
	 *         no BonDecaissement is found, this method returns null.
	 */
	@Query("select e from BonDecaissement e where e.userResponsableAchat.id = :userResponsableAchatId and e.isDeleted = :isDeleted")
	List<BonDecaissement> findByUserResponsableAchatId(@Param("userResponsableAchatId")Integer userResponsableAchatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one BonDecaissement by using userResponsableAchatId as a search criteria.
   *
   * @param userResponsableAchatId
   * @return An Object BonDecaissement whose userResponsableAchatId is equals to the given userResponsableAchatId. If
   *         no BonDecaissement is found, this method returns null.
   */
  @Query("select e from BonDecaissement e where e.userResponsableAchat.id = :userResponsableAchatId and e.isDeleted = :isDeleted")
  BonDecaissement findBonDecaissementByUserResponsableAchatId(@Param("userResponsableAchatId")Integer userResponsableAchatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of BonDecaissement by using bonDecaissementDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of BonDecaissement
	 * @throws DataAccessException,ParseException
	 */
	public default List<BonDecaissement> getByCriteria(Request<BonDecaissementDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from BonDecaissement e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<BonDecaissement> query = em.createQuery(req, BonDecaissement.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of BonDecaissement by using bonDecaissementDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of BonDecaissement
	 *
	 */
	public default Long count(Request<BonDecaissementDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from BonDecaissement e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<BonDecaissementDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		BonDecaissementDto dto = request.getData() != null ? request.getData() : new BonDecaissementDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (BonDecaissementDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(BonDecaissementDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsValidedByChefAtelier()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByChefAtelier", dto.getIsValidedByChefAtelier(), "e.isValidedByChefAtelier", "Boolean", dto.getIsValidedByChefAtelierParam(), param, index, locale));
			}
			if (dto.getIsValidedByResponsableTechnique()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByResponsableTechnique", dto.getIsValidedByResponsableTechnique(), "e.isValidedByResponsableTechnique", "Boolean", dto.getIsValidedByResponsableTechniqueParam(), param, index, locale));
			}
			if (dto.getIsValidedByResponsableDecaissement()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByResponsableDecaissement", dto.getIsValidedByResponsableDecaissement(), "e.isValidedByResponsableDecaissement", "Boolean", dto.getIsValidedByResponsableDecaissementParam(), param, index, locale));
			}
			if (dto.getIsValidedByResponsableAchat()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByResponsableAchat", dto.getIsValidedByResponsableAchat(), "e.isValidedByResponsableAchat", "Boolean", dto.getIsValidedByResponsableAchatParam(), param, index, locale));
			}
			if (dto.getUserResponsableTechniqueId()!= null && dto.getUserResponsableTechniqueId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniqueId", dto.getUserResponsableTechniqueId(), "e.userResponsableTechnique.id", "Integer", dto.getUserResponsableTechniqueIdParam(), param, index, locale));
			}
			if (dto.getUserChefAtelierId()!= null && dto.getUserChefAtelierId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierId", dto.getUserChefAtelierId(), "e.userChefAtelier. id", "Integer", dto.getUserChefAtelierIdParam(), param, index, locale));
			}
			if (dto.getUserResponsableDecaissementId()!= null && dto.getUserResponsableDecaissementId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableDecaissementId", dto.getUserResponsableDecaissementId(), "e.userResponsableDecaissement.id", "Integer", dto.getUserResponsableDecaissementIdParam(), param, index, locale));
			}
			if (dto.getDevisId()!= null && dto.getDevisId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("devisId", dto.getDevisId(), "e.devis.id", "Integer", dto.getDevisIdParam(), param, index, locale));
			}
			if (dto.getEtatId()!= null && dto.getEtatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatId", dto.getEtatId(), "e.etat.id", "Integer", dto.getEtatIdParam(), param, index, locale));
			}
			if (dto.getUserResponsableAchatId()!= null && dto.getUserResponsableAchatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableAchatId", dto.getUserResponsableAchatId(), "e.userResponsableAchat.id", "Integer", dto.getUserResponsableAchatIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableTechniqueNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniqueNom", dto.getUserResponsableTechniqueNom(), "e.userResponsableTechnique.nom", "String", dto.getUserResponsableTechniqueNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableTechniquePrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniquePrenom", dto.getUserResponsableTechniquePrenom(), "e.userResponsableTechnique.prenom", "String", dto.getUserResponsableTechniquePrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableTechniqueLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniqueLogin", dto.getUserResponsableTechniqueLogin(), "e.userResponsableTechnique.login", "String", dto.getUserResponsableTechniqueLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierNom", dto.getUserChefAtelierNom(), "e.userChefAtelier.nom", "String", dto.getUserChefAtelierNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierPrenom", dto.getUserChefAtelierPrenom(), "e.userChefAtelier.prenom", "String", dto.getUserChefAtelierPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierLogin", dto.getUserChefAtelierLogin(), "e.userChefAtelier.login", "String", dto.getUserChefAtelierLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableDecaissementNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableDecaissementNom", dto.getUserResponsableDecaissementNom(), "e.userResponsableDecaissement.nom", "String", dto.getUserResponsableDecaissementNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableDecaissementPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableDecaissementPrenom", dto.getUserResponsableDecaissementPrenom(), "e.userResponsableDecaissement.prenom", "String", dto.getUserResponsableDecaissementPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableDecaissementLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableDecaissementLogin", dto.getUserResponsableDecaissementLogin(), "e.userResponsableDecaissement.login", "String", dto.getUserResponsableDecaissementLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatLibelle", dto.getEtatLibelle(), "e.etat.libelle", "String", dto.getEtatLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableAchatNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableAchatNom", dto.getUserResponsableAchatNom(), "e.userResponsableAchat.nom", "String", dto.getUserResponsableAchatNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableAchatPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableAchatPrenom", dto.getUserResponsableAchatPrenom(), "e.userResponsableAchat.prenom", "String", dto.getUserResponsableAchatPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableAchatLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableAchatLogin", dto.getUserResponsableAchatLogin(), "e.userResponsableAchat.login", "String", dto.getUserResponsableAchatLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
