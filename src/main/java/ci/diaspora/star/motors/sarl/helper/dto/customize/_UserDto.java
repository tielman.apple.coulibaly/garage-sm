
/*
 * Java dto for entity table user 
 * Created on 2019-08-19 ( Time 15:02:29 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.dto.FonctionnaliteDto;

/**
 * DTO customize for table "user"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _UserDto {
	
	protected List<FonctionnaliteDto> datasFonctionnalite;
	
	protected String newPassword ;
	
	

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public List<FonctionnaliteDto> getDatasFonctionnalite() {
		return datasFonctionnalite;
	}

	public void setDatasFonctionnalite(List<FonctionnaliteDto> datasFonctionnalite) {
		this.datasFonctionnalite = datasFonctionnalite;
	}
	
	

}
