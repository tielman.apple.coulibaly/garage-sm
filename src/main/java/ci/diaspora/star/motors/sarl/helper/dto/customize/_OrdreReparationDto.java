
/*
 * Java dto for entity table ordre_reparation 
 * Created on 2019-08-18 ( Time 17:15:12 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.dto.PanneDeclareeDto;

/**
 * DTO customize for table "ordre_reparation"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _OrdreReparationDto {
	
	List<PanneDeclareeDto> datasPannesDeclare;

	public List<PanneDeclareeDto> getDatasPannesDeclare() {
		return datasPannesDeclare;
	}

	public void setDatasPannesDeclare(List<PanneDeclareeDto> datasPannesDeclare) {
		this.datasPannesDeclare = datasPannesDeclare;
	}
}
