
/*
 * Java dto for entity table type_prestation_fiche_reception 
 * Created on 2019-08-18 ( Time 17:15:17 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.dao.entity.FicheReception;
import ci.diaspora.star.motors.sarl.dao.entity.TypePrestation;

/**
 * DTO customize for table "type_prestation_fiche_reception"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _TypePrestationFicheReceptionDto {
	
	protected FicheReception dataFicheReception ;
	protected TypePrestation dataTypePrestation ;

	public FicheReception getDataFicheReception() {
		return dataFicheReception;
	}

	public void setDataFicheReception(FicheReception dataFicheReception) {
		this.dataFicheReception = dataFicheReception;
	}

	public TypePrestation getDataTypePrestation() {
		return dataTypePrestation;
	}

	public void setDataTypePrestation(TypePrestation dataTypePrestation) {
		this.dataTypePrestation = dataTypePrestation;
	}
	
}
