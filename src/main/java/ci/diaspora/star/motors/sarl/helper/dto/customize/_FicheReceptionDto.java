
/*
 * Java dto for entity table fiche_reception 
 * Created on 2019-08-23 ( Time 14:59:37 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.dto.ClientDto;
import ci.diaspora.star.motors.sarl.helper.dto.ElementEtatVehiculeDto;
import ci.diaspora.star.motors.sarl.helper.dto.PanneDeclareeDto;
import ci.diaspora.star.motors.sarl.helper.dto.PieceVehiculeDto;
import ci.diaspora.star.motors.sarl.helper.dto.TypePrestationDto;
import ci.diaspora.star.motors.sarl.helper.dto.VehiculeDto;

/**
 * DTO customize for table "fiche_reception"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _FicheReceptionDto {
	
	protected String matriculeClient;
	protected String immatriculationVehicule;
	
	protected ClientDto dataClient;
	protected VehiculeDto dataVehicule;
	List<PieceVehiculeDto> datasPieceVehicule ;
	List<TypePrestationDto> datasTypePrestation;
	List<PanneDeclareeDto> datasPannesDeclare;
	List<ElementEtatVehiculeDto> datasElementEtatVehicule;
	
	
	
	public List<PieceVehiculeDto> getDatasPieceVehicule() {
		return datasPieceVehicule;
	}
	public void setDatasPieceVehicule(List<PieceVehiculeDto> datasPieceVehicule) {
		this.datasPieceVehicule = datasPieceVehicule;
	}
	public List<TypePrestationDto> getDatasTypePrestation() {
		return datasTypePrestation;
	}
	public void setDatasTypePrestation(List<TypePrestationDto> datasTypePrestation) {
		this.datasTypePrestation = datasTypePrestation;
	}
	public List<PanneDeclareeDto> getDatasPannesDeclare() {
		return datasPannesDeclare;
	}
	public void setDatasPannesDeclare(List<PanneDeclareeDto> datasPannesDeclare) {
		this.datasPannesDeclare = datasPannesDeclare;
	}
	public List<ElementEtatVehiculeDto> getDatasElementEtatVehicule() {
		return datasElementEtatVehicule;
	}
	public void setDatasElementEtatVehicule(List<ElementEtatVehiculeDto> datasElementEtatVehicule) {
		this.datasElementEtatVehicule = datasElementEtatVehicule;
	}
	public String getMatriculeClient() {
		return matriculeClient;
	}
	public void setMatriculeClient(String matriculeClient) {
		this.matriculeClient = matriculeClient;
	}
	public String getImmatriculationVehicule() {
		return immatriculationVehicule;
	}
	public void setImmatriculationVehicule(String immatriculationVehicule) {
		this.immatriculationVehicule = immatriculationVehicule;
	}
	public ClientDto getDataClient() {
		return dataClient;
	}
	public void setDataClient(ClientDto dataClient) {
		this.dataClient = dataClient;
	}
	public VehiculeDto getDataVehicule() {
		return dataVehicule;
	}
	public void setDataVehicule(VehiculeDto dataVehicule) {
		this.dataVehicule = dataVehicule;
	}

	
}
