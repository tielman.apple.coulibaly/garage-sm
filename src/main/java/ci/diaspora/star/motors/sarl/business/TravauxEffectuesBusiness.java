


                                                                                                  /*
 * Java transformer for entity table travaux_effectues
 * Created on 2019-08-18 ( Time 17:15:15 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "travaux_effectues"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class TravauxEffectuesBusiness implements IBasicBusiness<Request<TravauxEffectuesDto>, Response<TravauxEffectuesDto>> {

  private Response<TravauxEffectuesDto> response;
  @Autowired
  private TravauxEffectuesRepository travauxEffectuesRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private DesignationTravauxEffectuesRepository designationTravauxEffectuesRepository;
  @Autowired
  private OrdreReparationRepository ordreReparationRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

              

  public TravauxEffectuesBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create TravauxEffectues by using TravauxEffectuesDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<TravauxEffectuesDto> create(Request<TravauxEffectuesDto> request, Locale locale)  {
    slf4jLogger.info("----begin create TravauxEffectues-----");

    response = new Response<TravauxEffectuesDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<TravauxEffectues> items = new ArrayList<TravauxEffectues>();

      for (TravauxEffectuesDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("dateDebutTravaux", dto.getDateDebutTravaux());
        fieldsToVerify.put("dateFinTravaux", dto.getDateFinTravaux());
        fieldsToVerify.put("ordreReparationId", dto.getOrdreReparationId());
        fieldsToVerify.put("designationTravauxEffectuesId", dto.getDesignationTravauxEffectuesId());
        fieldsToVerify.put("userTechnicienId", dto.getUserTechnicienId());
        fieldsToVerify.put("isSigned", dto.getIsSigned());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if travauxEffectues to insert do not exist
        TravauxEffectues existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("travauxEffectues -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if designationTravauxEffectues exist
        DesignationTravauxEffectues existingDesignationTravauxEffectues = designationTravauxEffectuesRepository.findById(dto.getDesignationTravauxEffectuesId(), false);
        if (existingDesignationTravauxEffectues == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("designationTravauxEffectues -> " + dto.getDesignationTravauxEffectuesId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if user exist
        User existingUser = userRepository.findById(dto.getUserTechnicienId(), false);
        if (existingUser == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserTechnicienId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if ordreReparation exist
        OrdreReparation existingOrdreReparation = ordreReparationRepository.findById(dto.getOrdreReparationId(), false);
        if (existingOrdreReparation == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getOrdreReparationId(), locale));
          response.setHasError(true);
          return response;
        }
        TravauxEffectues entityToSave = null;
        entityToSave = TravauxEffectuesTransformer.INSTANCE.toEntity(dto, existingDesignationTravauxEffectues, existingUser, existingOrdreReparation);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<TravauxEffectues> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = travauxEffectuesRepository.save((Iterable<TravauxEffectues>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("travauxEffectues", locale));
          response.setHasError(true);
          return response;
        }
        List<TravauxEffectuesDto> itemsDto = new ArrayList<TravauxEffectuesDto>();
        for (TravauxEffectues entity : itemsSaved) {
          TravauxEffectuesDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create TravauxEffectues-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update TravauxEffectues by using TravauxEffectuesDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<TravauxEffectuesDto> update(Request<TravauxEffectuesDto> request, Locale locale)  {
    slf4jLogger.info("----begin update TravauxEffectues-----");

    response = new Response<TravauxEffectuesDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<TravauxEffectues> items = new ArrayList<TravauxEffectues>();

      for (TravauxEffectuesDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la travauxEffectues existe
        TravauxEffectues entityToSave = null;
        entityToSave = travauxEffectuesRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("travauxEffectues -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if designationTravauxEffectues exist
        if (dto.getDesignationTravauxEffectuesId() != null && dto.getDesignationTravauxEffectuesId() > 0){
          DesignationTravauxEffectues existingDesignationTravauxEffectues = designationTravauxEffectuesRepository.findById(dto.getDesignationTravauxEffectuesId(), false);
          if (existingDesignationTravauxEffectues == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("designationTravauxEffectues -> " + dto.getDesignationTravauxEffectuesId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setDesignationTravauxEffectues(existingDesignationTravauxEffectues);
        }
        // Verify if user exist
        if (dto.getUserTechnicienId() != null && dto.getUserTechnicienId() > 0){
          User existingUser = userRepository.findById(dto.getUserTechnicienId(), false);
          if (existingUser == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserTechnicienId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUser(existingUser);
        }
        // Verify if ordreReparation exist
        if (dto.getOrdreReparationId() != null && dto.getOrdreReparationId() > 0){
          OrdreReparation existingOrdreReparation = ordreReparationRepository.findById(dto.getOrdreReparationId(), false);
          if (existingOrdreReparation == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getOrdreReparationId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setOrdreReparation(existingOrdreReparation);
        }
        if (Utilities.notBlank(dto.getDateDebutTravaux())) {
          entityToSave.setDateDebutTravaux(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateDebutTravaux()));
        }
        if (Utilities.notBlank(dto.getDateFinTravaux())) {
          entityToSave.setDateFinTravaux(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateFinTravaux()));
        }
        if (dto.getIsSigned() != null) {
          entityToSave.setIsSigned(dto.getIsSigned());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<TravauxEffectues> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = travauxEffectuesRepository.save((Iterable<TravauxEffectues>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("travauxEffectues", locale));
          response.setHasError(true);
          return response;
        }
        List<TravauxEffectuesDto> itemsDto = new ArrayList<TravauxEffectuesDto>();
        for (TravauxEffectues entity : itemsSaved) {
          TravauxEffectuesDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update TravauxEffectues-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete TravauxEffectues by using TravauxEffectuesDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<TravauxEffectuesDto> delete(Request<TravauxEffectuesDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete TravauxEffectues-----");

    response = new Response<TravauxEffectuesDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<TravauxEffectues> items = new ArrayList<TravauxEffectues>();

      for (TravauxEffectuesDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la travauxEffectues existe
        TravauxEffectues existingEntity = null;
        existingEntity = travauxEffectuesRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("travauxEffectues -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        travauxEffectuesRepository.save((Iterable<TravauxEffectues>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete TravauxEffectues-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete TravauxEffectues by using TravauxEffectuesDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<TravauxEffectuesDto> forceDelete(Request<TravauxEffectuesDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete TravauxEffectues-----");

    response = new Response<TravauxEffectuesDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<TravauxEffectues> items = new ArrayList<TravauxEffectues>();

      for (TravauxEffectuesDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la travauxEffectues existe
        TravauxEffectues existingEntity = null;
          existingEntity = travauxEffectuesRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("travauxEffectues -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

              

                                    existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          travauxEffectuesRepository.save((Iterable<TravauxEffectues>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete TravauxEffectues-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get TravauxEffectues by using TravauxEffectuesDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<TravauxEffectuesDto> getByCriteria(Request<TravauxEffectuesDto> request, Locale locale) {
    slf4jLogger.info("----begin get TravauxEffectues-----");

    response = new Response<TravauxEffectuesDto>();

    try {
      List<TravauxEffectues> items = null;
      items = travauxEffectuesRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<TravauxEffectuesDto> itemsDto = new ArrayList<TravauxEffectuesDto>();
        for (TravauxEffectues entity : items) {
          TravauxEffectuesDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(travauxEffectuesRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("travauxEffectues", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get TravauxEffectues-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full TravauxEffectuesDto by using TravauxEffectues as object.
   *
   * @param entity, locale
   * @return TravauxEffectuesDto
   *
   */
  private TravauxEffectuesDto getFullInfos(TravauxEffectues entity, Integer size, Locale locale) throws Exception {
    TravauxEffectuesDto dto = TravauxEffectuesTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
