package ci.diaspora.star.motors.sarl.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ParamsUtils {
	// -----------------------------
	// ATTRIBUTES
	// -----------------------------   


	@Value("${smtp.mail.host}")
	private String	smtpHost;

	@Value("${smtp.mail.port}")
	private Integer	smtpPort;

	@Value("${smtp.mail.username}")
	private String	smtpMailUsername;

	@Value("${smtp.mail.password}")
	private String	smtpPassword;
	
	@Value("${smtp.mail.adresse}")
	private String smtpMailAdresse;
	
	@Value("${smtp.mail.user}")
	private String smtpMailUser;
	
	@Value("${template.newsletter}")
	private String templateNewsletter;
	
	
	@Value("${newsletter.subject}")
	private String newsletterSubject;
	
	@Value("${template.succes.change.password}")
	private String templateSuccesChangePassword;
	
	@Value("${template.creation.compte.par.admin}")
	private String templateCreationCompteParAdmin;
	@Value("${template.creation.compte.par.user}")
	private String templateCreationCompteParUser;
	
	
	
	@Value("${template.reset.user.password}")
	private String templateResetUserPassword;
	
	
	@Value("${url.root.admin}")
	private String urlRootAdmin;
	
	@Value("${url.root.user}")
	private String urlRootUser;
	
	
	
	
	public String getSmtpHost() {
		return smtpHost;
	}

	/**
	 * @return the smtpPort
	 */

	public Integer getSmtpPort() {
		return smtpPort;
	}

	/**
	 * @return the smtpLogin
	 */
	public String getSmtpMailUser() {
		return smtpMailUser;
	}

	/**
	 * @return the smtpPassword
	 */
	public String getSmtpMailAdresse() {
		return smtpMailAdresse;
	}

	public String getSmtpMailUsername() {
		return smtpMailUsername;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public String getTemplateNewsletter() {
		return templateNewsletter;
	}

	public String getNewsletterSubject() {
		return newsletterSubject;
	}

	// ATTRIBUTES
	// -----------------------------

	@Value("${url.root}")
	private String	rootUrlFiles;
	
	public String getTemplateSuccesChangePassword() {
		return templateSuccesChangePassword;
	}

	public String getTemplateResetUserPassword() {
		return templateResetUserPassword;
	}

	/**
	 * @return the rootUrlFiles
	 */
	public String getRootUrlFiles() {
		return rootUrlFiles;
	}

	public String getUrlRootAdmin() {
		return urlRootAdmin;
	}

	public String getUrlRootUser() {
		return urlRootUser;
	}


	public String getTemplateCreationCompteParAdmin() {
		return templateCreationCompteParAdmin;
	}

	public String getTemplateCreationCompteParUser() {
		return templateCreationCompteParUser;
	}
	
	

	// -----------------------------

}