
/*
 * Java dto for entity table ordre_reparation
 * Created on 2019-08-18 ( Time 17:15:12 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._OrdreReparationDto;

/**
 * DTO for table "ordre_reparation"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class OrdreReparationDto extends _OrdreReparationDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    ficheReceptionId     ;
	/*
	 * 
	 */
    private Integer    typeOrdreReparationId ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Integer    etatTravauxId        ;
	/*
	 * 
	 */
    private Integer    etatOrdreId          ;
	/*
	 * 
	 */
    private Integer    userChefAtelierId    ;
	/*
	 * 
	 */
    private Integer    userSuperviseurId    ;
	/*
	 * 
	 */
    private Integer    userReceptionId      ;
	/*
	 * 
	 */
    private Boolean    isValidedByChefAtelierOrdre ;
	/*
	 * 
	 */
    private Boolean    isValidedByChefAtelierTravaux ;
	/*
	 * 
	 */
    private Boolean    isValidedByReception ;
	/*
	 * 
	 */
    private Boolean    isValidedBySuperviseurOrdre ;
	/*
	 * 
	 */
    private Boolean    isValidedBySuperviseurTravaux ;
	/*
	 * 
	 */
    private String     observationsChefAtelier ;
	/*
	 * 
	 */
    private String     observationsSuperviseur ;
	/*
	 * 
	 */
	private String     dateValidationReception ;
	/*
	 * 
	 */
	private String     dateValidationOrdreChefAtelier ;
	/*
	 * 
	 */
	private String     dateValidationTravauxChefAtelier ;
	/*
	 * 
	 */
	private String     dateValidationOrdreSuperviseur ;
	/*
	 * 
	 */
	private String     dateValidationTravauxSuperviseur ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String userChefAtelierNom;
	private String userChefAtelierPrenom;
	private String userChefAtelierLogin;
	private String typeOrdreReparationLibelle;
	private String etatOrdreCode;
	private String etatOrdreLibelle;
	private String etatTravauxCode;
	private String etatTravauxLibelle;
	private String userSuperviseurNom;
	private String userSuperviseurPrenom;
	private String userSuperviseurLogin;
	private String userReceptionNom;
	private String userReceptionPrenom;
	private String userReceptionLogin;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  ficheReceptionIdParam ;                     
	private SearchParam<Integer>  typeOrdreReparationIdParam;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Integer>  etatTravauxIdParam    ;                     
	private SearchParam<Integer>  etatOrdreIdParam      ;                     
	private SearchParam<Integer>  userChefAtelierIdParam;                     
	private SearchParam<Integer>  userSuperviseurIdParam;                     
	private SearchParam<Integer>  userReceptionIdParam  ;                     
	private SearchParam<Boolean>  isValidedByChefAtelierOrdreParam;                     
	private SearchParam<Boolean>  isValidedByChefAtelierTravauxParam;                     
	private SearchParam<Boolean>  isValidedByReceptionParam;                     
	private SearchParam<Boolean>  isValidedBySuperviseurOrdreParam;                     
	private SearchParam<Boolean>  isValidedBySuperviseurTravauxParam;                     
	private SearchParam<String>   observationsChefAtelierParam;                     
	private SearchParam<String>   observationsSuperviseurParam;                     

		private SearchParam<String>   dateValidationReceptionParam;                     

		private SearchParam<String>   dateValidationOrdreChefAtelierParam;                     

		private SearchParam<String>   dateValidationTravauxChefAtelierParam;                     

		private SearchParam<String>   dateValidationOrdreSuperviseurParam;                     

		private SearchParam<String>   dateValidationTravauxSuperviseurParam;                     
	private SearchParam<String>   userChefAtelierNomParam          ;                     
	private SearchParam<String>   userChefAtelierPrenomParam       ;                     
	private SearchParam<String>   userChefAtelierLoginParam        ;                     
	private SearchParam<String>   typeOrdreReparationLibelleParam;                     
	private SearchParam<String>   etatOrdreCodeParam         ;                     
	private SearchParam<String>   etatOrdreLibelleParam      ;                     
	private SearchParam<String>   etatTravauxCodeParam         ;                     
	private SearchParam<String>   etatTravauxLibelleParam      ;                     
	private SearchParam<String>   userSuperviseurNomParam          ;                     
	private SearchParam<String>   userSuperviseurPrenomParam       ;                     
	private SearchParam<String>   userSuperviseurLoginParam        ;                     
	private SearchParam<String>   userReceptionNomParam          ;                     
	private SearchParam<String>   userReceptionPrenomParam       ;                     
	private SearchParam<String>   userReceptionLoginParam        ;                     
    /**
     * Default constructor
     */
    public OrdreReparationDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "ficheReceptionId" field value
     * @param ficheReceptionId
     */
	public void setFicheReceptionId( Integer ficheReceptionId )
    {
        this.ficheReceptionId = ficheReceptionId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getFicheReceptionId()
    {
        return this.ficheReceptionId;
    }

    /**
     * Set the "typeOrdreReparationId" field value
     * @param typeOrdreReparationId
     */
	public void setTypeOrdreReparationId( Integer typeOrdreReparationId )
    {
        this.typeOrdreReparationId = typeOrdreReparationId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getTypeOrdreReparationId()
    {
        return this.typeOrdreReparationId;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }

    /**
     * Set the "etatTravauxId" field value
     * @param etatTravauxId
     */
	public void setEtatTravauxId( Integer etatTravauxId )
    {
        this.etatTravauxId = etatTravauxId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getEtatTravauxId()
    {
        return this.etatTravauxId;
    }

    /**
     * Set the "etatOrdreId" field value
     * @param etatOrdreId
     */
	public void setEtatOrdreId( Integer etatOrdreId )
    {
        this.etatOrdreId = etatOrdreId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getEtatOrdreId()
    {
        return this.etatOrdreId;
    }

    /**
     * Set the "userChefAtelierId" field value
     * @param userChefAtelierId
     */
	public void setUserChefAtelierId( Integer userChefAtelierId )
    {
        this.userChefAtelierId = userChefAtelierId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserChefAtelierId()
    {
        return this.userChefAtelierId;
    }

    /**
     * Set the "userSuperviseurId" field value
     * @param userSuperviseurId
     */
	public void setUserSuperviseurId( Integer userSuperviseurId )
    {
        this.userSuperviseurId = userSuperviseurId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserSuperviseurId()
    {
        return this.userSuperviseurId;
    }

    /**
     * Set the "userReceptionId" field value
     * @param userReceptionId
     */
	public void setUserReceptionId( Integer userReceptionId )
    {
        this.userReceptionId = userReceptionId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserReceptionId()
    {
        return this.userReceptionId;
    }

    /**
     * Set the "isValidedByChefAtelierOrdre" field value
     * @param isValidedByChefAtelierOrdre
     */
	public void setIsValidedByChefAtelierOrdre( Boolean isValidedByChefAtelierOrdre )
    {
        this.isValidedByChefAtelierOrdre = isValidedByChefAtelierOrdre ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByChefAtelierOrdre()
    {
        return this.isValidedByChefAtelierOrdre;
    }

    /**
     * Set the "isValidedByChefAtelierTravaux" field value
     * @param isValidedByChefAtelierTravaux
     */
	public void setIsValidedByChefAtelierTravaux( Boolean isValidedByChefAtelierTravaux )
    {
        this.isValidedByChefAtelierTravaux = isValidedByChefAtelierTravaux ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByChefAtelierTravaux()
    {
        return this.isValidedByChefAtelierTravaux;
    }

    /**
     * Set the "isValidedByReception" field value
     * @param isValidedByReception
     */
	public void setIsValidedByReception( Boolean isValidedByReception )
    {
        this.isValidedByReception = isValidedByReception ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByReception()
    {
        return this.isValidedByReception;
    }

    /**
     * Set the "isValidedBySuperviseurOrdre" field value
     * @param isValidedBySuperviseurOrdre
     */
	public void setIsValidedBySuperviseurOrdre( Boolean isValidedBySuperviseurOrdre )
    {
        this.isValidedBySuperviseurOrdre = isValidedBySuperviseurOrdre ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedBySuperviseurOrdre()
    {
        return this.isValidedBySuperviseurOrdre;
    }

    /**
     * Set the "isValidedBySuperviseurTravaux" field value
     * @param isValidedBySuperviseurTravaux
     */
	public void setIsValidedBySuperviseurTravaux( Boolean isValidedBySuperviseurTravaux )
    {
        this.isValidedBySuperviseurTravaux = isValidedBySuperviseurTravaux ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedBySuperviseurTravaux()
    {
        return this.isValidedBySuperviseurTravaux;
    }

    /**
     * Set the "observationsChefAtelier" field value
     * @param observationsChefAtelier
     */
	public void setObservationsChefAtelier( String observationsChefAtelier )
    {
        this.observationsChefAtelier = observationsChefAtelier ;
    }
    /**
     * 
     * @return the field value
     */
	public String getObservationsChefAtelier()
    {
        return this.observationsChefAtelier;
    }

    /**
     * Set the "observationsSuperviseur" field value
     * @param observationsSuperviseur
     */
	public void setObservationsSuperviseur( String observationsSuperviseur )
    {
        this.observationsSuperviseur = observationsSuperviseur ;
    }
    /**
     * 
     * @return the field value
     */
	public String getObservationsSuperviseur()
    {
        return this.observationsSuperviseur;
    }

    /**
     * Set the "dateValidationReception" field value
     * @param dateValidationReception
     */
	public void setDateValidationReception( String dateValidationReception )
    {
        this.dateValidationReception = dateValidationReception ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateValidationReception()
    {
        return this.dateValidationReception;
    }

    /**
     * Set the "dateValidationOrdreChefAtelier" field value
     * @param dateValidationOrdreChefAtelier
     */
	public void setDateValidationOrdreChefAtelier( String dateValidationOrdreChefAtelier )
    {
        this.dateValidationOrdreChefAtelier = dateValidationOrdreChefAtelier ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateValidationOrdreChefAtelier()
    {
        return this.dateValidationOrdreChefAtelier;
    }

    /**
     * Set the "dateValidationTravauxChefAtelier" field value
     * @param dateValidationTravauxChefAtelier
     */
	public void setDateValidationTravauxChefAtelier( String dateValidationTravauxChefAtelier )
    {
        this.dateValidationTravauxChefAtelier = dateValidationTravauxChefAtelier ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateValidationTravauxChefAtelier()
    {
        return this.dateValidationTravauxChefAtelier;
    }

    /**
     * Set the "dateValidationOrdreSuperviseur" field value
     * @param dateValidationOrdreSuperviseur
     */
	public void setDateValidationOrdreSuperviseur( String dateValidationOrdreSuperviseur )
    {
        this.dateValidationOrdreSuperviseur = dateValidationOrdreSuperviseur ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateValidationOrdreSuperviseur()
    {
        return this.dateValidationOrdreSuperviseur;
    }

    /**
     * Set the "dateValidationTravauxSuperviseur" field value
     * @param dateValidationTravauxSuperviseur
     */
	public void setDateValidationTravauxSuperviseur( String dateValidationTravauxSuperviseur )
    {
        this.dateValidationTravauxSuperviseur = dateValidationTravauxSuperviseur ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateValidationTravauxSuperviseur()
    {
        return this.dateValidationTravauxSuperviseur;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	

	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "ficheReceptionIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getFicheReceptionIdParam(){
		return this.ficheReceptionIdParam;
	}
	/**
     * Set the "ficheReceptionIdParam" field value
     * @param ficheReceptionIdParam
     */
    public void setFicheReceptionIdParam( SearchParam<Integer> ficheReceptionIdParam ){
        this.ficheReceptionIdParam = ficheReceptionIdParam;
    }

	/**
     * Get the "typeOrdreReparationIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getTypeOrdreReparationIdParam(){
		return this.typeOrdreReparationIdParam;
	}
	/**
     * Set the "typeOrdreReparationIdParam" field value
     * @param typeOrdreReparationIdParam
     */
    public void setTypeOrdreReparationIdParam( SearchParam<Integer> typeOrdreReparationIdParam ){
        this.typeOrdreReparationIdParam = typeOrdreReparationIdParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

	/**
     * Get the "etatTravauxIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getEtatTravauxIdParam(){
		return this.etatTravauxIdParam;
	}
	/**
     * Set the "etatTravauxIdParam" field value
     * @param etatTravauxIdParam
     */
    public void setEtatTravauxIdParam( SearchParam<Integer> etatTravauxIdParam ){
        this.etatTravauxIdParam = etatTravauxIdParam;
    }

	/**
     * Get the "etatOrdreIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getEtatOrdreIdParam(){
		return this.etatOrdreIdParam;
	}
	/**
     * Set the "etatOrdreIdParam" field value
     * @param etatOrdreIdParam
     */
    public void setEtatOrdreIdParam( SearchParam<Integer> etatOrdreIdParam ){
        this.etatOrdreIdParam = etatOrdreIdParam;
    }

	/**
     * Get the "userChefAtelierIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserChefAtelierIdParam(){
		return this.userChefAtelierIdParam;
	}
	/**
     * Set the "userChefAtelierIdParam" field value
     * @param userChefAtelierIdParam
     */
    public void setUserChefAtelierIdParam( SearchParam<Integer> userChefAtelierIdParam ){
        this.userChefAtelierIdParam = userChefAtelierIdParam;
    }

	/**
     * Get the "userSuperviseurIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserSuperviseurIdParam(){
		return this.userSuperviseurIdParam;
	}
	/**
     * Set the "userSuperviseurIdParam" field value
     * @param userSuperviseurIdParam
     */
    public void setUserSuperviseurIdParam( SearchParam<Integer> userSuperviseurIdParam ){
        this.userSuperviseurIdParam = userSuperviseurIdParam;
    }

	/**
     * Get the "userReceptionIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserReceptionIdParam(){
		return this.userReceptionIdParam;
	}
	/**
     * Set the "userReceptionIdParam" field value
     * @param userReceptionIdParam
     */
    public void setUserReceptionIdParam( SearchParam<Integer> userReceptionIdParam ){
        this.userReceptionIdParam = userReceptionIdParam;
    }

	/**
     * Get the "isValidedByChefAtelierOrdreParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByChefAtelierOrdreParam(){
		return this.isValidedByChefAtelierOrdreParam;
	}
	/**
     * Set the "isValidedByChefAtelierOrdreParam" field value
     * @param isValidedByChefAtelierOrdreParam
     */
    public void setIsValidedByChefAtelierOrdreParam( SearchParam<Boolean> isValidedByChefAtelierOrdreParam ){
        this.isValidedByChefAtelierOrdreParam = isValidedByChefAtelierOrdreParam;
    }

	/**
     * Get the "isValidedByChefAtelierTravauxParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByChefAtelierTravauxParam(){
		return this.isValidedByChefAtelierTravauxParam;
	}
	/**
     * Set the "isValidedByChefAtelierTravauxParam" field value
     * @param isValidedByChefAtelierTravauxParam
     */
    public void setIsValidedByChefAtelierTravauxParam( SearchParam<Boolean> isValidedByChefAtelierTravauxParam ){
        this.isValidedByChefAtelierTravauxParam = isValidedByChefAtelierTravauxParam;
    }

	/**
     * Get the "isValidedByReceptionParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByReceptionParam(){
		return this.isValidedByReceptionParam;
	}
	/**
     * Set the "isValidedByReceptionParam" field value
     * @param isValidedByReceptionParam
     */
    public void setIsValidedByReceptionParam( SearchParam<Boolean> isValidedByReceptionParam ){
        this.isValidedByReceptionParam = isValidedByReceptionParam;
    }

	/**
     * Get the "isValidedBySuperviseurOrdreParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedBySuperviseurOrdreParam(){
		return this.isValidedBySuperviseurOrdreParam;
	}
	/**
     * Set the "isValidedBySuperviseurOrdreParam" field value
     * @param isValidedBySuperviseurOrdreParam
     */
    public void setIsValidedBySuperviseurOrdreParam( SearchParam<Boolean> isValidedBySuperviseurOrdreParam ){
        this.isValidedBySuperviseurOrdreParam = isValidedBySuperviseurOrdreParam;
    }

	/**
     * Get the "isValidedBySuperviseurTravauxParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedBySuperviseurTravauxParam(){
		return this.isValidedBySuperviseurTravauxParam;
	}
	/**
     * Set the "isValidedBySuperviseurTravauxParam" field value
     * @param isValidedBySuperviseurTravauxParam
     */
    public void setIsValidedBySuperviseurTravauxParam( SearchParam<Boolean> isValidedBySuperviseurTravauxParam ){
        this.isValidedBySuperviseurTravauxParam = isValidedBySuperviseurTravauxParam;
    }

	/**
     * Get the "observationsChefAtelierParam" field value
     * @return the field value
     */
	public SearchParam<String> getObservationsChefAtelierParam(){
		return this.observationsChefAtelierParam;
	}
	/**
     * Set the "observationsChefAtelierParam" field value
     * @param observationsChefAtelierParam
     */
    public void setObservationsChefAtelierParam( SearchParam<String> observationsChefAtelierParam ){
        this.observationsChefAtelierParam = observationsChefAtelierParam;
    }

	/**
     * Get the "observationsSuperviseurParam" field value
     * @return the field value
     */
	public SearchParam<String> getObservationsSuperviseurParam(){
		return this.observationsSuperviseurParam;
	}
	/**
     * Set the "observationsSuperviseurParam" field value
     * @param observationsSuperviseurParam
     */
    public void setObservationsSuperviseurParam( SearchParam<String> observationsSuperviseurParam ){
        this.observationsSuperviseurParam = observationsSuperviseurParam;
    }

	/**
     * Get the "dateValidationReceptionParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateValidationReceptionParam(){
		return this.dateValidationReceptionParam;
	}
	/**
     * Set the "dateValidationReceptionParam" field value
     * @param dateValidationReceptionParam
     */
    public void setDateValidationReceptionParam( SearchParam<String> dateValidationReceptionParam ){
        this.dateValidationReceptionParam = dateValidationReceptionParam;
    }

	/**
     * Get the "dateValidationOrdreChefAtelierParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateValidationOrdreChefAtelierParam(){
		return this.dateValidationOrdreChefAtelierParam;
	}
	/**
     * Set the "dateValidationOrdreChefAtelierParam" field value
     * @param dateValidationOrdreChefAtelierParam
     */
    public void setDateValidationOrdreChefAtelierParam( SearchParam<String> dateValidationOrdreChefAtelierParam ){
        this.dateValidationOrdreChefAtelierParam = dateValidationOrdreChefAtelierParam;
    }

	/**
     * Get the "dateValidationTravauxChefAtelierParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateValidationTravauxChefAtelierParam(){
		return this.dateValidationTravauxChefAtelierParam;
	}
	/**
     * Set the "dateValidationTravauxChefAtelierParam" field value
     * @param dateValidationTravauxChefAtelierParam
     */
    public void setDateValidationTravauxChefAtelierParam( SearchParam<String> dateValidationTravauxChefAtelierParam ){
        this.dateValidationTravauxChefAtelierParam = dateValidationTravauxChefAtelierParam;
    }

	/**
     * Get the "dateValidationOrdreSuperviseurParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateValidationOrdreSuperviseurParam(){
		return this.dateValidationOrdreSuperviseurParam;
	}
	/**
     * Set the "dateValidationOrdreSuperviseurParam" field value
     * @param dateValidationOrdreSuperviseurParam
     */
    public void setDateValidationOrdreSuperviseurParam( SearchParam<String> dateValidationOrdreSuperviseurParam ){
        this.dateValidationOrdreSuperviseurParam = dateValidationOrdreSuperviseurParam;
    }

	/**
     * Get the "dateValidationTravauxSuperviseurParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateValidationTravauxSuperviseurParam(){
		return this.dateValidationTravauxSuperviseurParam;
	}
	/**
     * Set the "dateValidationTravauxSuperviseurParam" field value
     * @param dateValidationTravauxSuperviseurParam
     */
    public void setDateValidationTravauxSuperviseurParam( SearchParam<String> dateValidationTravauxSuperviseurParam ){
        this.dateValidationTravauxSuperviseurParam = dateValidationTravauxSuperviseurParam;
    }

	

	public String getUserChefAtelierNom() {
		return userChefAtelierNom;
	}

	public void setUserChefAtelierNom(String userChefAtelierNom) {
		this.userChefAtelierNom = userChefAtelierNom;
	}

	public String getUserChefAtelierPrenom() {
		return userChefAtelierPrenom;
	}

	public void setUserChefAtelierPrenom(String userChefAtelierPrenom) {
		this.userChefAtelierPrenom = userChefAtelierPrenom;
	}

	public String getUserChefAtelierLogin() {
		return userChefAtelierLogin;
	}

	public void setUserChefAtelierLogin(String userChefAtelierLogin) {
		this.userChefAtelierLogin = userChefAtelierLogin;
	}

	public String getTypeOrdreReparationLibelle() {
		return typeOrdreReparationLibelle;
	}

	public void setTypeOrdreReparationLibelle(String typeOrdreReparationLibelle) {
		this.typeOrdreReparationLibelle = typeOrdreReparationLibelle;
	}

	public String getEtatOrdreCode() {
		return etatOrdreCode;
	}

	public void setEtatOrdreCode(String etatOrdreCode) {
		this.etatOrdreCode = etatOrdreCode;
	}

	public String getEtatOrdreLibelle() {
		return etatOrdreLibelle;
	}

	public void setEtatOrdreLibelle(String etatOrdreLibelle) {
		this.etatOrdreLibelle = etatOrdreLibelle;
	}

	public String getEtatTravauxCode() {
		return etatTravauxCode;
	}

	public void setEtatTravauxCode(String etatTravauxCode) {
		this.etatTravauxCode = etatTravauxCode;
	}

	public String getEtatTravauxLibelle() {
		return etatTravauxLibelle;
	}

	public void setEtatTravauxLibelle(String etatTravauxLibelle) {
		this.etatTravauxLibelle = etatTravauxLibelle;
	}

	public String getUserSuperviseurNom() {
		return userSuperviseurNom;
	}

	public void setUserSuperviseurNom(String userSuperviseurNom) {
		this.userSuperviseurNom = userSuperviseurNom;
	}

	public String getUserSuperviseurPrenom() {
		return userSuperviseurPrenom;
	}

	public void setUserSuperviseurPrenom(String userSuperviseurPrenom) {
		this.userSuperviseurPrenom = userSuperviseurPrenom;
	}

	public String getUserSuperviseurLogin() {
		return userSuperviseurLogin;
	}

	public void setUserSuperviseurLogin(String userSuperviseurLogin) {
		this.userSuperviseurLogin = userSuperviseurLogin;
	}

	public String getUserReceptionNom() {
		return userReceptionNom;
	}

	public void setUserReceptionNom(String userReceptionNom) {
		this.userReceptionNom = userReceptionNom;
	}

	public String getUserReceptionPrenom() {
		return userReceptionPrenom;
	}

	public void setUserReceptionPrenom(String userReceptionPrenom) {
		this.userReceptionPrenom = userReceptionPrenom;
	}

	public String getUserReceptionLogin() {
		return userReceptionLogin;
	}

	public void setUserReceptionLogin(String userReceptionLogin) {
		this.userReceptionLogin = userReceptionLogin;
	}

	public SearchParam<String> getUserChefAtelierNomParam() {
		return userChefAtelierNomParam;
	}

	public void setUserChefAtelierNomParam(SearchParam<String> userChefAtelierNomParam) {
		this.userChefAtelierNomParam = userChefAtelierNomParam;
	}

	public SearchParam<String> getUserChefAtelierPrenomParam() {
		return userChefAtelierPrenomParam;
	}

	public void setUserChefAtelierPrenomParam(SearchParam<String> userChefAtelierPrenomParam) {
		this.userChefAtelierPrenomParam = userChefAtelierPrenomParam;
	}

	public SearchParam<String> getUserChefAtelierLoginParam() {
		return userChefAtelierLoginParam;
	}

	public void setUserChefAtelierLoginParam(SearchParam<String> userChefAtelierLoginParam) {
		this.userChefAtelierLoginParam = userChefAtelierLoginParam;
	}

	public SearchParam<String> getTypeOrdreReparationLibelleParam() {
		return typeOrdreReparationLibelleParam;
	}

	public void setTypeOrdreReparationLibelleParam(SearchParam<String> typeOrdreReparationLibelleParam) {
		this.typeOrdreReparationLibelleParam = typeOrdreReparationLibelleParam;
	}

	public SearchParam<String> getEtatOrdreCodeParam() {
		return etatOrdreCodeParam;
	}

	public void setEtatOrdreCodeParam(SearchParam<String> etatOrdreCodeParam) {
		this.etatOrdreCodeParam = etatOrdreCodeParam;
	}

	public SearchParam<String> getEtatOrdreLibelleParam() {
		return etatOrdreLibelleParam;
	}

	public void setEtatOrdreLibelleParam(SearchParam<String> etatOrdreLibelleParam) {
		this.etatOrdreLibelleParam = etatOrdreLibelleParam;
	}

	public SearchParam<String> getEtatTravauxCodeParam() {
		return etatTravauxCodeParam;
	}

	public void setEtatTravauxCodeParam(SearchParam<String> etatTravauxCodeParam) {
		this.etatTravauxCodeParam = etatTravauxCodeParam;
	}

	public SearchParam<String> getEtatTravauxLibelleParam() {
		return etatTravauxLibelleParam;
	}

	public void setEtatTravauxLibelleParam(SearchParam<String> etatTravauxLibelleParam) {
		this.etatTravauxLibelleParam = etatTravauxLibelleParam;
	}

	public SearchParam<String> getUserSuperviseurNomParam() {
		return userSuperviseurNomParam;
	}

	public void setUserSuperviseurNomParam(SearchParam<String> userSuperviseurNomParam) {
		this.userSuperviseurNomParam = userSuperviseurNomParam;
	}

	public SearchParam<String> getUserSuperviseurPrenomParam() {
		return userSuperviseurPrenomParam;
	}

	public void setUserSuperviseurPrenomParam(SearchParam<String> userSuperviseurPrenomParam) {
		this.userSuperviseurPrenomParam = userSuperviseurPrenomParam;
	}

	public SearchParam<String> getUserSuperviseurLoginParam() {
		return userSuperviseurLoginParam;
	}

	public void setUserSuperviseurLoginParam(SearchParam<String> userSuperviseurLoginParam) {
		this.userSuperviseurLoginParam = userSuperviseurLoginParam;
	}

	public SearchParam<String> getUserReceptionNomParam() {
		return userReceptionNomParam;
	}

	public void setUserReceptionNomParam(SearchParam<String> userReceptionNomParam) {
		this.userReceptionNomParam = userReceptionNomParam;
	}

	public SearchParam<String> getUserReceptionPrenomParam() {
		return userReceptionPrenomParam;
	}

	public void setUserReceptionPrenomParam(SearchParam<String> userReceptionPrenomParam) {
		this.userReceptionPrenomParam = userReceptionPrenomParam;
	}

	public SearchParam<String> getUserReceptionLoginParam() {
		return userReceptionLoginParam;
	}

	public void setUserReceptionLoginParam(SearchParam<String> userReceptionLoginParam) {
		this.userReceptionLoginParam = userReceptionLoginParam;
	}

	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		OrdreReparationDto other = (OrdreReparationDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
		sb.append("|");
		sb.append("isValidedByChefAtelierOrdre:"+isValidedByChefAtelierOrdre);
		sb.append("|");
		sb.append("isValidedByChefAtelierTravaux:"+isValidedByChefAtelierTravaux);
		sb.append("|");
		sb.append("isValidedByReception:"+isValidedByReception);
		sb.append("|");
		sb.append("isValidedBySuperviseurOrdre:"+isValidedBySuperviseurOrdre);
		sb.append("|");
		sb.append("isValidedBySuperviseurTravaux:"+isValidedBySuperviseurTravaux);
		sb.append("|");
		sb.append("observationsChefAtelier:"+observationsChefAtelier);
		sb.append("|");
		sb.append("observationsSuperviseur:"+observationsSuperviseur);
		sb.append("|");
		sb.append("dateValidationReception:"+dateValidationReception);
		sb.append("|");
		sb.append("dateValidationOrdreChefAtelier:"+dateValidationOrdreChefAtelier);
		sb.append("|");
		sb.append("dateValidationTravauxChefAtelier:"+dateValidationTravauxChefAtelier);
		sb.append("|");
		sb.append("dateValidationOrdreSuperviseur:"+dateValidationOrdreSuperviseur);
		sb.append("|");
		sb.append("dateValidationTravauxSuperviseur:"+dateValidationTravauxSuperviseur);
        return sb.toString();
    }
}
