package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._PieceDetacheeFichePreparationDevisRepository;

/**
 * Repository : PieceDetacheeFichePreparationDevis.
 */
@Repository
public interface PieceDetacheeFichePreparationDevisRepository extends JpaRepository<PieceDetacheeFichePreparationDevis, Integer>, _PieceDetacheeFichePreparationDevisRepository {
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object PieceDetacheeFichePreparationDevis whose id is equals to the given id. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.id = :id and e.isDeleted = :isDeleted")
	PieceDetacheeFichePreparationDevis findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PieceDetacheeFichePreparationDevis by using quantite as a search criteria.
	 *
	 * @param quantite
	 * @return An Object PieceDetacheeFichePreparationDevis whose quantite is equals to the given quantite. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.quantite = :quantite and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByQuantite(@Param("quantite")Integer quantite, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using prixUnitaire as a search criteria.
	 *
	 * @param prixUnitaire
	 * @return An Object PieceDetacheeFichePreparationDevis whose prixUnitaire is equals to the given prixUnitaire. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.prixUnitaire = :prixUnitaire and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByPrixUnitaire(@Param("prixUnitaire")Float prixUnitaire, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using total as a search criteria.
	 *
	 * @param total
	 * @return An Object PieceDetacheeFichePreparationDevis whose total is equals to the given total. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.total = :total and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByTotal(@Param("total")Float total, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using isValided as a search criteria.
	 *
	 * @param isValided
	 * @return An Object PieceDetacheeFichePreparationDevis whose isValided is equals to the given isValided. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.isValided = :isValided and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByIsValided(@Param("isValided")Boolean isValided, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object PieceDetacheeFichePreparationDevis whose isDeleted is equals to the given isDeleted. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object PieceDetacheeFichePreparationDevis whose createdAt is equals to the given createdAt. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object PieceDetacheeFichePreparationDevis whose updatedAt is equals to the given updatedAt. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object PieceDetacheeFichePreparationDevis whose deletedAt is equals to the given deletedAt. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object PieceDetacheeFichePreparationDevis whose createdBy is equals to the given createdBy. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object PieceDetacheeFichePreparationDevis whose updatedBy is equals to the given updatedBy. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceDetacheeFichePreparationDevis by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object PieceDetacheeFichePreparationDevis whose deletedBy is equals to the given deletedBy. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PieceDetacheeFichePreparationDevis by using pieceDetacheeId as a search criteria.
	 *
	 * @param pieceDetacheeId
	 * @return A list of Object PieceDetacheeFichePreparationDevis whose pieceDetacheeId is equals to the given pieceDetacheeId. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.pieceDetachee.id = :pieceDetacheeId and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByPieceDetacheeId(@Param("pieceDetacheeId")Integer pieceDetacheeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PieceDetacheeFichePreparationDevis by using pieceDetacheeId as a search criteria.
   *
   * @param pieceDetacheeId
   * @return An Object PieceDetacheeFichePreparationDevis whose pieceDetacheeId is equals to the given pieceDetacheeId. If
   *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
   */
  @Query("select e from PieceDetacheeFichePreparationDevis e where e.pieceDetachee.id = :pieceDetacheeId and e.isDeleted = :isDeleted")
  PieceDetacheeFichePreparationDevis findPieceDetacheeFichePreparationDevisByPieceDetacheeId(@Param("pieceDetacheeId")Integer pieceDetacheeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds PieceDetacheeFichePreparationDevis by using fournisseurId as a search criteria.
	 *
	 * @param fournisseurId
	 * @return A list of Object PieceDetacheeFichePreparationDevis whose fournisseurId is equals to the given fournisseurId. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.fournisseur.id = :fournisseurId and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByFournisseurId(@Param("fournisseurId")Integer fournisseurId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PieceDetacheeFichePreparationDevis by using fournisseurId as a search criteria.
   *
   * @param fournisseurId
   * @return An Object PieceDetacheeFichePreparationDevis whose fournisseurId is equals to the given fournisseurId. If
   *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
   */
  @Query("select e from PieceDetacheeFichePreparationDevis e where e.fournisseur.id = :fournisseurId and e.isDeleted = :isDeleted")
  PieceDetacheeFichePreparationDevis findPieceDetacheeFichePreparationDevisByFournisseurId(@Param("fournisseurId")Integer fournisseurId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds PieceDetacheeFichePreparationDevis by using fichePreparationDevisId as a search criteria.
	 *
	 * @param fichePreparationDevisId
	 * @return A list of Object PieceDetacheeFichePreparationDevis whose fichePreparationDevisId is equals to the given fichePreparationDevisId. If
	 *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from PieceDetacheeFichePreparationDevis e where e.fichePreparationDevis.id = :fichePreparationDevisId and e.isDeleted = :isDeleted")
	List<PieceDetacheeFichePreparationDevis> findByFichePreparationDevisId(@Param("fichePreparationDevisId")Integer fichePreparationDevisId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PieceDetacheeFichePreparationDevis by using fichePreparationDevisId as a search criteria.
   *
   * @param fichePreparationDevisId
   * @return An Object PieceDetacheeFichePreparationDevis whose fichePreparationDevisId is equals to the given fichePreparationDevisId. If
   *         no PieceDetacheeFichePreparationDevis is found, this method returns null.
   */
  @Query("select e from PieceDetacheeFichePreparationDevis e where e.fichePreparationDevis.id = :fichePreparationDevisId and e.isDeleted = :isDeleted")
  PieceDetacheeFichePreparationDevis findPieceDetacheeFichePreparationDevisByFichePreparationDevisId(@Param("fichePreparationDevisId")Integer fichePreparationDevisId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of PieceDetacheeFichePreparationDevis by using pieceDetacheeFichePreparationDevisDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of PieceDetacheeFichePreparationDevis
	 * @throws DataAccessException,ParseException
	 */
	public default List<PieceDetacheeFichePreparationDevis> getByCriteria(Request<PieceDetacheeFichePreparationDevisDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from PieceDetacheeFichePreparationDevis e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<PieceDetacheeFichePreparationDevis> query = em.createQuery(req, PieceDetacheeFichePreparationDevis.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of PieceDetacheeFichePreparationDevis by using pieceDetacheeFichePreparationDevisDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of PieceDetacheeFichePreparationDevis
	 *
	 */
	public default Long count(Request<PieceDetacheeFichePreparationDevisDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from PieceDetacheeFichePreparationDevis e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PieceDetacheeFichePreparationDevisDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		PieceDetacheeFichePreparationDevisDto dto = request.getData() != null ? request.getData() : new PieceDetacheeFichePreparationDevisDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PieceDetacheeFichePreparationDevisDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PieceDetacheeFichePreparationDevisDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getQuantite()!= null && dto.getQuantite() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("quantite", dto.getQuantite(), "e.quantite", "Integer", dto.getQuantiteParam(), param, index, locale));
			}
			if (dto.getPrixUnitaire()!= null && dto.getPrixUnitaire() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prixUnitaire", dto.getPrixUnitaire(), "e.prixUnitaire", "Float", dto.getPrixUnitaireParam(), param, index, locale));
			}
			if (dto.getTotal()!= null && dto.getTotal() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("total", dto.getTotal(), "e.total", "Float", dto.getTotalParam(), param, index, locale));
			}
			if (dto.getIsValided()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValided", dto.getIsValided(), "e.isValided", "Boolean", dto.getIsValidedParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getPieceDetacheeId()!= null && dto.getPieceDetacheeId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pieceDetacheeId", dto.getPieceDetacheeId(), "e.pieceDetachee.id", "Integer", dto.getPieceDetacheeIdParam(), param, index, locale));
			}
			if (dto.getFournisseurId()!= null && dto.getFournisseurId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("fournisseurId", dto.getFournisseurId(), "e.fournisseur.id", "Integer", dto.getFournisseurIdParam(), param, index, locale));
			}
			if (dto.getFichePreparationDevisId()!= null && dto.getFichePreparationDevisId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("fichePreparationDevisId", dto.getFichePreparationDevisId(), "e.fichePreparationDevis.id", "Integer", dto.getFichePreparationDevisIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPieceDetacheeLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pieceDetacheeLibelle", dto.getPieceDetacheeLibelle(), "e.pieceDetachee.libelle", "String", dto.getPieceDetacheeLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getFournisseurNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("fournisseurNom", dto.getFournisseurNom(), "e.fournisseur.nom", "String", dto.getFournisseurNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getFournisseurPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("fournisseurPrenom", dto.getFournisseurPrenom(), "e.fournisseur.prenom", "String", dto.getFournisseurPrenomParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
