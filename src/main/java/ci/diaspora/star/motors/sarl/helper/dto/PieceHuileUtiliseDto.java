
/*
 * Java dto for entity table piece_huile_utilise
 * Created on 2019-08-18 ( Time 17:15:14 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._PieceHuileUtiliseDto;

/**
 * DTO for table "piece_huile_utilise"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class PieceHuileUtiliseDto extends _PieceHuileUtiliseDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    designationPieceHuileUtiliseId ;
	/*
	 * 
	 */
    private Integer    ordreReparationId    ;
	/*
	 * 
	 */
    private Integer    quantite             ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String designationPieceHuileUtiliseLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  designationPieceHuileUtiliseIdParam;                     
	private SearchParam<Integer>  ordreReparationIdParam;                     
	private SearchParam<Integer>  quantiteParam         ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<String>   designationPieceHuileUtiliseLibelleParam;                     
    /**
     * Default constructor
     */
    public PieceHuileUtiliseDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "designationPieceHuileUtiliseId" field value
     * @param designationPieceHuileUtiliseId
     */
	public void setDesignationPieceHuileUtiliseId( Integer designationPieceHuileUtiliseId )
    {
        this.designationPieceHuileUtiliseId = designationPieceHuileUtiliseId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDesignationPieceHuileUtiliseId()
    {
        return this.designationPieceHuileUtiliseId;
    }

    /**
     * Set the "ordreReparationId" field value
     * @param ordreReparationId
     */
	public void setOrdreReparationId( Integer ordreReparationId )
    {
        this.ordreReparationId = ordreReparationId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getOrdreReparationId()
    {
        return this.ordreReparationId;
    }

    /**
     * Set the "quantite" field value
     * @param quantite
     */
	public void setQuantite( Integer quantite )
    {
        this.quantite = quantite ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getQuantite()
    {
        return this.quantite;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getDesignationPieceHuileUtiliseLibelle()
    {
        return this.designationPieceHuileUtiliseLibelle;
    }
	public void setDesignationPieceHuileUtiliseLibelle(String designationPieceHuileUtiliseLibelle)
    {
        this.designationPieceHuileUtiliseLibelle = designationPieceHuileUtiliseLibelle;
    }


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "designationPieceHuileUtiliseIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDesignationPieceHuileUtiliseIdParam(){
		return this.designationPieceHuileUtiliseIdParam;
	}
	/**
     * Set the "designationPieceHuileUtiliseIdParam" field value
     * @param designationPieceHuileUtiliseIdParam
     */
    public void setDesignationPieceHuileUtiliseIdParam( SearchParam<Integer> designationPieceHuileUtiliseIdParam ){
        this.designationPieceHuileUtiliseIdParam = designationPieceHuileUtiliseIdParam;
    }

	/**
     * Get the "ordreReparationIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getOrdreReparationIdParam(){
		return this.ordreReparationIdParam;
	}
	/**
     * Set the "ordreReparationIdParam" field value
     * @param ordreReparationIdParam
     */
    public void setOrdreReparationIdParam( SearchParam<Integer> ordreReparationIdParam ){
        this.ordreReparationIdParam = ordreReparationIdParam;
    }

	/**
     * Get the "quantiteParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getQuantiteParam(){
		return this.quantiteParam;
	}
	/**
     * Set the "quantiteParam" field value
     * @param quantiteParam
     */
    public void setQuantiteParam( SearchParam<Integer> quantiteParam ){
        this.quantiteParam = quantiteParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

		/**
     * Get the "designationPieceHuileUtiliseLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getDesignationPieceHuileUtiliseLibelleParam(){
		return this.designationPieceHuileUtiliseLibelleParam;
	}
	/**
     * Set the "designationPieceHuileUtiliseLibelleParam" field value
     * @param designationPieceHuileUtiliseLibelleParam
     */
    public void setDesignationPieceHuileUtiliseLibelleParam( SearchParam<String> designationPieceHuileUtiliseLibelleParam ){
        this.designationPieceHuileUtiliseLibelleParam = designationPieceHuileUtiliseLibelleParam;
    }


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		PieceHuileUtiliseDto other = (PieceHuileUtiliseDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("quantite:"+quantite);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
        return sb.toString();
    }
}
