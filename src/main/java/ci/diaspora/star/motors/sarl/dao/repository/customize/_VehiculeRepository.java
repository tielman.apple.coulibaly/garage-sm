package ci.diaspora.star.motors.sarl.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;

/**
 * Repository customize : Vehicule.
 */
@Repository
public interface _VehiculeRepository {
	default List<String> _generateCriteria(VehiculeDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds Vehicule by using immatriculation as a search criteria.
	 *
	 * @param immatriculation
	 * @return An Object Vehicule whose immatriculation is equals to the given immatriculation. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.immatriculation = :immatriculation and e.isDeleted = :isDeleted")
	Vehicule findVehiculeByImmatriculation(@Param("immatriculation")String immatriculation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using numeroChassis as a search criteria.
	 *
	 * @param numeroChassis
	 * @return An Object Vehicule whose numeroChassis is equals to the given numeroChassis. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.numeroChassis = :numeroChassis and e.isDeleted = :isDeleted")
	Vehicule findVehiculeByNumeroChassis(@Param("numeroChassis")String numeroChassis, @Param("isDeleted")Boolean isDeleted);

}
