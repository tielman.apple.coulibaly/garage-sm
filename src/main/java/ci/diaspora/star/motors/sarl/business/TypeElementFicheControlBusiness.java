


/*
 * Java transformer for entity table type_element_fiche_control
 * Created on 2019-08-18 ( Time 17:15:16 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "type_element_fiche_control"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class TypeElementFicheControlBusiness implements IBasicBusiness<Request<TypeElementFicheControlDto>, Response<TypeElementFicheControlDto>> {

	private Response<TypeElementFicheControlDto> response;
	@Autowired
	private TypeElementFicheControlRepository typeElementFicheControlRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private DesignationElementFicheControleTypeElementFicheControlRepository designationElementFicheControleTypeElementFicheControlRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private DesignationElementFicheControleTypeElementFicheControlBusiness designationElementFicheControleTypeElementFicheControlBusiness;



	public TypeElementFicheControlBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create TypeElementFicheControl by using TypeElementFicheControlDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<TypeElementFicheControlDto> create(Request<TypeElementFicheControlDto> request, Locale locale)  {
		slf4jLogger.info("----begin create TypeElementFicheControl-----");

		response = new Response<TypeElementFicheControlDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<TypeElementFicheControl> items = new ArrayList<TypeElementFicheControl>();

			for (TypeElementFicheControlDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("code", dto.getCode());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if typeElementFicheControl to insert do not exist
				TypeElementFicheControl existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("typeElementFicheControl -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = typeElementFicheControlRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("typeElementFicheControl -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les typeElementFicheControls", locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = typeElementFicheControlRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("typeElementFicheControl -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les typeElementFicheControls", locale));
					response.setHasError(true);
					return response;
				}

				TypeElementFicheControl entityToSave = null;
				entityToSave = TypeElementFicheControlTransformer.INSTANCE.toEntity(dto);
				entityToSave.setIsDeleted(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<TypeElementFicheControl> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = typeElementFicheControlRepository.save((Iterable<TypeElementFicheControl>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("typeElementFicheControl", locale));
					response.setHasError(true);
					return response;
				}
				List<TypeElementFicheControlDto> itemsDto = new ArrayList<TypeElementFicheControlDto>();
				for (TypeElementFicheControl entity : itemsSaved) {
					TypeElementFicheControlDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create TypeElementFicheControl-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update TypeElementFicheControl by using TypeElementFicheControlDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<TypeElementFicheControlDto> update(Request<TypeElementFicheControlDto> request, Locale locale)  {
		slf4jLogger.info("----begin update TypeElementFicheControl-----");

		response = new Response<TypeElementFicheControlDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<TypeElementFicheControl> items = new ArrayList<TypeElementFicheControl>();

			for (TypeElementFicheControlDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la typeElementFicheControl existe
				TypeElementFicheControl entityToSave = null;
				entityToSave = typeElementFicheControlRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeElementFicheControl -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				if (Utilities.notBlank(dto.getLibelle())) {
					TypeElementFicheControl existingEntity = typeElementFicheControlRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("typeElementFicheControl -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les typeElementFicheControls", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (Utilities.notBlank(dto.getCode())) {
					TypeElementFicheControl existingEntity = typeElementFicheControlRepository.findByCode(dto.getCode(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("typeElementFicheControl -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les typeElementFicheControls", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}
				
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<TypeElementFicheControl> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = typeElementFicheControlRepository.save((Iterable<TypeElementFicheControl>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("typeElementFicheControl", locale));
					response.setHasError(true);
					return response;
				}
				List<TypeElementFicheControlDto> itemsDto = new ArrayList<TypeElementFicheControlDto>();
				for (TypeElementFicheControl entity : itemsSaved) {
					TypeElementFicheControlDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update TypeElementFicheControl-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete TypeElementFicheControl by using TypeElementFicheControlDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<TypeElementFicheControlDto> delete(Request<TypeElementFicheControlDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete TypeElementFicheControl-----");

		response = new Response<TypeElementFicheControlDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<TypeElementFicheControl> items = new ArrayList<TypeElementFicheControl>();

			for (TypeElementFicheControlDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la typeElementFicheControl existe
				TypeElementFicheControl existingEntity = null;
				existingEntity = typeElementFicheControlRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeElementFicheControl -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// designationElementFicheControleTypeElementFicheControl
				List<DesignationElementFicheControleTypeElementFicheControl> listOfDesignationElementFicheControleTypeElementFicheControl = designationElementFicheControleTypeElementFicheControlRepository.findByTypeNiveauControlId(existingEntity.getId(), false);
				if (listOfDesignationElementFicheControleTypeElementFicheControl != null && !listOfDesignationElementFicheControleTypeElementFicheControl.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDesignationElementFicheControleTypeElementFicheControl.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				typeElementFicheControlRepository.save((Iterable<TypeElementFicheControl>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete TypeElementFicheControl-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete TypeElementFicheControl by using TypeElementFicheControlDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<TypeElementFicheControlDto> forceDelete(Request<TypeElementFicheControlDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete TypeElementFicheControl-----");

		response = new Response<TypeElementFicheControlDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<TypeElementFicheControl> items = new ArrayList<TypeElementFicheControl>();

			for (TypeElementFicheControlDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la typeElementFicheControl existe
				TypeElementFicheControl existingEntity = null;
				existingEntity = typeElementFicheControlRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeElementFicheControl -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// designationElementFicheControleTypeElementFicheControl
				List<DesignationElementFicheControleTypeElementFicheControl> listOfDesignationElementFicheControleTypeElementFicheControl = designationElementFicheControleTypeElementFicheControlRepository.findByTypeNiveauControlId(existingEntity.getId(), false);
				if (listOfDesignationElementFicheControleTypeElementFicheControl != null && !listOfDesignationElementFicheControleTypeElementFicheControl.isEmpty()){
					Request<DesignationElementFicheControleTypeElementFicheControlDto> deleteRequest = new Request<DesignationElementFicheControleTypeElementFicheControlDto>();
					deleteRequest.setDatas(DesignationElementFicheControleTypeElementFicheControlTransformer.INSTANCE.toDtos(listOfDesignationElementFicheControleTypeElementFicheControl));
					deleteRequest.setUser(request.getUser());
					Response<DesignationElementFicheControleTypeElementFicheControlDto> deleteResponse = designationElementFicheControleTypeElementFicheControlBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				typeElementFicheControlRepository.save((Iterable<TypeElementFicheControl>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete TypeElementFicheControl-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get TypeElementFicheControl by using TypeElementFicheControlDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<TypeElementFicheControlDto> getByCriteria(Request<TypeElementFicheControlDto> request, Locale locale) {
		slf4jLogger.info("----begin get TypeElementFicheControl-----");

		response = new Response<TypeElementFicheControlDto>();

		try {
			List<TypeElementFicheControl> items = null;
			items = typeElementFicheControlRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<TypeElementFicheControlDto> itemsDto = new ArrayList<TypeElementFicheControlDto>();
				for (TypeElementFicheControl entity : items) {
					TypeElementFicheControlDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(typeElementFicheControlRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("typeElementFicheControl", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get TypeElementFicheControl-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full TypeElementFicheControlDto by using TypeElementFicheControl as object.
	 *
	 * @param entity, locale
	 * @return TypeElementFicheControlDto
	 *
	 */
	private TypeElementFicheControlDto getFullInfos(TypeElementFicheControl entity, Integer size, Locale locale) throws Exception {
		TypeElementFicheControlDto dto = TypeElementFicheControlTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
