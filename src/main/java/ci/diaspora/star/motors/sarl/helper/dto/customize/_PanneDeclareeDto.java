
/*
 * Java dto for entity table panne_declaree 
 * Created on 2019-08-18 ( Time 17:15:13 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * DTO customize for table "panne_declaree"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _PanneDeclareeDto {
	
	protected String observations;
	protected String diagnosticAtelier;
	
	protected Boolean isDeclaredByClient;

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public Boolean getIsDeclaredByClient() {
		return isDeclaredByClient;
	}

	public void setIsDeclaredByClient(Boolean isDeclaredByClient) {
		this.isDeclaredByClient = isDeclaredByClient;
	}

	public String getDiagnosticAtelier() {
		return diagnosticAtelier;
	}

	public void setDiagnosticAtelier(String diagnosticAtelier) {
		this.diagnosticAtelier = diagnosticAtelier;
	}
	
}
