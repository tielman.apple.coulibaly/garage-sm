

/*
 * Java transformer for entity table facture 
 * Created on 2019-08-18 ( Time 17:15:10 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "facture"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface FactureTransformer {

	FactureTransformer INSTANCE = Mappers.getMapper(FactureTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.userDirection.id", target="userDirectionId"),
		@Mapping(source="entity.userDirection.nom", target="userDirectionNom"),
		@Mapping(source="entity.userDirection.prenom", target="userDirectionPrenom"),
		@Mapping(source="entity.userDirection.login", target="userDirectionLogin"),
		@Mapping(source="entity.etat.id", target="etatId"),
		@Mapping(source="entity.etat.code", target="etatCode"),
		@Mapping(source="entity.etat.libelle", target="etatLibelle"),
		@Mapping(source="entity.userChefAtelier.id", target="userChefAtelierId"),
		@Mapping(source="entity.userChefAtelier.nom", target="userChefAtelierNom"),
		@Mapping(source="entity.userChefAtelier.prenom", target="userChefAtelierPrenom"),
		@Mapping(source="entity.userChefAtelier.login", target="userChefAtelierLogin"),
		@Mapping(source="entity.userResponsableTechnique.id", target="userResponsableTechniqueId"),
		@Mapping(source="entity.userResponsableTechnique.nom", target="userResponsableTechniqueNom"),
		@Mapping(source="entity.userResponsableTechnique.prenom", target="userResponsableTechniquePrenom"),
		@Mapping(source="entity.userResponsableTechnique.login", target="userResponsableTechniqueLogin"),
		@Mapping(source="entity.ficheReception.id", target="ficheReceptionId"),
	})
	FactureDto toDto(Facture entity) throws ParseException;

    List<FactureDto> toDtos(List<Facture> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.isValidedByChefAtelier", target="isValidedByChefAtelier"),
		@Mapping(source="dto.isValidedByResponsableTechnique", target="isValidedByResponsableTechnique"),
		@Mapping(source="dto.isValidedByDirection", target="isValidedByDirection"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="userDirection", target="userDirection"),
		@Mapping(source="etat", target="etat"),
		@Mapping(source="userChefAtelier", target="userChefAtelier"),
		@Mapping(source="userResponsableTechnique", target="userResponsableTechnique"),
		@Mapping(source="ficheReception", target="ficheReception"),
	})
    Facture toEntity(FactureDto dto, User userDirection, Etat etat, User userChefAtelier, User userResponsableTechnique, FicheReception ficheReception) throws ParseException;

    //List<Facture> toEntities(List<FactureDto> dtos) throws ParseException;

}
