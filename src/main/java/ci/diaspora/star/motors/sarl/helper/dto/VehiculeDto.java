
/*
 * Java dto for entity table vehicule
 * Created on 2019-09-10 ( Time 09:02:06 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._VehiculeDto;

/**
 * DTO for table "vehicule"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class VehiculeDto extends _VehiculeDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    clientId             ;
	/*
	 * 
	 */
    private Integer    typeId               ;
	/*
	 * 
	 */
    private Integer    marqueId             ;
	/*
	 * 
	 */
    private Integer    energieId            ;
	/*
	 * 
	 */
    private Integer    couleurVehiculeId    ;
	/*
	 * 
	 */
    private String     immatriculation      ;
	/*
	 * 
	 */
    private String     numeroChassis        ;
	/*
	 * 
	 */
	private String     dateProchaineRevision ;
	/*
	 * 
	 */
	private String     dateVisiteTechnique  ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String typeVehiculeLibelle;
	private String marqueLibelle;
	private String couleurVehiculeLibelle;
	private String energieVehiculeLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  clientIdParam         ;                     
	private SearchParam<Integer>  typeIdParam           ;                     
	private SearchParam<Integer>  marqueIdParam         ;                     
	private SearchParam<Integer>  energieIdParam        ;                     
	private SearchParam<Integer>  couleurVehiculeIdParam;                     
	private SearchParam<String>   immatriculationParam  ;                     
	private SearchParam<String>   numeroChassisParam    ;                     

		private SearchParam<String>   dateProchaineRevisionParam;                     

		private SearchParam<String>   dateVisiteTechniqueParam;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<String>   typeVehiculeLibelleParam;                     
	private SearchParam<String>   marqueLibelleParam    ;                     
	private SearchParam<String>   couleurVehiculeLibelleParam;                     
	private SearchParam<String>   energieVehiculeLibelleParam;                     
    /**
     * Default constructor
     */
    public VehiculeDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "clientId" field value
     * @param clientId
     */
	public void setClientId( Integer clientId )
    {
        this.clientId = clientId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getClientId()
    {
        return this.clientId;
    }

    /**
     * Set the "typeId" field value
     * @param typeId
     */
	public void setTypeId( Integer typeId )
    {
        this.typeId = typeId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getTypeId()
    {
        return this.typeId;
    }

    /**
     * Set the "marqueId" field value
     * @param marqueId
     */
	public void setMarqueId( Integer marqueId )
    {
        this.marqueId = marqueId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getMarqueId()
    {
        return this.marqueId;
    }

    /**
     * Set the "energieId" field value
     * @param energieId
     */
	public void setEnergieId( Integer energieId )
    {
        this.energieId = energieId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getEnergieId()
    {
        return this.energieId;
    }

    /**
     * Set the "couleurVehiculeId" field value
     * @param couleurVehiculeId
     */
	public void setCouleurVehiculeId( Integer couleurVehiculeId )
    {
        this.couleurVehiculeId = couleurVehiculeId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCouleurVehiculeId()
    {
        return this.couleurVehiculeId;
    }

    /**
     * Set the "immatriculation" field value
     * @param immatriculation
     */
	public void setImmatriculation( String immatriculation )
    {
        this.immatriculation = immatriculation ;
    }
    /**
     * 
     * @return the field value
     */
	public String getImmatriculation()
    {
        return this.immatriculation;
    }

    /**
     * Set the "numeroChassis" field value
     * @param numeroChassis
     */
	public void setNumeroChassis( String numeroChassis )
    {
        this.numeroChassis = numeroChassis ;
    }
    /**
     * 
     * @return the field value
     */
	public String getNumeroChassis()
    {
        return this.numeroChassis;
    }

    /**
     * Set the "dateProchaineRevision" field value
     * @param dateProchaineRevision
     */
	public void setDateProchaineRevision( String dateProchaineRevision )
    {
        this.dateProchaineRevision = dateProchaineRevision ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateProchaineRevision()
    {
        return this.dateProchaineRevision;
    }

    /**
     * Set the "dateVisiteTechnique" field value
     * @param dateVisiteTechnique
     */
	public void setDateVisiteTechnique( String dateVisiteTechnique )
    {
        this.dateVisiteTechnique = dateVisiteTechnique ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateVisiteTechnique()
    {
        return this.dateVisiteTechnique;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getTypeVehiculeLibelle()
    {
        return this.typeVehiculeLibelle;
    }
	public void setTypeVehiculeLibelle(String typeVehiculeLibelle)
    {
        this.typeVehiculeLibelle = typeVehiculeLibelle;
    }

	public String getMarqueLibelle()
    {
        return this.marqueLibelle;
    }
	public void setMarqueLibelle(String marqueLibelle)
    {
        this.marqueLibelle = marqueLibelle;
    }

	public String getCouleurVehiculeLibelle()
    {
        return this.couleurVehiculeLibelle;
    }
	public void setCouleurVehiculeLibelle(String couleurVehiculeLibelle)
    {
        this.couleurVehiculeLibelle = couleurVehiculeLibelle;
    }

	public String getEnergieVehiculeLibelle()
    {
        return this.energieVehiculeLibelle;
    }
	public void setEnergieVehiculeLibelle(String energieVehiculeLibelle)
    {
        this.energieVehiculeLibelle = energieVehiculeLibelle;
    }


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "clientIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getClientIdParam(){
		return this.clientIdParam;
	}
	/**
     * Set the "clientIdParam" field value
     * @param clientIdParam
     */
    public void setClientIdParam( SearchParam<Integer> clientIdParam ){
        this.clientIdParam = clientIdParam;
    }

	/**
     * Get the "typeIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getTypeIdParam(){
		return this.typeIdParam;
	}
	/**
     * Set the "typeIdParam" field value
     * @param typeIdParam
     */
    public void setTypeIdParam( SearchParam<Integer> typeIdParam ){
        this.typeIdParam = typeIdParam;
    }

	/**
     * Get the "marqueIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getMarqueIdParam(){
		return this.marqueIdParam;
	}
	/**
     * Set the "marqueIdParam" field value
     * @param marqueIdParam
     */
    public void setMarqueIdParam( SearchParam<Integer> marqueIdParam ){
        this.marqueIdParam = marqueIdParam;
    }

	/**
     * Get the "energieIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getEnergieIdParam(){
		return this.energieIdParam;
	}
	/**
     * Set the "energieIdParam" field value
     * @param energieIdParam
     */
    public void setEnergieIdParam( SearchParam<Integer> energieIdParam ){
        this.energieIdParam = energieIdParam;
    }

	/**
     * Get the "couleurVehiculeIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCouleurVehiculeIdParam(){
		return this.couleurVehiculeIdParam;
	}
	/**
     * Set the "couleurVehiculeIdParam" field value
     * @param couleurVehiculeIdParam
     */
    public void setCouleurVehiculeIdParam( SearchParam<Integer> couleurVehiculeIdParam ){
        this.couleurVehiculeIdParam = couleurVehiculeIdParam;
    }

	/**
     * Get the "immatriculationParam" field value
     * @return the field value
     */
	public SearchParam<String> getImmatriculationParam(){
		return this.immatriculationParam;
	}
	/**
     * Set the "immatriculationParam" field value
     * @param immatriculationParam
     */
    public void setImmatriculationParam( SearchParam<String> immatriculationParam ){
        this.immatriculationParam = immatriculationParam;
    }

	/**
     * Get the "numeroChassisParam" field value
     * @return the field value
     */
	public SearchParam<String> getNumeroChassisParam(){
		return this.numeroChassisParam;
	}
	/**
     * Set the "numeroChassisParam" field value
     * @param numeroChassisParam
     */
    public void setNumeroChassisParam( SearchParam<String> numeroChassisParam ){
        this.numeroChassisParam = numeroChassisParam;
    }

	/**
     * Get the "dateProchaineRevisionParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateProchaineRevisionParam(){
		return this.dateProchaineRevisionParam;
	}
	/**
     * Set the "dateProchaineRevisionParam" field value
     * @param dateProchaineRevisionParam
     */
    public void setDateProchaineRevisionParam( SearchParam<String> dateProchaineRevisionParam ){
        this.dateProchaineRevisionParam = dateProchaineRevisionParam;
    }

	/**
     * Get the "dateVisiteTechniqueParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateVisiteTechniqueParam(){
		return this.dateVisiteTechniqueParam;
	}
	/**
     * Set the "dateVisiteTechniqueParam" field value
     * @param dateVisiteTechniqueParam
     */
    public void setDateVisiteTechniqueParam( SearchParam<String> dateVisiteTechniqueParam ){
        this.dateVisiteTechniqueParam = dateVisiteTechniqueParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

		/**
     * Get the "typeVehiculeLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getTypeVehiculeLibelleParam(){
		return this.typeVehiculeLibelleParam;
	}
	/**
     * Set the "typeVehiculeLibelleParam" field value
     * @param typeVehiculeLibelleParam
     */
    public void setTypeVehiculeLibelleParam( SearchParam<String> typeVehiculeLibelleParam ){
        this.typeVehiculeLibelleParam = typeVehiculeLibelleParam;
    }

		/**
     * Get the "marqueLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getMarqueLibelleParam(){
		return this.marqueLibelleParam;
	}
	/**
     * Set the "marqueLibelleParam" field value
     * @param marqueLibelleParam
     */
    public void setMarqueLibelleParam( SearchParam<String> marqueLibelleParam ){
        this.marqueLibelleParam = marqueLibelleParam;
    }

		/**
     * Get the "couleurVehiculeLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getCouleurVehiculeLibelleParam(){
		return this.couleurVehiculeLibelleParam;
	}
	/**
     * Set the "couleurVehiculeLibelleParam" field value
     * @param couleurVehiculeLibelleParam
     */
    public void setCouleurVehiculeLibelleParam( SearchParam<String> couleurVehiculeLibelleParam ){
        this.couleurVehiculeLibelleParam = couleurVehiculeLibelleParam;
    }

		/**
     * Get the "energieVehiculeLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getEnergieVehiculeLibelleParam(){
		return this.energieVehiculeLibelleParam;
	}
	/**
     * Set the "energieVehiculeLibelleParam" field value
     * @param energieVehiculeLibelleParam
     */
    public void setEnergieVehiculeLibelleParam( SearchParam<String> energieVehiculeLibelleParam ){
        this.energieVehiculeLibelleParam = energieVehiculeLibelleParam;
    }


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		VehiculeDto other = (VehiculeDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("immatriculation:"+immatriculation);
		sb.append("|");
		sb.append("numeroChassis:"+numeroChassis);
		sb.append("|");
		sb.append("dateProchaineRevision:"+dateProchaineRevision);
		sb.append("|");
		sb.append("dateVisiteTechnique:"+dateVisiteTechnique);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
        return sb.toString();
    }
}
