

/*
 * Java transformer for entity table element_etat_vehicule_fiche_reception 
 * Created on 2019-08-18 ( Time 17:15:09 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "element_etat_vehicule_fiche_reception"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface ElementEtatVehiculeFicheReceptionTransformer {

	ElementEtatVehiculeFicheReceptionTransformer INSTANCE = Mappers.getMapper(ElementEtatVehiculeFicheReceptionTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.ficheReception.id", target="ficheReceptionId"),
		@Mapping(source="entity.elementEtatVehicule.id", target="elementEtatVehiculeId"),
		@Mapping(source="entity.elementEtatVehicule.libelle", target="elementEtatVehiculeLibelle"),
	})
	ElementEtatVehiculeFicheReceptionDto toDto(ElementEtatVehiculeFicheReception entity) throws ParseException;

    List<ElementEtatVehiculeFicheReceptionDto> toDtos(List<ElementEtatVehiculeFicheReception> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.isGood", target="isGood"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="ficheReception", target="ficheReception"),
		@Mapping(source="elementEtatVehicule", target="elementEtatVehicule"),
	})
    ElementEtatVehiculeFicheReception toEntity(ElementEtatVehiculeFicheReceptionDto dto, FicheReception ficheReception, ElementEtatVehicule elementEtatVehicule) throws ParseException;

    //List<ElementEtatVehiculeFicheReception> toEntities(List<ElementEtatVehiculeFicheReceptionDto> dtos) throws ParseException;

}
