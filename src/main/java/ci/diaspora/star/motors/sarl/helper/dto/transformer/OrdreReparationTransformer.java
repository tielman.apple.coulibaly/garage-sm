

/*
 * Java transformer for entity table ordre_reparation 
 * Created on 2019-08-18 ( Time 17:15:13 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "ordre_reparation"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface OrdreReparationTransformer {

	OrdreReparationTransformer INSTANCE = Mappers.getMapper(OrdreReparationTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),

		@Mapping(source="entity.dateValidationReception", dateFormat="dd/MM/yyyy",target="dateValidationReception"),

		@Mapping(source="entity.dateValidationOrdreChefAtelier", dateFormat="dd/MM/yyyy",target="dateValidationOrdreChefAtelier"),

		@Mapping(source="entity.dateValidationTravauxChefAtelier", dateFormat="dd/MM/yyyy",target="dateValidationTravauxChefAtelier"),

		@Mapping(source="entity.dateValidationOrdreSuperviseur", dateFormat="dd/MM/yyyy",target="dateValidationOrdreSuperviseur"),

		@Mapping(source="entity.dateValidationTravauxSuperviseur", dateFormat="dd/MM/yyyy",target="dateValidationTravauxSuperviseur"),
		@Mapping(source="entity.userSuperviseur.id", target="userSuperviseurId"),
		@Mapping(source="entity.userSuperviseur.nom", target="userSuperviseurNom"),
		@Mapping(source="entity.userSuperviseur.prenom", target="userSuperviseurPrenom"),
		@Mapping(source="entity.userSuperviseur.login", target="userSuperviseurLogin"),
		@Mapping(source="entity.typeOrdreReparation.id", target="typeOrdreReparationId"),
		@Mapping(source="entity.typeOrdreReparation.libelle", target="typeOrdreReparationLibelle"),
		@Mapping(source="entity.etatOrdre.id", target="etatOrdreId"),
		@Mapping(source="entity.etatOrdre.code", target="etatOrdreCode"),
		@Mapping(source="entity.etatOrdre.libelle", target="etatOrdreLibelle"),
		@Mapping(source="entity.ficheReception.id", target="ficheReceptionId"),
		@Mapping(source="entity.etatTravaux.id", target="etatTravauxId"),
		@Mapping(source="entity.etatTravaux.code", target="etatTravauxCode"),
		@Mapping(source="entity.etatTravaux.libelle", target="etatTravauxLibelle"),
		@Mapping(source="entity.userChefAtelier.id", target="userChefAtelierId"),
		@Mapping(source="entity.userChefAtelier.nom", target="userChefAtelierNom"),
		@Mapping(source="entity.userChefAtelier.prenom", target="userChefAtelierPrenom"),
		@Mapping(source="entity.userChefAtelier.login", target="userChefAtelierLogin"),
		@Mapping(source="entity.userReception.id", target="userReceptionId"),
		@Mapping(source="entity.userReception.nom", target="userReceptionNom"),
		@Mapping(source="entity.userReception.prenom", target="userReceptionPrenom"),
		@Mapping(source="entity.userReception.login", target="userReceptionLogin"),
	})
	OrdreReparationDto toDto(OrdreReparation entity) throws ParseException;

    List<OrdreReparationDto> toDtos(List<OrdreReparation> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isValidedByChefAtelierOrdre", target="isValidedByChefAtelierOrdre"),
		@Mapping(source="dto.isValidedByChefAtelierTravaux", target="isValidedByChefAtelierTravaux"),
		@Mapping(source="dto.isValidedByReception", target="isValidedByReception"),
		@Mapping(source="dto.isValidedBySuperviseurOrdre", target="isValidedBySuperviseurOrdre"),
		@Mapping(source="dto.isValidedBySuperviseurTravaux", target="isValidedBySuperviseurTravaux"),
		@Mapping(source="dto.observationsChefAtelier", target="observationsChefAtelier"),
		@Mapping(source="dto.observationsSuperviseur", target="observationsSuperviseur"),
		@Mapping(source="dto.dateValidationReception", dateFormat="dd/MM/yyyy",target="dateValidationReception"),
		@Mapping(source="dto.dateValidationOrdreChefAtelier", dateFormat="dd/MM/yyyy",target="dateValidationOrdreChefAtelier"),
		@Mapping(source="dto.dateValidationTravauxChefAtelier", dateFormat="dd/MM/yyyy",target="dateValidationTravauxChefAtelier"),
		@Mapping(source="dto.dateValidationOrdreSuperviseur", dateFormat="dd/MM/yyyy",target="dateValidationOrdreSuperviseur"),
		@Mapping(source="dto.dateValidationTravauxSuperviseur", dateFormat="dd/MM/yyyy",target="dateValidationTravauxSuperviseur"),
		@Mapping(source="userSuperviseur", target="userSuperviseur"),
		@Mapping(source="typeOrdreReparation", target="typeOrdreReparation"),
		@Mapping(source="etatOrdre", target="etatOrdre"),
		@Mapping(source="ficheReception", target="ficheReception"),
		@Mapping(source="etatTravaux", target="etatTravaux"),
		@Mapping(source="userChefAtelier", target="userChefAtelier"),
		@Mapping(source="userReception", target="userReception"),
	})
    OrdreReparation toEntity(OrdreReparationDto dto, User userSuperviseur, TypeOrdreReparation typeOrdreReparation, Etat etatOrdre, FicheReception ficheReception, Etat etatTravaux, User userChefAtelier, User userReception) throws ParseException;

    //List<OrdreReparation> toEntities(List<OrdreReparationDto> dtos) throws ParseException;

}
