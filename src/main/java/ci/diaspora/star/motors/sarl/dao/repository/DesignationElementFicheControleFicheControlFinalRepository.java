package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._DesignationElementFicheControleFicheControlFinalRepository;

/**
 * Repository : DesignationElementFicheControleFicheControlFinal.
 */
@Repository
public interface DesignationElementFicheControleFicheControlFinalRepository extends JpaRepository<DesignationElementFicheControleFicheControlFinal, Integer>, _DesignationElementFicheControleFicheControlFinalRepository {
	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object DesignationElementFicheControleFicheControlFinal whose id is equals to the given id. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.id = :id and e.isDeleted = :isDeleted")
	DesignationElementFicheControleFicheControlFinal findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using observations as a search criteria.
	 *
	 * @param observations
	 * @return An Object DesignationElementFicheControleFicheControlFinal whose observations is equals to the given observations. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.observations = :observations and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByObservations(@Param("observations")String observations, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using isGood as a search criteria.
	 *
	 * @param isGood
	 * @return An Object DesignationElementFicheControleFicheControlFinal whose isGood is equals to the given isGood. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.isGood = :isGood and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByIsGood(@Param("isGood")Boolean isGood, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object DesignationElementFicheControleFicheControlFinal whose isDeleted is equals to the given isDeleted. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object DesignationElementFicheControleFicheControlFinal whose createdAt is equals to the given createdAt. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object DesignationElementFicheControleFicheControlFinal whose updatedAt is equals to the given updatedAt. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object DesignationElementFicheControleFicheControlFinal whose deletedAt is equals to the given deletedAt. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object DesignationElementFicheControleFicheControlFinal whose createdBy is equals to the given createdBy. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object DesignationElementFicheControleFicheControlFinal whose updatedBy is equals to the given updatedBy. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object DesignationElementFicheControleFicheControlFinal whose deletedBy is equals to the given deletedBy. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using designationNiveauControlId as a search criteria.
	 *
	 * @param designationNiveauControlId
	 * @return A list of Object DesignationElementFicheControleFicheControlFinal whose designationNiveauControlId is equals to the given designationNiveauControlId. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.designationElementFicheControle.id = :designationNiveauControlId and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByDesignationNiveauControlId(@Param("designationNiveauControlId")Integer designationNiveauControlId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one DesignationElementFicheControleFicheControlFinal by using designationNiveauControlId as a search criteria.
   *
   * @param designationNiveauControlId
   * @return An Object DesignationElementFicheControleFicheControlFinal whose designationNiveauControlId is equals to the given designationNiveauControlId. If
   *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
   */
  @Query("select e from DesignationElementFicheControleFicheControlFinal e where e.designationElementFicheControle.id = :designationNiveauControlId and e.isDeleted = :isDeleted")
  DesignationElementFicheControleFicheControlFinal findDesignationElementFicheControleFicheControlFinalByDesignationNiveauControlId(@Param("designationNiveauControlId")Integer designationNiveauControlId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds DesignationElementFicheControleFicheControlFinal by using ficheControlFinalId as a search criteria.
	 *
	 * @param ficheControlFinalId
	 * @return A list of Object DesignationElementFicheControleFicheControlFinal whose ficheControlFinalId is equals to the given ficheControlFinalId. If
	 *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleFicheControlFinal e where e.ficheControlFinal.id = :ficheControlFinalId and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleFicheControlFinal> findByFicheControlFinalId(@Param("ficheControlFinalId")Integer ficheControlFinalId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one DesignationElementFicheControleFicheControlFinal by using ficheControlFinalId as a search criteria.
   *
   * @param ficheControlFinalId
   * @return An Object DesignationElementFicheControleFicheControlFinal whose ficheControlFinalId is equals to the given ficheControlFinalId. If
   *         no DesignationElementFicheControleFicheControlFinal is found, this method returns null.
   */
  @Query("select e from DesignationElementFicheControleFicheControlFinal e where e.ficheControlFinal.id = :ficheControlFinalId and e.isDeleted = :isDeleted")
  DesignationElementFicheControleFicheControlFinal findDesignationElementFicheControleFicheControlFinalByFicheControlFinalId(@Param("ficheControlFinalId")Integer ficheControlFinalId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of DesignationElementFicheControleFicheControlFinal by using designationElementFicheControleFicheControlFinalDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of DesignationElementFicheControleFicheControlFinal
	 * @throws DataAccessException,ParseException
	 */
	public default List<DesignationElementFicheControleFicheControlFinal> getByCriteria(Request<DesignationElementFicheControleFicheControlFinalDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from DesignationElementFicheControleFicheControlFinal e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<DesignationElementFicheControleFicheControlFinal> query = em.createQuery(req, DesignationElementFicheControleFicheControlFinal.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of DesignationElementFicheControleFicheControlFinal by using designationElementFicheControleFicheControlFinalDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of DesignationElementFicheControleFicheControlFinal
	 *
	 */
	public default Long count(Request<DesignationElementFicheControleFicheControlFinalDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from DesignationElementFicheControleFicheControlFinal e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<DesignationElementFicheControleFicheControlFinalDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		DesignationElementFicheControleFicheControlFinalDto dto = request.getData() != null ? request.getData() : new DesignationElementFicheControleFicheControlFinalDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (DesignationElementFicheControleFicheControlFinalDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(DesignationElementFicheControleFicheControlFinalDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getObservations())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("observations", dto.getObservations(), "e.observations", "String", dto.getObservationsParam(), param, index, locale));
			}
			if (dto.getIsGood()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isGood", dto.getIsGood(), "e.isGood", "Boolean", dto.getIsGoodParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getDesignationNiveauControlId()!= null && dto.getDesignationNiveauControlId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("designationNiveauControlId", dto.getDesignationNiveauControlId(), "e.designationElementFicheControle.id", "Integer", dto.getDesignationNiveauControlIdParam(), param, index, locale));
			}
			if (dto.getFicheControlFinalId()!= null && dto.getFicheControlFinalId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ficheControlFinalId", dto.getFicheControlFinalId(), "e.ficheControlFinal.id", "Integer", dto.getFicheControlFinalIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDesignationElementFicheControleLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("designationElementFicheControleLibelle", dto.getDesignationElementFicheControleLibelle(), "e.designationElementFicheControle.libelle", "String", dto.getDesignationElementFicheControleLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
