

/*
 * Java transformer for entity table travaux_effectues 
 * Created on 2019-08-18 ( Time 17:15:15 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "travaux_effectues"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface TravauxEffectuesTransformer {

	TravauxEffectuesTransformer INSTANCE = Mappers.getMapper(TravauxEffectuesTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateDebutTravaux", dateFormat="dd/MM/yyyy",target="dateDebutTravaux"),

		@Mapping(source="entity.dateFinTravaux", dateFormat="dd/MM/yyyy",target="dateFinTravaux"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.designationTravauxEffectues.id", target="designationTravauxEffectuesId"),
		@Mapping(source="entity.designationTravauxEffectues.libelle", target="designationTravauxEffectuesLibelle"),
		@Mapping(source="entity.user.id", target="userTechnicienId"),
		@Mapping(source="entity.user.nom", target="userNom"),
		@Mapping(source="entity.user.prenom", target="userPrenom"),
		@Mapping(source="entity.user.login", target="userLogin"),
		@Mapping(source="entity.ordreReparation.id", target="ordreReparationId"),
	})
	TravauxEffectuesDto toDto(TravauxEffectues entity) throws ParseException;

    List<TravauxEffectuesDto> toDtos(List<TravauxEffectues> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.dateDebutTravaux", dateFormat="dd/MM/yyyy",target="dateDebutTravaux"),
		@Mapping(source="dto.dateFinTravaux", dateFormat="dd/MM/yyyy",target="dateFinTravaux"),
		@Mapping(source="dto.isSigned", target="isSigned"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="designationTravauxEffectues", target="designationTravauxEffectues"),
		@Mapping(source="user", target="user"),
		@Mapping(source="ordreReparation", target="ordreReparation"),
	})
    TravauxEffectues toEntity(TravauxEffectuesDto dto, DesignationTravauxEffectues designationTravauxEffectues, User user, OrdreReparation ordreReparation) throws ParseException;

    //List<TravauxEffectues> toEntities(List<TravauxEffectuesDto> dtos) throws ParseException;

}
