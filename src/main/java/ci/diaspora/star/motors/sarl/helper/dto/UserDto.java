
/*
 * Java dto for entity table user
 * Created on 2019-08-19 ( Time 15:02:29 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._UserDto;

/**
 * DTO for table "user"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class UserDto extends _UserDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     nom                  ;
	/*
	 * 
	 */
    private String     prenom               ;
	/*
	 * 
	 */
    private String     email                ;
	/*
	 * 
	 */
    private String     login                ;
	/*
	 * 
	 */
    private String     password             ;
	/*
	 * 
	 */
    private String     telephone            ;
	/*
	 * 
	 */
    private String     token                ;
    /*
	 * 
	 */
    private Boolean    isValidToken         ;
    
    /*
	 * 
	 */
    private Boolean    isSuperUser         ;
    
    
	/*
	 * 
	 */
	private String     dateExpireToken      ;
	/*
	 * 
	 */
    private String     lastPassword         ;
	/*
	 * 
	 */
    private String     signatureUrl         ;
	/*
	 * 
	 */
    private Integer    roleId               ;
	/*
	 * 
	 */
    private Boolean    isLocked             ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String roleCode;
	private String roleLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   nomParam              ;                     
	private SearchParam<String>   prenomParam           ;                     
	private SearchParam<String>   emailParam            ;                     
	private SearchParam<String>   loginParam            ;                     
	private SearchParam<String>   passwordParam         ;                     
	private SearchParam<String>   telephoneParam        ;                     
	private SearchParam<String>   tokenParam            ;                     
	private SearchParam<Boolean>  isValidTokenParam     ;  
	private SearchParam<Boolean>  isSuperUserParam     ;  
	

		private SearchParam<String>   dateExpireTokenParam  ;                     
	private SearchParam<String>   lastPasswordParam     ;                     
	private SearchParam<String>   signatureUrlParam     ;                     
	private SearchParam<Integer>  roleIdParam           ;                     
	private SearchParam<Boolean>  isLockedParam         ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<String>   roleCodeParam         ;                     
	private SearchParam<String>   roleLibelleParam      ;                     
    /**
     * Default constructor
     */
    public UserDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "nom" field value
     * @param nom
     */
	public void setNom( String nom )
    {
        this.nom = nom ;
    }
    /**
     * 
     * @return the field value
     */
	public String getNom()
    {
        return this.nom;
    }

    /**
     * Set the "prenom" field value
     * @param prenom
     */
	public void setPrenom( String prenom )
    {
        this.prenom = prenom ;
    }
    /**
     * 
     * @return the field value
     */
	public String getPrenom()
    {
        return this.prenom;
    }

    /**
     * Set the "email" field value
     * @param email
     */
	public void setEmail( String email )
    {
        this.email = email ;
    }
    /**
     * 
     * @return the field value
     */
	public String getEmail()
    {
        return this.email;
    }

    /**
     * Set the "login" field value
     * @param login
     */
	public void setLogin( String login )
    {
        this.login = login ;
    }
    /**
     * 
     * @return the field value
     */
	public String getLogin()
    {
        return this.login;
    }

    /**
     * Set the "password" field value
     * @param password
     */
	public void setPassword( String password )
    {
        this.password = password ;
    }
    /**
     * 
     * @return the field value
     */
	public String getPassword()
    {
        return this.password;
    }

    /**
     * Set the "telephone" field value
     * @param telephone
     */
	public void setTelephone( String telephone )
    {
        this.telephone = telephone ;
    }
    /**
     * 
     * @return the field value
     */
	public String getTelephone()
    {
        return this.telephone;
    }

    /**
     * Set the "token" field value
     * @param token
     */
	public void setToken( String token )
    {
        this.token = token ;
    }
    /**
     * 
     * @return the field value
     */
	public String getToken()
    {
        return this.token;
    }

    /**
     * Set the "isValidToken" field value
     * @param isValidToken
     */
	public void setIsValidToken( Boolean isValidToken )
    {
        this.isValidToken = isValidToken ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidToken()
    {
        return this.isValidToken;
    }

    /**
     * Set the "dateExpireToken" field value
     * @param dateExpireToken
     */
	public void setDateExpireToken( String dateExpireToken )
    {
        this.dateExpireToken = dateExpireToken ;
    }
	
	
    public Boolean getIsSuperUser() {
		return isSuperUser;
	}

	public void setIsSuperUser(Boolean isSuperUser) {
		this.isSuperUser = isSuperUser;
	}

	public SearchParam<Boolean> getIsSuperUserParam() {
		return isSuperUserParam;
	}

	public void setIsSuperUserParam(SearchParam<Boolean> isSuperUserParam) {
		this.isSuperUserParam = isSuperUserParam;
	}

	/**
     * 
     * @return the field value
     */
	public String getDateExpireToken()
    {
        return this.dateExpireToken;
    }

    /**
     * Set the "lastPassword" field value
     * @param lastPassword
     */
	public void setLastPassword( String lastPassword )
    {
        this.lastPassword = lastPassword ;
    }
    /**
     * 
     * @return the field value
     */
	public String getLastPassword()
    {
        return this.lastPassword;
    }

    /**
     * Set the "signatureUrl" field value
     * @param signatureUrl
     */
	public void setSignatureUrl( String signatureUrl )
    {
        this.signatureUrl = signatureUrl ;
    }
    /**
     * 
     * @return the field value
     */
	public String getSignatureUrl()
    {
        return this.signatureUrl;
    }

    /**
     * Set the "roleId" field value
     * @param roleId
     */
	public void setRoleId( Integer roleId )
    {
        this.roleId = roleId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getRoleId()
    {
        return this.roleId;
    }

    /**
     * Set the "isLocked" field value
     * @param isLocked
     */
	public void setIsLocked( Boolean isLocked )
    {
        this.isLocked = isLocked ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsLocked()
    {
        return this.isLocked;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getRoleCode()
    {
        return this.roleCode;
    }
	public void setRoleCode(String roleCode)
    {
        this.roleCode = roleCode;
    }

	public String getRoleLibelle()
    {
        return this.roleLibelle;
    }
	public void setRoleLibelle(String roleLibelle)
    {
        this.roleLibelle = roleLibelle;
    }


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "nomParam" field value
     * @return the field value
     */
	public SearchParam<String> getNomParam(){
		return this.nomParam;
	}
	/**
     * Set the "nomParam" field value
     * @param nomParam
     */
    public void setNomParam( SearchParam<String> nomParam ){
        this.nomParam = nomParam;
    }

	/**
     * Get the "prenomParam" field value
     * @return the field value
     */
	public SearchParam<String> getPrenomParam(){
		return this.prenomParam;
	}
	/**
     * Set the "prenomParam" field value
     * @param prenomParam
     */
    public void setPrenomParam( SearchParam<String> prenomParam ){
        this.prenomParam = prenomParam;
    }

	/**
     * Get the "emailParam" field value
     * @return the field value
     */
	public SearchParam<String> getEmailParam(){
		return this.emailParam;
	}
	/**
     * Set the "emailParam" field value
     * @param emailParam
     */
    public void setEmailParam( SearchParam<String> emailParam ){
        this.emailParam = emailParam;
    }

	/**
     * Get the "loginParam" field value
     * @return the field value
     */
	public SearchParam<String> getLoginParam(){
		return this.loginParam;
	}
	/**
     * Set the "loginParam" field value
     * @param loginParam
     */
    public void setLoginParam( SearchParam<String> loginParam ){
        this.loginParam = loginParam;
    }

	/**
     * Get the "passwordParam" field value
     * @return the field value
     */
	public SearchParam<String> getPasswordParam(){
		return this.passwordParam;
	}
	/**
     * Set the "passwordParam" field value
     * @param passwordParam
     */
    public void setPasswordParam( SearchParam<String> passwordParam ){
        this.passwordParam = passwordParam;
    }

	/**
     * Get the "telephoneParam" field value
     * @return the field value
     */
	public SearchParam<String> getTelephoneParam(){
		return this.telephoneParam;
	}
	/**
     * Set the "telephoneParam" field value
     * @param telephoneParam
     */
    public void setTelephoneParam( SearchParam<String> telephoneParam ){
        this.telephoneParam = telephoneParam;
    }

	/**
     * Get the "tokenParam" field value
     * @return the field value
     */
	public SearchParam<String> getTokenParam(){
		return this.tokenParam;
	}
	/**
     * Set the "tokenParam" field value
     * @param tokenParam
     */
    public void setTokenParam( SearchParam<String> tokenParam ){
        this.tokenParam = tokenParam;
    }

	/**
     * Get the "isValidTokenParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidTokenParam(){
		return this.isValidTokenParam;
	}
	/**
     * Set the "isValidTokenParam" field value
     * @param isValidTokenParam
     */
    public void setIsValidTokenParam( SearchParam<Boolean> isValidTokenParam ){
        this.isValidTokenParam = isValidTokenParam;
    }

	/**
     * Get the "dateExpireTokenParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateExpireTokenParam(){
		return this.dateExpireTokenParam;
	}
	/**
     * Set the "dateExpireTokenParam" field value
     * @param dateExpireTokenParam
     */
    public void setDateExpireTokenParam( SearchParam<String> dateExpireTokenParam ){
        this.dateExpireTokenParam = dateExpireTokenParam;
    }

	/**
     * Get the "lastPasswordParam" field value
     * @return the field value
     */
	public SearchParam<String> getLastPasswordParam(){
		return this.lastPasswordParam;
	}
	/**
     * Set the "lastPasswordParam" field value
     * @param lastPasswordParam
     */
    public void setLastPasswordParam( SearchParam<String> lastPasswordParam ){
        this.lastPasswordParam = lastPasswordParam;
    }

	/**
     * Get the "signatureUrlParam" field value
     * @return the field value
     */
	public SearchParam<String> getSignatureUrlParam(){
		return this.signatureUrlParam;
	}
	/**
     * Set the "signatureUrlParam" field value
     * @param signatureUrlParam
     */
    public void setSignatureUrlParam( SearchParam<String> signatureUrlParam ){
        this.signatureUrlParam = signatureUrlParam;
    }

	/**
     * Get the "roleIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getRoleIdParam(){
		return this.roleIdParam;
	}
	/**
     * Set the "roleIdParam" field value
     * @param roleIdParam
     */
    public void setRoleIdParam( SearchParam<Integer> roleIdParam ){
        this.roleIdParam = roleIdParam;
    }

	/**
     * Get the "isLockedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsLockedParam(){
		return this.isLockedParam;
	}
	/**
     * Set the "isLockedParam" field value
     * @param isLockedParam
     */
    public void setIsLockedParam( SearchParam<Boolean> isLockedParam ){
        this.isLockedParam = isLockedParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

		/**
     * Get the "roleCodeParam" field value
     * @return the field value
     */
	public SearchParam<String> getRoleCodeParam(){
		return this.roleCodeParam;
	}
	/**
     * Set the "roleCodeParam" field value
     * @param roleCodeParam
     */
    public void setRoleCodeParam( SearchParam<String> roleCodeParam ){
        this.roleCodeParam = roleCodeParam;
    }

		/**
     * Get the "roleLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getRoleLibelleParam(){
		return this.roleLibelleParam;
	}
	/**
     * Set the "roleLibelleParam" field value
     * @param roleLibelleParam
     */
    public void setRoleLibelleParam( SearchParam<String> roleLibelleParam ){
        this.roleLibelleParam = roleLibelleParam;
    }


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		UserDto other = (UserDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("nom:"+nom);
		sb.append("|");
		sb.append("prenom:"+prenom);
		sb.append("|");
		sb.append("email:"+email);
		sb.append("|");
		sb.append("login:"+login);
		sb.append("|");
		sb.append("password:"+password);
		sb.append("|");
		sb.append("telephone:"+telephone);
		sb.append("|");
		sb.append("token:"+token);
		sb.append("|");
		sb.append("isValidToken:"+isValidToken);
		sb.append("|");
		sb.append("dateExpireToken:"+dateExpireToken);
		sb.append("|");
		sb.append("lastPassword:"+lastPassword);
		sb.append("|");
		sb.append("signatureUrl:"+signatureUrl);
		sb.append("|");
		sb.append("isLocked:"+isLocked);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
        return sb.toString();
    }
}
