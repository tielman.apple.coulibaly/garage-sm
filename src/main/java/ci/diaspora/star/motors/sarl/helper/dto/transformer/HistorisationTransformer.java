

/*
 * Java transformer for entity table historisation 
 * Created on 2019-08-18 ( Time 17:15:12 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "historisation"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface HistorisationTransformer {

	HistorisationTransformer INSTANCE = Mappers.getMapper(HistorisationTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.action.id", target="actionId"),
		@Mapping(source="entity.action.code", target="actionCode"),
		@Mapping(source="entity.action.libelle", target="actionLibelle"),
		@Mapping(source="entity.typeFiche.id", target="typeFicheId"),
		@Mapping(source="entity.typeFiche.code", target="typeFicheCode"),
		@Mapping(source="entity.typeFiche.libelle", target="typeFicheLibelle"),
		@Mapping(source="entity.user.id", target="userId"),
		@Mapping(source="entity.user.nom", target="userNom"),
		@Mapping(source="entity.user.prenom", target="userPrenom"),
		@Mapping(source="entity.user.login", target="userLogin"),
		@Mapping(source="entity.etat.id", target="etatId"),
		@Mapping(source="entity.etat.code", target="etatCode"),
		@Mapping(source="entity.etat.libelle", target="etatLibelle"),
	})
	HistorisationDto toDto(Historisation entity) throws ParseException;

    List<HistorisationDto> toDtos(List<Historisation> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.ficheId", target="ficheId"),
		@Mapping(source="dto.ficheAvant", target="ficheAvant"),
		@Mapping(source="dto.ficheApres", target="ficheApres"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="action", target="action"),
		@Mapping(source="typeFiche", target="typeFiche"),
		@Mapping(source="user", target="user"),
		@Mapping(source="etat", target="etat"),
	})
    Historisation toEntity(HistorisationDto dto, Action action, TypeFiche typeFiche, User user, Etat etat) throws ParseException;

    //List<Historisation> toEntities(List<HistorisationDto> dtos) throws ParseException;

}
