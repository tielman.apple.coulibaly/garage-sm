


                                                                                /*
 * Java transformer for entity table fiche_reception_piece_vehicule
 * Created on 2019-09-09 ( Time 13:30:34 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "fiche_reception_piece_vehicule"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class FicheReceptionPieceVehiculeBusiness implements IBasicBusiness<Request<FicheReceptionPieceVehiculeDto>, Response<FicheReceptionPieceVehiculeDto>> {

  private Response<FicheReceptionPieceVehiculeDto> response;
  @Autowired
  private FicheReceptionPieceVehiculeRepository ficheReceptionPieceVehiculeRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private PieceVehiculeRepository pieceVehiculeRepository;
  @Autowired
  private FicheReceptionRepository ficheReceptionRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

          

  public FicheReceptionPieceVehiculeBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create FicheReceptionPieceVehicule by using FicheReceptionPieceVehiculeDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FicheReceptionPieceVehiculeDto> create(Request<FicheReceptionPieceVehiculeDto> request, Locale locale)  {
    slf4jLogger.info("----begin create FicheReceptionPieceVehicule-----");

    response = new Response<FicheReceptionPieceVehiculeDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FicheReceptionPieceVehicule> items = new ArrayList<FicheReceptionPieceVehicule>();

      for (FicheReceptionPieceVehiculeDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("ficheReceptionId", dto.getFicheReceptionId());
        fieldsToVerify.put("pieceVehiculeId", dto.getPieceVehiculeId());
        fieldsToVerify.put("detailAutres", dto.getDetailAutres());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if ficheReceptionPieceVehicule to insert do not exist
        FicheReceptionPieceVehicule existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("ficheReceptionPieceVehicule -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if pieceVehicule exist
        PieceVehicule existingPieceVehicule = pieceVehiculeRepository.findById(dto.getPieceVehiculeId(), false);
        if (existingPieceVehicule == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pieceVehicule -> " + dto.getPieceVehiculeId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if ficheReception exist
        FicheReception existingFicheReception = ficheReceptionRepository.findById(dto.getFicheReceptionId(), false);
        if (existingFicheReception == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getFicheReceptionId(), locale));
          response.setHasError(true);
          return response;
        }
        FicheReceptionPieceVehicule entityToSave = null;
        entityToSave = FicheReceptionPieceVehiculeTransformer.INSTANCE.toEntity(dto, existingPieceVehicule, existingFicheReception);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<FicheReceptionPieceVehicule> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = ficheReceptionPieceVehiculeRepository.save((Iterable<FicheReceptionPieceVehicule>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("ficheReceptionPieceVehicule", locale));
          response.setHasError(true);
          return response;
        }
        List<FicheReceptionPieceVehiculeDto> itemsDto = new ArrayList<FicheReceptionPieceVehiculeDto>();
        for (FicheReceptionPieceVehicule entity : itemsSaved) {
          FicheReceptionPieceVehiculeDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create FicheReceptionPieceVehicule-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update FicheReceptionPieceVehicule by using FicheReceptionPieceVehiculeDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FicheReceptionPieceVehiculeDto> update(Request<FicheReceptionPieceVehiculeDto> request, Locale locale)  {
    slf4jLogger.info("----begin update FicheReceptionPieceVehicule-----");

    response = new Response<FicheReceptionPieceVehiculeDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FicheReceptionPieceVehicule> items = new ArrayList<FicheReceptionPieceVehicule>();

      for (FicheReceptionPieceVehiculeDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la ficheReceptionPieceVehicule existe
        FicheReceptionPieceVehicule entityToSave = null;
        entityToSave = ficheReceptionPieceVehiculeRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ficheReceptionPieceVehicule -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if pieceVehicule exist
        if (dto.getPieceVehiculeId() != null && dto.getPieceVehiculeId() > 0){
          PieceVehicule existingPieceVehicule = pieceVehiculeRepository.findById(dto.getPieceVehiculeId(), false);
          if (existingPieceVehicule == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("pieceVehicule -> " + dto.getPieceVehiculeId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setPieceVehicule(existingPieceVehicule);
        }
        // Verify if ficheReception exist
        if (dto.getFicheReceptionId() != null && dto.getFicheReceptionId() > 0){
          FicheReception existingFicheReception = ficheReceptionRepository.findById(dto.getFicheReceptionId(), false);
          if (existingFicheReception == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getFicheReceptionId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setFicheReception(existingFicheReception);
        }
        if (Utilities.notBlank(dto.getDetailAutres())) {
                  entityToSave.setDetailAutres(dto.getDetailAutres());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<FicheReceptionPieceVehicule> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = ficheReceptionPieceVehiculeRepository.save((Iterable<FicheReceptionPieceVehicule>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("ficheReceptionPieceVehicule", locale));
          response.setHasError(true);
          return response;
        }
        List<FicheReceptionPieceVehiculeDto> itemsDto = new ArrayList<FicheReceptionPieceVehiculeDto>();
        for (FicheReceptionPieceVehicule entity : itemsSaved) {
          FicheReceptionPieceVehiculeDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update FicheReceptionPieceVehicule-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete FicheReceptionPieceVehicule by using FicheReceptionPieceVehiculeDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FicheReceptionPieceVehiculeDto> delete(Request<FicheReceptionPieceVehiculeDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete FicheReceptionPieceVehicule-----");

    response = new Response<FicheReceptionPieceVehiculeDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FicheReceptionPieceVehicule> items = new ArrayList<FicheReceptionPieceVehicule>();

      for (FicheReceptionPieceVehiculeDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la ficheReceptionPieceVehicule existe
        FicheReceptionPieceVehicule existingEntity = null;
        existingEntity = ficheReceptionPieceVehiculeRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ficheReceptionPieceVehicule -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        ficheReceptionPieceVehiculeRepository.save((Iterable<FicheReceptionPieceVehicule>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete FicheReceptionPieceVehicule-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete FicheReceptionPieceVehicule by using FicheReceptionPieceVehiculeDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<FicheReceptionPieceVehiculeDto> forceDelete(Request<FicheReceptionPieceVehiculeDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete FicheReceptionPieceVehicule-----");

    response = new Response<FicheReceptionPieceVehiculeDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FicheReceptionPieceVehicule> items = new ArrayList<FicheReceptionPieceVehicule>();

      for (FicheReceptionPieceVehiculeDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la ficheReceptionPieceVehicule existe
        FicheReceptionPieceVehicule existingEntity = null;
          existingEntity = ficheReceptionPieceVehiculeRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ficheReceptionPieceVehicule -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

          

                    existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          ficheReceptionPieceVehiculeRepository.save((Iterable<FicheReceptionPieceVehicule>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete FicheReceptionPieceVehicule-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get FicheReceptionPieceVehicule by using FicheReceptionPieceVehiculeDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<FicheReceptionPieceVehiculeDto> getByCriteria(Request<FicheReceptionPieceVehiculeDto> request, Locale locale) {
    slf4jLogger.info("----begin get FicheReceptionPieceVehicule-----");

    response = new Response<FicheReceptionPieceVehiculeDto>();

    try {
      List<FicheReceptionPieceVehicule> items = null;
      items = ficheReceptionPieceVehiculeRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<FicheReceptionPieceVehiculeDto> itemsDto = new ArrayList<FicheReceptionPieceVehiculeDto>();
        for (FicheReceptionPieceVehicule entity : items) {
          FicheReceptionPieceVehiculeDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(ficheReceptionPieceVehiculeRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("ficheReceptionPieceVehicule", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get FicheReceptionPieceVehicule-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full FicheReceptionPieceVehiculeDto by using FicheReceptionPieceVehicule as object.
   *
   * @param entity, locale
   * @return FicheReceptionPieceVehiculeDto
   *
   */
  private FicheReceptionPieceVehiculeDto getFullInfos(FicheReceptionPieceVehicule entity, Integer size, Locale locale) throws Exception {
    FicheReceptionPieceVehiculeDto dto = FicheReceptionPieceVehiculeTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
