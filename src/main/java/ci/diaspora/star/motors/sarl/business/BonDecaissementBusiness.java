


                                                                                                                          /*
 * Java transformer for entity table bon_decaissement
 * Created on 2019-08-18 ( Time 17:15:05 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "bon_decaissement"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class BonDecaissementBusiness implements IBasicBusiness<Request<BonDecaissementDto>, Response<BonDecaissementDto>> {

  private Response<BonDecaissementDto> response;
  @Autowired
  private BonDecaissementRepository bonDecaissementRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private UserRepository userResponsableTechniqueRepository;
  @Autowired
  private UserRepository userResponsableDecaissementRepository;
  @Autowired
  private DevisRepository devisRepository;
  @Autowired
  private EtatRepository etatRepository;
  @Autowired
  private UserRepository userResponsableAchatRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                          

  public BonDecaissementBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create BonDecaissement by using BonDecaissementDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<BonDecaissementDto> create(Request<BonDecaissementDto> request, Locale locale)  {
    slf4jLogger.info("----begin create BonDecaissement-----");

    response = new Response<BonDecaissementDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<BonDecaissement> items = new ArrayList<BonDecaissement>();

      for (BonDecaissementDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("devisId", dto.getDevisId());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        fieldsToVerify.put("etatId", dto.getEtatId());
        fieldsToVerify.put("userChefAtelierId", dto.getUserChefAtelierId());
        fieldsToVerify.put("userResponsableTechniqueId", dto.getUserResponsableTechniqueId());
        fieldsToVerify.put("userResponsableDecaissementId", dto.getUserResponsableDecaissementId());
        fieldsToVerify.put("userResponsableAchatId", dto.getUserResponsableAchatId());
        fieldsToVerify.put("isValidedByChefAtelier", dto.getIsValidedByChefAtelier());
        fieldsToVerify.put("isValidedByResponsableTechnique", dto.getIsValidedByResponsableTechnique());
        fieldsToVerify.put("isValidedByResponsableDecaissement", dto.getIsValidedByResponsableDecaissement());
        fieldsToVerify.put("isValidedByResponsableAchat", dto.getIsValidedByResponsableAchat());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if bonDecaissement to insert do not exist
        BonDecaissement existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("bonDecaissement -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if userResponsableTechnique exist
        User existingUserResponsableTechnique = userResponsableTechniqueRepository.findById(dto.getUserResponsableTechniqueId(), false);
        if (existingUserResponsableTechnique == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("userResponsableTechnique -> " + dto.getUserResponsableTechniqueId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if user exist
        User existingUser = userRepository.findById(dto.getUserChefAtelierId(), false);
        if (existingUser == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserChefAtelierId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if userResponsableDecaissement exist
        User existingUserResponsableDecaissement = userResponsableDecaissementRepository.findById(dto.getUserResponsableDecaissementId(), false);
        if (existingUserResponsableDecaissement == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("userResponsableDecaissement -> " + dto.getUserResponsableDecaissementId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if devis exist
        Devis existingDevis = devisRepository.findById(dto.getDevisId(), false);
        if (existingDevis == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("devis -> " + dto.getDevisId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if etat exist
        Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
        if (existingEtat == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if userResponsableAchat exist
        User existingUserResponsableAchat = userResponsableAchatRepository.findById(dto.getUserResponsableAchatId(), false);
        if (existingUserResponsableAchat == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("userResponsableAchat -> " + dto.getUserResponsableAchatId(), locale));
          response.setHasError(true);
          return response;
        }
        BonDecaissement entityToSave = null;
        entityToSave = BonDecaissementTransformer.INSTANCE.toEntity(dto, existingUserResponsableTechnique, existingUser, existingUserResponsableDecaissement, existingDevis, existingEtat, existingUserResponsableAchat);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<BonDecaissement> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = bonDecaissementRepository.save((Iterable<BonDecaissement>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("bonDecaissement", locale));
          response.setHasError(true);
          return response;
        }
        List<BonDecaissementDto> itemsDto = new ArrayList<BonDecaissementDto>();
        for (BonDecaissement entity : itemsSaved) {
          BonDecaissementDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create BonDecaissement-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update BonDecaissement by using BonDecaissementDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<BonDecaissementDto> update(Request<BonDecaissementDto> request, Locale locale)  {
    slf4jLogger.info("----begin update BonDecaissement-----");

    response = new Response<BonDecaissementDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<BonDecaissement> items = new ArrayList<BonDecaissement>();

      for (BonDecaissementDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la bonDecaissement existe
        BonDecaissement entityToSave = null;
        entityToSave = bonDecaissementRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("bonDecaissement -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if userResponsableTechnique exist
        if (dto.getUserResponsableTechniqueId() != null && dto.getUserResponsableTechniqueId() > 0){
          User existingUserResponsableTechnique = userResponsableTechniqueRepository.findById(dto.getUserResponsableTechniqueId(), false);
          if (existingUserResponsableTechnique == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("userResponsableTechnique -> " + dto.getUserResponsableTechniqueId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserResponsableTechnique(existingUserResponsableTechnique);
        }
        // Verify if user exist
        if (dto.getUserChefAtelierId() != null && dto.getUserChefAtelierId() > 0){
          User existingUser = userRepository.findById(dto.getUserChefAtelierId(), false);
          if (existingUser == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserChefAtelierId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUser(existingUser);
        }
        // Verify if userResponsableDecaissement exist
        if (dto.getUserResponsableDecaissementId() != null && dto.getUserResponsableDecaissementId() > 0){
          User existingUserResponsableDecaissement = userResponsableDecaissementRepository.findById(dto.getUserResponsableDecaissementId(), false);
          if (existingUserResponsableDecaissement == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("userResponsableDecaissement -> " + dto.getUserResponsableDecaissementId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserResponsableDecaissement(existingUserResponsableDecaissement);
        }
        // Verify if devis exist
        if (dto.getDevisId() != null && dto.getDevisId() > 0){
          Devis existingDevis = devisRepository.findById(dto.getDevisId(), false);
          if (existingDevis == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("devis -> " + dto.getDevisId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setDevis(existingDevis);
        }
        // Verify if etat exist
        if (dto.getEtatId() != null && dto.getEtatId() > 0){
          Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
          if (existingEtat == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setEtat(existingEtat);
        }
        // Verify if userResponsableAchat exist
        if (dto.getUserResponsableAchatId() != null && dto.getUserResponsableAchatId() > 0){
          User existingUserResponsableAchat = userResponsableAchatRepository.findById(dto.getUserResponsableAchatId(), false);
          if (existingUserResponsableAchat == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("userResponsableAchat -> " + dto.getUserResponsableAchatId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserResponsableAchat(existingUserResponsableAchat);
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        if (dto.getIsValidedByChefAtelier() != null) {
          entityToSave.setIsValidedByChefAtelier(dto.getIsValidedByChefAtelier());
        }
        if (dto.getIsValidedByResponsableTechnique() != null) {
          entityToSave.setIsValidedByResponsableTechnique(dto.getIsValidedByResponsableTechnique());
        }
        if (dto.getIsValidedByResponsableDecaissement() != null) {
          entityToSave.setIsValidedByResponsableDecaissement(dto.getIsValidedByResponsableDecaissement());
        }
        if (dto.getIsValidedByResponsableAchat() != null) {
          entityToSave.setIsValidedByResponsableAchat(dto.getIsValidedByResponsableAchat());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<BonDecaissement> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = bonDecaissementRepository.save((Iterable<BonDecaissement>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("bonDecaissement", locale));
          response.setHasError(true);
          return response;
        }
        List<BonDecaissementDto> itemsDto = new ArrayList<BonDecaissementDto>();
        for (BonDecaissement entity : itemsSaved) {
          BonDecaissementDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update BonDecaissement-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete BonDecaissement by using BonDecaissementDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<BonDecaissementDto> delete(Request<BonDecaissementDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete BonDecaissement-----");

    response = new Response<BonDecaissementDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<BonDecaissement> items = new ArrayList<BonDecaissement>();

      for (BonDecaissementDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la bonDecaissement existe
        BonDecaissement existingEntity = null;
        existingEntity = bonDecaissementRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("bonDecaissement -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        bonDecaissementRepository.save((Iterable<BonDecaissement>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete BonDecaissement-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete BonDecaissement by using BonDecaissementDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<BonDecaissementDto> forceDelete(Request<BonDecaissementDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete BonDecaissement-----");

    response = new Response<BonDecaissementDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<BonDecaissement> items = new ArrayList<BonDecaissement>();

      for (BonDecaissementDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la bonDecaissement existe
        BonDecaissement existingEntity = null;
          existingEntity = bonDecaissementRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("bonDecaissement -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                          

            existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
                                            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          bonDecaissementRepository.save((Iterable<BonDecaissement>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete BonDecaissement-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get BonDecaissement by using BonDecaissementDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<BonDecaissementDto> getByCriteria(Request<BonDecaissementDto> request, Locale locale) {
    slf4jLogger.info("----begin get BonDecaissement-----");

    response = new Response<BonDecaissementDto>();

    try {
      List<BonDecaissement> items = null;
      items = bonDecaissementRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<BonDecaissementDto> itemsDto = new ArrayList<BonDecaissementDto>();
        for (BonDecaissement entity : items) {
          BonDecaissementDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(bonDecaissementRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("bonDecaissement", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get BonDecaissement-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full BonDecaissementDto by using BonDecaissement as object.
   *
   * @param entity, locale
   * @return BonDecaissementDto
   *
   */
  private BonDecaissementDto getFullInfos(BonDecaissement entity, Integer size, Locale locale) throws Exception {
    BonDecaissementDto dto = BonDecaissementTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
