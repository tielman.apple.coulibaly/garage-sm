


                                                                                      /*
 * Java transformer for entity table designation_element_fiche_controle_fiche_control_final
 * Created on 2019-08-18 ( Time 17:15:07 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "designation_element_fiche_controle_fiche_control_final"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class DesignationElementFicheControleFicheControlFinalBusiness implements IBasicBusiness<Request<DesignationElementFicheControleFicheControlFinalDto>, Response<DesignationElementFicheControleFicheControlFinalDto>> {

  private Response<DesignationElementFicheControleFicheControlFinalDto> response;
  @Autowired
  private DesignationElementFicheControleFicheControlFinalRepository designationElementFicheControleFicheControlFinalRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private DesignationElementFicheControleRepository designationElementFicheControleRepository;
  @Autowired
  private FicheControlFinalRepository ficheControlFinalRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

          

  public DesignationElementFicheControleFicheControlFinalBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create DesignationElementFicheControleFicheControlFinal by using DesignationElementFicheControleFicheControlFinalDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<DesignationElementFicheControleFicheControlFinalDto> create(Request<DesignationElementFicheControleFicheControlFinalDto> request, Locale locale)  {
    slf4jLogger.info("----begin create DesignationElementFicheControleFicheControlFinal-----");

    response = new Response<DesignationElementFicheControleFicheControlFinalDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<DesignationElementFicheControleFicheControlFinal> items = new ArrayList<DesignationElementFicheControleFicheControlFinal>();

      for (DesignationElementFicheControleFicheControlFinalDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("designationNiveauControlId", dto.getDesignationNiveauControlId());
        fieldsToVerify.put("ficheControlFinalId", dto.getFicheControlFinalId());
        fieldsToVerify.put("observations", dto.getObservations());
        fieldsToVerify.put("isGood", dto.getIsGood());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if designationElementFicheControleFicheControlFinal to insert do not exist
        DesignationElementFicheControleFicheControlFinal existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("designationElementFicheControleFicheControlFinal -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if designationElementFicheControle exist
        DesignationElementFicheControle existingDesignationElementFicheControle = designationElementFicheControleRepository.findById(dto.getDesignationNiveauControlId(), false);
        if (existingDesignationElementFicheControle == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControle -> " + dto.getDesignationNiveauControlId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if ficheControlFinal exist
        FicheControlFinal existingFicheControlFinal = ficheControlFinalRepository.findById(dto.getFicheControlFinalId(), false);
        if (existingFicheControlFinal == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ficheControlFinal -> " + dto.getFicheControlFinalId(), locale));
          response.setHasError(true);
          return response;
        }
        DesignationElementFicheControleFicheControlFinal entityToSave = null;
        entityToSave = DesignationElementFicheControleFicheControlFinalTransformer.INSTANCE.toEntity(dto, existingDesignationElementFicheControle, existingFicheControlFinal);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<DesignationElementFicheControleFicheControlFinal> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = designationElementFicheControleFicheControlFinalRepository.save((Iterable<DesignationElementFicheControleFicheControlFinal>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("designationElementFicheControleFicheControlFinal", locale));
          response.setHasError(true);
          return response;
        }
        List<DesignationElementFicheControleFicheControlFinalDto> itemsDto = new ArrayList<DesignationElementFicheControleFicheControlFinalDto>();
        for (DesignationElementFicheControleFicheControlFinal entity : itemsSaved) {
          DesignationElementFicheControleFicheControlFinalDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create DesignationElementFicheControleFicheControlFinal-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update DesignationElementFicheControleFicheControlFinal by using DesignationElementFicheControleFicheControlFinalDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<DesignationElementFicheControleFicheControlFinalDto> update(Request<DesignationElementFicheControleFicheControlFinalDto> request, Locale locale)  {
    slf4jLogger.info("----begin update DesignationElementFicheControleFicheControlFinal-----");

    response = new Response<DesignationElementFicheControleFicheControlFinalDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<DesignationElementFicheControleFicheControlFinal> items = new ArrayList<DesignationElementFicheControleFicheControlFinal>();

      for (DesignationElementFicheControleFicheControlFinalDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la designationElementFicheControleFicheControlFinal existe
        DesignationElementFicheControleFicheControlFinal entityToSave = null;
        entityToSave = designationElementFicheControleFicheControlFinalRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControleFicheControlFinal -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if designationElementFicheControle exist
        if (dto.getDesignationNiveauControlId() != null && dto.getDesignationNiveauControlId() > 0){
          DesignationElementFicheControle existingDesignationElementFicheControle = designationElementFicheControleRepository.findById(dto.getDesignationNiveauControlId(), false);
          if (existingDesignationElementFicheControle == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControle -> " + dto.getDesignationNiveauControlId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setDesignationElementFicheControle(existingDesignationElementFicheControle);
        }
        // Verify if ficheControlFinal exist
        if (dto.getFicheControlFinalId() != null && dto.getFicheControlFinalId() > 0){
          FicheControlFinal existingFicheControlFinal = ficheControlFinalRepository.findById(dto.getFicheControlFinalId(), false);
          if (existingFicheControlFinal == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("ficheControlFinal -> " + dto.getFicheControlFinalId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setFicheControlFinal(existingFicheControlFinal);
        }
        if (Utilities.notBlank(dto.getObservations())) {
                  entityToSave.setObservations(dto.getObservations());
        }
        if (dto.getIsGood() != null) {
          entityToSave.setIsGood(dto.getIsGood());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<DesignationElementFicheControleFicheControlFinal> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = designationElementFicheControleFicheControlFinalRepository.save((Iterable<DesignationElementFicheControleFicheControlFinal>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("designationElementFicheControleFicheControlFinal", locale));
          response.setHasError(true);
          return response;
        }
        List<DesignationElementFicheControleFicheControlFinalDto> itemsDto = new ArrayList<DesignationElementFicheControleFicheControlFinalDto>();
        for (DesignationElementFicheControleFicheControlFinal entity : itemsSaved) {
          DesignationElementFicheControleFicheControlFinalDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update DesignationElementFicheControleFicheControlFinal-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete DesignationElementFicheControleFicheControlFinal by using DesignationElementFicheControleFicheControlFinalDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<DesignationElementFicheControleFicheControlFinalDto> delete(Request<DesignationElementFicheControleFicheControlFinalDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete DesignationElementFicheControleFicheControlFinal-----");

    response = new Response<DesignationElementFicheControleFicheControlFinalDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<DesignationElementFicheControleFicheControlFinal> items = new ArrayList<DesignationElementFicheControleFicheControlFinal>();

      for (DesignationElementFicheControleFicheControlFinalDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la designationElementFicheControleFicheControlFinal existe
        DesignationElementFicheControleFicheControlFinal existingEntity = null;
        existingEntity = designationElementFicheControleFicheControlFinalRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControleFicheControlFinal -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        designationElementFicheControleFicheControlFinalRepository.save((Iterable<DesignationElementFicheControleFicheControlFinal>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete DesignationElementFicheControleFicheControlFinal-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete DesignationElementFicheControleFicheControlFinal by using DesignationElementFicheControleFicheControlFinalDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<DesignationElementFicheControleFicheControlFinalDto> forceDelete(Request<DesignationElementFicheControleFicheControlFinalDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete DesignationElementFicheControleFicheControlFinal-----");

    response = new Response<DesignationElementFicheControleFicheControlFinalDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<DesignationElementFicheControleFicheControlFinal> items = new ArrayList<DesignationElementFicheControleFicheControlFinal>();

      for (DesignationElementFicheControleFicheControlFinalDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la designationElementFicheControleFicheControlFinal existe
        DesignationElementFicheControleFicheControlFinal existingEntity = null;
          existingEntity = designationElementFicheControleFicheControlFinalRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("designationElementFicheControleFicheControlFinal -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

          

                            existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          designationElementFicheControleFicheControlFinalRepository.save((Iterable<DesignationElementFicheControleFicheControlFinal>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete DesignationElementFicheControleFicheControlFinal-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get DesignationElementFicheControleFicheControlFinal by using DesignationElementFicheControleFicheControlFinalDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<DesignationElementFicheControleFicheControlFinalDto> getByCriteria(Request<DesignationElementFicheControleFicheControlFinalDto> request, Locale locale) {
    slf4jLogger.info("----begin get DesignationElementFicheControleFicheControlFinal-----");

    response = new Response<DesignationElementFicheControleFicheControlFinalDto>();

    try {
      List<DesignationElementFicheControleFicheControlFinal> items = null;
      items = designationElementFicheControleFicheControlFinalRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<DesignationElementFicheControleFicheControlFinalDto> itemsDto = new ArrayList<DesignationElementFicheControleFicheControlFinalDto>();
        for (DesignationElementFicheControleFicheControlFinal entity : items) {
          DesignationElementFicheControleFicheControlFinalDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(designationElementFicheControleFicheControlFinalRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("designationElementFicheControleFicheControlFinal", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get DesignationElementFicheControleFicheControlFinal-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full DesignationElementFicheControleFicheControlFinalDto by using DesignationElementFicheControleFicheControlFinal as object.
   *
   * @param entity, locale
   * @return DesignationElementFicheControleFicheControlFinalDto
   *
   */
  private DesignationElementFicheControleFicheControlFinalDto getFullInfos(DesignationElementFicheControleFicheControlFinal entity, Integer size, Locale locale) throws Exception {
    DesignationElementFicheControleFicheControlFinalDto dto = DesignationElementFicheControleFicheControlFinalTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
