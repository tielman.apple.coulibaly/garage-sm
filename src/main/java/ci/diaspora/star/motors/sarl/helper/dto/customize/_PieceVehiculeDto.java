
/*
 * Java dto for entity table piece_vehicule 
 * Created on 2019-08-18 ( Time 17:15:14 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * DTO customize for table "piece_vehicule"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _PieceVehiculeDto {
	
	protected String detailAutres;

	public String getDetailAutres() {
		return detailAutres;
	}

	public void setDetailAutres(String detailAutres) {
		this.detailAutres = detailAutres;
	}
	
	
	

}
