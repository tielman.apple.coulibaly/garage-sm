
/*
 * Java dto for entity table travaux_effectues
 * Created on 2019-08-18 ( Time 17:15:15 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._TravauxEffectuesDto;

/**
 * DTO for table "travaux_effectues"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class TravauxEffectuesDto extends _TravauxEffectuesDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
	private String     dateDebutTravaux     ;
	/*
	 * 
	 */
	private String     dateFinTravaux       ;
	/*
	 * 
	 */
    private Integer    ordreReparationId    ;
	/*
	 * 
	 */
    private Integer    designationTravauxEffectuesId ;
	/*
	 * 
	 */
    private Integer    userTechnicienId     ;
	/*
	 * 
	 */
    private Boolean    isSigned             ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String designationTravauxEffectuesLibelle;
	private String userNom;
	private String userPrenom;
	private String userLogin;


	// Search param
	private SearchParam<Integer>  idParam               ;                     

		private SearchParam<String>   dateDebutTravauxParam ;                     

		private SearchParam<String>   dateFinTravauxParam   ;                     
	private SearchParam<Integer>  ordreReparationIdParam;                     
	private SearchParam<Integer>  designationTravauxEffectuesIdParam;                     
	private SearchParam<Integer>  userTechnicienIdParam ;                     
	private SearchParam<Boolean>  isSignedParam         ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<String>   designationTravauxEffectuesLibelleParam;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomParam       ;                     
	private SearchParam<String>   userLoginParam        ;                     
    /**
     * Default constructor
     */
    public TravauxEffectuesDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "dateDebutTravaux" field value
     * @param dateDebutTravaux
     */
	public void setDateDebutTravaux( String dateDebutTravaux )
    {
        this.dateDebutTravaux = dateDebutTravaux ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateDebutTravaux()
    {
        return this.dateDebutTravaux;
    }

    /**
     * Set the "dateFinTravaux" field value
     * @param dateFinTravaux
     */
	public void setDateFinTravaux( String dateFinTravaux )
    {
        this.dateFinTravaux = dateFinTravaux ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateFinTravaux()
    {
        return this.dateFinTravaux;
    }

    /**
     * Set the "ordreReparationId" field value
     * @param ordreReparationId
     */
	public void setOrdreReparationId( Integer ordreReparationId )
    {
        this.ordreReparationId = ordreReparationId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getOrdreReparationId()
    {
        return this.ordreReparationId;
    }

    /**
     * Set the "designationTravauxEffectuesId" field value
     * @param designationTravauxEffectuesId
     */
	public void setDesignationTravauxEffectuesId( Integer designationTravauxEffectuesId )
    {
        this.designationTravauxEffectuesId = designationTravauxEffectuesId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDesignationTravauxEffectuesId()
    {
        return this.designationTravauxEffectuesId;
    }

    /**
     * Set the "userTechnicienId" field value
     * @param userTechnicienId
     */
	public void setUserTechnicienId( Integer userTechnicienId )
    {
        this.userTechnicienId = userTechnicienId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserTechnicienId()
    {
        return this.userTechnicienId;
    }

    /**
     * Set the "isSigned" field value
     * @param isSigned
     */
	public void setIsSigned( Boolean isSigned )
    {
        this.isSigned = isSigned ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsSigned()
    {
        return this.isSigned;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getDesignationTravauxEffectuesLibelle()
    {
        return this.designationTravauxEffectuesLibelle;
    }
	public void setDesignationTravauxEffectuesLibelle(String designationTravauxEffectuesLibelle)
    {
        this.designationTravauxEffectuesLibelle = designationTravauxEffectuesLibelle;
    }

	public String getUserNom()
    {
        return this.userNom;
    }
	public void setUserNom(String userNom)
    {
        this.userNom = userNom;
    }

	public String getUserPrenom()
    {
        return this.userPrenom;
    }
	public void setUserPrenom(String userPrenom)
    {
        this.userPrenom = userPrenom;
    }

	public String getUserLogin()
    {
        return this.userLogin;
    }
	public void setUserLogin(String userLogin)
    {
        this.userLogin = userLogin;
    }


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "dateDebutTravauxParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateDebutTravauxParam(){
		return this.dateDebutTravauxParam;
	}
	/**
     * Set the "dateDebutTravauxParam" field value
     * @param dateDebutTravauxParam
     */
    public void setDateDebutTravauxParam( SearchParam<String> dateDebutTravauxParam ){
        this.dateDebutTravauxParam = dateDebutTravauxParam;
    }

	/**
     * Get the "dateFinTravauxParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateFinTravauxParam(){
		return this.dateFinTravauxParam;
	}
	/**
     * Set the "dateFinTravauxParam" field value
     * @param dateFinTravauxParam
     */
    public void setDateFinTravauxParam( SearchParam<String> dateFinTravauxParam ){
        this.dateFinTravauxParam = dateFinTravauxParam;
    }

	/**
     * Get the "ordreReparationIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getOrdreReparationIdParam(){
		return this.ordreReparationIdParam;
	}
	/**
     * Set the "ordreReparationIdParam" field value
     * @param ordreReparationIdParam
     */
    public void setOrdreReparationIdParam( SearchParam<Integer> ordreReparationIdParam ){
        this.ordreReparationIdParam = ordreReparationIdParam;
    }

	/**
     * Get the "designationTravauxEffectuesIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDesignationTravauxEffectuesIdParam(){
		return this.designationTravauxEffectuesIdParam;
	}
	/**
     * Set the "designationTravauxEffectuesIdParam" field value
     * @param designationTravauxEffectuesIdParam
     */
    public void setDesignationTravauxEffectuesIdParam( SearchParam<Integer> designationTravauxEffectuesIdParam ){
        this.designationTravauxEffectuesIdParam = designationTravauxEffectuesIdParam;
    }

	/**
     * Get the "userTechnicienIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserTechnicienIdParam(){
		return this.userTechnicienIdParam;
	}
	/**
     * Set the "userTechnicienIdParam" field value
     * @param userTechnicienIdParam
     */
    public void setUserTechnicienIdParam( SearchParam<Integer> userTechnicienIdParam ){
        this.userTechnicienIdParam = userTechnicienIdParam;
    }

	/**
     * Get the "isSignedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsSignedParam(){
		return this.isSignedParam;
	}
	/**
     * Set the "isSignedParam" field value
     * @param isSignedParam
     */
    public void setIsSignedParam( SearchParam<Boolean> isSignedParam ){
        this.isSignedParam = isSignedParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

		/**
     * Get the "designationTravauxEffectuesLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getDesignationTravauxEffectuesLibelleParam(){
		return this.designationTravauxEffectuesLibelleParam;
	}
	/**
     * Set the "designationTravauxEffectuesLibelleParam" field value
     * @param designationTravauxEffectuesLibelleParam
     */
    public void setDesignationTravauxEffectuesLibelleParam( SearchParam<String> designationTravauxEffectuesLibelleParam ){
        this.designationTravauxEffectuesLibelleParam = designationTravauxEffectuesLibelleParam;
    }

		/**
     * Get the "userNomParam" field value
     * @return the field value
     */
	public SearchParam<String> getUserNomParam(){
		return this.userNomParam;
	}
	/**
     * Set the "userNomParam" field value
     * @param userNomParam
     */
    public void setUserNomParam( SearchParam<String> userNomParam ){
        this.userNomParam = userNomParam;
    }

		/**
     * Get the "userPrenomParam" field value
     * @return the field value
     */
	public SearchParam<String> getUserPrenomParam(){
		return this.userPrenomParam;
	}
	/**
     * Set the "userPrenomParam" field value
     * @param userPrenomParam
     */
    public void setUserPrenomParam( SearchParam<String> userPrenomParam ){
        this.userPrenomParam = userPrenomParam;
    }

		/**
     * Get the "userLoginParam" field value
     * @return the field value
     */
	public SearchParam<String> getUserLoginParam(){
		return this.userLoginParam;
	}
	/**
     * Set the "userLoginParam" field value
     * @param userLoginParam
     */
    public void setUserLoginParam( SearchParam<String> userLoginParam ){
        this.userLoginParam = userLoginParam;
    }


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		TravauxEffectuesDto other = (TravauxEffectuesDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("dateDebutTravaux:"+dateDebutTravaux);
		sb.append("|");
		sb.append("dateFinTravaux:"+dateFinTravaux);
		sb.append("|");
		sb.append("isSigned:"+isSigned);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
        return sb.toString();
    }
}
