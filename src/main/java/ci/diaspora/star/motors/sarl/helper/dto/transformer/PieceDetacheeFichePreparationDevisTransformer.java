

/*
 * Java transformer for entity table piece_detachee_fiche_preparation_devis 
 * Created on 2019-08-18 ( Time 17:15:14 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "piece_detachee_fiche_preparation_devis"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface PieceDetacheeFichePreparationDevisTransformer {

	PieceDetacheeFichePreparationDevisTransformer INSTANCE = Mappers.getMapper(PieceDetacheeFichePreparationDevisTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.pieceDetachee.id", target="pieceDetacheeId"),
		@Mapping(source="entity.pieceDetachee.libelle", target="pieceDetacheeLibelle"),
		@Mapping(source="entity.fournisseur.id", target="fournisseurId"),
		@Mapping(source="entity.fournisseur.nom", target="fournisseurNom"),
		@Mapping(source="entity.fournisseur.prenom", target="fournisseurPrenom"),
		@Mapping(source="entity.fichePreparationDevis.id", target="fichePreparationDevisId"),
	})
	PieceDetacheeFichePreparationDevisDto toDto(PieceDetacheeFichePreparationDevis entity) throws ParseException;

    List<PieceDetacheeFichePreparationDevisDto> toDtos(List<PieceDetacheeFichePreparationDevis> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.quantite", target="quantite"),
		@Mapping(source="dto.prixUnitaire", target="prixUnitaire"),
		@Mapping(source="dto.total", target="total"),
		@Mapping(source="dto.isValided", target="isValided"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="pieceDetachee", target="pieceDetachee"),
		@Mapping(source="fournisseur", target="fournisseur"),
		@Mapping(source="fichePreparationDevis", target="fichePreparationDevis"),
	})
    PieceDetacheeFichePreparationDevis toEntity(PieceDetacheeFichePreparationDevisDto dto, PieceDetachee pieceDetachee, Fournisseur fournisseur, FichePreparationDevis fichePreparationDevis) throws ParseException;

    //List<PieceDetacheeFichePreparationDevis> toEntities(List<PieceDetacheeFichePreparationDevisDto> dtos) throws ParseException;

}
