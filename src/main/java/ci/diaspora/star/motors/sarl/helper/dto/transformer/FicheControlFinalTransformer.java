

/*
 * Java transformer for entity table fiche_control_final 
 * Created on 2019-08-18 ( Time 17:15:10 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "fiche_control_final"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface FicheControlFinalTransformer {

	FicheControlFinalTransformer INSTANCE = Mappers.getMapper(FicheControlFinalTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateValidationControleurFinal", dateFormat="dd/MM/yyyy",target="dateValidationControleurFinal"),

		@Mapping(source="entity.dateValidationReception", dateFormat="dd/MM/yyyy",target="dateValidationReception"),

		@Mapping(source="entity.dateValidationFacturation", dateFormat="dd/MM/yyyy",target="dateValidationFacturation"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.userControleurFinal.id", target="userControleurFinalId"),
		@Mapping(source="entity.userControleurFinal.nom", target="userControleurFinalNom"),
		@Mapping(source="entity.userControleurFinal.prenom", target="userControleurFinalPrenom"),
		@Mapping(source="entity.userControleurFinal.login", target="userControleurFinalLogin"),
		@Mapping(source="entity.etat.id", target="etatId"),
		@Mapping(source="entity.etat.code", target="etatCode"),
		@Mapping(source="entity.etat.libelle", target="etatLibelle"),
		@Mapping(source="entity.userFacturation.id", target="userFacturationId"),
		@Mapping(source="entity.userFacturation.nom", target="userFacturationNom"),
		@Mapping(source="entity.userFacturation.prenom", target="userFacturationPrenom"),
		@Mapping(source="entity.userFacturation.login", target="userFacturationLogin"),
		@Mapping(source="entity.userReception.id", target="userReceptionId"),
		@Mapping(source="entity.userReception.nom", target="userReceptionNom"),
		@Mapping(source="entity.userReception.prenom", target="userReceptionPrenom"),
		@Mapping(source="entity.userReception.login", target="userReceptionLogin"),
		@Mapping(source="entity.ordreReparation.id", target="ordreReparationId"),
	})
	FicheControlFinalDto toDto(FicheControlFinal entity) throws ParseException;

    List<FicheControlFinalDto> toDtos(List<FicheControlFinal> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.isValidedByControleurFinal", target="isValidedByControleurFinal"),
		@Mapping(source="dto.isValidedByReception", target="isValidedByReception"),
		@Mapping(source="dto.isValidedByFacturation", target="isValidedByFacturation"),
		@Mapping(source="dto.observationsControleurFinal", target="observationsControleurFinal"),
		@Mapping(source="dto.observationsReception", target="observationsReception"),
		@Mapping(source="dto.observationsFacturation", target="observationsFacturation"),
		@Mapping(source="dto.dateValidationControleurFinal", dateFormat="dd/MM/yyyy",target="dateValidationControleurFinal"),
		@Mapping(source="dto.dateValidationReception", dateFormat="dd/MM/yyyy",target="dateValidationReception"),
		@Mapping(source="dto.dateValidationFacturation", dateFormat="dd/MM/yyyy",target="dateValidationFacturation"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="userControleurFinal", target="userControleurFinal"),
		@Mapping(source="etat", target="etat"),
		@Mapping(source="userFacturation", target="userFacturation"),
		@Mapping(source="userReception", target="userReception"),
		@Mapping(source="ordreReparation", target="ordreReparation"),
	})
    FicheControlFinal toEntity(FicheControlFinalDto dto, User userControleurFinal, Etat etat, User userFacturation, User userReception, OrdreReparation ordreReparation) throws ParseException;

    //List<FicheControlFinal> toEntities(List<FicheControlFinalDto> dtos) throws ParseException;

}
