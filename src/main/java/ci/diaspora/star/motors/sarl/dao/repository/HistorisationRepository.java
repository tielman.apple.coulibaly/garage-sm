package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._HistorisationRepository;

/**
 * Repository : Historisation.
 */
@Repository
public interface HistorisationRepository extends JpaRepository<Historisation, Integer>, _HistorisationRepository {
	/**
	 * Finds Historisation by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Historisation whose id is equals to the given id. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.id = :id and e.isDeleted = :isDeleted")
	Historisation findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Historisation by using ficheId as a search criteria.
	 *
	 * @param ficheId
	 * @return An Object Historisation whose ficheId is equals to the given ficheId. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.ficheId = :ficheId and e.isDeleted = :isDeleted")
	List<Historisation> findByFicheId(@Param("ficheId")Integer ficheId, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Historisation by using ficheAvant as a search criteria.
	 *
	 * @param ficheAvant
	 * @return An Object Historisation whose ficheAvant is equals to the given ficheAvant. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.ficheAvant = :ficheAvant and e.isDeleted = :isDeleted")
	List<Historisation> findByFicheAvant(@Param("ficheAvant")String ficheAvant, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Historisation by using ficheApres as a search criteria.
	 *
	 * @param ficheApres
	 * @return An Object Historisation whose ficheApres is equals to the given ficheApres. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.ficheApres = :ficheApres and e.isDeleted = :isDeleted")
	List<Historisation> findByFicheApres(@Param("ficheApres")String ficheApres, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Historisation by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Historisation whose isDeleted is equals to the given isDeleted. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.isDeleted = :isDeleted")
	List<Historisation> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Historisation by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Historisation whose createdAt is equals to the given createdAt. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Historisation> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Historisation by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Historisation whose updatedAt is equals to the given updatedAt. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Historisation> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Historisation by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Historisation whose deletedAt is equals to the given deletedAt. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Historisation> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Historisation by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Historisation whose createdBy is equals to the given createdBy. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Historisation> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Historisation by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Historisation whose updatedBy is equals to the given updatedBy. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Historisation> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Historisation by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Historisation whose deletedBy is equals to the given deletedBy. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Historisation> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Historisation by using actionId as a search criteria.
	 *
	 * @param actionId
	 * @return A list of Object Historisation whose actionId is equals to the given actionId. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.action.id = :actionId and e.isDeleted = :isDeleted")
	List<Historisation> findByActionId(@Param("actionId")Integer actionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Historisation by using actionId as a search criteria.
   *
   * @param actionId
   * @return An Object Historisation whose actionId is equals to the given actionId. If
   *         no Historisation is found, this method returns null.
   */
  @Query("select e from Historisation e where e.action.id = :actionId and e.isDeleted = :isDeleted")
  Historisation findHistorisationByActionId(@Param("actionId")Integer actionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Historisation by using typeFicheId as a search criteria.
	 *
	 * @param typeFicheId
	 * @return A list of Object Historisation whose typeFicheId is equals to the given typeFicheId. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.typeFiche.id = :typeFicheId and e.isDeleted = :isDeleted")
	List<Historisation> findByTypeFicheId(@Param("typeFicheId")Integer typeFicheId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Historisation by using typeFicheId as a search criteria.
   *
   * @param typeFicheId
   * @return An Object Historisation whose typeFicheId is equals to the given typeFicheId. If
   *         no Historisation is found, this method returns null.
   */
  @Query("select e from Historisation e where e.typeFiche.id = :typeFicheId and e.isDeleted = :isDeleted")
  Historisation findHistorisationByTypeFicheId(@Param("typeFicheId")Integer typeFicheId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Historisation by using userId as a search criteria.
	 *
	 * @param userId
	 * @return A list of Object Historisation whose userId is equals to the given userId. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.user.id = :userId and e.isDeleted = :isDeleted")
	List<Historisation> findByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Historisation by using userId as a search criteria.
   *
   * @param userId
   * @return An Object Historisation whose userId is equals to the given userId. If
   *         no Historisation is found, this method returns null.
   */
  @Query("select e from Historisation e where e.user.id = :userId and e.isDeleted = :isDeleted")
  Historisation findHistorisationByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Historisation by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object Historisation whose etatId is equals to the given etatId. If
	 *         no Historisation is found, this method returns null.
	 */
	@Query("select e from Historisation e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
	List<Historisation> findByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Historisation by using etatId as a search criteria.
   *
   * @param etatId
   * @return An Object Historisation whose etatId is equals to the given etatId. If
   *         no Historisation is found, this method returns null.
   */
  @Query("select e from Historisation e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
  Historisation findHistorisationByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Historisation by using historisationDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Historisation
	 * @throws DataAccessException,ParseException
	 */
	public default List<Historisation> getByCriteria(Request<HistorisationDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Historisation e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Historisation> query = em.createQuery(req, Historisation.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Historisation by using historisationDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Historisation
	 *
	 */
	public default Long count(Request<HistorisationDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Historisation e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<HistorisationDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		HistorisationDto dto = request.getData() != null ? request.getData() : new HistorisationDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (HistorisationDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(HistorisationDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getFicheId()!= null && dto.getFicheId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ficheId", dto.getFicheId(), "e.ficheId", "Integer", dto.getFicheIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getFicheAvant())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ficheAvant", dto.getFicheAvant(), "e.ficheAvant", "String", dto.getFicheAvantParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getFicheApres())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ficheApres", dto.getFicheApres(), "e.ficheApres", "String", dto.getFicheApresParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getActionId()!= null && dto.getActionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("actionId", dto.getActionId(), "e.action.id", "Integer", dto.getActionIdParam(), param, index, locale));
			}
			if (dto.getTypeFicheId()!= null && dto.getTypeFicheId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeFicheId", dto.getTypeFicheId(), "e.typeFiche.id", "Integer", dto.getTypeFicheIdParam(), param, index, locale));
			}
			if (dto.getUserId()!= null && dto.getUserId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
			}
			if (dto.getEtatId()!= null && dto.getEtatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatId", dto.getEtatId(), "e.etat.id", "Integer", dto.getEtatIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getActionCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("actionCode", dto.getActionCode(), "e.action.code", "String", dto.getActionCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getActionLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("actionLibelle", dto.getActionLibelle(), "e.action.libelle", "String", dto.getActionLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeFicheCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeFicheCode", dto.getTypeFicheCode(), "e.typeFiche.code", "String", dto.getTypeFicheCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeFicheLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeFicheLibelle", dto.getTypeFicheLibelle(), "e.typeFiche.libelle", "String", dto.getTypeFicheLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenom", dto.getUserPrenom(), "e.user.prenom", "String", dto.getUserPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatLibelle", dto.getEtatLibelle(), "e.etat.libelle", "String", dto.getEtatLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
