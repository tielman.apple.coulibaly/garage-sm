package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._FicheReceptionPieceVehiculeRepository;

/**
 * Repository : FicheReceptionPieceVehicule.
 */
@Repository
public interface FicheReceptionPieceVehiculeRepository extends JpaRepository<FicheReceptionPieceVehicule, Integer>, _FicheReceptionPieceVehiculeRepository {
	/**
	 * Finds FicheReceptionPieceVehicule by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object FicheReceptionPieceVehicule whose id is equals to the given id. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.id = :id and e.isDeleted = :isDeleted")
	FicheReceptionPieceVehicule findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds FicheReceptionPieceVehicule by using detailAutres as a search criteria.
	 *
	 * @param detailAutres
	 * @return An Object FicheReceptionPieceVehicule whose detailAutres is equals to the given detailAutres. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.detailAutres = :detailAutres and e.isDeleted = :isDeleted")
	List<FicheReceptionPieceVehicule> findByDetailAutres(@Param("detailAutres")String detailAutres, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReceptionPieceVehicule by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object FicheReceptionPieceVehicule whose isDeleted is equals to the given isDeleted. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.isDeleted = :isDeleted")
	List<FicheReceptionPieceVehicule> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReceptionPieceVehicule by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object FicheReceptionPieceVehicule whose createdAt is equals to the given createdAt. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<FicheReceptionPieceVehicule> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReceptionPieceVehicule by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object FicheReceptionPieceVehicule whose updatedAt is equals to the given updatedAt. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<FicheReceptionPieceVehicule> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReceptionPieceVehicule by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object FicheReceptionPieceVehicule whose deletedAt is equals to the given deletedAt. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<FicheReceptionPieceVehicule> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReceptionPieceVehicule by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object FicheReceptionPieceVehicule whose createdBy is equals to the given createdBy. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<FicheReceptionPieceVehicule> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReceptionPieceVehicule by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object FicheReceptionPieceVehicule whose updatedBy is equals to the given updatedBy. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<FicheReceptionPieceVehicule> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReceptionPieceVehicule by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object FicheReceptionPieceVehicule whose deletedBy is equals to the given deletedBy. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<FicheReceptionPieceVehicule> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds FicheReceptionPieceVehicule by using pieceVehiculeId as a search criteria.
	 *
	 * @param pieceVehiculeId
	 * @return A list of Object FicheReceptionPieceVehicule whose pieceVehiculeId is equals to the given pieceVehiculeId. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.pieceVehicule.id = :pieceVehiculeId and e.isDeleted = :isDeleted")
	List<FicheReceptionPieceVehicule> findByPieceVehiculeId(@Param("pieceVehiculeId")Integer pieceVehiculeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheReceptionPieceVehicule by using pieceVehiculeId as a search criteria.
   *
   * @param pieceVehiculeId
   * @return An Object FicheReceptionPieceVehicule whose pieceVehiculeId is equals to the given pieceVehiculeId. If
   *         no FicheReceptionPieceVehicule is found, this method returns null.
   */
  @Query("select e from FicheReceptionPieceVehicule e where e.pieceVehicule.id = :pieceVehiculeId and e.isDeleted = :isDeleted")
  FicheReceptionPieceVehicule findFicheReceptionPieceVehiculeByPieceVehiculeId(@Param("pieceVehiculeId")Integer pieceVehiculeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FicheReceptionPieceVehicule by using ficheReceptionId as a search criteria.
	 *
	 * @param ficheReceptionId
	 * @return A list of Object FicheReceptionPieceVehicule whose ficheReceptionId is equals to the given ficheReceptionId. If
	 *         no FicheReceptionPieceVehicule is found, this method returns null.
	 */
	@Query("select e from FicheReceptionPieceVehicule e where e.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
	List<FicheReceptionPieceVehicule> findByFicheReceptionId(@Param("ficheReceptionId")Integer ficheReceptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheReceptionPieceVehicule by using ficheReceptionId as a search criteria.
   *
   * @param ficheReceptionId
   * @return An Object FicheReceptionPieceVehicule whose ficheReceptionId is equals to the given ficheReceptionId. If
   *         no FicheReceptionPieceVehicule is found, this method returns null.
   */
  @Query("select e from FicheReceptionPieceVehicule e where e.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
  FicheReceptionPieceVehicule findFicheReceptionPieceVehiculeByFicheReceptionId(@Param("ficheReceptionId")Integer ficheReceptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of FicheReceptionPieceVehicule by using ficheReceptionPieceVehiculeDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of FicheReceptionPieceVehicule
	 * @throws DataAccessException,ParseException
	 */
	public default List<FicheReceptionPieceVehicule> getByCriteria(Request<FicheReceptionPieceVehiculeDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from FicheReceptionPieceVehicule e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<FicheReceptionPieceVehicule> query = em.createQuery(req, FicheReceptionPieceVehicule.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of FicheReceptionPieceVehicule by using ficheReceptionPieceVehiculeDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of FicheReceptionPieceVehicule
	 *
	 */
	public default Long count(Request<FicheReceptionPieceVehiculeDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from FicheReceptionPieceVehicule e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<FicheReceptionPieceVehiculeDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		FicheReceptionPieceVehiculeDto dto = request.getData() != null ? request.getData() : new FicheReceptionPieceVehiculeDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (FicheReceptionPieceVehiculeDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(FicheReceptionPieceVehiculeDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDetailAutres())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("detailAutres", dto.getDetailAutres(), "e.detailAutres", "String", dto.getDetailAutresParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getPieceVehiculeId()!= null && dto.getPieceVehiculeId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pieceVehiculeId", dto.getPieceVehiculeId(), "e.pieceVehicule.id", "Integer", dto.getPieceVehiculeIdParam(), param, index, locale));
			}
			if (dto.getFicheReceptionId()!= null && dto.getFicheReceptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ficheReceptionId", dto.getFicheReceptionId(), "e.ficheReception.id", "Integer", dto.getFicheReceptionIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPieceVehiculeLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pieceVehiculeLibelle", dto.getPieceVehiculeLibelle(), "e.pieceVehicule.libelle", "String", dto.getPieceVehiculeLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
