package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._DesignationElementFicheControleTypeElementFicheControlRepository;

/**
 * Repository : DesignationElementFicheControleTypeElementFicheControl.
 */
@Repository
public interface DesignationElementFicheControleTypeElementFicheControlRepository extends JpaRepository<DesignationElementFicheControleTypeElementFicheControl, Integer>, _DesignationElementFicheControleTypeElementFicheControlRepository {
	/**
	 * Finds DesignationElementFicheControleTypeElementFicheControl by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object DesignationElementFicheControleTypeElementFicheControl whose id is equals to the given id. If
	 *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.id = :id and e.isDeleted = :isDeleted")
	DesignationElementFicheControleTypeElementFicheControl findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds DesignationElementFicheControleTypeElementFicheControl by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object DesignationElementFicheControleTypeElementFicheControl whose isDeleted is equals to the given isDeleted. If
	 *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleTypeElementFicheControl> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleTypeElementFicheControl by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object DesignationElementFicheControleTypeElementFicheControl whose createdAt is equals to the given createdAt. If
	 *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleTypeElementFicheControl> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleTypeElementFicheControl by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object DesignationElementFicheControleTypeElementFicheControl whose updatedAt is equals to the given updatedAt. If
	 *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleTypeElementFicheControl> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleTypeElementFicheControl by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object DesignationElementFicheControleTypeElementFicheControl whose deletedAt is equals to the given deletedAt. If
	 *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleTypeElementFicheControl> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleTypeElementFicheControl by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object DesignationElementFicheControleTypeElementFicheControl whose createdBy is equals to the given createdBy. If
	 *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleTypeElementFicheControl> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleTypeElementFicheControl by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object DesignationElementFicheControleTypeElementFicheControl whose updatedBy is equals to the given updatedBy. If
	 *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleTypeElementFicheControl> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DesignationElementFicheControleTypeElementFicheControl by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object DesignationElementFicheControleTypeElementFicheControl whose deletedBy is equals to the given deletedBy. If
	 *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleTypeElementFicheControl> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds DesignationElementFicheControleTypeElementFicheControl by using typeNiveauControlId as a search criteria.
	 *
	 * @param typeNiveauControlId
	 * @return A list of Object DesignationElementFicheControleTypeElementFicheControl whose typeNiveauControlId is equals to the given typeNiveauControlId. If
	 *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.typeElementFicheControl.id = :typeNiveauControlId and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleTypeElementFicheControl> findByTypeNiveauControlId(@Param("typeNiveauControlId")Integer typeNiveauControlId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one DesignationElementFicheControleTypeElementFicheControl by using typeNiveauControlId as a search criteria.
   *
   * @param typeNiveauControlId
   * @return An Object DesignationElementFicheControleTypeElementFicheControl whose typeNiveauControlId is equals to the given typeNiveauControlId. If
   *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
   */
  @Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.typeElementFicheControl.id = :typeNiveauControlId and e.isDeleted = :isDeleted")
  DesignationElementFicheControleTypeElementFicheControl findDesignationElementFicheControleTypeElementFicheControlByTypeNiveauControlId(@Param("typeNiveauControlId")Integer typeNiveauControlId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds DesignationElementFicheControleTypeElementFicheControl by using designationNiveauControlId as a search criteria.
	 *
	 * @param designationNiveauControlId
	 * @return A list of Object DesignationElementFicheControleTypeElementFicheControl whose designationNiveauControlId is equals to the given designationNiveauControlId. If
	 *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.designationElementFicheControle.id = :designationNiveauControlId and e.isDeleted = :isDeleted")
	List<DesignationElementFicheControleTypeElementFicheControl> findByDesignationNiveauControlId(@Param("designationNiveauControlId")Integer designationNiveauControlId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one DesignationElementFicheControleTypeElementFicheControl by using designationNiveauControlId as a search criteria.
   *
   * @param designationNiveauControlId
   * @return An Object DesignationElementFicheControleTypeElementFicheControl whose designationNiveauControlId is equals to the given designationNiveauControlId. If
   *         no DesignationElementFicheControleTypeElementFicheControl is found, this method returns null.
   */
  @Query("select e from DesignationElementFicheControleTypeElementFicheControl e where e.designationElementFicheControle.id = :designationNiveauControlId and e.isDeleted = :isDeleted")
  DesignationElementFicheControleTypeElementFicheControl findDesignationElementFicheControleTypeElementFicheControlByDesignationNiveauControlId(@Param("designationNiveauControlId")Integer designationNiveauControlId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of DesignationElementFicheControleTypeElementFicheControl by using designationElementFicheControleTypeElementFicheControlDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of DesignationElementFicheControleTypeElementFicheControl
	 * @throws DataAccessException,ParseException
	 */
	public default List<DesignationElementFicheControleTypeElementFicheControl> getByCriteria(Request<DesignationElementFicheControleTypeElementFicheControlDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from DesignationElementFicheControleTypeElementFicheControl e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<DesignationElementFicheControleTypeElementFicheControl> query = em.createQuery(req, DesignationElementFicheControleTypeElementFicheControl.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of DesignationElementFicheControleTypeElementFicheControl by using designationElementFicheControleTypeElementFicheControlDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of DesignationElementFicheControleTypeElementFicheControl
	 *
	 */
	public default Long count(Request<DesignationElementFicheControleTypeElementFicheControlDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from DesignationElementFicheControleTypeElementFicheControl e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<DesignationElementFicheControleTypeElementFicheControlDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		DesignationElementFicheControleTypeElementFicheControlDto dto = request.getData() != null ? request.getData() : new DesignationElementFicheControleTypeElementFicheControlDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (DesignationElementFicheControleTypeElementFicheControlDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(DesignationElementFicheControleTypeElementFicheControlDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getTypeNiveauControlId()!= null && dto.getTypeNiveauControlId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeNiveauControlId", dto.getTypeNiveauControlId(), "e.typeElementFicheControl.id", "Integer", dto.getTypeNiveauControlIdParam(), param, index, locale));
			}
			if (dto.getDesignationNiveauControlId()!= null && dto.getDesignationNiveauControlId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("designationNiveauControlId", dto.getDesignationNiveauControlId(), "e.designationElementFicheControle.id", "Integer", dto.getDesignationNiveauControlIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeElementFicheControlLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeElementFicheControlLibelle", dto.getTypeElementFicheControlLibelle(), "e.typeElementFicheControl.libelle", "String", dto.getTypeElementFicheControlLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeElementFicheControlCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeElementFicheControlCode", dto.getTypeElementFicheControlCode(), "e.typeElementFicheControl.code", "String", dto.getTypeElementFicheControlCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDesignationElementFicheControleLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("designationElementFicheControleLibelle", dto.getDesignationElementFicheControleLibelle(), "e.designationElementFicheControle.libelle", "String", dto.getDesignationElementFicheControleLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
