
/*
 * Java dto for entity table fiche_reception_piece_vehicule 
 * Created on 2019-09-09 ( Time 13:30:34 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;

/**
 * DTO customize for table "fiche_reception_piece_vehicule"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _FicheReceptionPieceVehiculeDto {

}
