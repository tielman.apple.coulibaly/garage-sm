
/*
 * Java dto for entity table fiche_control_final
 * Created on 2019-08-18 ( Time 17:15:10 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._FicheControlFinalDto;

/**
 * DTO for table "fiche_control_final"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class FicheControlFinalDto extends _FicheControlFinalDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    ordreReparationId    ;
	/*
	 * 
	 */
    private Integer    userControleurFinalId ;
	/*
	 * 
	 */
    private Integer    userReceptionId      ;
	/*
	 * 
	 */
    private Integer    userFacturationId    ;
	/*
	 * 
	 */
    private Boolean    isValidedByControleurFinal ;
	/*
	 * 
	 */
    private Boolean    isValidedByReception ;
	/*
	 * 
	 */
    private Boolean    isValidedByFacturation ;
	/*
	 * 
	 */
    private String     observationsControleurFinal ;
	/*
	 * 
	 */
    private String     observationsReception ;
	/*
	 * 
	 */
    private String     observationsFacturation ;
	/*
	 * 
	 */
	private String     dateValidationControleurFinal ;
	/*
	 * 
	 */
	private String     dateValidationReception ;
	/*
	 * 
	 */
	private String     dateValidationFacturation ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Integer    etatId               ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String userReceptionNom;
	private String userReceptionPrenom;
	private String userReceptionLogin;
	private String etatCode;
	private String etatLibelle;
	private String userControleurFinalNom;
	private String userControleurFinalPrenom;
	private String userControleurFinalLogin;
	private String userFacturationNom;
	private String userFacturationPrenom;
	private String userFacturationLogin;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  ordreReparationIdParam;                     
	private SearchParam<Integer>  userControleurFinalIdParam;                     
	private SearchParam<Integer>  userReceptionIdParam  ;                     
	private SearchParam<Integer>  userFacturationIdParam;                     
	private SearchParam<Boolean>  isValidedByControleurFinalParam;                     
	private SearchParam<Boolean>  isValidedByReceptionParam;                     
	private SearchParam<Boolean>  isValidedByFacturationParam;                     
	private SearchParam<String>   observationsControleurFinalParam;                     
	private SearchParam<String>   observationsReceptionParam;                     
	private SearchParam<String>   observationsFacturationParam;                     

		private SearchParam<String>   dateValidationControleurFinalParam;                     

		private SearchParam<String>   dateValidationReceptionParam;                     

		private SearchParam<String>   dateValidationFacturationParam;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Integer>  etatIdParam           ;                     
	private SearchParam<String>   userReceptionNomParam          ;                     
	private SearchParam<String>   userReceptionPrenomParam       ;                     
	private SearchParam<String>   userReceptionLoginParam        ;                     
	private SearchParam<String>   etatCodeParam         ;                     
	private SearchParam<String>   etatLibelleParam      ;                     
	private SearchParam<String>   userControleurFinalNomParam          ;                     
	private SearchParam<String>   userControleurFinalPrenomParam       ;                     
	private SearchParam<String>   userControleurFinalLoginParam        ;                     
	private SearchParam<String>   userFacturationNomParam          ;                     
	private SearchParam<String>   userFacturationPrenomParam       ;                     
	private SearchParam<String>   userFacturationLoginParam        ;                     
    /**
     * Default constructor
     */
    public FicheControlFinalDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "ordreReparationId" field value
     * @param ordreReparationId
     */
	public void setOrdreReparationId( Integer ordreReparationId )
    {
        this.ordreReparationId = ordreReparationId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getOrdreReparationId()
    {
        return this.ordreReparationId;
    }

    /**
     * Set the "userControleurFinalId" field value
     * @param userControleurFinalId
     */
	public void setUserControleurFinalId( Integer userControleurFinalId )
    {
        this.userControleurFinalId = userControleurFinalId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserControleurFinalId()
    {
        return this.userControleurFinalId;
    }

    /**
     * Set the "userReceptionId" field value
     * @param userReceptionId
     */
	public void setUserReceptionId( Integer userReceptionId )
    {
        this.userReceptionId = userReceptionId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserReceptionId()
    {
        return this.userReceptionId;
    }

    /**
     * Set the "userFacturationId" field value
     * @param userFacturationId
     */
	public void setUserFacturationId( Integer userFacturationId )
    {
        this.userFacturationId = userFacturationId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserFacturationId()
    {
        return this.userFacturationId;
    }

    /**
     * Set the "isValidedByControleurFinal" field value
     * @param isValidedByControleurFinal
     */
	public void setIsValidedByControleurFinal( Boolean isValidedByControleurFinal )
    {
        this.isValidedByControleurFinal = isValidedByControleurFinal ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByControleurFinal()
    {
        return this.isValidedByControleurFinal;
    }

    /**
     * Set the "isValidedByReception" field value
     * @param isValidedByReception
     */
	public void setIsValidedByReception( Boolean isValidedByReception )
    {
        this.isValidedByReception = isValidedByReception ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByReception()
    {
        return this.isValidedByReception;
    }

    /**
     * Set the "isValidedByFacturation" field value
     * @param isValidedByFacturation
     */
	public void setIsValidedByFacturation( Boolean isValidedByFacturation )
    {
        this.isValidedByFacturation = isValidedByFacturation ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValidedByFacturation()
    {
        return this.isValidedByFacturation;
    }

    /**
     * Set the "observationsControleurFinal" field value
     * @param observationsControleurFinal
     */
	public void setObservationsControleurFinal( String observationsControleurFinal )
    {
        this.observationsControleurFinal = observationsControleurFinal ;
    }
    /**
     * 
     * @return the field value
     */
	public String getObservationsControleurFinal()
    {
        return this.observationsControleurFinal;
    }

    /**
     * Set the "observationsReception" field value
     * @param observationsReception
     */
	public void setObservationsReception( String observationsReception )
    {
        this.observationsReception = observationsReception ;
    }
    /**
     * 
     * @return the field value
     */
	public String getObservationsReception()
    {
        return this.observationsReception;
    }

    /**
     * Set the "observationsFacturation" field value
     * @param observationsFacturation
     */
	public void setObservationsFacturation( String observationsFacturation )
    {
        this.observationsFacturation = observationsFacturation ;
    }
    /**
     * 
     * @return the field value
     */
	public String getObservationsFacturation()
    {
        return this.observationsFacturation;
    }

    /**
     * Set the "dateValidationControleurFinal" field value
     * @param dateValidationControleurFinal
     */
	public void setDateValidationControleurFinal( String dateValidationControleurFinal )
    {
        this.dateValidationControleurFinal = dateValidationControleurFinal ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateValidationControleurFinal()
    {
        return this.dateValidationControleurFinal;
    }

    /**
     * Set the "dateValidationReception" field value
     * @param dateValidationReception
     */
	public void setDateValidationReception( String dateValidationReception )
    {
        this.dateValidationReception = dateValidationReception ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateValidationReception()
    {
        return this.dateValidationReception;
    }

    /**
     * Set the "dateValidationFacturation" field value
     * @param dateValidationFacturation
     */
	public void setDateValidationFacturation( String dateValidationFacturation )
    {
        this.dateValidationFacturation = dateValidationFacturation ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDateValidationFacturation()
    {
        return this.dateValidationFacturation;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }

    /**
     * Set the "etatId" field value
     * @param etatId
     */
	public void setEtatId( Integer etatId )
    {
        this.etatId = etatId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getEtatId()
    {
        return this.etatId;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	
	public String getEtatCode()
    {
        return this.etatCode;
    }
	public void setEtatCode(String etatCode)
    {
        this.etatCode = etatCode;
    }

	public String getEtatLibelle()
    {
        return this.etatLibelle;
    }
	public void setEtatLibelle(String etatLibelle)
    {
        this.etatLibelle = etatLibelle;
    }

	


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "ordreReparationIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getOrdreReparationIdParam(){
		return this.ordreReparationIdParam;
	}
	/**
     * Set the "ordreReparationIdParam" field value
     * @param ordreReparationIdParam
     */
    public void setOrdreReparationIdParam( SearchParam<Integer> ordreReparationIdParam ){
        this.ordreReparationIdParam = ordreReparationIdParam;
    }

	/**
     * Get the "userControleurFinalIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserControleurFinalIdParam(){
		return this.userControleurFinalIdParam;
	}
	/**
     * Set the "userControleurFinalIdParam" field value
     * @param userControleurFinalIdParam
     */
    public void setUserControleurFinalIdParam( SearchParam<Integer> userControleurFinalIdParam ){
        this.userControleurFinalIdParam = userControleurFinalIdParam;
    }

	/**
     * Get the "userReceptionIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserReceptionIdParam(){
		return this.userReceptionIdParam;
	}
	/**
     * Set the "userReceptionIdParam" field value
     * @param userReceptionIdParam
     */
    public void setUserReceptionIdParam( SearchParam<Integer> userReceptionIdParam ){
        this.userReceptionIdParam = userReceptionIdParam;
    }

	/**
     * Get the "userFacturationIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserFacturationIdParam(){
		return this.userFacturationIdParam;
	}
	/**
     * Set the "userFacturationIdParam" field value
     * @param userFacturationIdParam
     */
    public void setUserFacturationIdParam( SearchParam<Integer> userFacturationIdParam ){
        this.userFacturationIdParam = userFacturationIdParam;
    }

	/**
     * Get the "isValidedByControleurFinalParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByControleurFinalParam(){
		return this.isValidedByControleurFinalParam;
	}
	/**
     * Set the "isValidedByControleurFinalParam" field value
     * @param isValidedByControleurFinalParam
     */
    public void setIsValidedByControleurFinalParam( SearchParam<Boolean> isValidedByControleurFinalParam ){
        this.isValidedByControleurFinalParam = isValidedByControleurFinalParam;
    }

	/**
     * Get the "isValidedByReceptionParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByReceptionParam(){
		return this.isValidedByReceptionParam;
	}
	/**
     * Set the "isValidedByReceptionParam" field value
     * @param isValidedByReceptionParam
     */
    public void setIsValidedByReceptionParam( SearchParam<Boolean> isValidedByReceptionParam ){
        this.isValidedByReceptionParam = isValidedByReceptionParam;
    }

	/**
     * Get the "isValidedByFacturationParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedByFacturationParam(){
		return this.isValidedByFacturationParam;
	}
	/**
     * Set the "isValidedByFacturationParam" field value
     * @param isValidedByFacturationParam
     */
    public void setIsValidedByFacturationParam( SearchParam<Boolean> isValidedByFacturationParam ){
        this.isValidedByFacturationParam = isValidedByFacturationParam;
    }

	/**
     * Get the "observationsControleurFinalParam" field value
     * @return the field value
     */
	public SearchParam<String> getObservationsControleurFinalParam(){
		return this.observationsControleurFinalParam;
	}
	/**
     * Set the "observationsControleurFinalParam" field value
     * @param observationsControleurFinalParam
     */
    public void setObservationsControleurFinalParam( SearchParam<String> observationsControleurFinalParam ){
        this.observationsControleurFinalParam = observationsControleurFinalParam;
    }

	/**
     * Get the "observationsReceptionParam" field value
     * @return the field value
     */
	public SearchParam<String> getObservationsReceptionParam(){
		return this.observationsReceptionParam;
	}
	/**
     * Set the "observationsReceptionParam" field value
     * @param observationsReceptionParam
     */
    public void setObservationsReceptionParam( SearchParam<String> observationsReceptionParam ){
        this.observationsReceptionParam = observationsReceptionParam;
    }

	/**
     * Get the "observationsFacturationParam" field value
     * @return the field value
     */
	public SearchParam<String> getObservationsFacturationParam(){
		return this.observationsFacturationParam;
	}
	/**
     * Set the "observationsFacturationParam" field value
     * @param observationsFacturationParam
     */
    public void setObservationsFacturationParam( SearchParam<String> observationsFacturationParam ){
        this.observationsFacturationParam = observationsFacturationParam;
    }

	/**
     * Get the "dateValidationControleurFinalParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateValidationControleurFinalParam(){
		return this.dateValidationControleurFinalParam;
	}
	/**
     * Set the "dateValidationControleurFinalParam" field value
     * @param dateValidationControleurFinalParam
     */
    public void setDateValidationControleurFinalParam( SearchParam<String> dateValidationControleurFinalParam ){
        this.dateValidationControleurFinalParam = dateValidationControleurFinalParam;
    }

	/**
     * Get the "dateValidationReceptionParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateValidationReceptionParam(){
		return this.dateValidationReceptionParam;
	}
	/**
     * Set the "dateValidationReceptionParam" field value
     * @param dateValidationReceptionParam
     */
    public void setDateValidationReceptionParam( SearchParam<String> dateValidationReceptionParam ){
        this.dateValidationReceptionParam = dateValidationReceptionParam;
    }

	/**
     * Get the "dateValidationFacturationParam" field value
     * @return the field value
     */
	public SearchParam<String> getDateValidationFacturationParam(){
		return this.dateValidationFacturationParam;
	}
	/**
     * Set the "dateValidationFacturationParam" field value
     * @param dateValidationFacturationParam
     */
    public void setDateValidationFacturationParam( SearchParam<String> dateValidationFacturationParam ){
        this.dateValidationFacturationParam = dateValidationFacturationParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

	/**
     * Get the "etatIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getEtatIdParam(){
		return this.etatIdParam;
	}
	/**
     * Set the "etatIdParam" field value
     * @param etatIdParam
     */
    public void setEtatIdParam( SearchParam<Integer> etatIdParam ){
        this.etatIdParam = etatIdParam;
    }

	
    
    
    
    
		/**
     * Get the "etatCodeParam" field value
     * @return the field value
     */
	public SearchParam<String> getEtatCodeParam(){
		return this.etatCodeParam;
	}
	/**
     * Set the "etatCodeParam" field value
     * @param etatCodeParam
     */
    public void setEtatCodeParam( SearchParam<String> etatCodeParam ){
        this.etatCodeParam = etatCodeParam;
    }

		/**
     * Get the "etatLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getEtatLibelleParam(){
		return this.etatLibelleParam;
	}
	/**
     * Set the "etatLibelleParam" field value
     * @param etatLibelleParam
     */
    public void setEtatLibelleParam( SearchParam<String> etatLibelleParam ){
        this.etatLibelleParam = etatLibelleParam;
    }

	
	public String getUserReceptionNom() {
		return userReceptionNom;
	}

	public void setUserReceptionNom(String userReceptionNom) {
		this.userReceptionNom = userReceptionNom;
	}

	public String getUserReceptionPrenom() {
		return userReceptionPrenom;
	}

	public void setUserReceptionPrenom(String userReceptionPrenom) {
		this.userReceptionPrenom = userReceptionPrenom;
	}

	public String getUserReceptionLogin() {
		return userReceptionLogin;
	}

	public void setUserReceptionLogin(String userReceptionLogin) {
		this.userReceptionLogin = userReceptionLogin;
	}

	public String getUserControleurFinalNom() {
		return userControleurFinalNom;
	}

	public void setUserControleurFinalNom(String userControleurFinalNom) {
		this.userControleurFinalNom = userControleurFinalNom;
	}

	public String getUserControleurFinalPrenom() {
		return userControleurFinalPrenom;
	}

	public void setUserControleurFinalPrenom(String userControleurFinalPrenom) {
		this.userControleurFinalPrenom = userControleurFinalPrenom;
	}

	public String getUserControleurFinalLogin() {
		return userControleurFinalLogin;
	}

	public void setUserControleurFinalLogin(String userControleurFinalLogin) {
		this.userControleurFinalLogin = userControleurFinalLogin;
	}

	public String getUserFacturationNom() {
		return userFacturationNom;
	}

	public void setUserFacturationNom(String userFacturationNom) {
		this.userFacturationNom = userFacturationNom;
	}

	public String getUserFacturationPrenom() {
		return userFacturationPrenom;
	}

	public void setUserFacturationPrenom(String userFacturationPrenom) {
		this.userFacturationPrenom = userFacturationPrenom;
	}

	public String getUserFacturationLogin() {
		return userFacturationLogin;
	}

	public void setUserFacturationLogin(String userFacturationLogin) {
		this.userFacturationLogin = userFacturationLogin;
	}

	public SearchParam<String> getUserReceptionNomParam() {
		return userReceptionNomParam;
	}

	public void setUserReceptionNomParam(SearchParam<String> userReceptionNomParam) {
		this.userReceptionNomParam = userReceptionNomParam;
	}

	public SearchParam<String> getUserReceptionPrenomParam() {
		return userReceptionPrenomParam;
	}

	public void setUserReceptionPrenomParam(SearchParam<String> userReceptionPrenomParam) {
		this.userReceptionPrenomParam = userReceptionPrenomParam;
	}

	public SearchParam<String> getUserReceptionLoginParam() {
		return userReceptionLoginParam;
	}

	public void setUserReceptionLoginParam(SearchParam<String> userReceptionLoginParam) {
		this.userReceptionLoginParam = userReceptionLoginParam;
	}

	public SearchParam<String> getUserControleurFinalNomParam() {
		return userControleurFinalNomParam;
	}

	public void setUserControleurFinalNomParam(SearchParam<String> userControleurFinalNomParam) {
		this.userControleurFinalNomParam = userControleurFinalNomParam;
	}

	public SearchParam<String> getUserControleurFinalPrenomParam() {
		return userControleurFinalPrenomParam;
	}

	public void setUserControleurFinalPrenomParam(SearchParam<String> userControleurFinalPrenomParam) {
		this.userControleurFinalPrenomParam = userControleurFinalPrenomParam;
	}

	public SearchParam<String> getUserControleurFinalLoginParam() {
		return userControleurFinalLoginParam;
	}

	public void setUserControleurFinalLoginParam(SearchParam<String> userControleurFinalLoginParam) {
		this.userControleurFinalLoginParam = userControleurFinalLoginParam;
	}

	public SearchParam<String> getUserFacturationNomParam() {
		return userFacturationNomParam;
	}

	public void setUserFacturationNomParam(SearchParam<String> userFacturationNomParam) {
		this.userFacturationNomParam = userFacturationNomParam;
	}

	public SearchParam<String> getUserFacturationPrenomParam() {
		return userFacturationPrenomParam;
	}

	public void setUserFacturationPrenomParam(SearchParam<String> userFacturationPrenomParam) {
		this.userFacturationPrenomParam = userFacturationPrenomParam;
	}

	public SearchParam<String> getUserFacturationLoginParam() {
		return userFacturationLoginParam;
	}

	public void setUserFacturationLoginParam(SearchParam<String> userFacturationLoginParam) {
		this.userFacturationLoginParam = userFacturationLoginParam;
	}

	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		FicheControlFinalDto other = (FicheControlFinalDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("isValidedByControleurFinal:"+isValidedByControleurFinal);
		sb.append("|");
		sb.append("isValidedByReception:"+isValidedByReception);
		sb.append("|");
		sb.append("isValidedByFacturation:"+isValidedByFacturation);
		sb.append("|");
		sb.append("observationsControleurFinal:"+observationsControleurFinal);
		sb.append("|");
		sb.append("observationsReception:"+observationsReception);
		sb.append("|");
		sb.append("observationsFacturation:"+observationsFacturation);
		sb.append("|");
		sb.append("dateValidationControleurFinal:"+dateValidationControleurFinal);
		sb.append("|");
		sb.append("dateValidationReception:"+dateValidationReception);
		sb.append("|");
		sb.append("dateValidationFacturation:"+dateValidationFacturation);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
        return sb.toString();
    }
}
