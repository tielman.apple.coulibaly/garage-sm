


/*
 * Java transformer for entity table element_etat_vehicule
 * Created on 2019-08-19 ( Time 10:29:46 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "element_etat_vehicule"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class ElementEtatVehiculeBusiness implements IBasicBusiness<Request<ElementEtatVehiculeDto>, Response<ElementEtatVehiculeDto>> {

	private Response<ElementEtatVehiculeDto> response;
	@Autowired
	private ElementEtatVehiculeRepository elementEtatVehiculeRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ElementEtatVehiculeFicheReceptionRepository elementEtatVehiculeFicheReceptionRepository;
	@Autowired
	private CategorieRepository categorieRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private ElementEtatVehiculeFicheReceptionBusiness elementEtatVehiculeFicheReceptionBusiness;



	public ElementEtatVehiculeBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create ElementEtatVehicule by using ElementEtatVehiculeDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ElementEtatVehiculeDto> create(Request<ElementEtatVehiculeDto> request, Locale locale)  {
		slf4jLogger.info("----begin create ElementEtatVehicule-----");

		response = new Response<ElementEtatVehiculeDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ElementEtatVehicule> items = new ArrayList<ElementEtatVehicule>();

			for (ElementEtatVehiculeDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("categorieId", dto.getCategorieId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if elementEtatVehicule to insert do not exist
				ElementEtatVehicule existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("elementEtatVehicule -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if categorie exist
				Categorie existingCategorie = categorieRepository.findById(dto.getCategorieId(), false);
				if (existingCategorie == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("categorie -> " + dto.getCategorieId(), locale));
					response.setHasError(true);
					return response;
				}
				
				existingEntity = elementEtatVehiculeRepository.findByLibelleAndCategorieId(dto.getLibelle(), dto.getCategorieId(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("elementEtatVehicule -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && a.getCategorie().getId().equals(dto.getCategorieId()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les elementEtatVehicules", locale));
					response.setHasError(true);
					return response;
				}

				
				ElementEtatVehicule entityToSave = null;
				entityToSave = ElementEtatVehiculeTransformer.INSTANCE.toEntity(dto, existingCategorie);
				entityToSave.setIsDeleted(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ElementEtatVehicule> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = elementEtatVehiculeRepository.save((Iterable<ElementEtatVehicule>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("elementEtatVehicule", locale));
					response.setHasError(true);
					return response;
				}
				List<ElementEtatVehiculeDto> itemsDto = new ArrayList<ElementEtatVehiculeDto>();
				for (ElementEtatVehicule entity : itemsSaved) {
					ElementEtatVehiculeDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create ElementEtatVehicule-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ElementEtatVehicule by using ElementEtatVehiculeDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ElementEtatVehiculeDto> update(Request<ElementEtatVehiculeDto> request, Locale locale)  {
		slf4jLogger.info("----begin update ElementEtatVehicule-----");

		response = new Response<ElementEtatVehiculeDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ElementEtatVehicule> items = new ArrayList<ElementEtatVehicule>();

			for (ElementEtatVehiculeDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la elementEtatVehicule existe
				ElementEtatVehicule entityToSave = null;
				entityToSave = elementEtatVehiculeRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("elementEtatVehicule -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if categorie exist
				if (dto.getCategorieId() != null && dto.getCategorieId() > 0){
					Categorie existingCategorie = categorieRepository.findById(dto.getCategorieId(), false);
					if (existingCategorie == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("categorie -> " + dto.getCategorieId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCategorie(existingCategorie);
				}
				
				Integer categorieId = entityToSave.getCategorie().getId();
				if (Utilities.notBlank(dto.getLibelle())) {
					ElementEtatVehicule existingEntity = elementEtatVehiculeRepository.findByLibelleAndCategorieId(dto.getLibelle(), categorieId, false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("elementEtatVehicule -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && a.getCategorie().getId().equals(categorieId) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les elementEtatVehicules", locale));
						response.setHasError(true);
						return response;
					}

					entityToSave.setLibelle(dto.getLibelle());
				}
				
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ElementEtatVehicule> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = elementEtatVehiculeRepository.save((Iterable<ElementEtatVehicule>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("elementEtatVehicule", locale));
					response.setHasError(true);
					return response;
				}
				List<ElementEtatVehiculeDto> itemsDto = new ArrayList<ElementEtatVehiculeDto>();
				for (ElementEtatVehicule entity : itemsSaved) {
					ElementEtatVehiculeDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update ElementEtatVehicule-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete ElementEtatVehicule by using ElementEtatVehiculeDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ElementEtatVehiculeDto> delete(Request<ElementEtatVehiculeDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete ElementEtatVehicule-----");

		response = new Response<ElementEtatVehiculeDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ElementEtatVehicule> items = new ArrayList<ElementEtatVehicule>();

			for (ElementEtatVehiculeDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la elementEtatVehicule existe
				ElementEtatVehicule existingEntity = null;
				existingEntity = elementEtatVehiculeRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("elementEtatVehicule -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// elementEtatVehiculeFicheReception
				List<ElementEtatVehiculeFicheReception> listOfElementEtatVehiculeFicheReception = elementEtatVehiculeFicheReceptionRepository.findByElementEtatVehiculeId(existingEntity.getId(), false);
				if (listOfElementEtatVehiculeFicheReception != null && !listOfElementEtatVehiculeFicheReception.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfElementEtatVehiculeFicheReception.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				elementEtatVehiculeRepository.save((Iterable<ElementEtatVehicule>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete ElementEtatVehicule-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete ElementEtatVehicule by using ElementEtatVehiculeDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<ElementEtatVehiculeDto> forceDelete(Request<ElementEtatVehiculeDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete ElementEtatVehicule-----");

		response = new Response<ElementEtatVehiculeDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ElementEtatVehicule> items = new ArrayList<ElementEtatVehicule>();

			for (ElementEtatVehiculeDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la elementEtatVehicule existe
				ElementEtatVehicule existingEntity = null;
				existingEntity = elementEtatVehiculeRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("elementEtatVehicule -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// elementEtatVehiculeFicheReception
				List<ElementEtatVehiculeFicheReception> listOfElementEtatVehiculeFicheReception = elementEtatVehiculeFicheReceptionRepository.findByElementEtatVehiculeId(existingEntity.getId(), false);
				if (listOfElementEtatVehiculeFicheReception != null && !listOfElementEtatVehiculeFicheReception.isEmpty()){
					Request<ElementEtatVehiculeFicheReceptionDto> deleteRequest = new Request<ElementEtatVehiculeFicheReceptionDto>();
					deleteRequest.setDatas(ElementEtatVehiculeFicheReceptionTransformer.INSTANCE.toDtos(listOfElementEtatVehiculeFicheReception));
					deleteRequest.setUser(request.getUser());
					Response<ElementEtatVehiculeFicheReceptionDto> deleteResponse = elementEtatVehiculeFicheReceptionBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				elementEtatVehiculeRepository.save((Iterable<ElementEtatVehicule>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete ElementEtatVehicule-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get ElementEtatVehicule by using ElementEtatVehiculeDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<ElementEtatVehiculeDto> getByCriteria(Request<ElementEtatVehiculeDto> request, Locale locale) {
		slf4jLogger.info("----begin get ElementEtatVehicule-----");

		response = new Response<ElementEtatVehiculeDto>();

		try {
			List<ElementEtatVehicule> items = null;
			items = elementEtatVehiculeRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<ElementEtatVehiculeDto> itemsDto = new ArrayList<ElementEtatVehiculeDto>();
				for (ElementEtatVehicule entity : items) {
					ElementEtatVehiculeDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(elementEtatVehiculeRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("elementEtatVehicule", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get ElementEtatVehicule-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ElementEtatVehiculeDto by using ElementEtatVehicule as object.
	 *
	 * @param entity, locale
	 * @return ElementEtatVehiculeDto
	 *
	 */
	private ElementEtatVehiculeDto getFullInfos(ElementEtatVehicule entity, Integer size, Locale locale) throws Exception {
		ElementEtatVehiculeDto dto = ElementEtatVehiculeTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
