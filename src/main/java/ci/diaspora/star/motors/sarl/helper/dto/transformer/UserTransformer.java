

/*
 * Java transformer for entity table user 
 * Created on 2019-08-19 ( Time 15:02:29 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "user"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface UserTransformer {

	UserTransformer INSTANCE = Mappers.getMapper(UserTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateExpireToken", dateFormat="dd/MM/yyyy",target="dateExpireToken"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.role.id", target="roleId"),
		@Mapping(source="entity.role.code", target="roleCode"),
		@Mapping(source="entity.role.libelle", target="roleLibelle"),
	})
	UserDto toDto(User entity) throws ParseException;

    List<UserDto> toDtos(List<User> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenom", target="prenom"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.login", target="login"),
		@Mapping(source="dto.password", target="password"),
		@Mapping(source="dto.telephone", target="telephone"),
		@Mapping(source="dto.token", target="token"),
		@Mapping(source="dto.isValidToken", target="isValidToken"),
		@Mapping(source="dto.dateExpireToken", dateFormat="dd/MM/yyyy",target="dateExpireToken"),
		@Mapping(source="dto.lastPassword", target="lastPassword"),
		@Mapping(source="dto.signatureUrl", target="signatureUrl"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.isSuperUser", target="isSuperUser"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="role", target="role"),
	})
    User toEntity(UserDto dto, Role role) throws ParseException;

    //List<User> toEntities(List<UserDto> dtos) throws ParseException;

}
