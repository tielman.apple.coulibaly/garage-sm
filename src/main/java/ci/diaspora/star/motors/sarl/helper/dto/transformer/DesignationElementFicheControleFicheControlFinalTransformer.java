

/*
 * Java transformer for entity table designation_element_fiche_controle_fiche_control_final 
 * Created on 2019-08-18 ( Time 17:15:07 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "designation_element_fiche_controle_fiche_control_final"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface DesignationElementFicheControleFicheControlFinalTransformer {

	DesignationElementFicheControleFicheControlFinalTransformer INSTANCE = Mappers.getMapper(DesignationElementFicheControleFicheControlFinalTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.designationElementFicheControle.id", target="designationNiveauControlId"),
		@Mapping(source="entity.designationElementFicheControle.libelle", target="designationElementFicheControleLibelle"),
		@Mapping(source="entity.ficheControlFinal.id", target="ficheControlFinalId"),
	})
	DesignationElementFicheControleFicheControlFinalDto toDto(DesignationElementFicheControleFicheControlFinal entity) throws ParseException;

    List<DesignationElementFicheControleFicheControlFinalDto> toDtos(List<DesignationElementFicheControleFicheControlFinal> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.observations", target="observations"),
		@Mapping(source="dto.isGood", target="isGood"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="designationElementFicheControle", target="designationElementFicheControle"),
		@Mapping(source="ficheControlFinal", target="ficheControlFinal"),
	})
    DesignationElementFicheControleFicheControlFinal toEntity(DesignationElementFicheControleFicheControlFinalDto dto, DesignationElementFicheControle designationElementFicheControle, FicheControlFinal ficheControlFinal) throws ParseException;

    //List<DesignationElementFicheControleFicheControlFinal> toEntities(List<DesignationElementFicheControleFicheControlFinalDto> dtos) throws ParseException;

}
