package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._FichePreparationDevisRepository;

/**
 * Repository : FichePreparationDevis.
 */
@Repository
public interface FichePreparationDevisRepository extends JpaRepository<FichePreparationDevis, Integer>, _FichePreparationDevisRepository {
	/**
	 * Finds FichePreparationDevis by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object FichePreparationDevis whose id is equals to the given id. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.id = :id and e.isDeleted = :isDeleted")
	FichePreparationDevis findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds FichePreparationDevis by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object FichePreparationDevis whose isDeleted is equals to the given isDeleted. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FichePreparationDevis by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object FichePreparationDevis whose createdAt is equals to the given createdAt. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FichePreparationDevis by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object FichePreparationDevis whose updatedAt is equals to the given updatedAt. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FichePreparationDevis by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object FichePreparationDevis whose deletedAt is equals to the given deletedAt. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FichePreparationDevis by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object FichePreparationDevis whose createdBy is equals to the given createdBy. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FichePreparationDevis by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object FichePreparationDevis whose updatedBy is equals to the given updatedBy. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FichePreparationDevis by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object FichePreparationDevis whose deletedBy is equals to the given deletedBy. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FichePreparationDevis by using isValidedByVerificateur as a search criteria.
	 *
	 * @param isValidedByVerificateur
	 * @return An Object FichePreparationDevis whose isValidedByVerificateur is equals to the given isValidedByVerificateur. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.isValidedByVerificateur = :isValidedByVerificateur and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByIsValidedByVerificateur(@Param("isValidedByVerificateur")Boolean isValidedByVerificateur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FichePreparationDevis by using isValidedByValideur as a search criteria.
	 *
	 * @param isValidedByValideur
	 * @return An Object FichePreparationDevis whose isValidedByValideur is equals to the given isValidedByValideur. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.isValidedByValideur = :isValidedByValideur and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByIsValidedByValideur(@Param("isValidedByValideur")Boolean isValidedByValideur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FichePreparationDevis by using dateValidationVerificateur as a search criteria.
	 *
	 * @param dateValidationVerificateur
	 * @return An Object FichePreparationDevis whose dateValidationVerificateur is equals to the given dateValidationVerificateur. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.dateValidationVerificateur = :dateValidationVerificateur and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByDateValidationVerificateur(@Param("dateValidationVerificateur")Date dateValidationVerificateur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FichePreparationDevis by using dateValidationValideur as a search criteria.
	 *
	 * @param dateValidationValideur
	 * @return An Object FichePreparationDevis whose dateValidationValideur is equals to the given dateValidationValideur. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.dateValidationValideur = :dateValidationValideur and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByDateValidationValideur(@Param("dateValidationValideur")Date dateValidationValideur, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds FichePreparationDevis by using ordreReparationId as a search criteria.
	 *
	 * @param ordreReparationId
	 * @return A list of Object FichePreparationDevis whose ordreReparationId is equals to the given ordreReparationId. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.ordreReparation.id = :ordreReparationId and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByOrdreReparationId(@Param("ordreReparationId")Integer ordreReparationId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FichePreparationDevis by using ordreReparationId as a search criteria.
   *
   * @param ordreReparationId
   * @return An Object FichePreparationDevis whose ordreReparationId is equals to the given ordreReparationId. If
   *         no FichePreparationDevis is found, this method returns null.
   */
  @Query("select e from FichePreparationDevis e where e.ordreReparation.id = :ordreReparationId and e.isDeleted = :isDeleted")
  FichePreparationDevis findFichePreparationDevisByOrdreReparationId(@Param("ordreReparationId")Integer ordreReparationId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FichePreparationDevis by using userVerificateurId as a search criteria.
	 *
	 * @param userVerificateurId
	 * @return A list of Object FichePreparationDevis whose userVerificateurId is equals to the given userVerificateurId. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.userVerificateur.id = :userVerificateurId and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByUserVerificateurId(@Param("userVerificateurId")Integer userVerificateurId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FichePreparationDevis by using userVerificateurId as a search criteria.
   *
   * @param userVerificateurId
   * @return An Object FichePreparationDevis whose userVerificateurId is equals to the given userVerificateurId. If
   *         no FichePreparationDevis is found, this method returns null.
   */
  @Query("select e from FichePreparationDevis e where e.userVerificateur.id = :userVerificateurId and e.isDeleted = :isDeleted")
  FichePreparationDevis findFichePreparationDevisByUserVerificateurId(@Param("userVerificateurId")Integer userVerificateurId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FichePreparationDevis by using userValideurId as a search criteria.
	 *
	 * @param userValideurId
	 * @return A list of Object FichePreparationDevis whose userValideurId is equals to the given userValideurId. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.userValideur.id = :userValideurId and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByUserValideurId(@Param("userValideurId")Integer userValideurId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FichePreparationDevis by using userValideurId as a search criteria.
   *
   * @param userValideurId
   * @return An Object FichePreparationDevis whose userValideurId is equals to the given userValideurId. If
   *         no FichePreparationDevis is found, this method returns null.
   */
  @Query("select e from FichePreparationDevis e where e.userValideur.id = :userValideurId and e.isDeleted = :isDeleted")
  FichePreparationDevis findFichePreparationDevisByUserValideurId(@Param("userValideurId")Integer userValideurId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FichePreparationDevis by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object FichePreparationDevis whose etatId is equals to the given etatId. If
	 *         no FichePreparationDevis is found, this method returns null.
	 */
	@Query("select e from FichePreparationDevis e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
	List<FichePreparationDevis> findByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FichePreparationDevis by using etatId as a search criteria.
   *
   * @param etatId
   * @return An Object FichePreparationDevis whose etatId is equals to the given etatId. If
   *         no FichePreparationDevis is found, this method returns null.
   */
  @Query("select e from FichePreparationDevis e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
  FichePreparationDevis findFichePreparationDevisByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of FichePreparationDevis by using fichePreparationDevisDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of FichePreparationDevis
	 * @throws DataAccessException,ParseException
	 */
	public default List<FichePreparationDevis> getByCriteria(Request<FichePreparationDevisDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from FichePreparationDevis e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<FichePreparationDevis> query = em.createQuery(req, FichePreparationDevis.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of FichePreparationDevis by using fichePreparationDevisDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of FichePreparationDevis
	 *
	 */
	public default Long count(Request<FichePreparationDevisDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from FichePreparationDevis e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<FichePreparationDevisDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		FichePreparationDevisDto dto = request.getData() != null ? request.getData() : new FichePreparationDevisDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (FichePreparationDevisDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(FichePreparationDevisDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsValidedByVerificateur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByVerificateur", dto.getIsValidedByVerificateur(), "e.isValidedByVerificateur", "Boolean", dto.getIsValidedByVerificateurParam(), param, index, locale));
			}
			if (dto.getIsValidedByValideur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByValideur", dto.getIsValidedByValideur(), "e.isValidedByValideur", "Boolean", dto.getIsValidedByValideurParam(), param, index, locale));
			}
			if (dto.getDateValidationVerificateur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateValidationVerificateur", dto.getDateValidationVerificateur(), "e.dateValidationVerificateur", "Date", dto.getDateValidationVerificateurParam(), param, index, locale));
			}
			if (dto.getDateValidationValideur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateValidationValideur", dto.getDateValidationValideur(), "e.dateValidationValideur", "Date", dto.getDateValidationValideurParam(), param, index, locale));
			}
			if (dto.getOrdreReparationId()!= null && dto.getOrdreReparationId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ordreReparationId", dto.getOrdreReparationId(), "e.ordreReparation.id", "Integer", dto.getOrdreReparationIdParam(), param, index, locale));
			}
			if (dto.getUserVerificateurId()!= null && dto.getUserVerificateurId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userVerificateurId", dto.getUserVerificateurId(), "e.userVerificateur.id", "Integer", dto.getUserVerificateurIdParam(), param, index, locale));
			}
			if (dto.getUserValideurId()!= null && dto.getUserValideurId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userValideurId", dto.getUserValideurId(), "e.userValideur.id", "Integer", dto.getUserValideurIdParam(), param, index, locale));
			}
			if (dto.getEtatId()!= null && dto.getEtatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatId", dto.getEtatId(), "e.etat.id", "Integer", dto.getEtatIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserVerificateurNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userVerificateurNom", dto.getUserVerificateurNom(), "e.userVerificateur.nom", "String", dto.getUserVerificateurNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserVerificateurPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userVerificateurPrenom", dto.getUserVerificateurPrenom(), "e.userVerificateur.prenom", "String", dto.getUserVerificateurPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserVerificateurLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userVerificateurLogin", dto.getUserVerificateurLogin(), "e.userVerificateur.login", "String", dto.getUserVerificateurLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserValideurNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userValideurNom", dto.getUserValideurNom(), "e.userValideur.nom", "String", dto.getUserValideurNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserValideurPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userValideurPrenom", dto.getUserValideurPrenom(), "e.userValideur.prenom", "String", dto.getUserValideurPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserValideurLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userValideurLogin", dto.getUserValideurLogin(), "e.userValideur.login", "String", dto.getUserValideurLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatLibelle", dto.getEtatLibelle(), "e.etat.libelle", "String", dto.getEtatLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
