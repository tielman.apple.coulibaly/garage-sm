

/*
 * Java transformer for entity table fiche_preparation_devis 
 * Created on 2019-08-18 ( Time 17:15:11 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "fiche_preparation_devis"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface FichePreparationDevisTransformer {

	FichePreparationDevisTransformer INSTANCE = Mappers.getMapper(FichePreparationDevisTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),

		@Mapping(source="entity.dateValidationVerificateur", dateFormat="dd/MM/yyyy",target="dateValidationVerificateur"),

		@Mapping(source="entity.dateValidationValideur", dateFormat="dd/MM/yyyy",target="dateValidationValideur"),
		@Mapping(source="entity.ordreReparation.id", target="ordreReparationId"),
		@Mapping(source="entity.userVerificateur.id", target="userVerificateurId"),
		@Mapping(source="entity.userVerificateur.nom", target="userVerificateurNom"),
		@Mapping(source="entity.userVerificateur.prenom", target="userVerificateurPrenom"),
		@Mapping(source="entity.userVerificateur.login", target="userVerificateurLogin"),
		@Mapping(source="entity.userValideur.id", target="userValideurId"),
		@Mapping(source="entity.userValideur.nom", target="userValideurNom"),
		@Mapping(source="entity.userValideur.prenom", target="userValideurPrenom"),
		@Mapping(source="entity.userValideur.login", target="userValideurLogin"),
		@Mapping(source="entity.etat.id", target="etatId"),
		@Mapping(source="entity.etat.code", target="etatCode"),
		@Mapping(source="entity.etat.libelle", target="etatLibelle"),
	})
	FichePreparationDevisDto toDto(FichePreparationDevis entity) throws ParseException;

    List<FichePreparationDevisDto> toDtos(List<FichePreparationDevis> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isValidedByVerificateur", target="isValidedByVerificateur"),
		@Mapping(source="dto.isValidedByValideur", target="isValidedByValideur"),
		@Mapping(source="dto.dateValidationVerificateur", dateFormat="dd/MM/yyyy",target="dateValidationVerificateur"),
		@Mapping(source="dto.dateValidationValideur", dateFormat="dd/MM/yyyy",target="dateValidationValideur"),
		@Mapping(source="ordreReparation", target="ordreReparation"),
		@Mapping(source="userVerificateur", target="userVerificateur"),
		@Mapping(source="userValideur", target="userValideur"),
		@Mapping(source="etat", target="etat"),
	})
    FichePreparationDevis toEntity(FichePreparationDevisDto dto, OrdreReparation ordreReparation, User userVerificateur, User userValideur, Etat etat) throws ParseException;

    //List<FichePreparationDevis> toEntities(List<FichePreparationDevisDto> dtos) throws ParseException;

}
