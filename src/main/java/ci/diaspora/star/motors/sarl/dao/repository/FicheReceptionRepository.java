package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._FicheReceptionRepository;

/**
 * Repository : FicheReception.
 */
@Repository
public interface FicheReceptionRepository extends JpaRepository<FicheReception, Integer>, _FicheReceptionRepository {
	/**
	 * Finds FicheReception by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object FicheReception whose id is equals to the given id. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.id = :id and e.isDeleted = :isDeleted")
	FicheReception findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds FicheReception by using couleurVehiculeId as a search criteria.
	 *
	 * @param couleurVehiculeId
	 * @return An Object FicheReception whose couleurVehiculeId is equals to the given couleurVehiculeId. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.couleurVehiculeId = :couleurVehiculeId and e.isDeleted = :isDeleted")
	List<FicheReception> findByCouleurVehiculeId(@Param("couleurVehiculeId")Integer couleurVehiculeId, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using numeroFiche as a search criteria.
	 *
	 * @param numeroFiche
	 * @return An Object FicheReception whose numeroFiche is equals to the given numeroFiche. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.numeroFiche = :numeroFiche and e.isDeleted = :isDeleted")
	List<FicheReception> findByNumeroFiche(@Param("numeroFiche")String numeroFiche, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using interlocuteur as a search criteria.
	 *
	 * @param interlocuteur
	 * @return An Object FicheReception whose interlocuteur is equals to the given interlocuteur. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.interlocuteur = :interlocuteur and e.isDeleted = :isDeleted")
	List<FicheReception> findByInterlocuteur(@Param("interlocuteur")String interlocuteur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using nomDeposeur as a search criteria.
	 *
	 * @param nomDeposeur
	 * @return An Object FicheReception whose nomDeposeur is equals to the given nomDeposeur. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.nomDeposeur = :nomDeposeur and e.isDeleted = :isDeleted")
	List<FicheReception> findByNomDeposeur(@Param("nomDeposeur")String nomDeposeur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using nomRecepteur as a search criteria.
	 *
	 * @param nomRecepteur
	 * @return An Object FicheReception whose nomRecepteur is equals to the given nomRecepteur. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.nomRecepteur = :nomRecepteur and e.isDeleted = :isDeleted")
	List<FicheReception> findByNomRecepteur(@Param("nomRecepteur")String nomRecepteur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using kilometrage as a search criteria.
	 *
	 * @param kilometrage
	 * @return An Object FicheReception whose kilometrage is equals to the given kilometrage. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.kilometrage = :kilometrage and e.isDeleted = :isDeleted")
	List<FicheReception> findByKilometrage(@Param("kilometrage")String kilometrage, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using autres as a search criteria.
	 *
	 * @param autres
	 * @return An Object FicheReception whose autres is equals to the given autres. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.autres = :autres and e.isDeleted = :isDeleted")
	List<FicheReception> findByAutres(@Param("autres")String autres, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using dateProchaineRevision as a search criteria.
	 *
	 * @param dateProchaineRevision
	 * @return An Object FicheReception whose dateProchaineRevision is equals to the given dateProchaineRevision. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.dateProchaineRevision = :dateProchaineRevision and e.isDeleted = :isDeleted")
	List<FicheReception> findByDateProchaineRevision(@Param("dateProchaineRevision")Date dateProchaineRevision, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using dateReception as a search criteria.
	 *
	 * @param dateReception
	 * @return An Object FicheReception whose dateReception is equals to the given dateReception. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.dateReception = :dateReception and e.isDeleted = :isDeleted")
	List<FicheReception> findByDateReception(@Param("dateReception")Date dateReception, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using dateSortie as a search criteria.
	 *
	 * @param dateSortie
	 * @return An Object FicheReception whose dateSortie is equals to the given dateSortie. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.dateSortie = :dateSortie and e.isDeleted = :isDeleted")
	List<FicheReception> findByDateSortie(@Param("dateSortie")Date dateSortie, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using dateDepot as a search criteria.
	 *
	 * @param dateDepot
	 * @return An Object FicheReception whose dateDepot is equals to the given dateDepot. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.dateDepot = :dateDepot and e.isDeleted = :isDeleted")
	List<FicheReception> findByDateDepot(@Param("dateDepot")Date dateDepot, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using dateVisiteTechnique as a search criteria.
	 *
	 * @param dateVisiteTechnique
	 * @return An Object FicheReception whose dateVisiteTechnique is equals to the given dateVisiteTechnique. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.dateVisiteTechnique = :dateVisiteTechnique and e.isDeleted = :isDeleted")
	List<FicheReception> findByDateVisiteTechnique(@Param("dateVisiteTechnique")Date dateVisiteTechnique, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object FicheReception whose isDeleted is equals to the given isDeleted. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.isDeleted = :isDeleted")
	List<FicheReception> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using isDeposed as a search criteria.
	 *
	 * @param isDeposed
	 * @return An Object FicheReception whose isDeposed is equals to the given isDeposed. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.isDeposed = :isDeposed and e.isDeleted = :isDeleted")
	List<FicheReception> findByIsDeposed(@Param("isDeposed")Boolean isDeposed, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using isReceved as a search criteria.
	 *
	 * @param isReceved
	 * @return An Object FicheReception whose isReceved is equals to the given isReceved. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.isReceved = :isReceved and e.isDeleted = :isDeleted")
	List<FicheReception> findByIsReceved(@Param("isReceved")Boolean isReceved, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object FicheReception whose createdAt is equals to the given createdAt. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<FicheReception> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object FicheReception whose updatedAt is equals to the given updatedAt. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<FicheReception> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object FicheReception whose deletedAt is equals to the given deletedAt. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<FicheReception> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object FicheReception whose createdBy is equals to the given createdBy. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<FicheReception> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object FicheReception whose updatedBy is equals to the given updatedBy. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<FicheReception> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheReception by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object FicheReception whose deletedBy is equals to the given deletedBy. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<FicheReception> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds FicheReception by using userId as a search criteria.
	 *
	 * @param userId
	 * @return A list of Object FicheReception whose userId is equals to the given userId. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.user.id = :userId and e.isDeleted = :isDeleted")
	List<FicheReception> findByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheReception by using userId as a search criteria.
   *
   * @param userId
   * @return An Object FicheReception whose userId is equals to the given userId. If
   *         no FicheReception is found, this method returns null.
   */
  @Query("select e from FicheReception e where e.user.id = :userId and e.isDeleted = :isDeleted")
  FicheReception findFicheReceptionByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FicheReception by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object FicheReception whose etatId is equals to the given etatId. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
	List<FicheReception> findByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheReception by using etatId as a search criteria.
   *
   * @param etatId
   * @return An Object FicheReception whose etatId is equals to the given etatId. If
   *         no FicheReception is found, this method returns null.
   */
  @Query("select e from FicheReception e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
  FicheReception findFicheReceptionByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FicheReception by using niveauCarburantId as a search criteria.
	 *
	 * @param niveauCarburantId
	 * @return A list of Object FicheReception whose niveauCarburantId is equals to the given niveauCarburantId. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.niveauCarburant.id = :niveauCarburantId and e.isDeleted = :isDeleted")
	List<FicheReception> findByNiveauCarburantId(@Param("niveauCarburantId")Integer niveauCarburantId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheReception by using niveauCarburantId as a search criteria.
   *
   * @param niveauCarburantId
   * @return An Object FicheReception whose niveauCarburantId is equals to the given niveauCarburantId. If
   *         no FicheReception is found, this method returns null.
   */
  @Query("select e from FicheReception e where e.niveauCarburant.id = :niveauCarburantId and e.isDeleted = :isDeleted")
  FicheReception findFicheReceptionByNiveauCarburantId(@Param("niveauCarburantId")Integer niveauCarburantId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FicheReception by using vehiculeId as a search criteria.
	 *
	 * @param vehiculeId
	 * @return A list of Object FicheReception whose vehiculeId is equals to the given vehiculeId. If
	 *         no FicheReception is found, this method returns null.
	 */
	@Query("select e from FicheReception e where e.vehicule.id = :vehiculeId and e.isDeleted = :isDeleted")
	List<FicheReception> findByVehiculeId(@Param("vehiculeId")Integer vehiculeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheReception by using vehiculeId as a search criteria.
   *
   * @param vehiculeId
   * @return An Object FicheReception whose vehiculeId is equals to the given vehiculeId. If
   *         no FicheReception is found, this method returns null.
   */
  @Query("select e from FicheReception e where e.vehicule.id = :vehiculeId and e.isDeleted = :isDeleted")
  FicheReception findFicheReceptionByVehiculeId(@Param("vehiculeId")Integer vehiculeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of FicheReception by using ficheReceptionDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of FicheReception
	 * @throws DataAccessException,ParseException
	 */
	public default List<FicheReception> getByCriteria(Request<FicheReceptionDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from FicheReception e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<FicheReception> query = em.createQuery(req, FicheReception.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of FicheReception by using ficheReceptionDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of FicheReception
	 *
	 */
	public default Long count(Request<FicheReceptionDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from FicheReception e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<FicheReceptionDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		FicheReceptionDto dto = request.getData() != null ? request.getData() : new FicheReceptionDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (FicheReceptionDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(FicheReceptionDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCouleurVehiculeId()!= null && dto.getCouleurVehiculeId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("couleurVehiculeId", dto.getCouleurVehiculeId(), "e.couleurVehiculeId", "Integer", dto.getCouleurVehiculeIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNumeroFiche())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numeroFiche", dto.getNumeroFiche(), "e.numeroFiche", "String", dto.getNumeroFicheParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getInterlocuteur())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("interlocuteur", dto.getInterlocuteur(), "e.interlocuteur", "String", dto.getInterlocuteurParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNomDeposeur())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nomDeposeur", dto.getNomDeposeur(), "e.nomDeposeur", "String", dto.getNomDeposeurParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNomRecepteur())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nomRecepteur", dto.getNomRecepteur(), "e.nomRecepteur", "String", dto.getNomRecepteurParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getKilometrage())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("kilometrage", dto.getKilometrage(), "e.kilometrage", "String", dto.getKilometrageParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAutres())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("autres", dto.getAutres(), "e.autres", "String", dto.getAutresParam(), param, index, locale));
			}
			if (dto.getDateProchaineRevision()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateProchaineRevision", dto.getDateProchaineRevision(), "e.dateProchaineRevision", "Date", dto.getDateProchaineRevisionParam(), param, index, locale));
			}
			if (dto.getDateReception()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateReception", dto.getDateReception(), "e.dateReception", "Date", dto.getDateReceptionParam(), param, index, locale));
			}
			if (dto.getDateSortie()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateSortie", dto.getDateSortie(), "e.dateSortie", "Date", dto.getDateSortieParam(), param, index, locale));
			}
			if (dto.getDateDepot()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateDepot", dto.getDateDepot(), "e.dateDepot", "Date", dto.getDateDepotParam(), param, index, locale));
			}
			if (dto.getDateVisiteTechnique()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateVisiteTechnique", dto.getDateVisiteTechnique(), "e.dateVisiteTechnique", "Date", dto.getDateVisiteTechniqueParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getIsDeposed()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeposed", dto.getIsDeposed(), "e.isDeposed", "Boolean", dto.getIsDeposedParam(), param, index, locale));
			}
			if (dto.getIsReceved()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isReceved", dto.getIsReceved(), "e.isReceved", "Boolean", dto.getIsRecevedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getUserId()!= null && dto.getUserId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
			}
			if (dto.getEtatId()!= null && dto.getEtatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatId", dto.getEtatId(), "e.etat.id", "Integer", dto.getEtatIdParam(), param, index, locale));
			}
			if (dto.getNiveauCarburantId()!= null && dto.getNiveauCarburantId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("niveauCarburantId", dto.getNiveauCarburantId(), "e.niveauCarburant.id", "Integer", dto.getNiveauCarburantIdParam(), param, index, locale));
			}
			if (dto.getVehiculeId()!= null && dto.getVehiculeId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("vehiculeId", dto.getVehiculeId(), "e.vehicule.id", "Integer", dto.getVehiculeIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenom", dto.getUserPrenom(), "e.user.prenom", "String", dto.getUserPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatLibelle", dto.getEtatLibelle(), "e.etat.libelle", "String", dto.getEtatLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNiveauCarburantLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("niveauCarburantLibelle", dto.getNiveauCarburantLibelle(), "e.niveauCarburant.libelle", "String", dto.getNiveauCarburantLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
