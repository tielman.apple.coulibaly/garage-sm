


/*
 * Java transformer for entity table ordre_reparation
 * Created on 2019-08-18 ( Time 17:15:13 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "ordre_reparation"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class OrdreReparationBusiness implements IBasicBusiness<Request<OrdreReparationDto>, Response<OrdreReparationDto>> {

	private Response<OrdreReparationDto> response;
	@Autowired
	private OrdreReparationRepository ordreReparationRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserRepository userSuperviseurRepository;
	@Autowired
	private TypeOrdreReparationRepository typeOrdreReparationRepository;
	@Autowired
	private FicheControlFinalRepository ficheControlFinalRepository;
	@Autowired
	private EtatRepository etatRepository;
	@Autowired
	private PieceHuileUtiliseRepository pieceHuileUtiliseRepository;
	@Autowired
	private FicheReceptionRepository ficheReceptionRepository;
	@Autowired
	private TravauxEffectuesRepository travauxEffectuesRepository;
	@Autowired
	private EtatRepository etatTravauxRepository;
	@Autowired
	private PanneDeclareeOrdreReparationRepository panneDeclareeOrdreReparationRepository;
	@Autowired
	private FichePreparationDevisRepository fichePreparationDevisRepository;
	@Autowired
	private UserRepository userReceptionRepository;
	@Autowired
	private PanneDeclareeRepository panneDeclareeRepository;
	
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private FicheControlFinalBusiness ficheControlFinalBusiness;


	@Autowired
	private PieceHuileUtiliseBusiness pieceHuileUtiliseBusiness;


	@Autowired
	private TravauxEffectuesBusiness travauxEffectuesBusiness;


	@Autowired
	private PanneDeclareeOrdreReparationBusiness panneDeclareeOrdreReparationBusiness;


	@Autowired
	private FichePreparationDevisBusiness fichePreparationDevisBusiness;



	public OrdreReparationBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create OrdreReparation by using OrdreReparationDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<OrdreReparationDto> create(Request<OrdreReparationDto> request, Locale locale)  {
		slf4jLogger.info("----begin create OrdreReparation-----");

		response = new Response<OrdreReparationDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<OrdreReparation> items = new ArrayList<OrdreReparation>();
			List<PanneDeclareeOrdreReparation> itemsPanneDeclareeOrdreReparation = new ArrayList<>();


			for (OrdreReparationDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("ficheReceptionId", dto.getFicheReceptionId());
				fieldsToVerify.put("typeOrdreReparationId", dto.getTypeOrdreReparationId());
				fieldsToVerify.put("etatTravauxId", dto.getEtatTravauxId());
				fieldsToVerify.put("etatOrdreId", dto.getEtatOrdreId());
				fieldsToVerify.put("datasPannesDeclare", dto.getDatasPannesDeclare());
				
				
				//        fieldsToVerify.put("userChefAtelierId", dto.getUserChefAtelierId());
				//        fieldsToVerify.put("userSuperviseurId", dto.getUserSuperviseurId());
				//        fieldsToVerify.put("userReceptionId", dto.getUserReceptionId());
				//        fieldsToVerify.put("isValidedByChefAtelierOrdre", dto.getIsValidedByChefAtelierOrdre());
				//        fieldsToVerify.put("isValidedByChefAtelierTravaux", dto.getIsValidedByChefAtelierTravaux());
				//        fieldsToVerify.put("isValidedByReception", dto.getIsValidedByReception());
				//        fieldsToVerify.put("isValidedBySuperviseurOrdre", dto.getIsValidedBySuperviseurOrdre());
				//        fieldsToVerify.put("isValidedBySuperviseurTravaux", dto.getIsValidedBySuperviseurTravaux());
				//        fieldsToVerify.put("observationsChefAtelier", dto.getObservationsChefAtelier());
				//        fieldsToVerify.put("observationsSuperviseur", dto.getObservationsSuperviseur());
				//        fieldsToVerify.put("dateValidationReception", dto.getDateValidationReception());
				//        fieldsToVerify.put("dateValidationOrdreChefAtelier", dto.getDateValidationOrdreChefAtelier());
				//        fieldsToVerify.put("dateValidationTravauxChefAtelier", dto.getDateValidationTravauxChefAtelier());
				//        fieldsToVerify.put("dateValidationOrdreSuperviseur", dto.getDateValidationOrdreSuperviseur());
				//        fieldsToVerify.put("dateValidationTravauxSuperviseur", dto.getDateValidationTravauxSuperviseur());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				if (!Utilities.isNotEmpty(dto.getDatasPannesDeclare())) {
					response.setStatus(functionalError.FIELD_EMPTY("datasPannesDeclare", locale));
					response.setHasError(true);
					return response;
				}
				
				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if ordreReparation to insert do not exist
				OrdreReparation existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("ordreReparation -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if userSuperviseur exist
				User existingUserSuperviseur = userSuperviseurRepository.findById(dto.getUserSuperviseurId(), false);
				if (existingUserSuperviseur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("userSuperviseur -> " + dto.getUserSuperviseurId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if typeOrdreReparation exist
				TypeOrdreReparation existingTypeOrdreReparation = typeOrdreReparationRepository.findById(dto.getTypeOrdreReparationId(), false);
				if (existingTypeOrdreReparation == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeOrdreReparation -> " + dto.getTypeOrdreReparationId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if etat exist
				Etat existingEtat = etatRepository.findById(dto.getEtatOrdreId(), false);
				if (existingEtat == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatOrdreId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if ficheReception exist
				FicheReception existingFicheReception = ficheReceptionRepository.findById(dto.getFicheReceptionId(), false);
				if (existingFicheReception == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getFicheReceptionId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if etatTravaux exist
				Etat existingEtatTravaux = etatTravauxRepository.findById(dto.getEtatTravauxId(), false);
				if (existingEtatTravaux == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etatTravaux -> " + dto.getEtatTravauxId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if user exist
				User existingUser = userRepository.findById(dto.getUserChefAtelierId(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserChefAtelierId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if userReception exist
				User existingUserReception = userReceptionRepository.findById(dto.getUserReceptionId(), false);
				if (existingUserReception == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("userReception -> " + dto.getUserReceptionId(), locale));
					response.setHasError(true);
					return response;
				}
				OrdreReparation entityToSave = null;
				entityToSave = OrdreReparationTransformer.INSTANCE.toEntity(dto, existingUserSuperviseur, existingTypeOrdreReparation, existingEtat, existingFicheReception, existingEtatTravaux, existingUser, existingUserReception);
				entityToSave.setIsDeleted(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
				
				
				
				List<PanneDeclaree> itemsPanneDeclaree = new ArrayList<PanneDeclaree>();
				for (PanneDeclareeDto panneDeclareeDto : dto.getDatasPannesDeclare()) {

					PanneDeclaree existingPanneDeclaree = panneDeclareeRepository.findByLibelle(panneDeclareeDto.getLibelle(), false);
					if (existingPanneDeclaree == null) {
						existingPanneDeclaree = new PanneDeclaree();
						existingPanneDeclaree.setLibelle(panneDeclareeDto.getLibelle());
						existingPanneDeclaree.setIsDeleted(false);
						existingPanneDeclaree.setCreatedAt(Utilities.getCurrentDate());
						existingPanneDeclaree.setCreatedBy(request.getUser());
						panneDeclareeRepository.save(existingPanneDeclaree);
						
					}
					if (itemsPanneDeclaree.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(panneDeclareeDto.getLibelle()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + panneDeclareeDto.getLibelle()+"' pour les ordreReparations", locale));
						response.setHasError(true);
						return response;
					}
					itemsPanneDeclaree.add(existingPanneDeclaree);

					PanneDeclareeOrdreReparation panneDeclareeOrdreReparation = new PanneDeclareeOrdreReparation();
					panneDeclareeOrdreReparation.setPanneDeclaree(existingPanneDeclaree);
					panneDeclareeOrdreReparation.setOrdreReparation(entityToSave);
					panneDeclareeOrdreReparation.setIsDeclaredByClient(panneDeclareeDto.getIsDeclaredByClient());
					panneDeclareeOrdreReparation.setObservations(panneDeclareeDto.getObservations());
					panneDeclareeOrdreReparation.setDiagnosticAtelier(panneDeclareeDto.getDiagnosticAtelier());
					panneDeclareeOrdreReparation.setIsDeleted(false);
					panneDeclareeOrdreReparation.setCreatedAt(Utilities.getCurrentDate());
					panneDeclareeOrdreReparation.setCreatedBy(request.getUser());

					itemsPanneDeclareeOrdreReparation.add(panneDeclareeOrdreReparation);
				}
			}

			if (!items.isEmpty()) {
				List<OrdreReparation> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = ordreReparationRepository.save((Iterable<OrdreReparation>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("ordreReparation", locale));
					response.setHasError(true);
					return response;
				}
				
				if (!itemsPanneDeclareeOrdreReparation.isEmpty()) {
					List<PanneDeclareeOrdreReparation> itemsPanneDeclareeOrdreReparationSaved = null;
					// inserer les donnees en base de donnees
					itemsPanneDeclareeOrdreReparationSaved = panneDeclareeOrdreReparationRepository.save((Iterable<PanneDeclareeOrdreReparation>) itemsPanneDeclareeOrdreReparation);
					if (itemsPanneDeclareeOrdreReparationSaved == null || itemsPanneDeclareeOrdreReparationSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("panneDeclareeOrdreReparation", locale));
						response.setHasError(true);
						return response;
					}
				}

				List<OrdreReparationDto> itemsDto = new ArrayList<OrdreReparationDto>();
				for (OrdreReparation entity : itemsSaved) {
					OrdreReparationDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create OrdreReparation-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update OrdreReparation by using OrdreReparationDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<OrdreReparationDto> update(Request<OrdreReparationDto> request, Locale locale)  {
		slf4jLogger.info("----begin update OrdreReparation-----");

		response = new Response<OrdreReparationDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<OrdreReparation> items = new ArrayList<OrdreReparation>();
			List<PanneDeclareeOrdreReparation> itemsPanneDeclareeOrdreReparationCreate = new ArrayList<>();
			List<PanneDeclareeOrdreReparation> itemsPanneDeclareeOrdreReparationDelete = new ArrayList<>();
			

			for (OrdreReparationDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la ordreReparation existe
				OrdreReparation entityToSave = null;
				entityToSave = ordreReparationRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if userSuperviseur exist
				if (dto.getUserSuperviseurId() != null && dto.getUserSuperviseurId() > 0){
					User existingUserSuperviseur = userSuperviseurRepository.findById(dto.getUserSuperviseurId(), false);
					if (existingUserSuperviseur == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("userSuperviseur -> " + dto.getUserSuperviseurId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUserSuperviseur(existingUserSuperviseur);
				}
				// Verify if typeOrdreReparation exist
				if (dto.getTypeOrdreReparationId() != null && dto.getTypeOrdreReparationId() > 0){
					TypeOrdreReparation existingTypeOrdreReparation = typeOrdreReparationRepository.findById(dto.getTypeOrdreReparationId(), false);
					if (existingTypeOrdreReparation == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeOrdreReparation -> " + dto.getTypeOrdreReparationId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTypeOrdreReparation(existingTypeOrdreReparation);
				}
				// Verify if etat exist
				if (dto.getEtatOrdreId() != null && dto.getEtatOrdreId() > 0){
					Etat existingEtatOrdre = etatRepository.findById(dto.getEtatOrdreId(), false);
					if (existingEtatOrdre == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatOrdreId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEtatOrdre(existingEtatOrdre);
				}
				// Verify if ficheReception exist
				if (dto.getFicheReceptionId() != null && dto.getFicheReceptionId() > 0){
					FicheReception existingFicheReception = ficheReceptionRepository.findById(dto.getFicheReceptionId(), false);
					if (existingFicheReception == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getFicheReceptionId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setFicheReception(existingFicheReception);
				}
				// Verify if etatTravaux exist
				if (dto.getEtatTravauxId() != null && dto.getEtatTravauxId() > 0){
					Etat existingEtatTravaux = etatTravauxRepository.findById(dto.getEtatTravauxId(), false);
					if (existingEtatTravaux == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("etatTravaux -> " + dto.getEtatTravauxId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEtatTravaux(existingEtatTravaux);
				}
				// Verify if user exist
				if (dto.getUserChefAtelierId() != null && dto.getUserChefAtelierId() > 0){
					User existingUserChefAtelier = userRepository.findById(dto.getUserChefAtelierId(), false);
					if (existingUserChefAtelier == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserChefAtelierId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUserChefAtelier(existingUserChefAtelier);
				}
				// Verify if userReception exist
				if (dto.getUserReceptionId() != null && dto.getUserReceptionId() > 0){
					User existingUserReception = userReceptionRepository.findById(dto.getUserReceptionId(), false);
					if (existingUserReception == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("userReception -> " + dto.getUserReceptionId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUserReception(existingUserReception);
				}
				
				
				if (Utilities.isNotEmpty(dto.getDatasPannesDeclare())) {

					List<PanneDeclareeOrdreReparation> panneDeclareeOrdreReparations = panneDeclareeOrdreReparationRepository.findByOrdreReparationId(entityToSaveId, false);

					OrdreReparation ordreReparation = null;
					
					Boolean isDeclaredByClient = false;
					if (Utilities.isNotEmpty(panneDeclareeOrdreReparations)) {
						PanneDeclareeOrdreReparation panneDeclareeOrdreReparation = panneDeclareeOrdreReparations.get(0);
						
						isDeclaredByClient = panneDeclareeOrdreReparation.getIsDeclaredByClient();
						ordreReparation = panneDeclareeOrdreReparation.getOrdreReparation();
						itemsPanneDeclareeOrdreReparationDelete.addAll(panneDeclareeOrdreReparations);
					}
					
					List<PanneDeclaree> itemsPanneDeclaree = new ArrayList<PanneDeclaree>();
					for (PanneDeclareeDto panneDeclareeDto : dto.getDatasPannesDeclare()) {

						PanneDeclaree existingPanneDeclaree = panneDeclareeRepository.findByLibelle(panneDeclareeDto.getLibelle(), false);
						if (existingPanneDeclaree == null) {
							existingPanneDeclaree = new PanneDeclaree();
							existingPanneDeclaree.setLibelle(panneDeclareeDto.getLibelle());
							existingPanneDeclaree.setIsDeleted(false);
							existingPanneDeclaree.setCreatedAt(Utilities.getCurrentDate());
							existingPanneDeclaree.setCreatedBy(request.getUser());
							panneDeclareeRepository.save(existingPanneDeclaree);
							
						}
						if (itemsPanneDeclaree.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(panneDeclareeDto.getLibelle()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + panneDeclareeDto.getLibelle()+"' pour les ordreReparations", locale));
							response.setHasError(true);
							return response;
						}
						itemsPanneDeclaree.add(existingPanneDeclaree);

						PanneDeclareeOrdreReparation panneDeclareeOrdreReparation = new PanneDeclareeOrdreReparation();
						panneDeclareeOrdreReparation.setPanneDeclaree(existingPanneDeclaree);
						panneDeclareeOrdreReparation.setOrdreReparation(ordreReparation);
						
						//panneDeclareeOrdreReparation.setIsDeclaredByClient(panneDeclareeDto.getIsDeclaredByClient());
						panneDeclareeOrdreReparation.setIsDeclaredByClient(isDeclaredByClient);
						panneDeclareeOrdreReparation.setObservations(panneDeclareeDto.getObservations());
						panneDeclareeOrdreReparation.setIsDeleted(false);
						panneDeclareeOrdreReparation.setCreatedAt(Utilities.getCurrentDate());
						panneDeclareeOrdreReparation.setCreatedBy(request.getUser());

						itemsPanneDeclareeOrdreReparationCreate.add(panneDeclareeOrdreReparation);
					}
				}
				
				if (dto.getIsValidedByChefAtelierOrdre() != null) {
					entityToSave.setIsValidedByChefAtelierOrdre(dto.getIsValidedByChefAtelierOrdre());
				}
				if (dto.getIsValidedByChefAtelierTravaux() != null) {
					entityToSave.setIsValidedByChefAtelierTravaux(dto.getIsValidedByChefAtelierTravaux());
				}
				if (dto.getIsValidedByReception() != null) {
					entityToSave.setIsValidedByReception(dto.getIsValidedByReception());
				}
				if (dto.getIsValidedBySuperviseurOrdre() != null) {
					entityToSave.setIsValidedBySuperviseurOrdre(dto.getIsValidedBySuperviseurOrdre());
				}
				if (dto.getIsValidedBySuperviseurTravaux() != null) {
					entityToSave.setIsValidedBySuperviseurTravaux(dto.getIsValidedBySuperviseurTravaux());
				}
				if (Utilities.notBlank(dto.getObservationsChefAtelier())) {
					entityToSave.setObservationsChefAtelier(dto.getObservationsChefAtelier());
				}
				if (Utilities.notBlank(dto.getObservationsSuperviseur())) {
					entityToSave.setObservationsSuperviseur(dto.getObservationsSuperviseur());
				}
				if (Utilities.notBlank(dto.getDateValidationReception())) {
					entityToSave.setDateValidationReception(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateValidationReception()));
				}
				if (Utilities.notBlank(dto.getDateValidationOrdreChefAtelier())) {
					entityToSave.setDateValidationOrdreChefAtelier(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateValidationOrdreChefAtelier()));
				}
				if (Utilities.notBlank(dto.getDateValidationTravauxChefAtelier())) {
					entityToSave.setDateValidationTravauxChefAtelier(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateValidationTravauxChefAtelier()));
				}
				if (Utilities.notBlank(dto.getDateValidationOrdreSuperviseur())) {
					entityToSave.setDateValidationOrdreSuperviseur(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateValidationOrdreSuperviseur()));
				}
				if (Utilities.notBlank(dto.getDateValidationTravauxSuperviseur())) {
					entityToSave.setDateValidationTravauxSuperviseur(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateValidationTravauxSuperviseur()));
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<OrdreReparation> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = ordreReparationRepository.save((Iterable<OrdreReparation>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("ordreReparation", locale));
					response.setHasError(true);
					return response;
				}


				if (Utilities.isNotEmpty(itemsPanneDeclareeOrdreReparationCreate)) {

					if (Utilities.isNotEmpty(itemsPanneDeclareeOrdreReparationDelete)) {
						List<PanneDeclareeOrdreReparation> itemsDeleteSaved = null;

						for (PanneDeclareeOrdreReparation element : itemsPanneDeclareeOrdreReparationDelete) {
							element.setIsDeleted(true);
							element.setDeletedAt(Utilities.getCurrentDate());
							element.setDeletedBy(request.getUser());
						}
						// maj les donnees en base
						itemsDeleteSaved = panneDeclareeOrdreReparationRepository.save((Iterable<PanneDeclareeOrdreReparation>) itemsPanneDeclareeOrdreReparationDelete);
						if (itemsDeleteSaved == null || itemsDeleteSaved.isEmpty()) {
							response.setStatus(functionalError.SAVE_FAIL("", locale));
							response.setHasError(true);
							return response;
						}
					}
					List<PanneDeclareeOrdreReparation> itemsCreateSaved = null;
					// maj les donnees en base
					itemsCreateSaved = panneDeclareeOrdreReparationRepository.save((Iterable<PanneDeclareeOrdreReparation>) itemsPanneDeclareeOrdreReparationCreate);
					if (itemsCreateSaved == null || itemsCreateSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("", locale));
						response.setHasError(true);
						return response;
					}

				}
				
				
				List<OrdreReparationDto> itemsDto = new ArrayList<OrdreReparationDto>();
				for (OrdreReparation entity : itemsSaved) {
					OrdreReparationDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update OrdreReparation-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete OrdreReparation by using OrdreReparationDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<OrdreReparationDto> delete(Request<OrdreReparationDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete OrdreReparation-----");

		response = new Response<OrdreReparationDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<OrdreReparation> items = new ArrayList<OrdreReparation>();

			for (OrdreReparationDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la ordreReparation existe
				OrdreReparation existingEntity = null;
				existingEntity = ordreReparationRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// ficheControlFinal
				List<FicheControlFinal> listOfFicheControlFinal = ficheControlFinalRepository.findByOrdreReparationId(existingEntity.getId(), false);
				if (listOfFicheControlFinal != null && !listOfFicheControlFinal.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFicheControlFinal.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// pieceHuileUtilise
				List<PieceHuileUtilise> listOfPieceHuileUtilise = pieceHuileUtiliseRepository.findByOrdreReparationId(existingEntity.getId(), false);
				if (listOfPieceHuileUtilise != null && !listOfPieceHuileUtilise.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPieceHuileUtilise.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// travauxEffectues
				List<TravauxEffectues> listOfTravauxEffectues = travauxEffectuesRepository.findByOrdreReparationId(existingEntity.getId(), false);
				if (listOfTravauxEffectues != null && !listOfTravauxEffectues.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfTravauxEffectues.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// panneDeclareeOrdreReparation
				List<PanneDeclareeOrdreReparation> listOfPanneDeclareeOrdreReparation = panneDeclareeOrdreReparationRepository.findByOrdreReparationId(existingEntity.getId(), false);
				if (listOfPanneDeclareeOrdreReparation != null && !listOfPanneDeclareeOrdreReparation.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPanneDeclareeOrdreReparation.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// fichePreparationDevis
				List<FichePreparationDevis> listOfFichePreparationDevis = fichePreparationDevisRepository.findByOrdreReparationId(existingEntity.getId(), false);
				if (listOfFichePreparationDevis != null && !listOfFichePreparationDevis.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFichePreparationDevis.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				ordreReparationRepository.save((Iterable<OrdreReparation>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete OrdreReparation-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete OrdreReparation by using OrdreReparationDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<OrdreReparationDto> forceDelete(Request<OrdreReparationDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete OrdreReparation-----");

		response = new Response<OrdreReparationDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<OrdreReparation> items = new ArrayList<OrdreReparation>();

			for (OrdreReparationDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la ordreReparation existe
				OrdreReparation existingEntity = null;
				existingEntity = ordreReparationRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// ficheControlFinal
				List<FicheControlFinal> listOfFicheControlFinal = ficheControlFinalRepository.findByOrdreReparationId(existingEntity.getId(), false);
				if (listOfFicheControlFinal != null && !listOfFicheControlFinal.isEmpty()){
					Request<FicheControlFinalDto> deleteRequest = new Request<FicheControlFinalDto>();
					deleteRequest.setDatas(FicheControlFinalTransformer.INSTANCE.toDtos(listOfFicheControlFinal));
					deleteRequest.setUser(request.getUser());
					Response<FicheControlFinalDto> deleteResponse = ficheControlFinalBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// pieceHuileUtilise
				List<PieceHuileUtilise> listOfPieceHuileUtilise = pieceHuileUtiliseRepository.findByOrdreReparationId(existingEntity.getId(), false);
				if (listOfPieceHuileUtilise != null && !listOfPieceHuileUtilise.isEmpty()){
					Request<PieceHuileUtiliseDto> deleteRequest = new Request<PieceHuileUtiliseDto>();
					deleteRequest.setDatas(PieceHuileUtiliseTransformer.INSTANCE.toDtos(listOfPieceHuileUtilise));
					deleteRequest.setUser(request.getUser());
					Response<PieceHuileUtiliseDto> deleteResponse = pieceHuileUtiliseBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// travauxEffectues
				List<TravauxEffectues> listOfTravauxEffectues = travauxEffectuesRepository.findByOrdreReparationId(existingEntity.getId(), false);
				if (listOfTravauxEffectues != null && !listOfTravauxEffectues.isEmpty()){
					Request<TravauxEffectuesDto> deleteRequest = new Request<TravauxEffectuesDto>();
					deleteRequest.setDatas(TravauxEffectuesTransformer.INSTANCE.toDtos(listOfTravauxEffectues));
					deleteRequest.setUser(request.getUser());
					Response<TravauxEffectuesDto> deleteResponse = travauxEffectuesBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// panneDeclareeOrdreReparation
				List<PanneDeclareeOrdreReparation> listOfPanneDeclareeOrdreReparation = panneDeclareeOrdreReparationRepository.findByOrdreReparationId(existingEntity.getId(), false);
				if (listOfPanneDeclareeOrdreReparation != null && !listOfPanneDeclareeOrdreReparation.isEmpty()){
					Request<PanneDeclareeOrdreReparationDto> deleteRequest = new Request<PanneDeclareeOrdreReparationDto>();
					deleteRequest.setDatas(PanneDeclareeOrdreReparationTransformer.INSTANCE.toDtos(listOfPanneDeclareeOrdreReparation));
					deleteRequest.setUser(request.getUser());
					Response<PanneDeclareeOrdreReparationDto> deleteResponse = panneDeclareeOrdreReparationBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// fichePreparationDevis
				List<FichePreparationDevis> listOfFichePreparationDevis = fichePreparationDevisRepository.findByOrdreReparationId(existingEntity.getId(), false);
				if (listOfFichePreparationDevis != null && !listOfFichePreparationDevis.isEmpty()){
					Request<FichePreparationDevisDto> deleteRequest = new Request<FichePreparationDevisDto>();
					deleteRequest.setDatas(FichePreparationDevisTransformer.INSTANCE.toDtos(listOfFichePreparationDevis));
					deleteRequest.setUser(request.getUser());
					Response<FichePreparationDevisDto> deleteResponse = fichePreparationDevisBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				ordreReparationRepository.save((Iterable<OrdreReparation>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete OrdreReparation-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get OrdreReparation by using OrdreReparationDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<OrdreReparationDto> getByCriteria(Request<OrdreReparationDto> request, Locale locale) {
		slf4jLogger.info("----begin get OrdreReparation-----");

		response = new Response<OrdreReparationDto>();

		try {
			List<OrdreReparation> items = null;
			items = ordreReparationRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<OrdreReparationDto> itemsDto = new ArrayList<OrdreReparationDto>();
				for (OrdreReparation entity : items) {
					OrdreReparationDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(ordreReparationRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("ordreReparation", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get OrdreReparation-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full OrdreReparationDto by using OrdreReparation as object.
	 *
	 * @param entity, locale
	 * @return OrdreReparationDto
	 *
	 */
	private OrdreReparationDto getFullInfos(OrdreReparation entity, Integer size, Locale locale) throws Exception {
		OrdreReparationDto dto = OrdreReparationTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}
		
		List<PanneDeclareeOrdreReparation> panneDeclareeOrdreReparations = new ArrayList<>();

		panneDeclareeOrdreReparations = panneDeclareeOrdreReparationRepository.findByOrdreReparationId(dto.getId(), false);
		if (Utilities.isNotEmpty(panneDeclareeOrdreReparations)) {
			List<PanneDeclareeDto> panneDeclarees = new ArrayList<>();
			for (PanneDeclareeOrdreReparation element : panneDeclareeOrdreReparations) {
				PanneDeclareeDto panneDeclareeDto = PanneDeclareeTransformer.INSTANCE.toDto(element.getPanneDeclaree());
				panneDeclareeDto.setObservations(element.getObservations());
				panneDeclareeDto.setDiagnosticAtelier(element.getDiagnosticAtelier());
				panneDeclareeDto.setIsDeclaredByClient(element.getIsDeclaredByClient());
				panneDeclarees.add(panneDeclareeDto);
			}
			dto.setDatasPannesDeclare(panneDeclarees);
		}
		

		return dto;
	}
}
