
/*
 * Java dto for entity table historisation
 * Created on 2019-08-18 ( Time 17:15:12 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._HistorisationDto;

/**
 * DTO for table "historisation"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class HistorisationDto extends _HistorisationDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    userId               ;
	/*
	 * 
	 */
    private Integer    etatId               ;
	/*
	 * 
	 */
    private Integer    actionId             ;
	/*
	 * 
	 */
    private Integer    typeFicheId          ;
	/*
	 * 
	 */
    private Integer    ficheId              ;
	/*
	 * 
	 */
    private String     ficheAvant           ;
	/*
	 * 
	 */
    private String     ficheApres           ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String actionCode;
	private String actionLibelle;
	private String typeFicheCode;
	private String typeFicheLibelle;
	private String userNom;
	private String userPrenom;
	private String userLogin;
	private String etatCode;
	private String etatLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  userIdParam           ;                     
	private SearchParam<Integer>  etatIdParam           ;                     
	private SearchParam<Integer>  actionIdParam         ;                     
	private SearchParam<Integer>  typeFicheIdParam      ;                     
	private SearchParam<Integer>  ficheIdParam          ;                     
	private SearchParam<String>   ficheAvantParam       ;                     
	private SearchParam<String>   ficheApresParam       ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<String>   actionCodeParam       ;                     
	private SearchParam<String>   actionLibelleParam    ;                     
	private SearchParam<String>   typeFicheCodeParam    ;                     
	private SearchParam<String>   typeFicheLibelleParam ;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomParam       ;                     
	private SearchParam<String>   userLoginParam        ;                     
	private SearchParam<String>   etatCodeParam         ;                     
	private SearchParam<String>   etatLibelleParam      ;                     
    /**
     * Default constructor
     */
    public HistorisationDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "userId" field value
     * @param userId
     */
	public void setUserId( Integer userId )
    {
        this.userId = userId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUserId()
    {
        return this.userId;
    }

    /**
     * Set the "etatId" field value
     * @param etatId
     */
	public void setEtatId( Integer etatId )
    {
        this.etatId = etatId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getEtatId()
    {
        return this.etatId;
    }

    /**
     * Set the "actionId" field value
     * @param actionId
     */
	public void setActionId( Integer actionId )
    {
        this.actionId = actionId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getActionId()
    {
        return this.actionId;
    }

    /**
     * Set the "typeFicheId" field value
     * @param typeFicheId
     */
	public void setTypeFicheId( Integer typeFicheId )
    {
        this.typeFicheId = typeFicheId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getTypeFicheId()
    {
        return this.typeFicheId;
    }

    /**
     * Set the "ficheId" field value
     * @param ficheId
     */
	public void setFicheId( Integer ficheId )
    {
        this.ficheId = ficheId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getFicheId()
    {
        return this.ficheId;
    }

    /**
     * Set the "ficheAvant" field value
     * @param ficheAvant
     */
	public void setFicheAvant( String ficheAvant )
    {
        this.ficheAvant = ficheAvant ;
    }
    /**
     * 
     * @return the field value
     */
	public String getFicheAvant()
    {
        return this.ficheAvant;
    }

    /**
     * Set the "ficheApres" field value
     * @param ficheApres
     */
	public void setFicheApres( String ficheApres )
    {
        this.ficheApres = ficheApres ;
    }
    /**
     * 
     * @return the field value
     */
	public String getFicheApres()
    {
        return this.ficheApres;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getActionCode()
    {
        return this.actionCode;
    }
	public void setActionCode(String actionCode)
    {
        this.actionCode = actionCode;
    }

	public String getActionLibelle()
    {
        return this.actionLibelle;
    }
	public void setActionLibelle(String actionLibelle)
    {
        this.actionLibelle = actionLibelle;
    }

	public String getTypeFicheCode()
    {
        return this.typeFicheCode;
    }
	public void setTypeFicheCode(String typeFicheCode)
    {
        this.typeFicheCode = typeFicheCode;
    }

	public String getTypeFicheLibelle()
    {
        return this.typeFicheLibelle;
    }
	public void setTypeFicheLibelle(String typeFicheLibelle)
    {
        this.typeFicheLibelle = typeFicheLibelle;
    }

	public String getUserNom()
    {
        return this.userNom;
    }
	public void setUserNom(String userNom)
    {
        this.userNom = userNom;
    }

	public String getUserPrenom()
    {
        return this.userPrenom;
    }
	public void setUserPrenom(String userPrenom)
    {
        this.userPrenom = userPrenom;
    }

	public String getUserLogin()
    {
        return this.userLogin;
    }
	public void setUserLogin(String userLogin)
    {
        this.userLogin = userLogin;
    }

	public String getEtatCode()
    {
        return this.etatCode;
    }
	public void setEtatCode(String etatCode)
    {
        this.etatCode = etatCode;
    }

	public String getEtatLibelle()
    {
        return this.etatLibelle;
    }
	public void setEtatLibelle(String etatLibelle)
    {
        this.etatLibelle = etatLibelle;
    }


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "userIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUserIdParam(){
		return this.userIdParam;
	}
	/**
     * Set the "userIdParam" field value
     * @param userIdParam
     */
    public void setUserIdParam( SearchParam<Integer> userIdParam ){
        this.userIdParam = userIdParam;
    }

	/**
     * Get the "etatIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getEtatIdParam(){
		return this.etatIdParam;
	}
	/**
     * Set the "etatIdParam" field value
     * @param etatIdParam
     */
    public void setEtatIdParam( SearchParam<Integer> etatIdParam ){
        this.etatIdParam = etatIdParam;
    }

	/**
     * Get the "actionIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getActionIdParam(){
		return this.actionIdParam;
	}
	/**
     * Set the "actionIdParam" field value
     * @param actionIdParam
     */
    public void setActionIdParam( SearchParam<Integer> actionIdParam ){
        this.actionIdParam = actionIdParam;
    }

	/**
     * Get the "typeFicheIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getTypeFicheIdParam(){
		return this.typeFicheIdParam;
	}
	/**
     * Set the "typeFicheIdParam" field value
     * @param typeFicheIdParam
     */
    public void setTypeFicheIdParam( SearchParam<Integer> typeFicheIdParam ){
        this.typeFicheIdParam = typeFicheIdParam;
    }

	/**
     * Get the "ficheIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getFicheIdParam(){
		return this.ficheIdParam;
	}
	/**
     * Set the "ficheIdParam" field value
     * @param ficheIdParam
     */
    public void setFicheIdParam( SearchParam<Integer> ficheIdParam ){
        this.ficheIdParam = ficheIdParam;
    }

	/**
     * Get the "ficheAvantParam" field value
     * @return the field value
     */
	public SearchParam<String> getFicheAvantParam(){
		return this.ficheAvantParam;
	}
	/**
     * Set the "ficheAvantParam" field value
     * @param ficheAvantParam
     */
    public void setFicheAvantParam( SearchParam<String> ficheAvantParam ){
        this.ficheAvantParam = ficheAvantParam;
    }

	/**
     * Get the "ficheApresParam" field value
     * @return the field value
     */
	public SearchParam<String> getFicheApresParam(){
		return this.ficheApresParam;
	}
	/**
     * Set the "ficheApresParam" field value
     * @param ficheApresParam
     */
    public void setFicheApresParam( SearchParam<String> ficheApresParam ){
        this.ficheApresParam = ficheApresParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

		/**
     * Get the "actionCodeParam" field value
     * @return the field value
     */
	public SearchParam<String> getActionCodeParam(){
		return this.actionCodeParam;
	}
	/**
     * Set the "actionCodeParam" field value
     * @param actionCodeParam
     */
    public void setActionCodeParam( SearchParam<String> actionCodeParam ){
        this.actionCodeParam = actionCodeParam;
    }

		/**
     * Get the "actionLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getActionLibelleParam(){
		return this.actionLibelleParam;
	}
	/**
     * Set the "actionLibelleParam" field value
     * @param actionLibelleParam
     */
    public void setActionLibelleParam( SearchParam<String> actionLibelleParam ){
        this.actionLibelleParam = actionLibelleParam;
    }

		/**
     * Get the "typeFicheCodeParam" field value
     * @return the field value
     */
	public SearchParam<String> getTypeFicheCodeParam(){
		return this.typeFicheCodeParam;
	}
	/**
     * Set the "typeFicheCodeParam" field value
     * @param typeFicheCodeParam
     */
    public void setTypeFicheCodeParam( SearchParam<String> typeFicheCodeParam ){
        this.typeFicheCodeParam = typeFicheCodeParam;
    }

		/**
     * Get the "typeFicheLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getTypeFicheLibelleParam(){
		return this.typeFicheLibelleParam;
	}
	/**
     * Set the "typeFicheLibelleParam" field value
     * @param typeFicheLibelleParam
     */
    public void setTypeFicheLibelleParam( SearchParam<String> typeFicheLibelleParam ){
        this.typeFicheLibelleParam = typeFicheLibelleParam;
    }

		/**
     * Get the "userNomParam" field value
     * @return the field value
     */
	public SearchParam<String> getUserNomParam(){
		return this.userNomParam;
	}
	/**
     * Set the "userNomParam" field value
     * @param userNomParam
     */
    public void setUserNomParam( SearchParam<String> userNomParam ){
        this.userNomParam = userNomParam;
    }

		/**
     * Get the "userPrenomParam" field value
     * @return the field value
     */
	public SearchParam<String> getUserPrenomParam(){
		return this.userPrenomParam;
	}
	/**
     * Set the "userPrenomParam" field value
     * @param userPrenomParam
     */
    public void setUserPrenomParam( SearchParam<String> userPrenomParam ){
        this.userPrenomParam = userPrenomParam;
    }

		/**
     * Get the "userLoginParam" field value
     * @return the field value
     */
	public SearchParam<String> getUserLoginParam(){
		return this.userLoginParam;
	}
	/**
     * Set the "userLoginParam" field value
     * @param userLoginParam
     */
    public void setUserLoginParam( SearchParam<String> userLoginParam ){
        this.userLoginParam = userLoginParam;
    }

		/**
     * Get the "etatCodeParam" field value
     * @return the field value
     */
	public SearchParam<String> getEtatCodeParam(){
		return this.etatCodeParam;
	}
	/**
     * Set the "etatCodeParam" field value
     * @param etatCodeParam
     */
    public void setEtatCodeParam( SearchParam<String> etatCodeParam ){
        this.etatCodeParam = etatCodeParam;
    }

		/**
     * Get the "etatLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getEtatLibelleParam(){
		return this.etatLibelleParam;
	}
	/**
     * Set the "etatLibelleParam" field value
     * @param etatLibelleParam
     */
    public void setEtatLibelleParam( SearchParam<String> etatLibelleParam ){
        this.etatLibelleParam = etatLibelleParam;
    }


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		HistorisationDto other = (HistorisationDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("ficheId:"+ficheId);
		sb.append("|");
		sb.append("ficheAvant:"+ficheAvant);
		sb.append("|");
		sb.append("ficheApres:"+ficheApres);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
        return sb.toString();
    }
}
