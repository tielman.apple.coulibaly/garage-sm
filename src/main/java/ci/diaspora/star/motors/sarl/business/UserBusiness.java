


/*
 * Java transformer for entity table user
 * Created on 2019-08-19 ( Time 15:02:29 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import ci.diaspora.star.motors.sarl.dao.entity.BonDecaissement;
import ci.diaspora.star.motors.sarl.dao.entity.BonSortie;
import ci.diaspora.star.motors.sarl.dao.entity.Devis;
import ci.diaspora.star.motors.sarl.dao.entity.Facture;
import ci.diaspora.star.motors.sarl.dao.entity.FicheControlFinal;
import ci.diaspora.star.motors.sarl.dao.entity.FichePreparationDevis;
import ci.diaspora.star.motors.sarl.dao.entity.FicheReception;
import ci.diaspora.star.motors.sarl.dao.entity.Historisation;
import ci.diaspora.star.motors.sarl.dao.entity.OrdreReparation;
import ci.diaspora.star.motors.sarl.dao.entity.Role;
import ci.diaspora.star.motors.sarl.dao.entity.RoleFonctionnalite;
import ci.diaspora.star.motors.sarl.dao.entity.TravauxEffectues;
import ci.diaspora.star.motors.sarl.dao.entity.User;
import ci.diaspora.star.motors.sarl.dao.repository.BonDecaissementRepository;
import ci.diaspora.star.motors.sarl.dao.repository.BonSortieRepository;
import ci.diaspora.star.motors.sarl.dao.repository.DevisRepository;
import ci.diaspora.star.motors.sarl.dao.repository.FactureRepository;
import ci.diaspora.star.motors.sarl.dao.repository.FicheControlFinalRepository;
import ci.diaspora.star.motors.sarl.dao.repository.FichePreparationDevisRepository;
import ci.diaspora.star.motors.sarl.dao.repository.FicheReceptionRepository;
import ci.diaspora.star.motors.sarl.dao.repository.HistorisationRepository;
import ci.diaspora.star.motors.sarl.dao.repository.OrdreReparationRepository;
import ci.diaspora.star.motors.sarl.dao.repository.RoleFonctionnaliteRepository;
import ci.diaspora.star.motors.sarl.dao.repository.RoleRepository;
import ci.diaspora.star.motors.sarl.dao.repository.TravauxEffectuesRepository;
import ci.diaspora.star.motors.sarl.dao.repository.UserRepository;
import ci.diaspora.star.motors.sarl.helper.ExceptionUtils;
import ci.diaspora.star.motors.sarl.helper.FunctionalError;
import ci.diaspora.star.motors.sarl.helper.HostingUtils;
import ci.diaspora.star.motors.sarl.helper.ParamsUtils;
import ci.diaspora.star.motors.sarl.helper.TechnicalError;
import ci.diaspora.star.motors.sarl.helper.Utilities;
import ci.diaspora.star.motors.sarl.helper.Validate;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.dto.BonDecaissementDto;
import ci.diaspora.star.motors.sarl.helper.dto.BonSortieDto;
import ci.diaspora.star.motors.sarl.helper.dto.DevisDto;
import ci.diaspora.star.motors.sarl.helper.dto.FactureDto;
import ci.diaspora.star.motors.sarl.helper.dto.FicheControlFinalDto;
import ci.diaspora.star.motors.sarl.helper.dto.FichePreparationDevisDto;
import ci.diaspora.star.motors.sarl.helper.dto.FicheReceptionDto;
import ci.diaspora.star.motors.sarl.helper.dto.FonctionnaliteDto;
import ci.diaspora.star.motors.sarl.helper.dto.HistorisationDto;
import ci.diaspora.star.motors.sarl.helper.dto.OrdreReparationDto;
import ci.diaspora.star.motors.sarl.helper.dto.TravauxEffectuesDto;
import ci.diaspora.star.motors.sarl.helper.dto.UserDto;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.BonDecaissementTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.BonSortieTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.DevisTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.FactureTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.FicheControlFinalTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.FichePreparationDevisTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.FicheReceptionTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.FonctionnaliteTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.HistorisationTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.OrdreReparationTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.TravauxEffectuesTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.UserTransformer;
import ci.diaspora.star.motors.sarl.helper.enums.EmailEnum;

/**
BUSINESS for table "user"
 *
 * @author SFL Back-End developper
 *
 */
@Component
@EnableScheduling
public class UserBusiness implements IBasicBusiness<Request<UserDto>, Response<UserDto>> {

	private Response<UserDto> response;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private DevisRepository devisRepository;
	@Autowired
	private BonSortieRepository bonSortieRepository;
	@Autowired
	private FactureRepository factureRepository;
	@Autowired
	private OrdreReparationRepository ordreReparationRepository;
	@Autowired
	private TravauxEffectuesRepository travauxEffectuesRepository;
	@Autowired
	private BonDecaissementRepository bonDecaissementRepository;
	@Autowired
	private FicheControlFinalRepository ficheControlFinalRepository;
	@Autowired
	private FichePreparationDevisRepository fichePreparationDevisRepository;
	@Autowired
	private FicheReceptionRepository ficheReceptionRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private HistorisationRepository historisationRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private DevisBusiness devisBusiness;



	@Autowired
	private ParamsUtils paramsUtils;



	@Autowired
	private BonSortieBusiness bonSortieBusiness;


	@Autowired
	private FactureBusiness factureBusiness;


	@Autowired
	private OrdreReparationBusiness ordreReparationBusiness;


	@Autowired
	private TravauxEffectuesBusiness travauxEffectuesBusiness;


	@Autowired
	private BonDecaissementBusiness bonDecaissementBusiness;


	@Autowired
	private FicheControlFinalBusiness ficheControlFinalBusiness;


	@Autowired
	private FichePreparationDevisBusiness fichePreparationDevisBusiness;


	@Autowired
	private FicheReceptionBusiness ficheReceptionBusiness;


	@Autowired
	private HistorisationBusiness historisationBusiness;
	@Autowired
	private RoleFonctionnaliteRepository roleFonctionnaliteRepository;


	private Context context;


	@Autowired
	private HostingUtils hostingUtils;




	public UserBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserDto> create(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin create User-----");

		response = new Response<UserDto>();

		try {

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("nom", dto.getNom());
				fieldsToVerify.put("prenom", dto.getPrenom());
				fieldsToVerify.put("email", dto.getEmail());
				fieldsToVerify.put("login", dto.getLogin());
				fieldsToVerify.put("password", dto.getPassword());
				//fieldsToVerify.put("telephone", dto.getTelephone());
				//fieldsToVerify.put("token", dto.getToken());
				//fieldsToVerify.put("isValidToken", dto.getIsValidToken());
				//fieldsToVerify.put("dateExpireToken", dto.getDateExpireToken());
				//fieldsToVerify.put("lastPassword", dto.getLastPassword());
				//fieldsToVerify.put("signatureUrl", dto.getSignatureUrl());
				fieldsToVerify.put("roleId", dto.getRoleId());
				//fieldsToVerify.put("isLocked", dto.getIsLocked());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}



				// Verify if user to insert do not exist
				User existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = userRepository.findByEmail(dto.getEmail(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getEmail(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les users", locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = userRepository.findByLogin(dto.getLogin(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getLogin(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les users", locale));
					response.setHasError(true);
					return response;
				}
				if (Utilities.notBlank(dto.getTelephone())) {
					existingEntity = userRepository.findUserByTelephone(dto.getTelephone(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					
				}
				// cryptage du password
				dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));

				// Verify if role exist
				Role existingRole = roleRepository.findById(dto.getRoleId(), false);
				if (existingRole == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleId(), locale));
					response.setHasError(true);
					return response;
				}
				User entityToSave = null;

				entityToSave = UserTransformer.INSTANCE.toEntity(dto, existingRole);
				entityToSave.setIsDeleted(false);
				entityToSave.setIsLocked(false);
				entityToSave.setIsValidToken(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : itemsSaved) {
					UserDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
				
				// envoi de mail 

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (User user : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_CREATION_COMPTE;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateCreationCompteParUser();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}

				}

			}

			slf4jLogger.info("----end create User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * create User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> createBySuperAdmin(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin createBySuperAdmin User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			if (utilisateur.getIsSuperUser() != null && !utilisateur.getIsSuperUser()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un super admin" , locale));
				response.setHasError(true);
				return response;
			}

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("nom", dto.getNom());
				fieldsToVerify.put("prenom", dto.getPrenom());
				fieldsToVerify.put("email", dto.getEmail());
				//fieldsToVerify.put("login", dto.getLogin());
				//fieldsToVerify.put("password", dto.getPassword());
				fieldsToVerify.put("telephone", dto.getTelephone());
				//fieldsToVerify.put("token", dto.getToken());
				//fieldsToVerify.put("isValidToken", dto.getIsValidToken());
				//fieldsToVerify.put("dateExpireToken", dto.getDateExpireToken());
				//fieldsToVerify.put("lastPassword", dto.getLastPassword());
				//fieldsToVerify.put("signatureUrl", dto.getSignatureUrl());
				fieldsToVerify.put("roleId", dto.getRoleId());
				//fieldsToVerify.put("isLocked", dto.getIsLocked());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}



				// Verify if user to insert do not exist
				User existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = userRepository.findByEmail(dto.getEmail(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getEmail(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les users", locale));
					response.setHasError(true);
					return response;
				}
				if (Utilities.notBlank(dto.getLogin())) {
					existingEntity = userRepository.findByLogin(dto.getLogin(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getLogin(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					if (Utilities.notBlank(dto.getPassword())) {
						dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));
					}
				}
				
				if (Utilities.notBlank(dto.getTelephone())) {
					existingEntity = userRepository.findUserByTelephone(dto.getTelephone(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					
				}



				// Verify if role exist
				Role existingRole = roleRepository.findById(dto.getRoleId(), false);
				if (existingRole == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleId(), locale));
					response.setHasError(true);
					return response;
				}
				User entityToSave = null;

				entityToSave = UserTransformer.INSTANCE.toEntity(dto, existingRole);
				entityToSave.setIsDeleted(false);
				entityToSave.setIsLocked(false);
				entityToSave.setIsValidToken(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : itemsSaved) {
					UserDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
				
				// envoi de mail 

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (User user : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_FINALISATION_COMPTE;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateCreationCompteParAdmin();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable("email", user.getEmail()); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}

				}
			}

			slf4jLogger.info("----end createBySuperAdmin User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	/**
	 * update User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserDto> update(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin update User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la user existe
				User entityToSave = null;
				entityToSave = userRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if role exist
				if (dto.getRoleId() != null && dto.getRoleId() > 0){
					Role existingRole = roleRepository.findById(dto.getRoleId(), false);
					if (existingRole == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setRole(existingRole);
				}
				if (Utilities.notBlank(dto.getNom())) {
					entityToSave.setNom(dto.getNom());
				}
				if (Utilities.notBlank(dto.getPrenom())) {
					entityToSave.setPrenom(dto.getPrenom());
				}
				if (Utilities.notBlank(dto.getEmail())) {
					User existingEntity = userRepository.findByEmail(dto.getEmail(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getEmail(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEmail(dto.getEmail());
				}
//				if (Utilities.notBlank(dto.getLogin())) {
//					User existingEntity = userRepository.findByLogin(dto.getLogin(), false);
//					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
//						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getLogin(), locale));
//						response.setHasError(true);
//						return response;
//					}
//					if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()) && !a.getId().equals(entityToSaveId))) {
//						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les users", locale));
//						response.setHasError(true);
//						return response;
//					}
//					entityToSave.setLogin(dto.getLogin());
//				}
//				if (Utilities.notBlank(dto.getPassword())) {
//					entityToSave.setPassword(dto.getPassword());
//				}
				if (Utilities.notBlank(dto.getTelephone())) {
					User existingEntity = userRepository.findUserByTelephone(dto.getTelephone(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTelephone(dto.getTelephone());
				}
				if (Utilities.notBlank(dto.getToken())) {
					entityToSave.setToken(dto.getToken());
				}
				if (dto.getIsValidToken() != null) {
					entityToSave.setIsValidToken(dto.getIsValidToken());
				}
				if (Utilities.notBlank(dto.getDateExpireToken())) {
					entityToSave.setDateExpireToken(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateExpireToken()));
				}
//				if (Utilities.notBlank(dto.getLastPassword())) {
//					entityToSave.setLastPassword(dto.getLastPassword());
//				}
				if (Utilities.notBlank(dto.getSignatureUrl())) {
					entityToSave.setSignatureUrl(dto.getSignatureUrl());
				}
				if (dto.getIsLocked() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}
				
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : itemsSaved) {
					UserDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserDto> delete(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la user existe
				User existingEntity = null;
				existingEntity = userRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// devis
				List<Devis> listOfDevis2 = devisRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfDevis2 != null && !listOfDevis2.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDevis2.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// bonSortie
				List<BonSortie> listOfBonSortie = bonSortieRepository.findByUserId(existingEntity.getId(), false);
				if (listOfBonSortie != null && !listOfBonSortie.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfBonSortie.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// facture
				List<Facture> listOfFacture2 = factureRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfFacture2 != null && !listOfFacture2.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFacture2.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation2 = ordreReparationRepository.findByUserReceptionId(existingEntity.getId(), false);
				if (listOfOrdreReparation2 != null && !listOfOrdreReparation2.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfOrdreReparation2.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// travauxEffectues
				List<TravauxEffectues> listOfTravauxEffectues = travauxEffectuesRepository.findByUserTechnicienId(existingEntity.getId(), false);
				if (listOfTravauxEffectues != null && !listOfTravauxEffectues.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfTravauxEffectues.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// bonDecaissement
				List<BonDecaissement> listOfBonDecaissement2 = bonDecaissementRepository.findByUserResponsableAchatId(existingEntity.getId(), false);
				if (listOfBonDecaissement2 != null && !listOfBonDecaissement2.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfBonDecaissement2.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ficheControlFinal
				List<FicheControlFinal> listOfFicheControlFinal2 = ficheControlFinalRepository.findByUserFacturationId(existingEntity.getId(), false);
				if (listOfFicheControlFinal2 != null && !listOfFicheControlFinal2.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFicheControlFinal2.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// fichePreparationDevis
				List<FichePreparationDevis> listOfFichePreparationDevis2 = fichePreparationDevisRepository.findByUserValideurId(existingEntity.getId(), false);
				if (listOfFichePreparationDevis2 != null && !listOfFichePreparationDevis2.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFichePreparationDevis2.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// bonDecaissement
				List<BonDecaissement> listOfBonDecaissement4 = bonDecaissementRepository.findByUserResponsableAchatId(existingEntity.getId(), false);
				if (listOfBonDecaissement4 != null && !listOfBonDecaissement4.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfBonDecaissement4.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ficheReception
				List<FicheReception> listOfFicheReception = ficheReceptionRepository.findByUserId(existingEntity.getId(), false);
				if (listOfFicheReception != null && !listOfFicheReception.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFicheReception.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// devis
				List<Devis> listOfDevis = devisRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfDevis != null && !listOfDevis.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDevis.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// historisation
				List<Historisation> listOfHistorisation = historisationRepository.findByUserId(existingEntity.getId(), false);
				if (listOfHistorisation != null && !listOfHistorisation.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistorisation.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// devis
				List<Devis> listOfDevis3 = devisRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfDevis3 != null && !listOfDevis3.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDevis3.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// facture
				List<Facture> listOfFacture = factureRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfFacture != null && !listOfFacture.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFacture.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation = ordreReparationRepository.findByUserReceptionId(existingEntity.getId(), false);
				if (listOfOrdreReparation != null && !listOfOrdreReparation.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfOrdreReparation.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// bonDecaissement
				List<BonDecaissement> listOfBonDecaissement = bonDecaissementRepository.findByUserResponsableAchatId(existingEntity.getId(), false);
				if (listOfBonDecaissement != null && !listOfBonDecaissement.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfBonDecaissement.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// facture
				List<Facture> listOfFacture3 = factureRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfFacture3 != null && !listOfFacture3.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFacture3.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// fichePreparationDevis
				List<FichePreparationDevis> listOfFichePreparationDevis = fichePreparationDevisRepository.findByUserValideurId(existingEntity.getId(), false);
				if (listOfFichePreparationDevis != null && !listOfFichePreparationDevis.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFichePreparationDevis.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ficheControlFinal
				List<FicheControlFinal> listOfFicheControlFinal = ficheControlFinalRepository.findByUserFacturationId(existingEntity.getId(), false);
				if (listOfFicheControlFinal != null && !listOfFicheControlFinal.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFicheControlFinal.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation3 = ordreReparationRepository.findByUserReceptionId(existingEntity.getId(), false);
				if (listOfOrdreReparation3 != null && !listOfOrdreReparation3.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfOrdreReparation3.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// bonDecaissement
				List<BonDecaissement> listOfBonDecaissement3 = bonDecaissementRepository.findByUserResponsableAchatId(existingEntity.getId(), false);
				if (listOfBonDecaissement3 != null && !listOfBonDecaissement3.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfBonDecaissement3.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ficheControlFinal
				List<FicheControlFinal> listOfFicheControlFinal3 = ficheControlFinalRepository.findByUserFacturationId(existingEntity.getId(), false);
				if (listOfFicheControlFinal3 != null && !listOfFicheControlFinal3.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFicheControlFinal3.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				userRepository.save((Iterable<User>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<UserDto> forceDelete(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la user existe
				User existingEntity = null;
				existingEntity = userRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// devis
				List<Devis> listOfDevis2 = devisRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfDevis2 != null && !listOfDevis2.isEmpty()){
					Request<DevisDto> deleteRequest = new Request<DevisDto>();
					deleteRequest.setDatas(DevisTransformer.INSTANCE.toDtos(listOfDevis2));
					deleteRequest.setUser(request.getUser());
					Response<DevisDto> deleteResponse = devisBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// bonSortie
				List<BonSortie> listOfBonSortie = bonSortieRepository.findByUserId(existingEntity.getId(), false);
				if (listOfBonSortie != null && !listOfBonSortie.isEmpty()){
					Request<BonSortieDto> deleteRequest = new Request<BonSortieDto>();
					deleteRequest.setDatas(BonSortieTransformer.INSTANCE.toDtos(listOfBonSortie));
					deleteRequest.setUser(request.getUser());
					Response<BonSortieDto> deleteResponse = bonSortieBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// facture
				List<Facture> listOfFacture2 = factureRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfFacture2 != null && !listOfFacture2.isEmpty()){
					Request<FactureDto> deleteRequest = new Request<FactureDto>();
					deleteRequest.setDatas(FactureTransformer.INSTANCE.toDtos(listOfFacture2));
					deleteRequest.setUser(request.getUser());
					Response<FactureDto> deleteResponse = factureBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation2 = ordreReparationRepository.findByUserReceptionId(existingEntity.getId(), false);
				if (listOfOrdreReparation2 != null && !listOfOrdreReparation2.isEmpty()){
					Request<OrdreReparationDto> deleteRequest = new Request<OrdreReparationDto>();
					deleteRequest.setDatas(OrdreReparationTransformer.INSTANCE.toDtos(listOfOrdreReparation2));
					deleteRequest.setUser(request.getUser());
					Response<OrdreReparationDto> deleteResponse = ordreReparationBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// travauxEffectues
				List<TravauxEffectues> listOfTravauxEffectues = travauxEffectuesRepository.findByUserTechnicienId(existingEntity.getId(), false);
				if (listOfTravauxEffectues != null && !listOfTravauxEffectues.isEmpty()){
					Request<TravauxEffectuesDto> deleteRequest = new Request<TravauxEffectuesDto>();
					deleteRequest.setDatas(TravauxEffectuesTransformer.INSTANCE.toDtos(listOfTravauxEffectues));
					deleteRequest.setUser(request.getUser());
					Response<TravauxEffectuesDto> deleteResponse = travauxEffectuesBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// bonDecaissement
				List<BonDecaissement> listOfBonDecaissement2 = bonDecaissementRepository.findByUserResponsableAchatId(existingEntity.getId(), false);
				if (listOfBonDecaissement2 != null && !listOfBonDecaissement2.isEmpty()){
					Request<BonDecaissementDto> deleteRequest = new Request<BonDecaissementDto>();
					deleteRequest.setDatas(BonDecaissementTransformer.INSTANCE.toDtos(listOfBonDecaissement2));
					deleteRequest.setUser(request.getUser());
					Response<BonDecaissementDto> deleteResponse = bonDecaissementBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ficheControlFinal
				List<FicheControlFinal> listOfFicheControlFinal2 = ficheControlFinalRepository.findByUserFacturationId(existingEntity.getId(), false);
				if (listOfFicheControlFinal2 != null && !listOfFicheControlFinal2.isEmpty()){
					Request<FicheControlFinalDto> deleteRequest = new Request<FicheControlFinalDto>();
					deleteRequest.setDatas(FicheControlFinalTransformer.INSTANCE.toDtos(listOfFicheControlFinal2));
					deleteRequest.setUser(request.getUser());
					Response<FicheControlFinalDto> deleteResponse = ficheControlFinalBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// fichePreparationDevis
				List<FichePreparationDevis> listOfFichePreparationDevis2 = fichePreparationDevisRepository.findByUserValideurId(existingEntity.getId(), false);
				if (listOfFichePreparationDevis2 != null && !listOfFichePreparationDevis2.isEmpty()){
					Request<FichePreparationDevisDto> deleteRequest = new Request<FichePreparationDevisDto>();
					deleteRequest.setDatas(FichePreparationDevisTransformer.INSTANCE.toDtos(listOfFichePreparationDevis2));
					deleteRequest.setUser(request.getUser());
					Response<FichePreparationDevisDto> deleteResponse = fichePreparationDevisBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// bonDecaissement
				List<BonDecaissement> listOfBonDecaissement4 = bonDecaissementRepository.findByUserResponsableAchatId(existingEntity.getId(), false);
				if (listOfBonDecaissement4 != null && !listOfBonDecaissement4.isEmpty()){
					Request<BonDecaissementDto> deleteRequest = new Request<BonDecaissementDto>();
					deleteRequest.setDatas(BonDecaissementTransformer.INSTANCE.toDtos(listOfBonDecaissement4));
					deleteRequest.setUser(request.getUser());
					Response<BonDecaissementDto> deleteResponse = bonDecaissementBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ficheReception
				List<FicheReception> listOfFicheReception = ficheReceptionRepository.findByUserId(existingEntity.getId(), false);
				if (listOfFicheReception != null && !listOfFicheReception.isEmpty()){
					Request<FicheReceptionDto> deleteRequest = new Request<FicheReceptionDto>();
					deleteRequest.setDatas(FicheReceptionTransformer.INSTANCE.toDtos(listOfFicheReception));
					deleteRequest.setUser(request.getUser());
					Response<FicheReceptionDto> deleteResponse = ficheReceptionBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// devis
				List<Devis> listOfDevis = devisRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfDevis != null && !listOfDevis.isEmpty()){
					Request<DevisDto> deleteRequest = new Request<DevisDto>();
					deleteRequest.setDatas(DevisTransformer.INSTANCE.toDtos(listOfDevis));
					deleteRequest.setUser(request.getUser());
					Response<DevisDto> deleteResponse = devisBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// historisation
				List<Historisation> listOfHistorisation = historisationRepository.findByUserId(existingEntity.getId(), false);
				if (listOfHistorisation != null && !listOfHistorisation.isEmpty()){
					Request<HistorisationDto> deleteRequest = new Request<HistorisationDto>();
					deleteRequest.setDatas(HistorisationTransformer.INSTANCE.toDtos(listOfHistorisation));
					deleteRequest.setUser(request.getUser());
					Response<HistorisationDto> deleteResponse = historisationBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// devis
				List<Devis> listOfDevis3 = devisRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfDevis3 != null && !listOfDevis3.isEmpty()){
					Request<DevisDto> deleteRequest = new Request<DevisDto>();
					deleteRequest.setDatas(DevisTransformer.INSTANCE.toDtos(listOfDevis3));
					deleteRequest.setUser(request.getUser());
					Response<DevisDto> deleteResponse = devisBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// facture
				List<Facture> listOfFacture = factureRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfFacture != null && !listOfFacture.isEmpty()){
					Request<FactureDto> deleteRequest = new Request<FactureDto>();
					deleteRequest.setDatas(FactureTransformer.INSTANCE.toDtos(listOfFacture));
					deleteRequest.setUser(request.getUser());
					Response<FactureDto> deleteResponse = factureBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation = ordreReparationRepository.findByUserReceptionId(existingEntity.getId(), false);
				if (listOfOrdreReparation != null && !listOfOrdreReparation.isEmpty()){
					Request<OrdreReparationDto> deleteRequest = new Request<OrdreReparationDto>();
					deleteRequest.setDatas(OrdreReparationTransformer.INSTANCE.toDtos(listOfOrdreReparation));
					deleteRequest.setUser(request.getUser());
					Response<OrdreReparationDto> deleteResponse = ordreReparationBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// bonDecaissement
				List<BonDecaissement> listOfBonDecaissement = bonDecaissementRepository.findByUserResponsableAchatId(existingEntity.getId(), false);
				if (listOfBonDecaissement != null && !listOfBonDecaissement.isEmpty()){
					Request<BonDecaissementDto> deleteRequest = new Request<BonDecaissementDto>();
					deleteRequest.setDatas(BonDecaissementTransformer.INSTANCE.toDtos(listOfBonDecaissement));
					deleteRequest.setUser(request.getUser());
					Response<BonDecaissementDto> deleteResponse = bonDecaissementBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// facture
				List<Facture> listOfFacture3 = factureRepository.findByUserDirectionId(existingEntity.getId(), false);
				if (listOfFacture3 != null && !listOfFacture3.isEmpty()){
					Request<FactureDto> deleteRequest = new Request<FactureDto>();
					deleteRequest.setDatas(FactureTransformer.INSTANCE.toDtos(listOfFacture3));
					deleteRequest.setUser(request.getUser());
					Response<FactureDto> deleteResponse = factureBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// fichePreparationDevis
				List<FichePreparationDevis> listOfFichePreparationDevis = fichePreparationDevisRepository.findByUserValideurId(existingEntity.getId(), false);
				if (listOfFichePreparationDevis != null && !listOfFichePreparationDevis.isEmpty()){
					Request<FichePreparationDevisDto> deleteRequest = new Request<FichePreparationDevisDto>();
					deleteRequest.setDatas(FichePreparationDevisTransformer.INSTANCE.toDtos(listOfFichePreparationDevis));
					deleteRequest.setUser(request.getUser());
					Response<FichePreparationDevisDto> deleteResponse = fichePreparationDevisBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ficheControlFinal
				List<FicheControlFinal> listOfFicheControlFinal = ficheControlFinalRepository.findByUserFacturationId(existingEntity.getId(), false);
				if (listOfFicheControlFinal != null && !listOfFicheControlFinal.isEmpty()){
					Request<FicheControlFinalDto> deleteRequest = new Request<FicheControlFinalDto>();
					deleteRequest.setDatas(FicheControlFinalTransformer.INSTANCE.toDtos(listOfFicheControlFinal));
					deleteRequest.setUser(request.getUser());
					Response<FicheControlFinalDto> deleteResponse = ficheControlFinalBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation3 = ordreReparationRepository.findByUserReceptionId(existingEntity.getId(), false);
				if (listOfOrdreReparation3 != null && !listOfOrdreReparation3.isEmpty()){
					Request<OrdreReparationDto> deleteRequest = new Request<OrdreReparationDto>();
					deleteRequest.setDatas(OrdreReparationTransformer.INSTANCE.toDtos(listOfOrdreReparation3));
					deleteRequest.setUser(request.getUser());
					Response<OrdreReparationDto> deleteResponse = ordreReparationBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// bonDecaissement
				List<BonDecaissement> listOfBonDecaissement3 = bonDecaissementRepository.findByUserResponsableAchatId(existingEntity.getId(), false);
				if (listOfBonDecaissement3 != null && !listOfBonDecaissement3.isEmpty()){
					Request<BonDecaissementDto> deleteRequest = new Request<BonDecaissementDto>();
					deleteRequest.setDatas(BonDecaissementTransformer.INSTANCE.toDtos(listOfBonDecaissement3));
					deleteRequest.setUser(request.getUser());
					Response<BonDecaissementDto> deleteResponse = bonDecaissementBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ficheControlFinal
				List<FicheControlFinal> listOfFicheControlFinal3 = ficheControlFinalRepository.findByUserFacturationId(existingEntity.getId(), false);
				if (listOfFicheControlFinal3 != null && !listOfFicheControlFinal3.isEmpty()){
					Request<FicheControlFinalDto> deleteRequest = new Request<FicheControlFinalDto>();
					deleteRequest.setDatas(FicheControlFinalTransformer.INSTANCE.toDtos(listOfFicheControlFinal3));
					deleteRequest.setUser(request.getUser());
					Response<FicheControlFinalDto> deleteResponse = ficheControlFinalBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				userRepository.save((Iterable<User>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * connexion User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> connexion(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin connexion User-----");

		response = new Response<UserDto>();

		try {

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("password", dto.getPassword());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}


			// Verify if user to insert do not exist
			User existingEntity = null;

			// cryptage du password avant de rechercher dans la BD
			dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));


			existingEntity = userRepository.findByLoginAndPassword(dto.getLogin(), dto.getPassword(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user avec le login-> " + dto.getLogin() + " et le password n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}


			if (existingEntity.getIsLocked() != null && existingEntity.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+existingEntity.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			UserDto userDto = UserTransformer.INSTANCE.toDto(existingEntity);




			// renvoi de la listes des fonctionnality
			List<RoleFonctionnalite> listRoleFonctionality = roleFonctionnaliteRepository.findByRoleId(existingEntity.getRole().getId(), false);
			if (listRoleFonctionality != null && !listRoleFonctionality.isEmpty()) {
				List<FonctionnaliteDto> datasFonctionnalite = new ArrayList<>();
				for (RoleFonctionnalite roleFonctionnalite : listRoleFonctionality) {
					datasFonctionnalite.add(FonctionnaliteTransformer.INSTANCE.toDto(roleFonctionnalite.getFonctionnalite()));
				}
				userDto.setDatasFonctionnalite(datasFonctionnalite);
			}


			// le password doit etre invisible lors de la recuperation d'un user
			userDto.setPassword(null);
			response.setItems(Arrays.asList(userDto));
			response.setHasError(false);

			slf4jLogger.info("----end connexion User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	/**
	 * finaliserInscription User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> finaliserInscription(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin finaliserInscription User-----");

		response = new Response<UserDto>();

		try {

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("email", dto.getEmail());
			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("password", dto.getPassword());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}


			// Verify if user to insert do not exist
			User entityToSave = null;
			
			entityToSave = userRepository.findByEmail(dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user avec le login-> " + dto.getLogin() + " et le password n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}
			if (Utilities.notBlank(entityToSave.getLogin()) || Utilities.notBlank(entityToSave.getPassword())) {
				response.setStatus(functionalError.DATA_EXIST("cet user login-> " + dto.getLogin() + " a déjà finaliser son inscription." , locale));
				response.setHasError(true);
				return response;
			}
			
			User existingEntity = null;
			existingEntity = userRepository.findByLogin(dto.getLogin(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("user avec le login-> " + dto.getLogin() + " existe déjà." , locale));
				response.setHasError(true);
				return response;
			}
			

			// cryptage du password avant de rechercher dans la BD
			dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));

			entityToSave.setLogin(dto.getLogin());
			entityToSave.setPassword(dto.getPassword());
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(entityToSave.getId());
			
			
			userRepository.save(entityToSave);
			
			UserDto userDto = UserTransformer.INSTANCE.toDto(entityToSave);




			// renvoi de la listes des fonctionnality
			List<RoleFonctionnalite> listRoleFonctionality = roleFonctionnaliteRepository.findByRoleId(entityToSave.getRole().getId(), false);
			if (listRoleFonctionality != null && !listRoleFonctionality.isEmpty()) {
				List<FonctionnaliteDto> datasFonctionnalite = new ArrayList<>();
				for (RoleFonctionnalite roleFonctionnalite : listRoleFonctionality) {
					datasFonctionnalite.add(FonctionnaliteTransformer.INSTANCE.toDto(roleFonctionnalite.getFonctionnalite()));
				}
				userDto.setDatasFonctionnalite(datasFonctionnalite);
			}


			// le password doit etre invisible lors de la recuperation d'un user
			userDto.setPassword(null);
			response.setItems(Arrays.asList(userDto));
			response.setHasError(false);

			slf4jLogger.info("----end finaliserInscription User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * changerPassword User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> changerPassword(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin changerPassword User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();
			List<User> itemsAvant = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("newPassword", dto.getNewPassword());
			fieldsToVerify.put("password", dto.getPassword());
			fieldsToVerify.put("userId", request.getUser());

			//			 fieldsToVerify.put("newPassword", dto.getNewPassword());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if utilisateur exist
			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser() +" n'existe pas.", locale));
				response.setHasError(true);
				return response;
			}

			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			// Verify if user to insert do not exist
			User entityToSave = null;

			// cryptage du password avant de rechercher dans la BD
			dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));
			entityToSave = userRepository.findByLoginAndPassword(dto.getLogin(), dto.getPassword(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user avec le login-> " + dto.getLogin() + " avec ce password n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}

			// choisir la vraie url
			String appLink = "";
			//			if (entityToSave.getAdmin() != null ) {
			//				appLink = paramsUtils.getUrlRootAdmin();
			//			}else {
			//				appLink = paramsUtils.getUrlRootUser();
			//			}

			appLink = paramsUtils.getUrlRootUser();

			// verifier que le user connecter correspond à celui qui veut changer le password
			if (!entityToSave.getId().equals(utilisateur.getId()) && !entityToSave.getIsSuperUser()) {
				response.setStatus(functionalError.AUTH_FAIL("Utilisateur -> " + utilisateur.getId() +" n'a pas ce droit.", locale));
				response.setHasError(true);
				return response;
			}

			//			User entityToSaveOld = null;
			//			// pour historisation
			//			entityToSaveOld = (User) entityToSave.clone();
			//			itemsAvant.add(entityToSaveOld);

			Integer entityToSaveId = entityToSave.getId();

			//			 // renseigner les valeurs a MAJ
			//			 if (Utilities.notBlank(dto.getNewPassword())) {
			//				 entityToSave.setPassword(dto.getNewPassword());
			//			 }
			//			 if (Utilities.notBlank(dto.getNewLogin())) {
			//				 User existingEntity = userRepository.findByLogin(dto.getNewLogin(), false);
			//				 if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
			//					 response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getNewLogin() +" existe deja.", locale));
			//					 response.setHasError(true);
			//					 return response;
			//				 }
			//				 if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getNewLogin()) && !a.getId().equals(entityToSaveId))) {
			//					 response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getNewLogin()+"' pour les users", locale));
			//					 response.setHasError(true);
			//					 return response;
			//				 }
			//				 entityToSave.setLogin(dto.getNewLogin());
			//			 }


			// imposer que le newPassword >= 8 caracteres
			//			if (dto.getNewPassword().length() < EmailEnum.MIN_CARACTERE_PASSWORD) {
			//				response.setStatus(functionalError.INVALID_FORMAT("Votre nouveau password doit etre au moins de 8 caractères. Merci de renseigner un password d'au moins 8 caractères.", locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			// cryptage du NewPassword avant de l'inserer dans la BD
			dto.setNewPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getNewPassword()));

			entityToSave.setPassword(dto.getNewPassword());
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				//				Response<HistoriqueActionUserDto> responseHistoriqueActionUser = new Response<>();
				//
				//				responseHistoriqueActionUser =  historiqueActionUserBusiness.callHistorisation(itemsAvant, itemsSaved, request.getUrlService(), request.getUser(), locale);
				//
				//				if (responseHistoriqueActionUser.isHasError()) {
				//					response.setHasError(true);
				//					response.setStatus(responseHistoriqueActionUser.getStatus());
				//					return response;
				//				}

				response.setStatus(functionalError.SUCCESS("Votre login et/ou password à bien été modifié.", locale));
				response.setHasError(false);

				// envoi de mail 

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());

					for (User user : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_PASSWORD_RESET;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateSuccesChangePassword();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink",appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}

						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}

				}
			}

			slf4jLogger.info("----end changerPassword User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * passwordOublier User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> passwordOublier(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin passwordOublier User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			//			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("email", dto.getEmail());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}




			// Verify if user to insert do not exist
			User entityToSave = null;


			entityToSave = userRepository.findByEmail(dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  +" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}
			// choisir la vraie url
			String appLink = "";
			//			if (entityToSave.getAdmin() != null ) {
			//				appLink = paramsUtils.getUrlRootAdmin();
			//			}else {
			//				appLink = paramsUtils.getUrlRootUser();
			//			}

			appLink = paramsUtils.getUrlRootUser();


			//			// verifier que le user connecter correspond à celui qui a oublié le password
			//			if (!entityToSave.getId().equals(utilisateur.getId())) {
			//				response.setStatus(functionalError.AUTH_FAIL("Utilisateur -> " + utilisateur.getId() +" n'a pas ce droit.", locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			// setter token, isValidToken et tokenCreatedAt
			entityToSave.setToken(Utilities.randomString(50));
			GregorianCalendar calendar =new GregorianCalendar();

			calendar.setTime(Utilities.getCurrentDate());
			calendar.add(Calendar.HOUR, 24);
			entityToSave.setDateExpireToken(calendar.getTime());
			entityToSave.setIsValidToken(true);
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(entityToSave.getId());


			items.add(entityToSave);

			if (!items.isEmpty()) {

				// sauvegarde du user avec le champ token
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				// envoi de mail avec le lien comportant le token et email

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());
					for (User user : itemsSaved) {


						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//					for (User user : items) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//					recipient.put("user", user.getNom());
						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//			        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_FORGET_PASSWORD;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateResetUserPassword();

						context.setVariable("email", user.getEmail());
						context.setVariable("nom", user.getNom());
						context.setVariable("token", user.getToken());
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate()));
						context.setVariable("expiration", dateFormat.format(user.getDateExpireToken()));
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						// //app.link=http://ims-pp-webbo.ovh.swan.smile.ci
						//					context.setVariable("date", dateFormat.format(new Date()));


						//					context.setVariable("userName", entityToSave.getNom());
						//					context.setVariable("userName", entityToSave.getEmail());
						//					context.setVariable("url", paramsUtils.getUrlRootToken() + entityToSave.getEmail()+ "/"+entityToSave.getToken() );
						//					context.setVariable("expiration", dateFormat.format(entityToSave.getTokenCreatedAt()));
						//					context.setVariable("date", dateFormat.format(new Date()));
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail
					}

				}
				response.setStatus(functionalError.SUCCESS("Veuillez consulter votre mail pour saisir votre password.", locale));
				response.setHasError(false);
			}
			slf4jLogger.info("----end passwordOublier User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * changerPasswordOublier User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> changerPasswordOublier(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin changerPasswordOublier User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();
			List<User> itemsAvant = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("email", dto.getEmail());
			fieldsToVerify.put("newPassword", dto.getNewPassword());
			//			fieldsToVerify.put("password", dto.getPassword());
			//			fieldsToVerify.put("user", request.getUser());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if utilisateur exist
			//			User utilisateur = userRepository.findById(request.getUser(), false);
			//			if (utilisateur == null) {
			//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser() +" n'existe pas.", locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			// Verify if user to insert do not exist
			User entityToSave = null;

			entityToSave = userRepository.findByEmail(dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  +" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}

			// choisir la vraie url
			String appLink = "";
			//			if (entityToSave.getAdmin() != null ) {
			//				appLink = paramsUtils.getUrlRootAdmin();
			//			}else {
			//				appLink = paramsUtils.getUrlRootUser();
			//			}

			appLink = paramsUtils.getUrlRootUser();


			User entityToSaveOld = null;
			// pour historisation
			entityToSaveOld = (User) entityToSave.clone();
			itemsAvant.add(entityToSaveOld);

			// cryptage du password avant de le setter dans la BD
			entityToSave.setPassword(Utilities.encryptPasswordBy_(entityToSave.getLogin(), dto.getNewPassword()));

			Integer entityToSaveId = entityToSave.getId();

			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(entityToSaveId);
			items.add(entityToSave);

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				//				Response<HistoriqueActionUserDto> responseHistoriqueActionUser = new Response<>();
				//
				//				responseHistoriqueActionUser =  historiqueActionUserBusiness.callHistorisation(itemsAvant, itemsSaved, request.getUrlService(), entityToSave.getId(), locale);
				//
				//				if (responseHistoriqueActionUser.isHasError()) {
				//					response.setHasError(true);
				//					response.setStatus(responseHistoriqueActionUser.getStatus());
				//					return response;
				//				}

				response.setStatus(functionalError.SUCCESS("Votre login et/ou password à bien été modifié.", locale));
				response.setHasError(false);

				//				// envoi de mail password changer avec succes
				//				if (Utilities.isNotEmpty(itemsSaved)) {
				//					
				//					//set mail to user
				//					Map<String, String> from=new HashMap<>();
				//		        	from.put("email", paramsUtils.getSmtpMailUsername());
				//		        	from.put("user", paramsUtils.getSmtpMailUser());
				//		        	//recipients
				//		        	List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
				////		        	for (User user : itemsSaved) {
				//		        	User user = itemsSaved.get(0);
				//		        	Map<String, String> recipient = new HashMap<String, String>();
				//		        	recipient = new HashMap<String, String>();
				//		        	recipient.put("email", user.getEmail());
				//		        	recipient.put("user", user.getLogin());
				////		        	recipient.put("user", user.getNom());
				//
				//		        	toRecipients.add(recipient); 
				////					}
				//		        	
				//		        	//subject
				//		        	// ajout du champ objet a la newsletter
				////		        	String subject = "NEWSLETTER DU VOLONTARIAT";
				//		        	String subject = EmailEnum.SUBJECT_RESET_PASSWORD_OK;
				//		        	String body = "";
				//		        	context = new Context();
				//		        	String templateChangerPassword = paramsUtils.getTemplateSuccesChangePassword();
				////		        	context.setVariable("email", user.getEmail());
				////					context.setVariable("login", user.getLogin());
				////					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
				//					context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
				//					
				//		        	response = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
				//		        	// si jamais une exception c'est produite pour un mail
				//
				//				}
				
				// envoi de mail 

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());

					for (User user : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_PASSWORD_RESET;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateSuccesChangePassword();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink",appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}

						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}

				}
			}

			slf4jLogger.info("----end changerPasswordOublier User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * checkEmailWithTokenIsValid User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> checkEmailWithTokenIsValid(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin checkEmailWithTokenIsValid User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("token", dto.getToken());
			fieldsToVerify.put("email", dto.getEmail());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}
			// Verify if user to insert do not exist
			User entityToSave = null;

			entityToSave = userRepository.findUserByTokenAndEmail(dto.getToken(), dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  + " avec le token "+ dto.getToken()+" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}else{
				if (entityToSave.getIsValidToken() != null && entityToSave.getDateExpireToken() != null) {
					if (!entityToSave.getIsValidToken() || Utilities.getCurrentDate().after(entityToSave.getDateExpireToken())  ) {
						response.setStatus(functionalError.DATA_NOT_EXIST("ce token a deja été utilisé ou est expiré", locale));
						response.setHasError(true);
						return response;
					}
				}else {
					response.setStatus(functionalError.AUTH_FAIL("Veuillez signifier d'abord que vous avez oublié votre password.", locale));
					response.setHasError(true);
					return response;
				}

				// setter ces valeurs a null car elles n'ont plus de sens
				entityToSave.setToken(null);
				entityToSave.setIsValidToken(false);
				//entityToSave.setTokenCreatedAt(null);
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(entityToSave.getId());
				items.add(entityToSave);

				// maj les donnees en base
				items = userRepository.save((Iterable<User>) items);
				if (!Utilities.isNotEmpty(items)) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				response.setHasError(false);
			}

			slf4jLogger.info("----end checkEmailWithTokenIsValid User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	
	/**
	 * checkEmailIsValid User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> checkEmailIsValid(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin checkEmailIsValid User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("email", dto.getEmail());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}
			// Verify if user to insert do not exist
			User entityToSave = null;

			entityToSave = userRepository.findByEmail(dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  + " n'appartient pas au système." , locale));
				response.setHasError(true);
				return response;
			}
			if (Utilities.notBlank(entityToSave.getLogin()) || Utilities.notBlank(entityToSave.getPassword())) {
				response.setStatus(functionalError.DATA_NOT_EXIST("cet user email-> " + dto.getEmail() + " a déjà finaliser son inscription." , locale));
				response.setHasError(true);
				return response;
			}
			
			response.setHasError(false);


			slf4jLogger.info("----end checkEmailIsValid User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}





	/**
	 * get User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<UserDto> getByCriteria(Request<UserDto> request, Locale locale) {
		slf4jLogger.info("----begin get User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = null;
			items = userRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : items) {
					UserDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(userRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("user", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full UserDto by using User as object.
	 *
	 * @param entity, locale
	 * @return UserDto
	 *
	 */
	private UserDto getFullInfos(User entity, Integer size, Locale locale) throws Exception {
		UserDto dto = UserTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		dto.setPassword("***************");
		dto.setLastPassword("***************");
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
