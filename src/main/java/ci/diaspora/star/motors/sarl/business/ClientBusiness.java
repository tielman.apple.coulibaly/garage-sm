


/*
 * Java transformer for entity table client
 * Created on 2019-08-18 ( Time 17:15:06 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.dao.entity.Client;
import ci.diaspora.star.motors.sarl.dao.entity.FicheReception;
import ci.diaspora.star.motors.sarl.dao.entity.TypeClient;
import ci.diaspora.star.motors.sarl.dao.entity.User;
import ci.diaspora.star.motors.sarl.dao.entity.Vehicule;
import ci.diaspora.star.motors.sarl.dao.repository.ClientRepository;
import ci.diaspora.star.motors.sarl.dao.repository.TypeClientRepository;
import ci.diaspora.star.motors.sarl.dao.repository.UserRepository;
import ci.diaspora.star.motors.sarl.dao.repository.VehiculeRepository;
import ci.diaspora.star.motors.sarl.helper.ExceptionUtils;
import ci.diaspora.star.motors.sarl.helper.FunctionalError;
import ci.diaspora.star.motors.sarl.helper.TechnicalError;
import ci.diaspora.star.motors.sarl.helper.Utilities;
import ci.diaspora.star.motors.sarl.helper.Validate;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.dto.ClientDto;
import ci.diaspora.star.motors.sarl.helper.dto.VehiculeDto;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.ClientTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.VehiculeTransformer;
import ci.diaspora.star.motors.sarl.helper.enums.EmailEnum;

/**
BUSINESS for table "client"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class ClientBusiness implements IBasicBusiness<Request<ClientDto>, Response<ClientDto>> {

	private Response<ClientDto> response;
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private VehiculeRepository vehiculeRepository;
	@Autowired
	private TypeClientRepository typeClientRepository;
	

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private ClientBusiness clientBusiness;


	@Autowired
	private VehiculeBusiness vehiculeBusiness;



	public ClientBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Client by using ClientDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ClientDto> create(Request<ClientDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Client-----");

		response = new Response<ClientDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Client> items = new ArrayList<Client>();

			for (ClientDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("matricule", dto.getMatricule());
				fieldsToVerify.put("identificationProprietaire", dto.getIdentificationProprietaire());
				fieldsToVerify.put("email", dto.getEmail());
				fieldsToVerify.put("adresseGeographique", dto.getAdresseGeographique());
				fieldsToVerify.put("telephone", dto.getTelephone());
				fieldsToVerify.put("typeClientId", dto.getTypeClientId());
				//fieldsToVerify.put("isOld", dto.getIsOld());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if client to insert do not exist
				Client existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("client -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = clientRepository.findByEmail(dto.getEmail(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("client -> " + dto.getEmail(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les clients", locale));
					response.setHasError(true);
					return response;
				}

				// generation de matricule valide
				String matricule = EmailEnum.MATRICULE_STAR_MOTORS + Utilities.getCurrentDate().getYear() + Utilities.rand.nextInt(4);
				while (Utilities.notBlank(matricule)) {
					existingEntity = clientRepository.findClientByMatricule(matricule, false);
					if (existingEntity != null) {
						matricule = EmailEnum.MATRICULE_STAR_MOTORS + Utilities.getCurrentDate().getYear() + Utilities.rand.nextInt(4);
					}else {
						dto.setMatricule(matricule);
						if (items.stream().anyMatch(a->a.getMatricule().equalsIgnoreCase(dto.getMatricule()))) {
							matricule = EmailEnum.MATRICULE_STAR_MOTORS + Utilities.getCurrentDate().getYear() + Utilities.rand.nextInt(4);
						}else {
							matricule = "";
						}
					}
				}



				// Verify if typeClient exist
		        
				TypeClient existingTypeClient = typeClientRepository.findById(dto.getTypeClientId(), false);
				if (existingTypeClient == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeClient -> " + dto.getTypeClientId(), locale));
					response.setHasError(true);
					return response;
				}
				Client entityToSave = null;
				entityToSave = ClientTransformer.INSTANCE.toEntity(dto,existingTypeClient);
				entityToSave.setIsDeleted(false);
				entityToSave.setIsOld(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Client> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = clientRepository.save((Iterable<Client>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("client", locale));
					response.setHasError(true);
					return response;
				}
				List<ClientDto> itemsDto = new ArrayList<ClientDto>();
				for (Client entity : itemsSaved) {
					ClientDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create Client-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Client by using ClientDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ClientDto> update(Request<ClientDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Client-----");

		response = new Response<ClientDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Client> items = new ArrayList<Client>();

			for (ClientDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la client existe
				Client entityToSave = null;
				entityToSave = clientRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("client -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if client exist
				if (dto.getTypeClientId() != null && dto.getTypeClientId() > 0){
					TypeClient existingTypeClient = typeClientRepository.findById(dto.getTypeClientId(), false);
					if (existingTypeClient == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeClient -> " + dto.getTypeClientId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTypeClient(existingTypeClient);
				}

				if (utilisateur.getIsSuperUser() != null  && utilisateur.getIsSuperUser()) {
					if (Utilities.notBlank(dto.getMatricule())) {
						Client existingEntity = clientRepository.findClientByMatricule(dto.getMatricule(), false);

						if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
							response.setStatus(functionalError.DATA_EXIST("client -> " + dto.getMatricule(), locale));
							response.setHasError(true);
							return response;
						}
						if (items.stream().anyMatch(a->a.getMatricule().equalsIgnoreCase(dto.getMatricule()) && !a.getId().equals(entityToSaveId))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du matricule '" + dto.getMatricule()+"' pour les clients", locale));
							response.setHasError(true);
							return response;
						}
						entityToSave.setMatricule(dto.getMatricule());

					}
				}

				if (Utilities.notBlank(dto.getIdentificationProprietaire())) {
					entityToSave.setIdentificationProprietaire(dto.getIdentificationProprietaire());
				}
				if (Utilities.notBlank(dto.getEmail())) {
					Client existingEntity = clientRepository.findByEmail(dto.getEmail(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("client -> " + dto.getEmail(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les clients", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEmail(dto.getEmail());
				}
				if (Utilities.notBlank(dto.getAdresseGeographique())) {
					entityToSave.setAdresseGeographique(dto.getAdresseGeographique());
				}
				if (Utilities.notBlank(dto.getTelephone())) {
					entityToSave.setTelephone(dto.getTelephone());
				}
				if (dto.getIsOld() != null) {
					entityToSave.setIsOld(dto.getIsOld());
				}

				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Client> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = clientRepository.save((Iterable<Client>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("client", locale));
					response.setHasError(true);
					return response;
				}
				List<ClientDto> itemsDto = new ArrayList<ClientDto>();
				for (Client entity : itemsSaved) {
					ClientDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update Client-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete Client by using ClientDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ClientDto> delete(Request<ClientDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Client-----");

		response = new Response<ClientDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Client> items = new ArrayList<Client>();

			for (ClientDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la client existe
				Client existingEntity = null;
				existingEntity = clientRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("client -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// client
				List<Client> listOfClient = clientRepository.findByTypeClientId(existingEntity.getId(), false);
				if (listOfClient != null && !listOfClient.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfClient.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// vehicule
				List<Vehicule> listOfVehicule = vehiculeRepository.findByClientId(existingEntity.getId(), false);
				if (listOfVehicule != null && !listOfVehicule.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfVehicule.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				clientRepository.save((Iterable<Client>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Client-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Client by using ClientDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<ClientDto> forceDelete(Request<ClientDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Client-----");

		response = new Response<ClientDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Client> items = new ArrayList<Client>();

			for (ClientDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la client existe
				Client existingEntity = null;
				existingEntity = clientRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("client -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// client
				List<Client> listOfClient = clientRepository.findByTypeClientId(existingEntity.getId(), false);
				if (listOfClient != null && !listOfClient.isEmpty()){
					Request<ClientDto> deleteRequest = new Request<ClientDto>();
					deleteRequest.setDatas(ClientTransformer.INSTANCE.toDtos(listOfClient));
					deleteRequest.setUser(request.getUser());
					Response<ClientDto> deleteResponse = clientBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// vehicule
				List<Vehicule> listOfVehicule = vehiculeRepository.findByClientId(existingEntity.getId(), false);
				if (listOfVehicule != null && !listOfVehicule.isEmpty()){
					Request<VehiculeDto> deleteRequest = new Request<VehiculeDto>();
					deleteRequest.setDatas(VehiculeTransformer.INSTANCE.toDtos(listOfVehicule));
					deleteRequest.setUser(request.getUser());
					Response<VehiculeDto> deleteResponse = vehiculeBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				clientRepository.save((Iterable<Client>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Client-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get Client by using ClientDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<ClientDto> getByCriteria(Request<ClientDto> request, Locale locale) {
		slf4jLogger.info("----begin get Client-----");

		response = new Response<ClientDto>();

		try {
			List<Client> items = null;
			items = clientRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<ClientDto> itemsDto = new ArrayList<ClientDto>();
				for (Client entity : items) {
					ClientDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(clientRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("client", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Client-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ClientDto by using Client as object.
	 *
	 * @param entity, locale
	 * @return ClientDto
	 *
	 */
	private ClientDto getFullInfos(Client entity, Integer size, Locale locale) throws Exception {
		ClientDto dto = ClientTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}
		
		
		List<Vehicule> vehicules = vehiculeRepository.findByClientId(dto.getId(), false);
		if (Utilities.isNotEmpty(vehicules)) {
			dto.setDatasVehicule(VehiculeTransformer.INSTANCE.toDtos(vehicules));
		}

		return dto;
	}
}
