package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._BonSortieRepository;

/**
 * Repository : BonSortie.
 */
@Repository
public interface BonSortieRepository extends JpaRepository<BonSortie, Integer>, _BonSortieRepository {
	/**
	 * Finds BonSortie by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object BonSortie whose id is equals to the given id. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.id = :id and e.isDeleted = :isDeleted")
	BonSortie findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds BonSortie by using okReceptionnaire as a search criteria.
	 *
	 * @param okReceptionnaire
	 * @return An Object BonSortie whose okReceptionnaire is equals to the given okReceptionnaire. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.okReceptionnaire = :okReceptionnaire and e.isDeleted = :isDeleted")
	List<BonSortie> findByOkReceptionnaire(@Param("okReceptionnaire")Boolean okReceptionnaire, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonSortie by using okStarMotors as a search criteria.
	 *
	 * @param okStarMotors
	 * @return An Object BonSortie whose okStarMotors is equals to the given okStarMotors. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.okStarMotors = :okStarMotors and e.isDeleted = :isDeleted")
	List<BonSortie> findByOkStarMotors(@Param("okStarMotors")Boolean okStarMotors, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonSortie by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object BonSortie whose isDeleted is equals to the given isDeleted. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.isDeleted = :isDeleted")
	List<BonSortie> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonSortie by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object BonSortie whose createdAt is equals to the given createdAt. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<BonSortie> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonSortie by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object BonSortie whose updatedAt is equals to the given updatedAt. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<BonSortie> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonSortie by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object BonSortie whose deletedAt is equals to the given deletedAt. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<BonSortie> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonSortie by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object BonSortie whose createdBy is equals to the given createdBy. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<BonSortie> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonSortie by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object BonSortie whose updatedBy is equals to the given updatedBy. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<BonSortie> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds BonSortie by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object BonSortie whose deletedBy is equals to the given deletedBy. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<BonSortie> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds BonSortie by using userId as a search criteria.
	 *
	 * @param userId
	 * @return A list of Object BonSortie whose userId is equals to the given userId. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.user.id = :userId and e.isDeleted = :isDeleted")
	List<BonSortie> findByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one BonSortie by using userId as a search criteria.
   *
   * @param userId
   * @return An Object BonSortie whose userId is equals to the given userId. If
   *         no BonSortie is found, this method returns null.
   */
  @Query("select e from BonSortie e where e.user.id = :userId and e.isDeleted = :isDeleted")
  BonSortie findBonSortieByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds BonSortie by using ficheReceptionId as a search criteria.
	 *
	 * @param ficheReceptionId
	 * @return A list of Object BonSortie whose ficheReceptionId is equals to the given ficheReceptionId. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
	List<BonSortie> findByFicheReceptionId(@Param("ficheReceptionId")Integer ficheReceptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one BonSortie by using ficheReceptionId as a search criteria.
   *
   * @param ficheReceptionId
   * @return An Object BonSortie whose ficheReceptionId is equals to the given ficheReceptionId. If
   *         no BonSortie is found, this method returns null.
   */
  @Query("select e from BonSortie e where e.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
  BonSortie findBonSortieByFicheReceptionId(@Param("ficheReceptionId")Integer ficheReceptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds BonSortie by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object BonSortie whose etatId is equals to the given etatId. If
	 *         no BonSortie is found, this method returns null.
	 */
	@Query("select e from BonSortie e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
	List<BonSortie> findByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one BonSortie by using etatId as a search criteria.
   *
   * @param etatId
   * @return An Object BonSortie whose etatId is equals to the given etatId. If
   *         no BonSortie is found, this method returns null.
   */
  @Query("select e from BonSortie e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
  BonSortie findBonSortieByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of BonSortie by using bonSortieDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of BonSortie
	 * @throws DataAccessException,ParseException
	 */
	public default List<BonSortie> getByCriteria(Request<BonSortieDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from BonSortie e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<BonSortie> query = em.createQuery(req, BonSortie.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of BonSortie by using bonSortieDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of BonSortie
	 *
	 */
	public default Long count(Request<BonSortieDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from BonSortie e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<BonSortieDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		BonSortieDto dto = request.getData() != null ? request.getData() : new BonSortieDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (BonSortieDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(BonSortieDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getOkReceptionnaire()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("okReceptionnaire", dto.getOkReceptionnaire(), "e.okReceptionnaire", "Boolean", dto.getOkReceptionnaireParam(), param, index, locale));
			}
			if (dto.getOkStarMotors()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("okStarMotors", dto.getOkStarMotors(), "e.okStarMotors", "Boolean", dto.getOkStarMotorsParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getUserId()!= null && dto.getUserId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
			}
			if (dto.getFicheReceptionId()!= null && dto.getFicheReceptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ficheReceptionId", dto.getFicheReceptionId(), "e.ficheReception.id", "Integer", dto.getFicheReceptionIdParam(), param, index, locale));
			}
			if (dto.getEtatId()!= null && dto.getEtatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatId", dto.getEtatId(), "e.etat.id", "Integer", dto.getEtatIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenom", dto.getUserPrenom(), "e.user.prenom", "String", dto.getUserPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatLibelle", dto.getEtatLibelle(), "e.etat.libelle", "String", dto.getEtatLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
