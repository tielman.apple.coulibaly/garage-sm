package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._OrdreReparationRepository;

/**
 * Repository : OrdreReparation.
 */
@Repository
public interface OrdreReparationRepository extends JpaRepository<OrdreReparation, Integer>, _OrdreReparationRepository {
	/**
	 * Finds OrdreReparation by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object OrdreReparation whose id is equals to the given id. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.id = :id and e.isDeleted = :isDeleted")
	OrdreReparation findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds OrdreReparation by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object OrdreReparation whose isDeleted is equals to the given isDeleted. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.isDeleted = :isDeleted")
	List<OrdreReparation> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object OrdreReparation whose createdAt is equals to the given createdAt. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object OrdreReparation whose updatedAt is equals to the given updatedAt. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object OrdreReparation whose deletedAt is equals to the given deletedAt. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object OrdreReparation whose createdBy is equals to the given createdBy. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object OrdreReparation whose updatedBy is equals to the given updatedBy. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object OrdreReparation whose deletedBy is equals to the given deletedBy. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using isValidedByChefAtelierOrdre as a search criteria.
	 *
	 * @param isValidedByChefAtelierOrdre
	 * @return An Object OrdreReparation whose isValidedByChefAtelierOrdre is equals to the given isValidedByChefAtelierOrdre. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.isValidedByChefAtelierOrdre = :isValidedByChefAtelierOrdre and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByIsValidedByChefAtelierOrdre(@Param("isValidedByChefAtelierOrdre")Boolean isValidedByChefAtelierOrdre, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using isValidedByChefAtelierTravaux as a search criteria.
	 *
	 * @param isValidedByChefAtelierTravaux
	 * @return An Object OrdreReparation whose isValidedByChefAtelierTravaux is equals to the given isValidedByChefAtelierTravaux. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.isValidedByChefAtelierTravaux = :isValidedByChefAtelierTravaux and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByIsValidedByChefAtelierTravaux(@Param("isValidedByChefAtelierTravaux")Boolean isValidedByChefAtelierTravaux, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using isValidedByReception as a search criteria.
	 *
	 * @param isValidedByReception
	 * @return An Object OrdreReparation whose isValidedByReception is equals to the given isValidedByReception. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.isValidedByReception = :isValidedByReception and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByIsValidedByReception(@Param("isValidedByReception")Boolean isValidedByReception, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using isValidedBySuperviseurOrdre as a search criteria.
	 *
	 * @param isValidedBySuperviseurOrdre
	 * @return An Object OrdreReparation whose isValidedBySuperviseurOrdre is equals to the given isValidedBySuperviseurOrdre. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.isValidedBySuperviseurOrdre = :isValidedBySuperviseurOrdre and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByIsValidedBySuperviseurOrdre(@Param("isValidedBySuperviseurOrdre")Boolean isValidedBySuperviseurOrdre, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using isValidedBySuperviseurTravaux as a search criteria.
	 *
	 * @param isValidedBySuperviseurTravaux
	 * @return An Object OrdreReparation whose isValidedBySuperviseurTravaux is equals to the given isValidedBySuperviseurTravaux. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.isValidedBySuperviseurTravaux = :isValidedBySuperviseurTravaux and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByIsValidedBySuperviseurTravaux(@Param("isValidedBySuperviseurTravaux")Boolean isValidedBySuperviseurTravaux, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using observationsChefAtelier as a search criteria.
	 *
	 * @param observationsChefAtelier
	 * @return An Object OrdreReparation whose observationsChefAtelier is equals to the given observationsChefAtelier. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.observationsChefAtelier = :observationsChefAtelier and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByObservationsChefAtelier(@Param("observationsChefAtelier")String observationsChefAtelier, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using observationsSuperviseur as a search criteria.
	 *
	 * @param observationsSuperviseur
	 * @return An Object OrdreReparation whose observationsSuperviseur is equals to the given observationsSuperviseur. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.observationsSuperviseur = :observationsSuperviseur and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByObservationsSuperviseur(@Param("observationsSuperviseur")String observationsSuperviseur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using dateValidationReception as a search criteria.
	 *
	 * @param dateValidationReception
	 * @return An Object OrdreReparation whose dateValidationReception is equals to the given dateValidationReception. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.dateValidationReception = :dateValidationReception and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByDateValidationReception(@Param("dateValidationReception")Date dateValidationReception, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using dateValidationOrdreChefAtelier as a search criteria.
	 *
	 * @param dateValidationOrdreChefAtelier
	 * @return An Object OrdreReparation whose dateValidationOrdreChefAtelier is equals to the given dateValidationOrdreChefAtelier. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.dateValidationOrdreChefAtelier = :dateValidationOrdreChefAtelier and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByDateValidationOrdreChefAtelier(@Param("dateValidationOrdreChefAtelier")Date dateValidationOrdreChefAtelier, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using dateValidationTravauxChefAtelier as a search criteria.
	 *
	 * @param dateValidationTravauxChefAtelier
	 * @return An Object OrdreReparation whose dateValidationTravauxChefAtelier is equals to the given dateValidationTravauxChefAtelier. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.dateValidationTravauxChefAtelier = :dateValidationTravauxChefAtelier and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByDateValidationTravauxChefAtelier(@Param("dateValidationTravauxChefAtelier")Date dateValidationTravauxChefAtelier, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using dateValidationOrdreSuperviseur as a search criteria.
	 *
	 * @param dateValidationOrdreSuperviseur
	 * @return An Object OrdreReparation whose dateValidationOrdreSuperviseur is equals to the given dateValidationOrdreSuperviseur. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.dateValidationOrdreSuperviseur = :dateValidationOrdreSuperviseur and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByDateValidationOrdreSuperviseur(@Param("dateValidationOrdreSuperviseur")Date dateValidationOrdreSuperviseur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds OrdreReparation by using dateValidationTravauxSuperviseur as a search criteria.
	 *
	 * @param dateValidationTravauxSuperviseur
	 * @return An Object OrdreReparation whose dateValidationTravauxSuperviseur is equals to the given dateValidationTravauxSuperviseur. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.dateValidationTravauxSuperviseur = :dateValidationTravauxSuperviseur and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByDateValidationTravauxSuperviseur(@Param("dateValidationTravauxSuperviseur")Date dateValidationTravauxSuperviseur, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds OrdreReparation by using userSuperviseurId as a search criteria.
	 *
	 * @param userSuperviseurId
	 * @return A list of Object OrdreReparation whose userSuperviseurId is equals to the given userSuperviseurId. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.userSuperviseur.id = :userSuperviseurId and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByUserSuperviseurId(@Param("userSuperviseurId")Integer userSuperviseurId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one OrdreReparation by using userSuperviseurId as a search criteria.
   *
   * @param userSuperviseurId
   * @return An Object OrdreReparation whose userSuperviseurId is equals to the given userSuperviseurId. If
   *         no OrdreReparation is found, this method returns null.
   */
  @Query("select e from OrdreReparation e where e.userSuperviseur.id = :userSuperviseurId and e.isDeleted = :isDeleted")
  OrdreReparation findOrdreReparationByUserSuperviseurId(@Param("userSuperviseurId")Integer userSuperviseurId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds OrdreReparation by using typeOrdreReparationId as a search criteria.
	 *
	 * @param typeOrdreReparationId
	 * @return A list of Object OrdreReparation whose typeOrdreReparationId is equals to the given typeOrdreReparationId. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.typeOrdreReparation.id = :typeOrdreReparationId and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByTypeOrdreReparationId(@Param("typeOrdreReparationId")Integer typeOrdreReparationId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one OrdreReparation by using typeOrdreReparationId as a search criteria.
   *
   * @param typeOrdreReparationId
   * @return An Object OrdreReparation whose typeOrdreReparationId is equals to the given typeOrdreReparationId. If
   *         no OrdreReparation is found, this method returns null.
   */
  @Query("select e from OrdreReparation e where e.typeOrdreReparation.id = :typeOrdreReparationId and e.isDeleted = :isDeleted")
  OrdreReparation findOrdreReparationByTypeOrdreReparationId(@Param("typeOrdreReparationId")Integer typeOrdreReparationId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds OrdreReparation by using etatOrdreId as a search criteria.
	 *
	 * @param etatOrdreId
	 * @return A list of Object OrdreReparation whose etatOrdreId is equals to the given etatOrdreId. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.etatOrdre.id = :etatOrdreId and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByEtatOrdreId(@Param("etatOrdreId")Integer etatOrdreId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one OrdreReparation by using etatOrdreId as a search criteria.
   *
   * @param etatOrdreId
   * @return An Object OrdreReparation whose etatOrdreId is equals to the given etatOrdreId. If
   *         no OrdreReparation is found, this method returns null.
   */
  @Query("select e from OrdreReparation e where e.etatOrdre.id = :etatOrdreId and e.isDeleted = :isDeleted")
  OrdreReparation findOrdreReparationByEtatOrdreId(@Param("etatOrdreId")Integer etatOrdreId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds OrdreReparation by using ficheReceptionId as a search criteria.
	 *
	 * @param ficheReceptionId
	 * @return A list of Object OrdreReparation whose ficheReceptionId is equals to the given ficheReceptionId. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByFicheReceptionId(@Param("ficheReceptionId")Integer ficheReceptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one OrdreReparation by using ficheReceptionId as a search criteria.
   *
   * @param ficheReceptionId
   * @return An Object OrdreReparation whose ficheReceptionId is equals to the given ficheReceptionId. If
   *         no OrdreReparation is found, this method returns null.
   */
  @Query("select e from OrdreReparation e where e.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
  OrdreReparation findOrdreReparationByFicheReceptionId(@Param("ficheReceptionId")Integer ficheReceptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds OrdreReparation by using etatTravauxId as a search criteria.
	 *
	 * @param etatTravauxId
	 * @return A list of Object OrdreReparation whose etatTravauxId is equals to the given etatTravauxId. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.etatTravaux.id = :etatTravauxId and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByEtatTravauxId(@Param("etatTravauxId")Integer etatTravauxId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one OrdreReparation by using etatTravauxId as a search criteria.
   *
   * @param etatTravauxId
   * @return An Object OrdreReparation whose etatTravauxId is equals to the given etatTravauxId. If
   *         no OrdreReparation is found, this method returns null.
   */
  @Query("select e from OrdreReparation e where e.etatTravaux.id = :etatTravauxId and e.isDeleted = :isDeleted")
  OrdreReparation findOrdreReparationByEtatTravauxId(@Param("etatTravauxId")Integer etatTravauxId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds OrdreReparation by using userChefAtelierId as a search criteria.
	 *
	 * @param userChefAtelierId
	 * @return A list of Object OrdreReparation whose userChefAtelierId is equals to the given userChefAtelierId. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.userChefAtelier.id = :userChefAtelierId and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByUserChefAtelierId(@Param("userChefAtelierId")Integer userChefAtelierId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one OrdreReparation by using userChefAtelierId as a search criteria.
   *
   * @param userChefAtelierId
   * @return An Object OrdreReparation whose userChefAtelierId is equals to the given userChefAtelierId. If
   *         no OrdreReparation is found, this method returns null.
   */
  @Query("select e from OrdreReparation e where e.userChefAtelier.id = :userChefAtelierId and e.isDeleted = :isDeleted")
  OrdreReparation findOrdreReparationByUserChefAtelierId(@Param("userChefAtelierId")Integer userChefAtelierId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds OrdreReparation by using userReceptionId as a search criteria.
	 *
	 * @param userReceptionId
	 * @return A list of Object OrdreReparation whose userReceptionId is equals to the given userReceptionId. If
	 *         no OrdreReparation is found, this method returns null.
	 */
	@Query("select e from OrdreReparation e where e.userReception.id = :userReceptionId and e.isDeleted = :isDeleted")
	List<OrdreReparation> findByUserReceptionId(@Param("userReceptionId")Integer userReceptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one OrdreReparation by using userReceptionId as a search criteria.
   *
   * @param userReceptionId
   * @return An Object OrdreReparation whose userReceptionId is equals to the given userReceptionId. If
   *         no OrdreReparation is found, this method returns null.
   */
  @Query("select e from OrdreReparation e where e.userReception.id = :userReceptionId and e.isDeleted = :isDeleted")
  OrdreReparation findOrdreReparationByUserReceptionId(@Param("userReceptionId")Integer userReceptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of OrdreReparation by using ordreReparationDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of OrdreReparation
	 * @throws DataAccessException,ParseException
	 */
	public default List<OrdreReparation> getByCriteria(Request<OrdreReparationDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from OrdreReparation e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<OrdreReparation> query = em.createQuery(req, OrdreReparation.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of OrdreReparation by using ordreReparationDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of OrdreReparation
	 *
	 */
	public default Long count(Request<OrdreReparationDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from OrdreReparation e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<OrdreReparationDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		OrdreReparationDto dto = request.getData() != null ? request.getData() : new OrdreReparationDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (OrdreReparationDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(OrdreReparationDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsValidedByChefAtelierOrdre()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByChefAtelierOrdre", dto.getIsValidedByChefAtelierOrdre(), "e.isValidedByChefAtelierOrdre", "Boolean", dto.getIsValidedByChefAtelierOrdreParam(), param, index, locale));
			}
			if (dto.getIsValidedByChefAtelierTravaux()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByChefAtelierTravaux", dto.getIsValidedByChefAtelierTravaux(), "e.isValidedByChefAtelierTravaux", "Boolean", dto.getIsValidedByChefAtelierTravauxParam(), param, index, locale));
			}
			if (dto.getIsValidedByReception()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByReception", dto.getIsValidedByReception(), "e.isValidedByReception", "Boolean", dto.getIsValidedByReceptionParam(), param, index, locale));
			}
			if (dto.getIsValidedBySuperviseurOrdre()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedBySuperviseurOrdre", dto.getIsValidedBySuperviseurOrdre(), "e.isValidedBySuperviseurOrdre", "Boolean", dto.getIsValidedBySuperviseurOrdreParam(), param, index, locale));
			}
			if (dto.getIsValidedBySuperviseurTravaux()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedBySuperviseurTravaux", dto.getIsValidedBySuperviseurTravaux(), "e.isValidedBySuperviseurTravaux", "Boolean", dto.getIsValidedBySuperviseurTravauxParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getObservationsChefAtelier())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("observationsChefAtelier", dto.getObservationsChefAtelier(), "e.observationsChefAtelier", "String", dto.getObservationsChefAtelierParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getObservationsSuperviseur())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("observationsSuperviseur", dto.getObservationsSuperviseur(), "e.observationsSuperviseur", "String", dto.getObservationsSuperviseurParam(), param, index, locale));
			}
			if (dto.getDateValidationReception()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateValidationReception", dto.getDateValidationReception(), "e.dateValidationReception", "Date", dto.getDateValidationReceptionParam(), param, index, locale));
			}
			if (dto.getDateValidationOrdreChefAtelier()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateValidationOrdreChefAtelier", dto.getDateValidationOrdreChefAtelier(), "e.dateValidationOrdreChefAtelier", "Date", dto.getDateValidationOrdreChefAtelierParam(), param, index, locale));
			}
			if (dto.getDateValidationTravauxChefAtelier()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateValidationTravauxChefAtelier", dto.getDateValidationTravauxChefAtelier(), "e.dateValidationTravauxChefAtelier", "Date", dto.getDateValidationTravauxChefAtelierParam(), param, index, locale));
			}
			if (dto.getDateValidationOrdreSuperviseur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateValidationOrdreSuperviseur", dto.getDateValidationOrdreSuperviseur(), "e.dateValidationOrdreSuperviseur", "Date", dto.getDateValidationOrdreSuperviseurParam(), param, index, locale));
			}
			if (dto.getDateValidationTravauxSuperviseur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateValidationTravauxSuperviseur", dto.getDateValidationTravauxSuperviseur(), "e.dateValidationTravauxSuperviseur", "Date", dto.getDateValidationTravauxSuperviseurParam(), param, index, locale));
			}
			if (dto.getUserSuperviseurId()!= null && dto.getUserSuperviseurId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userSuperviseurId", dto.getUserSuperviseurId(), "e.userSuperviseur.id", "Integer", dto.getUserSuperviseurIdParam(), param, index, locale));
			}
			if (dto.getTypeOrdreReparationId()!= null && dto.getTypeOrdreReparationId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeOrdreReparationId", dto.getTypeOrdreReparationId(), "e.typeOrdreReparation.id", "Integer", dto.getTypeOrdreReparationIdParam(), param, index, locale));
			}
			if (dto.getEtatOrdreId()!= null && dto.getEtatOrdreId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatOrdreId", dto.getEtatOrdreId(), "e.etatOrdre.id", "Integer", dto.getEtatOrdreIdParam(), param, index, locale));
			}
			if (dto.getFicheReceptionId()!= null && dto.getFicheReceptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ficheReceptionId", dto.getFicheReceptionId(), "e.ficheReception.id", "Integer", dto.getFicheReceptionIdParam(), param, index, locale));
			}
			if (dto.getEtatTravauxId()!= null && dto.getEtatTravauxId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatTravauxId", dto.getEtatTravauxId(), "e.etatTravaux.id", "Integer", dto.getEtatTravauxIdParam(), param, index, locale));
			}
			if (dto.getUserChefAtelierId()!= null && dto.getUserChefAtelierId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierId", dto.getUserChefAtelierId(), "e.userChefAtelier.id", "Integer", dto.getUserChefAtelierIdParam(), param, index, locale));
			}
			if (dto.getUserReceptionId()!= null && dto.getUserReceptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userReceptionId", dto.getUserReceptionId(), "e.userReception.id", "Integer", dto.getUserReceptionIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserSuperviseurNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userSuperviseurNom", dto.getUserSuperviseurNom(), "e.userSuperviseur.nom", "String", dto.getUserSuperviseurNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserSuperviseurPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userSuperviseurPrenom", dto.getUserSuperviseurPrenom(), "e.userSuperviseur.prenom", "String", dto.getUserSuperviseurPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserSuperviseurLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userSuperviseurLogin", dto.getUserSuperviseurLogin(), "e.userSuperviseur.login", "String", dto.getUserSuperviseurLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeOrdreReparationLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeOrdreReparationLibelle", dto.getTypeOrdreReparationLibelle(), "e.typeOrdreReparation.libelle", "String", dto.getTypeOrdreReparationLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatOrdreCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatOrdreCode", dto.getEtatOrdreCode(), "e.etatOrdre.code", "String", dto.getEtatOrdreCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatOrdreLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatOrdreLibelle", dto.getEtatOrdreLibelle(), "e.etatOrdre.libelle", "String", dto.getEtatOrdreLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatTravauxCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatTravauxCode", dto.getEtatTravauxCode(), "e.etatTravaux.code", "String", dto.getEtatTravauxCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatTravauxLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatTravauxLibelle", dto.getEtatTravauxLibelle(), "e.etatTravaux.libelle", "String", dto.getEtatTravauxLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierNom", dto.getUserChefAtelierNom(), "e.userChefAtelier.nom", "String", dto.getUserChefAtelierNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierPrenom", dto.getUserChefAtelierPrenom(), "e.userChefAtelier.prenom", "String", dto.getUserChefAtelierPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierLogin", dto.getUserChefAtelierLogin(), "e.userChefAtelier.login", "String", dto.getUserChefAtelierLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserReceptionNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userReceptionNom", dto.getUserReceptionNom(), "e.userReception.nom", "String", dto.getUserReceptionNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserReceptionPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userReceptionPrenom", dto.getUserReceptionPrenom(), "e.userReception.prenom", "String", dto.getUserReceptionPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserReceptionLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userReceptionLogin", dto.getUserReceptionLogin(), "e.userReception.login", "String", dto.getUserReceptionLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
