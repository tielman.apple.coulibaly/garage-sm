package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._FactureRepository;

/**
 * Repository : Facture.
 */
@Repository
public interface FactureRepository extends JpaRepository<Facture, Integer>, _FactureRepository {
	/**
	 * Finds Facture by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Facture whose id is equals to the given id. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.id = :id and e.isDeleted = :isDeleted")
	Facture findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Facture by using isValidedByChefAtelier as a search criteria.
	 *
	 * @param isValidedByChefAtelier
	 * @return An Object Facture whose isValidedByChefAtelier is equals to the given isValidedByChefAtelier. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.isValidedByChefAtelier = :isValidedByChefAtelier and e.isDeleted = :isDeleted")
	List<Facture> findByIsValidedByChefAtelier(@Param("isValidedByChefAtelier")Boolean isValidedByChefAtelier, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using isValidedByResponsableTechnique as a search criteria.
	 *
	 * @param isValidedByResponsableTechnique
	 * @return An Object Facture whose isValidedByResponsableTechnique is equals to the given isValidedByResponsableTechnique. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.isValidedByResponsableTechnique = :isValidedByResponsableTechnique and e.isDeleted = :isDeleted")
	List<Facture> findByIsValidedByResponsableTechnique(@Param("isValidedByResponsableTechnique")Boolean isValidedByResponsableTechnique, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using isValidedByDirection as a search criteria.
	 *
	 * @param isValidedByDirection
	 * @return An Object Facture whose isValidedByDirection is equals to the given isValidedByDirection. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.isValidedByDirection = :isValidedByDirection and e.isDeleted = :isDeleted")
	List<Facture> findByIsValidedByDirection(@Param("isValidedByDirection")Boolean isValidedByDirection, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Facture whose isDeleted is equals to the given isDeleted. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.isDeleted = :isDeleted")
	List<Facture> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Facture whose createdAt is equals to the given createdAt. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Facture> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Facture whose updatedAt is equals to the given updatedAt. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Facture> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Facture whose deletedAt is equals to the given deletedAt. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Facture> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Facture whose createdBy is equals to the given createdBy. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Facture> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Facture whose updatedBy is equals to the given updatedBy. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Facture> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Facture whose deletedBy is equals to the given deletedBy. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Facture> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Facture by using userDirectionId as a search criteria.
	 *
	 * @param userDirectionId
	 * @return A list of Object Facture whose userDirectionId is equals to the given userDirectionId. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.userDirection.id = :userDirectionId and e.isDeleted = :isDeleted")
	List<Facture> findByUserDirectionId(@Param("userDirectionId")Integer userDirectionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Facture by using userDirectionId as a search criteria.
   *
   * @param userDirectionId
   * @return An Object Facture whose userDirectionId is equals to the given userDirectionId. If
   *         no Facture is found, this method returns null.
   */
  @Query("select e from Facture e where e.userDirection.id = :userDirectionId and e.isDeleted = :isDeleted")
  Facture findFactureByUserDirectionId(@Param("userDirectionId")Integer userDirectionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Facture by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object Facture whose etatId is equals to the given etatId. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
	List<Facture> findByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Facture by using etatId as a search criteria.
   *
   * @param etatId
   * @return An Object Facture whose etatId is equals to the given etatId. If
   *         no Facture is found, this method returns null.
   */
  @Query("select e from Facture e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
  Facture findFactureByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Facture by using userChefAtelierId as a search criteria.
	 *
	 * @param userChefAtelierId
	 * @return A list of Object Facture whose userChefAtelierId is equals to the given userChefAtelierId. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.userChefAtelier.id = :userChefAtelierId and e.isDeleted = :isDeleted")
	List<Facture> findByUserChefAtelierId(@Param("userChefAtelierId")Integer userChefAtelierId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Facture by using userChefAtelierId as a search criteria.
   *
   * @param userChefAtelierId
   * @return An Object Facture whose userChefAtelierId is equals to the given userChefAtelierId. If
   *         no Facture is found, this method returns null.
   */
  @Query("select e from Facture e where e.userChefAtelier.id = :userChefAtelierId and e.isDeleted = :isDeleted")
  Facture findFactureByUserChefAtelierId(@Param("userChefAtelierId")Integer userChefAtelierId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Facture by using userResponsableTechniqueId as a search criteria.
	 *
	 * @param userResponsableTechniqueId
	 * @return A list of Object Facture whose userResponsableTechniqueId is equals to the given userResponsableTechniqueId. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.userResponsableTechnique.id = :userResponsableTechniqueId and e.isDeleted = :isDeleted")
	List<Facture> findByUserResponsableTechniqueId(@Param("userResponsableTechniqueId")Integer userResponsableTechniqueId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Facture by using userResponsableTechniqueId as a search criteria.
   *
   * @param userResponsableTechniqueId
   * @return An Object Facture whose userResponsableTechniqueId is equals to the given userResponsableTechniqueId. If
   *         no Facture is found, this method returns null.
   */
  @Query("select e from Facture e where e.userResponsableTechnique.id = :userResponsableTechniqueId and e.isDeleted = :isDeleted")
  Facture findFactureByUserResponsableTechniqueId(@Param("userResponsableTechniqueId")Integer userResponsableTechniqueId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Facture by using ficheReceptionId as a search criteria.
	 *
	 * @param ficheReceptionId
	 * @return A list of Object Facture whose ficheReceptionId is equals to the given ficheReceptionId. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
	List<Facture> findByFicheReceptionId(@Param("ficheReceptionId")Integer ficheReceptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Facture by using ficheReceptionId as a search criteria.
   *
   * @param ficheReceptionId
   * @return An Object Facture whose ficheReceptionId is equals to the given ficheReceptionId. If
   *         no Facture is found, this method returns null.
   */
  @Query("select e from Facture e where e.ficheReception.id = :ficheReceptionId and e.isDeleted = :isDeleted")
  Facture findFactureByFicheReceptionId(@Param("ficheReceptionId")Integer ficheReceptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Facture by using factureDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Facture
	 * @throws DataAccessException,ParseException
	 */
	public default List<Facture> getByCriteria(Request<FactureDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Facture e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Facture> query = em.createQuery(req, Facture.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Facture by using factureDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Facture
	 *
	 */
	public default Long count(Request<FactureDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Facture e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<FactureDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		FactureDto dto = request.getData() != null ? request.getData() : new FactureDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (FactureDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(FactureDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getIsValidedByChefAtelier()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByChefAtelier", dto.getIsValidedByChefAtelier(), "e.isValidedByChefAtelier", "Boolean", dto.getIsValidedByChefAtelierParam(), param, index, locale));
			}
			if (dto.getIsValidedByResponsableTechnique()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByResponsableTechnique", dto.getIsValidedByResponsableTechnique(), "e.isValidedByResponsableTechnique", "Boolean", dto.getIsValidedByResponsableTechniqueParam(), param, index, locale));
			}
			if (dto.getIsValidedByDirection()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByDirection", dto.getIsValidedByDirection(), "e.isValidedByDirection", "Boolean", dto.getIsValidedByDirectionParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getUserDirectionId()!= null && dto.getUserDirectionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userDirectionId", dto.getUserDirectionId(), "e.userDirection.id", "Integer", dto.getUserDirectionIdParam(), param, index, locale));
			}
			if (dto.getEtatId()!= null && dto.getEtatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatId", dto.getEtatId(), "e.etat.id", "Integer", dto.getEtatIdParam(), param, index, locale));
			}
			if (dto.getUserChefAtelierId()!= null && dto.getUserChefAtelierId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierId", dto.getUserChefAtelierId(), "e.userChefAtelier.id", "Integer", dto.getUserChefAtelierIdParam(), param, index, locale));
			}
			if (dto.getUserResponsableTechniqueId()!= null && dto.getUserResponsableTechniqueId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniqueId", dto.getUserResponsableTechniqueId(), "e.userResponsableTechnique.id", "Integer", dto.getUserResponsableTechniqueIdParam(), param, index, locale));
			}
			if (dto.getFicheReceptionId()!= null && dto.getFicheReceptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ficheReceptionId", dto.getFicheReceptionId(), "e.ficheReception.id", "Integer", dto.getFicheReceptionIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserDirectionNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userDirectionNom", dto.getUserDirectionNom(), "e.userDirection.nom", "String", dto.getUserDirectionNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserDirectionPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userDirectionPrenom", dto.getUserDirectionPrenom(), "e.userDirection.prenom", "String", dto.getUserDirectionPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserDirectionLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userDirectionLogin", dto.getUserDirectionLogin(), "e.userDirection.login", "String", dto.getUserDirectionLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatLibelle", dto.getEtatLibelle(), "e.etat.libelle", "String", dto.getEtatLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierNom", dto.getUserChefAtelierNom(), "e.userChefAtelier.nom", "String", dto.getUserChefAtelierNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierPrenom", dto.getUserChefAtelierPrenom(), "e.userChefAtelier.prenom", "String", dto.getUserChefAtelierPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierLogin", dto.getUserChefAtelierLogin(), "e.userChefAtelier.login", "String", dto.getUserChefAtelierLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableTechniqueNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniqueNom", dto.getUserResponsableTechniqueNom(), "e.userResponsableTechnique.nom", "String", dto.getUserResponsableTechniqueNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableTechniquePrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniquePrenom", dto.getUserResponsableTechniquePrenom(), "e.userResponsableTechnique.prenom", "String", dto.getUserResponsableTechniquePrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableTechniqueLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniqueLogin", dto.getUserResponsableTechniqueLogin(), "e.userResponsableTechnique.login", "String", dto.getUserResponsableTechniqueLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
