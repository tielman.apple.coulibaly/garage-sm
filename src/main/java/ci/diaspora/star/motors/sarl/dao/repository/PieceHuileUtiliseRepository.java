package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._PieceHuileUtiliseRepository;

/**
 * Repository : PieceHuileUtilise.
 */
@Repository
public interface PieceHuileUtiliseRepository extends JpaRepository<PieceHuileUtilise, Integer>, _PieceHuileUtiliseRepository {
	/**
	 * Finds PieceHuileUtilise by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object PieceHuileUtilise whose id is equals to the given id. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.id = :id and e.isDeleted = :isDeleted")
	PieceHuileUtilise findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PieceHuileUtilise by using quantite as a search criteria.
	 *
	 * @param quantite
	 * @return An Object PieceHuileUtilise whose quantite is equals to the given quantite. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.quantite = :quantite and e.isDeleted = :isDeleted")
	List<PieceHuileUtilise> findByQuantite(@Param("quantite")Integer quantite, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceHuileUtilise by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object PieceHuileUtilise whose isDeleted is equals to the given isDeleted. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.isDeleted = :isDeleted")
	List<PieceHuileUtilise> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceHuileUtilise by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object PieceHuileUtilise whose createdAt is equals to the given createdAt. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<PieceHuileUtilise> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceHuileUtilise by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object PieceHuileUtilise whose updatedAt is equals to the given updatedAt. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<PieceHuileUtilise> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceHuileUtilise by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object PieceHuileUtilise whose deletedAt is equals to the given deletedAt. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<PieceHuileUtilise> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceHuileUtilise by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object PieceHuileUtilise whose createdBy is equals to the given createdBy. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<PieceHuileUtilise> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceHuileUtilise by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object PieceHuileUtilise whose updatedBy is equals to the given updatedBy. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<PieceHuileUtilise> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PieceHuileUtilise by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object PieceHuileUtilise whose deletedBy is equals to the given deletedBy. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<PieceHuileUtilise> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PieceHuileUtilise by using designationPieceHuileUtiliseId as a search criteria.
	 *
	 * @param designationPieceHuileUtiliseId
	 * @return A list of Object PieceHuileUtilise whose designationPieceHuileUtiliseId is equals to the given designationPieceHuileUtiliseId. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.designationPieceHuileUtilise.id = :designationPieceHuileUtiliseId and e.isDeleted = :isDeleted")
	List<PieceHuileUtilise> findByDesignationPieceHuileUtiliseId(@Param("designationPieceHuileUtiliseId")Integer designationPieceHuileUtiliseId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PieceHuileUtilise by using designationPieceHuileUtiliseId as a search criteria.
   *
   * @param designationPieceHuileUtiliseId
   * @return An Object PieceHuileUtilise whose designationPieceHuileUtiliseId is equals to the given designationPieceHuileUtiliseId. If
   *         no PieceHuileUtilise is found, this method returns null.
   */
  @Query("select e from PieceHuileUtilise e where e.designationPieceHuileUtilise.id = :designationPieceHuileUtiliseId and e.isDeleted = :isDeleted")
  PieceHuileUtilise findPieceHuileUtiliseByDesignationPieceHuileUtiliseId(@Param("designationPieceHuileUtiliseId")Integer designationPieceHuileUtiliseId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds PieceHuileUtilise by using ordreReparationId as a search criteria.
	 *
	 * @param ordreReparationId
	 * @return A list of Object PieceHuileUtilise whose ordreReparationId is equals to the given ordreReparationId. If
	 *         no PieceHuileUtilise is found, this method returns null.
	 */
	@Query("select e from PieceHuileUtilise e where e.ordreReparation.id = :ordreReparationId and e.isDeleted = :isDeleted")
	List<PieceHuileUtilise> findByOrdreReparationId(@Param("ordreReparationId")Integer ordreReparationId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PieceHuileUtilise by using ordreReparationId as a search criteria.
   *
   * @param ordreReparationId
   * @return An Object PieceHuileUtilise whose ordreReparationId is equals to the given ordreReparationId. If
   *         no PieceHuileUtilise is found, this method returns null.
   */
  @Query("select e from PieceHuileUtilise e where e.ordreReparation.id = :ordreReparationId and e.isDeleted = :isDeleted")
  PieceHuileUtilise findPieceHuileUtiliseByOrdreReparationId(@Param("ordreReparationId")Integer ordreReparationId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of PieceHuileUtilise by using pieceHuileUtiliseDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of PieceHuileUtilise
	 * @throws DataAccessException,ParseException
	 */
	public default List<PieceHuileUtilise> getByCriteria(Request<PieceHuileUtiliseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from PieceHuileUtilise e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<PieceHuileUtilise> query = em.createQuery(req, PieceHuileUtilise.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of PieceHuileUtilise by using pieceHuileUtiliseDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of PieceHuileUtilise
	 *
	 */
	public default Long count(Request<PieceHuileUtiliseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from PieceHuileUtilise e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PieceHuileUtiliseDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		PieceHuileUtiliseDto dto = request.getData() != null ? request.getData() : new PieceHuileUtiliseDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PieceHuileUtiliseDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PieceHuileUtiliseDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getQuantite()!= null && dto.getQuantite() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("quantite", dto.getQuantite(), "e.quantite", "Integer", dto.getQuantiteParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getDesignationPieceHuileUtiliseId()!= null && dto.getDesignationPieceHuileUtiliseId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("designationPieceHuileUtiliseId", dto.getDesignationPieceHuileUtiliseId(), "e.designationPieceHuileUtilise.id", "Integer", dto.getDesignationPieceHuileUtiliseIdParam(), param, index, locale));
			}
			if (dto.getOrdreReparationId()!= null && dto.getOrdreReparationId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ordreReparationId", dto.getOrdreReparationId(), "e.ordreReparation.id", "Integer", dto.getOrdreReparationIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDesignationPieceHuileUtiliseLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("designationPieceHuileUtiliseLibelle", dto.getDesignationPieceHuileUtiliseLibelle(), "e.designationPieceHuileUtilise.libelle", "String", dto.getDesignationPieceHuileUtiliseLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
