

/*
 * Java transformer for entity table fiche_reception_piece_vehicule 
 * Created on 2019-09-09 ( Time 13:30:34 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "fiche_reception_piece_vehicule"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface FicheReceptionPieceVehiculeTransformer {

	FicheReceptionPieceVehiculeTransformer INSTANCE = Mappers.getMapper(FicheReceptionPieceVehiculeTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.pieceVehicule.id", target="pieceVehiculeId"),
		@Mapping(source="entity.pieceVehicule.libelle", target="pieceVehiculeLibelle"),
		@Mapping(source="entity.ficheReception.id", target="ficheReceptionId"),
	})
	FicheReceptionPieceVehiculeDto toDto(FicheReceptionPieceVehicule entity) throws ParseException;

    List<FicheReceptionPieceVehiculeDto> toDtos(List<FicheReceptionPieceVehicule> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.detailAutres", target="detailAutres"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="pieceVehicule", target="pieceVehicule"),
		@Mapping(source="ficheReception", target="ficheReception"),
	})
    FicheReceptionPieceVehicule toEntity(FicheReceptionPieceVehiculeDto dto, PieceVehicule pieceVehicule, FicheReception ficheReception) throws ParseException;

    //List<FicheReceptionPieceVehicule> toEntities(List<FicheReceptionPieceVehiculeDto> dtos) throws ParseException;

}
