package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._FicheControlFinalRepository;

/**
 * Repository : FicheControlFinal.
 */
@Repository
public interface FicheControlFinalRepository extends JpaRepository<FicheControlFinal, Integer>, _FicheControlFinalRepository {
	/**
	 * Finds FicheControlFinal by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object FicheControlFinal whose id is equals to the given id. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.id = :id and e.isDeleted = :isDeleted")
	FicheControlFinal findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds FicheControlFinal by using isValidedByControleurFinal as a search criteria.
	 *
	 * @param isValidedByControleurFinal
	 * @return An Object FicheControlFinal whose isValidedByControleurFinal is equals to the given isValidedByControleurFinal. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.isValidedByControleurFinal = :isValidedByControleurFinal and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByIsValidedByControleurFinal(@Param("isValidedByControleurFinal")Boolean isValidedByControleurFinal, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using isValidedByReception as a search criteria.
	 *
	 * @param isValidedByReception
	 * @return An Object FicheControlFinal whose isValidedByReception is equals to the given isValidedByReception. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.isValidedByReception = :isValidedByReception and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByIsValidedByReception(@Param("isValidedByReception")Boolean isValidedByReception, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using isValidedByFacturation as a search criteria.
	 *
	 * @param isValidedByFacturation
	 * @return An Object FicheControlFinal whose isValidedByFacturation is equals to the given isValidedByFacturation. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.isValidedByFacturation = :isValidedByFacturation and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByIsValidedByFacturation(@Param("isValidedByFacturation")Boolean isValidedByFacturation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using observationsControleurFinal as a search criteria.
	 *
	 * @param observationsControleurFinal
	 * @return An Object FicheControlFinal whose observationsControleurFinal is equals to the given observationsControleurFinal. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.observationsControleurFinal = :observationsControleurFinal and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByObservationsControleurFinal(@Param("observationsControleurFinal")String observationsControleurFinal, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using observationsReception as a search criteria.
	 *
	 * @param observationsReception
	 * @return An Object FicheControlFinal whose observationsReception is equals to the given observationsReception. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.observationsReception = :observationsReception and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByObservationsReception(@Param("observationsReception")String observationsReception, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using observationsFacturation as a search criteria.
	 *
	 * @param observationsFacturation
	 * @return An Object FicheControlFinal whose observationsFacturation is equals to the given observationsFacturation. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.observationsFacturation = :observationsFacturation and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByObservationsFacturation(@Param("observationsFacturation")String observationsFacturation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using dateValidationControleurFinal as a search criteria.
	 *
	 * @param dateValidationControleurFinal
	 * @return An Object FicheControlFinal whose dateValidationControleurFinal is equals to the given dateValidationControleurFinal. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.dateValidationControleurFinal = :dateValidationControleurFinal and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByDateValidationControleurFinal(@Param("dateValidationControleurFinal")Date dateValidationControleurFinal, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using dateValidationReception as a search criteria.
	 *
	 * @param dateValidationReception
	 * @return An Object FicheControlFinal whose dateValidationReception is equals to the given dateValidationReception. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.dateValidationReception = :dateValidationReception and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByDateValidationReception(@Param("dateValidationReception")Date dateValidationReception, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using dateValidationFacturation as a search criteria.
	 *
	 * @param dateValidationFacturation
	 * @return An Object FicheControlFinal whose dateValidationFacturation is equals to the given dateValidationFacturation. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.dateValidationFacturation = :dateValidationFacturation and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByDateValidationFacturation(@Param("dateValidationFacturation")Date dateValidationFacturation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object FicheControlFinal whose isDeleted is equals to the given isDeleted. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object FicheControlFinal whose createdAt is equals to the given createdAt. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object FicheControlFinal whose updatedAt is equals to the given updatedAt. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object FicheControlFinal whose deletedAt is equals to the given deletedAt. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object FicheControlFinal whose createdBy is equals to the given createdBy. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object FicheControlFinal whose updatedBy is equals to the given updatedBy. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds FicheControlFinal by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object FicheControlFinal whose deletedBy is equals to the given deletedBy. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds FicheControlFinal by using userControleurFinalId as a search criteria.
	 *
	 * @param userControleurFinalId
	 * @return A list of Object FicheControlFinal whose userControleurFinalId is equals to the given userControleurFinalId. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.userControleurFinal.id = :userControleurFinalId and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByUserControleurFinalId(@Param("userControleurFinalId")Integer userControleurFinalId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheControlFinal by using userControleurFinalId as a search criteria.
   *
   * @param userControleurFinalId
   * @return An Object FicheControlFinal whose userControleurFinalId is equals to the given userControleurFinalId. If
   *         no FicheControlFinal is found, this method returns null.
   */
  @Query("select e from FicheControlFinal e where e.userControleurFinal.id = :userControleurFinalId and e.isDeleted = :isDeleted")
  FicheControlFinal findFicheControlFinalByUserControleurFinalId(@Param("userControleurFinalId")Integer userControleurFinalId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FicheControlFinal by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object FicheControlFinal whose etatId is equals to the given etatId. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheControlFinal by using etatId as a search criteria.
   *
   * @param etatId
   * @return An Object FicheControlFinal whose etatId is equals to the given etatId. If
   *         no FicheControlFinal is found, this method returns null.
   */
  @Query("select e from FicheControlFinal e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
  FicheControlFinal findFicheControlFinalByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FicheControlFinal by using userFacturationId as a search criteria.
	 *
	 * @param userFacturationId
	 * @return A list of Object FicheControlFinal whose userFacturationId is equals to the given userFacturationId. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.userFacturation.id = :userFacturationId and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByUserFacturationId(@Param("userFacturationId")Integer userFacturationId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheControlFinal by using userFacturationId as a search criteria.
   *
   * @param userFacturationId
   * @return An Object FicheControlFinal whose userFacturationId is equals to the given userFacturationId. If
   *         no FicheControlFinal is found, this method returns null.
   */
  @Query("select e from FicheControlFinal e where e.userFacturation.id = :userFacturationId and e.isDeleted = :isDeleted")
  FicheControlFinal findFicheControlFinalByUserFacturationId(@Param("userFacturationId")Integer userFacturationId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FicheControlFinal by using userReceptionId as a search criteria.
	 *
	 * @param userReceptionId
	 * @return A list of Object FicheControlFinal whose userReceptionId is equals to the given userReceptionId. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.userReception.id = :userReceptionId and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByUserReceptionId(@Param("userReceptionId")Integer userReceptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheControlFinal by using userReceptionId as a search criteria.
   *
   * @param userReceptionId
   * @return An Object FicheControlFinal whose userReceptionId is equals to the given userReceptionId. If
   *         no FicheControlFinal is found, this method returns null.
   */
  @Query("select e from FicheControlFinal e where e.userReception.id = :userReceptionId and e.isDeleted = :isDeleted")
  FicheControlFinal findFicheControlFinalByUserReceptionId(@Param("userReceptionId")Integer userReceptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds FicheControlFinal by using ordreReparationId as a search criteria.
	 *
	 * @param ordreReparationId
	 * @return A list of Object FicheControlFinal whose ordreReparationId is equals to the given ordreReparationId. If
	 *         no FicheControlFinal is found, this method returns null.
	 */
	@Query("select e from FicheControlFinal e where e.ordreReparation.id = :ordreReparationId and e.isDeleted = :isDeleted")
	List<FicheControlFinal> findByOrdreReparationId(@Param("ordreReparationId")Integer ordreReparationId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one FicheControlFinal by using ordreReparationId as a search criteria.
   *
   * @param ordreReparationId
   * @return An Object FicheControlFinal whose ordreReparationId is equals to the given ordreReparationId. If
   *         no FicheControlFinal is found, this method returns null.
   */
  @Query("select e from FicheControlFinal e where e.ordreReparation.id = :ordreReparationId and e.isDeleted = :isDeleted")
  FicheControlFinal findFicheControlFinalByOrdreReparationId(@Param("ordreReparationId")Integer ordreReparationId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of FicheControlFinal by using ficheControlFinalDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of FicheControlFinal
	 * @throws DataAccessException,ParseException
	 */
	public default List<FicheControlFinal> getByCriteria(Request<FicheControlFinalDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from FicheControlFinal e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<FicheControlFinal> query = em.createQuery(req, FicheControlFinal.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of FicheControlFinal by using ficheControlFinalDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of FicheControlFinal
	 *
	 */
	public default Long count(Request<FicheControlFinalDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from FicheControlFinal e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<FicheControlFinalDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		FicheControlFinalDto dto = request.getData() != null ? request.getData() : new FicheControlFinalDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (FicheControlFinalDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(FicheControlFinalDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getIsValidedByControleurFinal()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByControleurFinal", dto.getIsValidedByControleurFinal(), "e.isValidedByControleurFinal", "Boolean", dto.getIsValidedByControleurFinalParam(), param, index, locale));
			}
			if (dto.getIsValidedByReception()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByReception", dto.getIsValidedByReception(), "e.isValidedByReception", "Boolean", dto.getIsValidedByReceptionParam(), param, index, locale));
			}
			if (dto.getIsValidedByFacturation()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByFacturation", dto.getIsValidedByFacturation(), "e.isValidedByFacturation", "Boolean", dto.getIsValidedByFacturationParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getObservationsControleurFinal())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("observationsControleurFinal", dto.getObservationsControleurFinal(), "e.observationsControleurFinal", "String", dto.getObservationsControleurFinalParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getObservationsReception())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("observationsReception", dto.getObservationsReception(), "e.observationsReception", "String", dto.getObservationsReceptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getObservationsFacturation())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("observationsFacturation", dto.getObservationsFacturation(), "e.observationsFacturation", "String", dto.getObservationsFacturationParam(), param, index, locale));
			}
			if (dto.getDateValidationControleurFinal()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateValidationControleurFinal", dto.getDateValidationControleurFinal(), "e.dateValidationControleurFinal", "Date", dto.getDateValidationControleurFinalParam(), param, index, locale));
			}
			if (dto.getDateValidationReception()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateValidationReception", dto.getDateValidationReception(), "e.dateValidationReception", "Date", dto.getDateValidationReceptionParam(), param, index, locale));
			}
			if (dto.getDateValidationFacturation()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateValidationFacturation", dto.getDateValidationFacturation(), "e.dateValidationFacturation", "Date", dto.getDateValidationFacturationParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getUserControleurFinalId()!= null && dto.getUserControleurFinalId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userControleurFinalId", dto.getUserControleurFinalId(), "e.userControleurFinal.id", "Integer", dto.getUserControleurFinalIdParam(), param, index, locale));
			}
			if (dto.getEtatId()!= null && dto.getEtatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatId", dto.getEtatId(), "e.etat.id", "Integer", dto.getEtatIdParam(), param, index, locale));
			}
			if (dto.getUserFacturationId()!= null && dto.getUserFacturationId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userFacturationId", dto.getUserFacturationId(), "e.userFacturation.id", "Integer", dto.getUserFacturationIdParam(), param, index, locale));
			}
			if (dto.getUserReceptionId()!= null && dto.getUserReceptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userReceptionId", dto.getUserReceptionId(), "e.userReception.id", "Integer", dto.getUserReceptionIdParam(), param, index, locale));
			}
			if (dto.getOrdreReparationId()!= null && dto.getOrdreReparationId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ordreReparationId", dto.getOrdreReparationId(), "e.ordreReparation.id", "Integer", dto.getOrdreReparationIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserControleurFinalNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userControleurFinalNom", dto.getUserControleurFinalNom(), "e.userControleurFinal.nom", "String", dto.getUserControleurFinalNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserControleurFinalPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userControleurFinalPrenom", dto.getUserControleurFinalPrenom(), "e.userControleurFinal.prenom", "String", dto.getUserControleurFinalPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserControleurFinalLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userControleurFinalLogin", dto.getUserControleurFinalLogin(), "e.userControleurFinal.login", "String", dto.getUserControleurFinalLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatLibelle", dto.getEtatLibelle(), "e.etat.libelle", "String", dto.getEtatLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserFacturationNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userFacturationNom", dto.getUserFacturationNom(), "e.userFacturation.nom", "String", dto.getUserFacturationNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserFacturationPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userFacturationPrenom", dto.getUserFacturationPrenom(), "e.userFacturation.prenom", "String", dto.getUserFacturationPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserFacturationLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userFacturationLogin", dto.getUserFacturationLogin(), "e.userFacturation.login", "String", dto.getUserFacturationLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserReceptionNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userReceptionNom", dto.getUserReceptionNom(), "e.userReception.nom", "String", dto.getUserReceptionNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserReceptionPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userReceptionPrenom", dto.getUserReceptionPrenom(), "e.userReception.prenom", "String", dto.getUserReceptionPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserReceptionLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userReceptionLogin", dto.getUserReceptionLogin(), "e.userReception.login", "String", dto.getUserReceptionLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
