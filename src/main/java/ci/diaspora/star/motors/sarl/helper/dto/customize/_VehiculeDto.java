
/*
 * Java dto for entity table vehicule 
 * Created on 2019-08-18 ( Time 17:15:17 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.dto.ClientDto;

/**
 * DTO customize for table "vehicule"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _VehiculeDto {
	
	ClientDto dataClient ;

	public ClientDto getDataClient() {
		return dataClient;
	}

	public void setDataClient(ClientDto dataClient) {
		this.dataClient = dataClient;
	}
}
