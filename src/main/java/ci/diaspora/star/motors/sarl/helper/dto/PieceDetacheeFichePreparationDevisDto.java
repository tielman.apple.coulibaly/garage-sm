
/*
 * Java dto for entity table piece_detachee_fiche_preparation_devis
 * Created on 2019-08-18 ( Time 17:15:14 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.dto.customize._PieceDetacheeFichePreparationDevisDto;

/**
 * DTO for table "piece_detachee_fiche_preparation_devis"
 *
 * @author SFL Back-End developper
 */
@JsonInclude(Include.NON_NULL)
public class PieceDetacheeFichePreparationDevisDto extends _PieceDetacheeFichePreparationDevisDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    pieceDetacheeId      ;
	/*
	 * 
	 */
    private Integer    fichePreparationDevisId ;
	/*
	 * 
	 */
    private Integer    fournisseurId        ;
	/*
	 * 
	 */
    private Integer    quantite             ;
	/*
	 * 
	 */
    private Float      prixUnitaire         ;
	/*
	 * 
	 */
    private Float      total                ;
	/*
	 * 
	 */
    private Boolean    isValided            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String pieceDetacheeLibelle;
	private String fournisseurNom;
	private String fournisseurPrenom;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  pieceDetacheeIdParam  ;                     
	private SearchParam<Integer>  fichePreparationDevisIdParam;                     
	private SearchParam<Integer>  fournisseurIdParam    ;                     
	private SearchParam<Integer>  quantiteParam         ;                     
	private SearchParam<Float>    prixUnitaireParam     ;                     
	private SearchParam<Float>    totalParam            ;                     
	private SearchParam<Boolean>  isValidedParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<String>   pieceDetacheeLibelleParam;                     
	private SearchParam<String>   fournisseurNomParam   ;                     
	private SearchParam<String>   fournisseurPrenomParam;                     
    /**
     * Default constructor
     */
    public PieceDetacheeFichePreparationDevisDto()
    {
        super();
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * 
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "pieceDetacheeId" field value
     * @param pieceDetacheeId
     */
	public void setPieceDetacheeId( Integer pieceDetacheeId )
    {
        this.pieceDetacheeId = pieceDetacheeId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getPieceDetacheeId()
    {
        return this.pieceDetacheeId;
    }

    /**
     * Set the "fichePreparationDevisId" field value
     * @param fichePreparationDevisId
     */
	public void setFichePreparationDevisId( Integer fichePreparationDevisId )
    {
        this.fichePreparationDevisId = fichePreparationDevisId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getFichePreparationDevisId()
    {
        return this.fichePreparationDevisId;
    }

    /**
     * Set the "fournisseurId" field value
     * @param fournisseurId
     */
	public void setFournisseurId( Integer fournisseurId )
    {
        this.fournisseurId = fournisseurId ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getFournisseurId()
    {
        return this.fournisseurId;
    }

    /**
     * Set the "quantite" field value
     * @param quantite
     */
	public void setQuantite( Integer quantite )
    {
        this.quantite = quantite ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getQuantite()
    {
        return this.quantite;
    }

    /**
     * Set the "prixUnitaire" field value
     * @param prixUnitaire
     */
	public void setPrixUnitaire( Float prixUnitaire )
    {
        this.prixUnitaire = prixUnitaire ;
    }
    /**
     * 
     * @return the field value
     */
	public Float getPrixUnitaire()
    {
        return this.prixUnitaire;
    }

    /**
     * Set the "total" field value
     * @param total
     */
	public void setTotal( Float total )
    {
        this.total = total ;
    }
    /**
     * 
     * @return the field value
     */
	public Float getTotal()
    {
        return this.total;
    }

    /**
     * Set the "isValided" field value
     * @param isValided
     */
	public void setIsValided( Boolean isValided )
    {
        this.isValided = isValided ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsValided()
    {
        return this.isValided;
    }

    /**
     * Set the "isDeleted" field value
     * @param isDeleted
     */
	public void setIsDeleted( Boolean isDeleted )
    {
        this.isDeleted = isDeleted ;
    }
    /**
     * 
     * @return the field value
     */
	public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    /**
     * Set the "createdAt" field value
     * @param createdAt
     */
	public void setCreatedAt( String createdAt )
    {
        this.createdAt = createdAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getCreatedAt()
    {
        return this.createdAt;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "deletedAt" field value
     * @param deletedAt
     */
	public void setDeletedAt( String deletedAt )
    {
        this.deletedAt = deletedAt ;
    }
    /**
     * 
     * @return the field value
     */
	public String getDeletedAt()
    {
        return this.deletedAt;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deletedBy" field value
     * @param deletedBy
     */
	public void setDeletedBy( Integer deletedBy )
    {
        this.deletedBy = deletedBy ;
    }
    /**
     * 
     * @return the field value
     */
	public Integer getDeletedBy()
    {
        return this.deletedBy;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getPieceDetacheeLibelle()
    {
        return this.pieceDetacheeLibelle;
    }
	public void setPieceDetacheeLibelle(String pieceDetacheeLibelle)
    {
        this.pieceDetacheeLibelle = pieceDetacheeLibelle;
    }

	public String getFournisseurNom()
    {
        return this.fournisseurNom;
    }
	public void setFournisseurNom(String fournisseurNom)
    {
        this.fournisseurNom = fournisseurNom;
    }

	public String getFournisseurPrenom()
    {
        return this.fournisseurPrenom;
    }
	public void setFournisseurPrenom(String fournisseurPrenom)
    {
        this.fournisseurPrenom = fournisseurPrenom;
    }


	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR SEARCH PARAM FIELDS
    //----------------------------------------------------------------------
	/**
     * Get the "idParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getIdParam(){
		return this.idParam;
	}
	/**
     * Set the "idParam" field value
     * @param idParam
     */
    public void setIdParam( SearchParam<Integer> idParam ){
        this.idParam = idParam;
    }

	/**
     * Get the "pieceDetacheeIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getPieceDetacheeIdParam(){
		return this.pieceDetacheeIdParam;
	}
	/**
     * Set the "pieceDetacheeIdParam" field value
     * @param pieceDetacheeIdParam
     */
    public void setPieceDetacheeIdParam( SearchParam<Integer> pieceDetacheeIdParam ){
        this.pieceDetacheeIdParam = pieceDetacheeIdParam;
    }

	/**
     * Get the "fichePreparationDevisIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getFichePreparationDevisIdParam(){
		return this.fichePreparationDevisIdParam;
	}
	/**
     * Set the "fichePreparationDevisIdParam" field value
     * @param fichePreparationDevisIdParam
     */
    public void setFichePreparationDevisIdParam( SearchParam<Integer> fichePreparationDevisIdParam ){
        this.fichePreparationDevisIdParam = fichePreparationDevisIdParam;
    }

	/**
     * Get the "fournisseurIdParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getFournisseurIdParam(){
		return this.fournisseurIdParam;
	}
	/**
     * Set the "fournisseurIdParam" field value
     * @param fournisseurIdParam
     */
    public void setFournisseurIdParam( SearchParam<Integer> fournisseurIdParam ){
        this.fournisseurIdParam = fournisseurIdParam;
    }

	/**
     * Get the "quantiteParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getQuantiteParam(){
		return this.quantiteParam;
	}
	/**
     * Set the "quantiteParam" field value
     * @param quantiteParam
     */
    public void setQuantiteParam( SearchParam<Integer> quantiteParam ){
        this.quantiteParam = quantiteParam;
    }

	/**
     * Get the "prixUnitaireParam" field value
     * @return the field value
     */
	public SearchParam<Float> getPrixUnitaireParam(){
		return this.prixUnitaireParam;
	}
	/**
     * Set the "prixUnitaireParam" field value
     * @param prixUnitaireParam
     */
    public void setPrixUnitaireParam( SearchParam<Float> prixUnitaireParam ){
        this.prixUnitaireParam = prixUnitaireParam;
    }

	/**
     * Get the "totalParam" field value
     * @return the field value
     */
	public SearchParam<Float> getTotalParam(){
		return this.totalParam;
	}
	/**
     * Set the "totalParam" field value
     * @param totalParam
     */
    public void setTotalParam( SearchParam<Float> totalParam ){
        this.totalParam = totalParam;
    }

	/**
     * Get the "isValidedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsValidedParam(){
		return this.isValidedParam;
	}
	/**
     * Set the "isValidedParam" field value
     * @param isValidedParam
     */
    public void setIsValidedParam( SearchParam<Boolean> isValidedParam ){
        this.isValidedParam = isValidedParam;
    }

	/**
     * Get the "isDeletedParam" field value
     * @return the field value
     */
	public SearchParam<Boolean> getIsDeletedParam(){
		return this.isDeletedParam;
	}
	/**
     * Set the "isDeletedParam" field value
     * @param isDeletedParam
     */
    public void setIsDeletedParam( SearchParam<Boolean> isDeletedParam ){
        this.isDeletedParam = isDeletedParam;
    }

	/**
     * Get the "createdAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getCreatedAtParam(){
		return this.createdAtParam;
	}
	/**
     * Set the "createdAtParam" field value
     * @param createdAtParam
     */
    public void setCreatedAtParam( SearchParam<String> createdAtParam ){
        this.createdAtParam = createdAtParam;
    }

	/**
     * Get the "updatedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getUpdatedAtParam(){
		return this.updatedAtParam;
	}
	/**
     * Set the "updatedAtParam" field value
     * @param updatedAtParam
     */
    public void setUpdatedAtParam( SearchParam<String> updatedAtParam ){
        this.updatedAtParam = updatedAtParam;
    }

	/**
     * Get the "deletedAtParam" field value
     * @return the field value
     */
	public SearchParam<String> getDeletedAtParam(){
		return this.deletedAtParam;
	}
	/**
     * Set the "deletedAtParam" field value
     * @param deletedAtParam
     */
    public void setDeletedAtParam( SearchParam<String> deletedAtParam ){
        this.deletedAtParam = deletedAtParam;
    }

	/**
     * Get the "createdByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getCreatedByParam(){
		return this.createdByParam;
	}
	/**
     * Set the "createdByParam" field value
     * @param createdByParam
     */
    public void setCreatedByParam( SearchParam<Integer> createdByParam ){
        this.createdByParam = createdByParam;
    }

	/**
     * Get the "updatedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getUpdatedByParam(){
		return this.updatedByParam;
	}
	/**
     * Set the "updatedByParam" field value
     * @param updatedByParam
     */
    public void setUpdatedByParam( SearchParam<Integer> updatedByParam ){
        this.updatedByParam = updatedByParam;
    }

	/**
     * Get the "deletedByParam" field value
     * @return the field value
     */
	public SearchParam<Integer> getDeletedByParam(){
		return this.deletedByParam;
	}
	/**
     * Set the "deletedByParam" field value
     * @param deletedByParam
     */
    public void setDeletedByParam( SearchParam<Integer> deletedByParam ){
        this.deletedByParam = deletedByParam;
    }

		/**
     * Get the "pieceDetacheeLibelleParam" field value
     * @return the field value
     */
	public SearchParam<String> getPieceDetacheeLibelleParam(){
		return this.pieceDetacheeLibelleParam;
	}
	/**
     * Set the "pieceDetacheeLibelleParam" field value
     * @param pieceDetacheeLibelleParam
     */
    public void setPieceDetacheeLibelleParam( SearchParam<String> pieceDetacheeLibelleParam ){
        this.pieceDetacheeLibelleParam = pieceDetacheeLibelleParam;
    }

		/**
     * Get the "fournisseurNomParam" field value
     * @return the field value
     */
	public SearchParam<String> getFournisseurNomParam(){
		return this.fournisseurNomParam;
	}
	/**
     * Set the "fournisseurNomParam" field value
     * @param fournisseurNomParam
     */
    public void setFournisseurNomParam( SearchParam<String> fournisseurNomParam ){
        this.fournisseurNomParam = fournisseurNomParam;
    }

		/**
     * Get the "fournisseurPrenomParam" field value
     * @return the field value
     */
	public SearchParam<String> getFournisseurPrenomParam(){
		return this.fournisseurPrenomParam;
	}
	/**
     * Set the "fournisseurPrenomParam" field value
     * @param fournisseurPrenomParam
     */
    public void setFournisseurPrenomParam( SearchParam<String> fournisseurPrenomParam ){
        this.fournisseurPrenomParam = fournisseurPrenomParam;
    }


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		PieceDetacheeFichePreparationDevisDto other = (PieceDetacheeFichePreparationDevisDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("id:"+id);
		sb.append("|");
		sb.append("quantite:"+quantite);
		sb.append("|");
		sb.append("prixUnitaire:"+prixUnitaire);
		sb.append("|");
		sb.append("total:"+total);
		sb.append("|");
		sb.append("isValided:"+isValided);
		sb.append("|");
		sb.append("isDeleted:"+isDeleted);
		sb.append("|");
		sb.append("createdAt:"+createdAt);
		sb.append("|");
		sb.append("updatedAt:"+updatedAt);
		sb.append("|");
		sb.append("deletedAt:"+deletedAt);
		sb.append("|");
		sb.append("createdBy:"+createdBy);
		sb.append("|");
		sb.append("updatedBy:"+updatedBy);
		sb.append("|");
		sb.append("deletedBy:"+deletedBy);
        return sb.toString();
    }
}
