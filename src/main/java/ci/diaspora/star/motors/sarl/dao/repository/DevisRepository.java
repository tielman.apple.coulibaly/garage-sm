package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._DevisRepository;

/**
 * Repository : Devis.
 */
@Repository
public interface DevisRepository extends JpaRepository<Devis, Integer>, _DevisRepository {
	/**
	 * Finds Devis by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Devis whose id is equals to the given id. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.id = :id and e.isDeleted = :isDeleted")
	Devis findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Devis by using isValidedByChefAtelier as a search criteria.
	 *
	 * @param isValidedByChefAtelier
	 * @return An Object Devis whose isValidedByChefAtelier is equals to the given isValidedByChefAtelier. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.isValidedByChefAtelier = :isValidedByChefAtelier and e.isDeleted = :isDeleted")
	List<Devis> findByIsValidedByChefAtelier(@Param("isValidedByChefAtelier")Boolean isValidedByChefAtelier, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Devis by using isValidedByResponsableTechnique as a search criteria.
	 *
	 * @param isValidedByResponsableTechnique
	 * @return An Object Devis whose isValidedByResponsableTechnique is equals to the given isValidedByResponsableTechnique. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.isValidedByResponsableTechnique = :isValidedByResponsableTechnique and e.isDeleted = :isDeleted")
	List<Devis> findByIsValidedByResponsableTechnique(@Param("isValidedByResponsableTechnique")Boolean isValidedByResponsableTechnique, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Devis by using isValidedByDirection as a search criteria.
	 *
	 * @param isValidedByDirection
	 * @return An Object Devis whose isValidedByDirection is equals to the given isValidedByDirection. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.isValidedByDirection = :isValidedByDirection and e.isDeleted = :isDeleted")
	List<Devis> findByIsValidedByDirection(@Param("isValidedByDirection")Boolean isValidedByDirection, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Devis by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Devis whose isDeleted is equals to the given isDeleted. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.isDeleted = :isDeleted")
	List<Devis> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Devis by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Devis whose createdAt is equals to the given createdAt. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Devis> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Devis by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Devis whose updatedAt is equals to the given updatedAt. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Devis> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Devis by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Devis whose deletedAt is equals to the given deletedAt. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Devis> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Devis by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Devis whose createdBy is equals to the given createdBy. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Devis> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Devis by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Devis whose updatedBy is equals to the given updatedBy. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Devis> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Devis by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Devis whose deletedBy is equals to the given deletedBy. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Devis> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Devis by using userDirectionId as a search criteria.
	 *
	 * @param userDirectionId
	 * @return A list of Object Devis whose userDirectionId is equals to the given userDirectionId. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.userDirection.id = :userDirectionId and e.isDeleted = :isDeleted")
	List<Devis> findByUserDirectionId(@Param("userDirectionId")Integer userDirectionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Devis by using userDirectionId as a search criteria.
   *
   * @param userDirectionId
   * @return An Object Devis whose userDirectionId is equals to the given userDirectionId. If
   *         no Devis is found, this method returns null.
   */
  @Query("select e from Devis e where e.userDirection.id = :userDirectionId and e.isDeleted = :isDeleted")
  Devis findDevisByUserDirectionId(@Param("userDirectionId")Integer userDirectionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Devis by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object Devis whose etatId is equals to the given etatId. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
	List<Devis> findByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Devis by using etatId as a search criteria.
   *
   * @param etatId
   * @return An Object Devis whose etatId is equals to the given etatId. If
   *         no Devis is found, this method returns null.
   */
  @Query("select e from Devis e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
  Devis findDevisByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Devis by using userChefAtelierId as a search criteria.
	 *
	 * @param userChefAtelierId
	 * @return A list of Object Devis whose userChefAtelierId is equals to the given userChefAtelierId. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.userChefAtelier.id = :userChefAtelierId and e.isDeleted = :isDeleted")
	List<Devis> findByUserChefAtelierId(@Param("userChefAtelierId")Integer userChefAtelierId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Devis by using userChefAtelierId as a search criteria.
   *
   * @param userChefAtelierId
   * @return An Object Devis whose userChefAtelierId is equals to the given userChefAtelierId. If
   *         no Devis is found, this method returns null.
   */
  @Query("select e from Devis e where e.userChefAtelier.id = :userChefAtelierId and e.isDeleted = :isDeleted")
  Devis findDevisByUserChefAtelierId(@Param("userChefAtelierId")Integer userChefAtelierId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Devis by using userResponsableTechniqueId as a search criteria.
	 *
	 * @param userResponsableTechniqueId
	 * @return A list of Object Devis whose userResponsableTechniqueId is equals to the given userResponsableTechniqueId. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.userResponsableTechnique.id = :userResponsableTechniqueId and e.isDeleted = :isDeleted")
	List<Devis> findByUserResponsableTechniqueId(@Param("userResponsableTechniqueId")Integer userResponsableTechniqueId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Devis by using userResponsableTechniqueId as a search criteria.
   *
   * @param userResponsableTechniqueId
   * @return An Object Devis whose userResponsableTechniqueId is equals to the given userResponsableTechniqueId. If
   *         no Devis is found, this method returns null.
   */
  @Query("select e from Devis e where e.userResponsableTechnique.id = :userResponsableTechniqueId and e.isDeleted = :isDeleted")
  Devis findDevisByUserResponsableTechniqueId(@Param("userResponsableTechniqueId")Integer userResponsableTechniqueId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Devis by using fichePreparationId as a search criteria.
	 *
	 * @param fichePreparationId
	 * @return A list of Object Devis whose fichePreparationId is equals to the given fichePreparationId. If
	 *         no Devis is found, this method returns null.
	 */
	@Query("select e from Devis e where e.fichePreparationDevis.id = :fichePreparationId and e.isDeleted = :isDeleted")
	List<Devis> findByFichePreparationId(@Param("fichePreparationId")Integer fichePreparationId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Devis by using fichePreparationId as a search criteria.
   *
   * @param fichePreparationId
   * @return An Object Devis whose fichePreparationId is equals to the given fichePreparationId. If
   *         no Devis is found, this method returns null.
   */
  @Query("select e from Devis e where e.fichePreparationDevis.id = :fichePreparationId and e.isDeleted = :isDeleted")
  Devis findDevisByFichePreparationId(@Param("fichePreparationId")Integer fichePreparationId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Devis by using devisDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Devis
	 * @throws DataAccessException,ParseException
	 */
	public default List<Devis> getByCriteria(Request<DevisDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Devis e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Devis> query = em.createQuery(req, Devis.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Devis by using devisDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Devis
	 *
	 */
	public default Long count(Request<DevisDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Devis e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<DevisDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		DevisDto dto = request.getData() != null ? request.getData() : new DevisDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (DevisDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(DevisDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getIsValidedByChefAtelier()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByChefAtelier", dto.getIsValidedByChefAtelier(), "e.isValidedByChefAtelier", "Boolean", dto.getIsValidedByChefAtelierParam(), param, index, locale));
			}
			if (dto.getIsValidedByResponsableTechnique()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByResponsableTechnique", dto.getIsValidedByResponsableTechnique(), "e.isValidedByResponsableTechnique", "Boolean", dto.getIsValidedByResponsableTechniqueParam(), param, index, locale));
			}
			if (dto.getIsValidedByDirection()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidedByDirection", dto.getIsValidedByDirection(), "e.isValidedByDirection", "Boolean", dto.getIsValidedByDirectionParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getUserDirectionId()!= null && dto.getUserDirectionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userDirectionId", dto.getUserDirectionId(), "e.userDirection.id", "Integer", dto.getUserDirectionIdParam(), param, index, locale));
			}
			if (dto.getEtatId()!= null && dto.getEtatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatId", dto.getEtatId(), "e.etat.id", "Integer", dto.getEtatIdParam(), param, index, locale));
			}
			if (dto.getUserChefAtelierId()!= null && dto.getUserChefAtelierId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierId", dto.getUserChefAtelierId(), "e.userChefAtelier.id", "Integer", dto.getUserChefAtelierIdParam(), param, index, locale));
			}
			if (dto.getUserResponsableTechniqueId()!= null && dto.getUserResponsableTechniqueId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniqueId", dto.getUserResponsableTechniqueId(), "e.userResponsableTechnique.id", "Integer", dto.getUserResponsableTechniqueIdParam(), param, index, locale));
			}
			if (dto.getFichePreparationId()!= null && dto.getFichePreparationId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("fichePreparationId", dto.getFichePreparationId(), "e.fichePreparationDevis.id", "Integer", dto.getFichePreparationIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserDirectionNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userDirectionNom", dto.getUserDirectionNom(), "e.userDirection.nom", "String", dto.getUserDirectionNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserDirectionPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userDirectionPrenom", dto.getUserDirectionPrenom(), "e.userDirection.prenom", "String", dto.getUserResponsableTechniquePrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserDirectionLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userDirectionLogin", dto.getUserDirectionLogin(), "e.userDirection.login", "String", dto.getUserDirectionLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatLibelle", dto.getEtatLibelle(), "e.etat.libelle", "String", dto.getEtatLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierNom", dto.getUserChefAtelierNom(), "e.userChefAtelier.nom", "String", dto.getUserChefAtelierNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierPrenom", dto.getUserChefAtelierPrenom(), "e.userChefAtelier.prenom", "String", dto.getUserChefAtelierPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserChefAtelierLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userChefAtelierLogin", dto.getUserChefAtelierLogin(), "e.userChefAtelier.login", "String", dto.getUserChefAtelierLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableTechniqueNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniqueNom", dto.getUserResponsableTechniqueNom(), "e.userResponsableTechnique.nom", "String", dto.getUserResponsableTechniqueNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableTechniquePrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniquePrenom", dto.getUserResponsableTechniquePrenom(), "e.userResponsableTechnique.prenom", "String", dto.getUserResponsableTechniquePrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserResponsableTechniqueLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userResponsableTechniqueLogin", dto.getUserResponsableTechniqueLogin(), "e.userResponsableTechnique.login", "String", dto.getUserResponsableTechniqueLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
