package ci.diaspora.star.motors.sarl.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.dao.entity.User;
import ci.diaspora.star.motors.sarl.helper.dto.UserDto;

/**
 * Repository customize : User.
 */
@Repository
public interface _UserRepository {
	default List<String> _generateCriteria(UserDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds User by using login and password as a search criteria.
	 *
	 * @param login, password
	 * @return An Object User whose login and password are equals to the given login and password. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.login = :login and e.password = :password and e.isDeleted = :isDeleted")
	User findByLoginAndPassword(@Param("login")String login, @Param("password")String password, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds User by using token as a search criteria.
	 *
	 * @param token
	 * @return An Object User whose token is equals to the given token. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.token = :token and e.isValidToken = :isValidToken and e.email = :email and e.isDeleted = :isDeleted")
	User findUserByTokenAndEmailAndIsValidToken(@Param("token")String token, @Param("email")String email, @Param("isValidToken")Boolean isValidToken,  @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds User by using telephone as a search criteria.
	 *
	 * @param telephone
	 * @return An Object User whose telephone is equals to the given telephone. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.telephone = :telephone and e.isDeleted = :isDeleted")
	User findUserByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds User by using token as a search criteria.
	 *
	 * @param token
	 * @return An Object User whose token is equals to the given token. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.token = :token and e.email = :email and e.isDeleted = :isDeleted")
	User findUserByTokenAndEmail(@Param("token")String token, @Param("email")String email, @Param("isDeleted")Boolean isDeleted);
}
