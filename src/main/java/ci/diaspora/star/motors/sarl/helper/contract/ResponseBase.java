
/*
 * Created on 2019-08-18 ( Time 17:15:28 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.contract;

import ci.diaspora.star.motors.sarl.helper.Status;

/**
 * Response Base
 * 
 * @author SFL Back-End developper
 *
 */
public class ResponseBase {

	protected Status	status;
	protected boolean	hasError;
	protected String	sessionUser;
	protected Long		count;

	public ResponseBase() {
		status = new Status();
	}

	public String getSessionUser() {
		return sessionUser;
	}

	public void setSessionUser(String sessionUser) {
		this.sessionUser = sessionUser;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public boolean isHasError() {
		return hasError;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}
}
