package ci.diaspora.star.motors.sarl.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;

/**
 * Repository customize : Client.
 */
@Repository
public interface _ClientRepository {
	default List<String> _generateCriteria(ClientDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	
	/**
	 * Finds Client by using matricule as a search criteria.
	 *
	 * @param matricule
	 * @return An Object Client whose matricule is equals to the given matricule. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.matricule = :matricule and e.isDeleted = :isDeleted")
	Client findClientByMatricule(@Param("matricule")String matricule, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds Client by using telephone as a search criteria.
	 *
	 * @param telephone
	 * @return An Object Client whose telephone is equals to the given telephone. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.telephone = :telephone and e.isDeleted = :isDeleted")
	Client findClientByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);
	
}
