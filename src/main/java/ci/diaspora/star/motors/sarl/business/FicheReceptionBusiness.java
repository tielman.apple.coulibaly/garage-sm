


/*
 * Java transformer for entity table fiche_reception
 * Created on 2019-08-23 ( Time 14:59:37 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.dao.entity.BonSortie;
import ci.diaspora.star.motors.sarl.dao.entity.ElementEtatVehicule;
import ci.diaspora.star.motors.sarl.dao.entity.ElementEtatVehiculeFicheReception;
import ci.diaspora.star.motors.sarl.dao.entity.Etat;
import ci.diaspora.star.motors.sarl.dao.entity.Facture;
import ci.diaspora.star.motors.sarl.dao.entity.FicheReception;
import ci.diaspora.star.motors.sarl.dao.entity.FicheReceptionPieceVehicule;
import ci.diaspora.star.motors.sarl.dao.entity.NiveauCarburant;
import ci.diaspora.star.motors.sarl.dao.entity.OrdreReparation;
import ci.diaspora.star.motors.sarl.dao.entity.PanneDeclaree;
import ci.diaspora.star.motors.sarl.dao.entity.PanneDeclareeOrdreReparation;
import ci.diaspora.star.motors.sarl.dao.entity.PieceVehicule;
import ci.diaspora.star.motors.sarl.dao.entity.TypePrestation;
import ci.diaspora.star.motors.sarl.dao.entity.TypePrestationFicheReception;
import ci.diaspora.star.motors.sarl.dao.entity.User;
import ci.diaspora.star.motors.sarl.dao.entity.Vehicule;
import ci.diaspora.star.motors.sarl.dao.repository.BonSortieRepository;
import ci.diaspora.star.motors.sarl.dao.repository.CouleurVehiculeRepository;
import ci.diaspora.star.motors.sarl.dao.repository.ElementEtatVehiculeFicheReceptionRepository;
import ci.diaspora.star.motors.sarl.dao.repository.ElementEtatVehiculeRepository;
import ci.diaspora.star.motors.sarl.dao.repository.EtatRepository;
import ci.diaspora.star.motors.sarl.dao.repository.FactureRepository;
import ci.diaspora.star.motors.sarl.dao.repository.FicheReceptionPieceVehiculeRepository;
import ci.diaspora.star.motors.sarl.dao.repository.FicheReceptionRepository;
import ci.diaspora.star.motors.sarl.dao.repository.NiveauCarburantRepository;
import ci.diaspora.star.motors.sarl.dao.repository.OrdreReparationRepository;
import ci.diaspora.star.motors.sarl.dao.repository.PanneDeclareeOrdreReparationRepository;
import ci.diaspora.star.motors.sarl.dao.repository.PanneDeclareeRepository;
import ci.diaspora.star.motors.sarl.dao.repository.PieceVehiculeRepository;
import ci.diaspora.star.motors.sarl.dao.repository.TypePrestationFicheReceptionRepository;
import ci.diaspora.star.motors.sarl.dao.repository.TypePrestationRepository;
import ci.diaspora.star.motors.sarl.dao.repository.UserRepository;
import ci.diaspora.star.motors.sarl.dao.repository.VehiculeRepository;
import ci.diaspora.star.motors.sarl.helper.ExceptionUtils;
import ci.diaspora.star.motors.sarl.helper.FunctionalError;
import ci.diaspora.star.motors.sarl.helper.TechnicalError;
import ci.diaspora.star.motors.sarl.helper.Utilities;
import ci.diaspora.star.motors.sarl.helper.Validate;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.dto.BonSortieDto;
import ci.diaspora.star.motors.sarl.helper.dto.ElementEtatVehiculeDto;
import ci.diaspora.star.motors.sarl.helper.dto.ElementEtatVehiculeFicheReceptionDto;
import ci.diaspora.star.motors.sarl.helper.dto.FactureDto;
import ci.diaspora.star.motors.sarl.helper.dto.FicheReceptionDto;
import ci.diaspora.star.motors.sarl.helper.dto.FicheReceptionPieceVehiculeDto;
import ci.diaspora.star.motors.sarl.helper.dto.OrdreReparationDto;
import ci.diaspora.star.motors.sarl.helper.dto.PanneDeclareeDto;
import ci.diaspora.star.motors.sarl.helper.dto.PieceVehiculeDto;
import ci.diaspora.star.motors.sarl.helper.dto.TypePrestationDto;
import ci.diaspora.star.motors.sarl.helper.dto.TypePrestationFicheReceptionDto;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.BonSortieTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.ClientTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.ElementEtatVehiculeFicheReceptionTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.ElementEtatVehiculeTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.FactureTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.FicheReceptionPieceVehiculeTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.FicheReceptionTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.OrdreReparationTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.PanneDeclareeTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.PieceVehiculeTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.TypePrestationFicheReceptionTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.TypePrestationTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.VehiculeTransformer;
import ci.diaspora.star.motors.sarl.helper.enums.EmailEnum;
import ci.diaspora.star.motors.sarl.helper.enums.EtatEnum;

/**
BUSINESS for table "fiche_reception"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class FicheReceptionBusiness implements IBasicBusiness<Request<FicheReceptionDto>, Response<FicheReceptionDto>> {

	private Response<FicheReceptionDto> response;
	@Autowired
	private FicheReceptionRepository ficheReceptionRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TypePrestationFicheReceptionRepository typePrestationFicheReceptionRepository;
	@Autowired
	private ElementEtatVehiculeFicheReceptionRepository elementEtatVehiculeFicheReceptionRepository;
	@Autowired
	private CouleurVehiculeRepository couleurVehiculeRepository;
	@Autowired
	private EtatRepository etatRepository;
	@Autowired
	private BonSortieRepository bonSortieRepository;
	@Autowired
	private OrdreReparationRepository ordreReparationRepository;
	@Autowired
	private FactureRepository factureRepository;
	@Autowired
	private VehiculeRepository vehiculeRepository;
	@Autowired
	private TypePrestationRepository typePrestationRepository;
	@Autowired
	private PieceVehiculeRepository pieceVehiculeRepository;
	@Autowired
	private FicheReceptionPieceVehiculeRepository ficheReceptionPieceVehiculeRepository;
	@Autowired
	private PanneDeclareeRepository panneDeclareeRepository;
	@Autowired
	private PanneDeclareeOrdreReparationRepository panneDeclareeOrdreReparationRepository;
	@Autowired
	private NiveauCarburantRepository niveauCarburantRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private TypePrestationFicheReceptionBusiness typePrestationFicheReceptionBusiness;


	@Autowired
	private ElementEtatVehiculeFicheReceptionBusiness elementEtatVehiculeFicheReceptionBusiness;
	
	@Autowired
	private ElementEtatVehiculeRepository elementEtatVehiculeRepository;
	
	
	@Autowired
	private BonSortieBusiness bonSortieBusiness;

	@Autowired
	private FicheReceptionPieceVehiculeBusiness ficheReceptionPieceVehiculeBusiness;

	@Autowired
	private OrdreReparationBusiness ordreReparationBusiness;


	@Autowired
	private FactureBusiness factureBusiness;



	public FicheReceptionBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create FicheReception by using FicheReceptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<FicheReceptionDto> create(Request<FicheReceptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin create FicheReception-----");

		response = new Response<FicheReceptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<FicheReception> items = new ArrayList<FicheReception>();
			List<OrdreReparation> itemsOrdreReparation = new ArrayList<>();
			List<PanneDeclareeOrdreReparation> itemsPanneDeclareeOrdreReparation = new ArrayList<>();
			List<OrdreReparationDto> itemsOrdreReparationDto = new ArrayList<>();
			List<PanneDeclaree> itemsCouleurVehicule = new ArrayList<PanneDeclaree>();
			List<TypePrestationFicheReception> itemsTypePrestationFicheReception = new ArrayList<TypePrestationFicheReception>();
			List<FicheReceptionPieceVehicule> itemsFicheReceptionPieceVehicule = new ArrayList<FicheReceptionPieceVehicule>();

			for (FicheReceptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("userId", request.getUser());
				fieldsToVerify.put("vehiculeId", dto.getVehiculeId());


				fieldsToVerify.put("datasPieceVehicule", dto.getDatasPieceVehicule());
				fieldsToVerify.put("datasTypePrestation", dto.getDatasTypePrestation());
				fieldsToVerify.put("datasPannesDeclare", dto.getDatasPannesDeclare());

				//fieldsToVerify.put("couleurVehiculeId", dto.getCouleurVehiculeId());				
				//fieldsToVerify.put("couleurVehiculeLibelle", dto.getCouleurVehiculeLibelle());				
				fieldsToVerify.put("niveauCarburantId", dto.getNiveauCarburantId());
				fieldsToVerify.put("kilometrage", dto.getKilometrage());
				fieldsToVerify.put("interlocuteur", dto.getInterlocuteur());
				fieldsToVerify.put("etatCode", dto.getEtatCode());




				//fieldsToVerify.put("numeroFiche", dto.getNumeroFiche());
				//fieldsToVerify.put("nomDeposeur", dto.getNomDeposeur());
				//fieldsToVerify.put("nomRecepteur", dto.getNomRecepteur());
				//fieldsToVerify.put("autres", dto.getAutres());
				//fieldsToVerify.put("dateProchaineRevision", dto.getDateProchaineRevision());
				//fieldsToVerify.put("dateReception", dto.getDateReception());
				//fieldsToVerify.put("dateSortie", dto.getDateSortie());
				//fieldsToVerify.put("dateDepot", dto.getDateDepot());
				//fieldsToVerify.put("dateVisiteTechnique", dto.getDateVisiteTechnique());
				//fieldsToVerify.put("isDeposed", dto.getIsDeposed());
				//fieldsToVerify.put("isReceved", dto.getIsReceved());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				if (!Utilities.isNotEmpty(dto.getDatasPieceVehicule())) {
					response.setStatus(functionalError.FIELD_EMPTY("datasPieceVehicule", locale));
					response.setHasError(true);
					return response;
				}
				if (!Utilities.isNotEmpty(dto.getDatasTypePrestation())) {
					response.setStatus(functionalError.FIELD_EMPTY("datasTypePrestation", locale));
					response.setHasError(true);
					return response;
				}
				if (!Utilities.isNotEmpty(dto.getDatasPannesDeclare())) {
					response.setStatus(functionalError.FIELD_EMPTY("datasPannesDeclare", locale));
					response.setHasError(true);
					return response;
				}



				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if ficheReception to insert do not exist
				FicheReception existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("ficheReception -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// generation de numeroFiche valide
				String numeroFiche = EmailEnum.FICHE_STAR_MOTORS + Utilities.getCurrentDate().getYear() + Utilities.rand.nextInt(7);
				while (Utilities.notBlank(numeroFiche)) {
					existingEntity = ficheReceptionRepository.findFicheReceptionByNumeroFiche(numeroFiche, false);
					if (existingEntity != null) {
						numeroFiche = EmailEnum.FICHE_STAR_MOTORS + Utilities.getCurrentDate().getYear() + Utilities.rand.nextInt(7);
					}else {
						dto.setNumeroFiche(numeroFiche);
						if (items.stream().anyMatch(a->a.getNumeroFiche().equalsIgnoreCase(dto.getNumeroFiche()))) {
							numeroFiche = EmailEnum.FICHE_STAR_MOTORS + Utilities.getCurrentDate().getYear() + Utilities.rand.nextInt(7);
						}else {
							numeroFiche = "";
						}
					}
				}
				// Verify if user exist
				User existingUser = userRepository.findById(request.getUser(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if couleurVehicule exist
				//				CouleurVehicule existingCouleurVehicule = couleurVehiculeRepository.findById(dto.getCouleurVehiculeId(), false);
				//				if (existingCouleurVehicule == null) {
				//					response.setStatus(functionalError.DATA_NOT_EXIST("couleurVehicule -> " + dto.getCouleurVehiculeId(), locale));
				//					response.setHasError(true);
				//					return response;
				//				}


				//				CouleurVehicule existingCouleurVehicule = couleurVehiculeRepository.findByLibelle(dto.getCouleurVehiculeLibelle(), false);
				//				if (existingCouleurVehicule == null) {
				//					
				//					
				//					CouleurVehiculeDto dtoCouleurVehicule = new CouleurVehiculeDto();
				//					dtoCouleurVehicule.setLibelle(dto.getCouleurVehiculeLibelle());
				//					
				//					if (existingEntity != null) {
				//						response.setStatus(functionalError.DATA_EXIST("couleurVehicule -> " + dtoCouleurVehicule.getLibelle(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					if (itemsCouleurVehicule.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dtoCouleurVehicule.getLibelle()))) {
				//						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dtoCouleurVehicule.getLibelle()+"' pour les couleurVehicules", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					CouleurVehicule entityCouleurVehiculeToSave = null;
				//					entityCouleurVehiculeToSave = CouleurVehiculeTransformer.INSTANCE.toEntity(dtoCouleurVehicule);
				//					entityCouleurVehiculeToSave.setIsDeleted(false);
				//					entityCouleurVehiculeToSave.setCreatedAt(Utilities.getCurrentDate());
				//					entityCouleurVehiculeToSave.setCreatedBy(request.getUser());
				//					CouleurVehicule itemSaved = null;
				//					
				//					
				//					// inserer les donnees en base de donnees
				//					itemSaved = couleurVehiculeRepository.save(entityCouleurVehiculeToSave);
				//					if (itemSaved == null) {
				//						response.setStatus(functionalError.SAVE_FAIL("couleurVehicule", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					
				//					itemsCouleurVehicule.add(itemSaved);
				//					dto.setCouleurVehiculeId(itemSaved.getId());
				//					
				//				}else {
				//					dto.setCouleurVehiculeId(existingCouleurVehicule.getId());
				//				}


				// Verify if etat exist
				Etat existingEtat = etatRepository.findByCode(dto.getEtatCode(), false);
				if (existingEtat == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (!existingEtat.getCode().equals(EtatEnum.BROUILLON)) {
					response.setStatus(functionalError.INVALID_DATA("etat -> " + dto.getEtatCode(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if vehicule exist
				Vehicule existingVehicule = vehiculeRepository.findById(dto.getVehiculeId(), false);
				if (existingVehicule == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("vehicule -> " + dto.getVehiculeId(), locale));
					response.setHasError(true);
					return response;
				}
				NiveauCarburant existingNiveauCarburant = niveauCarburantRepository.findById(dto.getNiveauCarburantId(), false);
				if (existingNiveauCarburant == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("niveauCarburant -> " + dto.getNiveauCarburantId(), locale));
					response.setHasError(true);
					return response;
				}

				FicheReception entityToSave = null;
				//				entityToSave = FicheReceptionTransformer.INSTANCE.toEntity(dto, existingUser, existingCouleurVehicule, existingEtat, existingVehicule);
				entityToSave = FicheReceptionTransformer.INSTANCE.toEntity(dto, existingUser, existingEtat, existingNiveauCarburant, existingVehicule);
				entityToSave.setIsDeleted(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setDateReception(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());

				entityToSave.setDateVisiteTechnique(existingVehicule.getDateVisiteTechnique());
				entityToSave.setDateProchaineRevision(existingVehicule.getDateProchaineRevision());

				items.add(entityToSave);


				OrdreReparation entityOrdreReparationToSave = new OrdreReparation();

				entityOrdreReparationToSave.setFicheReception(entityToSave);
				entityOrdreReparationToSave.setEtatOrdre(etatRepository.findByCode(EtatEnum.BROUILLON, false));
				entityOrdreReparationToSave.setIsDeleted(false);
				entityOrdreReparationToSave.setCreatedAt(Utilities.getCurrentDate());
				entityOrdreReparationToSave.setCreatedBy(request.getUser());
				itemsOrdreReparation.add(entityOrdreReparationToSave);


				List<TypePrestation> itemsTypePrestation = new ArrayList<TypePrestation>();
				for (TypePrestationDto typePrestationDto : dto.getDatasTypePrestation()) {

					//TypePrestation existingTypePrestation = typePrestationRepository.findByLibelle(typePrestationDto.getLibelle(), false);
					TypePrestation existingTypePrestation = typePrestationRepository.findById(typePrestationDto.getId(), false);
					if (existingTypePrestation == null) {
						//response.setStatus(functionalError.DATA_EXIST("typePrestation -> " + typePrestationDto.getLibelle(), locale));
						response.setStatus(functionalError.DATA_NOT_EXIST("typePrestation -> " + typePrestationDto.getId(), locale));
						response.setHasError(true);
						return response;
					}
					//if (itemsTypePrestation.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(typePrestationDto.getLibelle()))) {
					if (itemsTypePrestation.stream().anyMatch(a->a.getId().equals(typePrestationDto.getId()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du id '" + typePrestationDto.getId()+"' pour les ficheReceptions", locale));
						response.setHasError(true);
						return response;
					}
					itemsTypePrestation.add(existingTypePrestation);
					TypePrestationFicheReception typePrestationFicheReception = new TypePrestationFicheReception();
					typePrestationFicheReception.setTypePrestation(existingTypePrestation);
					typePrestationFicheReception.setFicheReception(entityToSave);
					typePrestationFicheReception.setIsDeleted(false);
					typePrestationFicheReception.setCreatedAt(Utilities.getCurrentDate());
					typePrestationFicheReception.setCreatedBy(request.getUser());

					itemsTypePrestationFicheReception.add(typePrestationFicheReception);
				}

				List<PieceVehicule> itemsPieceVehicule = new ArrayList<PieceVehicule>();
				for (PieceVehiculeDto pieceVehiculeDto : dto.getDatasPieceVehicule()) {

					//PieceVehicule existingPieceVehicule = pieceVehiculeRepository.findByLibelle(pieceVehiculeDto.getLibelle(), false);
					PieceVehicule existingPieceVehicule = pieceVehiculeRepository.findById(pieceVehiculeDto.getId(), false);
					if (existingPieceVehicule == null) {
						//response.setStatus(functionalError.DATA_EXIST("pieceVehicule -> " + pieceVehiculeDto.getLibelle(), locale));
						response.setStatus(functionalError.DATA_NOT_EXIST("pieceVehicule -> " + pieceVehiculeDto.getId(), locale));
						response.setHasError(true);
						return response;
					}
					//if (itemsPieceVehicule.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(pieceVehiculeDto.getLibelle()))) {
					if (itemsPieceVehicule.stream().anyMatch(a->a.getId().equals(pieceVehiculeDto.getId()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + pieceVehiculeDto.getLibelle()+"' pour les ficheReceptions", locale));
						response.setHasError(true);
						return response;
					}
					itemsPieceVehicule.add(existingPieceVehicule);
					FicheReceptionPieceVehicule ficheReceptionPieceVehicule = new FicheReceptionPieceVehicule();
					ficheReceptionPieceVehicule.setPieceVehicule(existingPieceVehicule);
					ficheReceptionPieceVehicule.setFicheReception(entityToSave);
					ficheReceptionPieceVehicule.setDetailAutres(pieceVehiculeDto.getDetailAutres());
					ficheReceptionPieceVehicule.setIsDeleted(false);
					ficheReceptionPieceVehicule.setCreatedAt(Utilities.getCurrentDate());
					ficheReceptionPieceVehicule.setCreatedBy(request.getUser());

					itemsFicheReceptionPieceVehicule.add(ficheReceptionPieceVehicule);
				}

				List<PanneDeclaree> itemsPanneDeclaree = new ArrayList<PanneDeclaree>();
				for (PanneDeclareeDto panneDeclareeDto : dto.getDatasPannesDeclare()) {

					PanneDeclaree existingPanneDeclaree = panneDeclareeRepository.findByLibelle(panneDeclareeDto.getLibelle(), false);
					if (existingPanneDeclaree == null) {
						existingPanneDeclaree = new PanneDeclaree();
						existingPanneDeclaree.setLibelle(panneDeclareeDto.getLibelle());
						existingPanneDeclaree.setIsDeleted(false);
						existingPanneDeclaree.setCreatedAt(Utilities.getCurrentDate());
						existingPanneDeclaree.setCreatedBy(request.getUser());
						panneDeclareeRepository.save(existingPanneDeclaree);
						
					}
					if (itemsPanneDeclaree.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(panneDeclareeDto.getLibelle()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + panneDeclareeDto.getLibelle()+"' pour les ficheReceptions", locale));
						response.setHasError(true);
						return response;
					}
					itemsPanneDeclaree.add(existingPanneDeclaree);
					PanneDeclareeOrdreReparation panneDeclareeOrdreReparation = new PanneDeclareeOrdreReparation();
					panneDeclareeOrdreReparation.setPanneDeclaree(existingPanneDeclaree);
					panneDeclareeOrdreReparation.setOrdreReparation(entityOrdreReparationToSave);
					panneDeclareeOrdreReparation.setIsDeclaredByClient(panneDeclareeDto.getIsDeclaredByClient());
					panneDeclareeOrdreReparation.setObservations(panneDeclareeDto.getObservations());
					panneDeclareeOrdreReparation.setIsDeleted(false);
					panneDeclareeOrdreReparation.setCreatedAt(Utilities.getCurrentDate());
					panneDeclareeOrdreReparation.setCreatedBy(request.getUser());

					itemsPanneDeclareeOrdreReparation.add(panneDeclareeOrdreReparation);
				}
			}

			if (!items.isEmpty()) {
				List<FicheReception> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = ficheReceptionRepository.save((Iterable<FicheReception>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("ficheReception", locale));
					response.setHasError(true);
					return response;
				}

				if (!itemsOrdreReparation.isEmpty()) {
					List<OrdreReparation> itemsOrdreReparationSaved = null;
					// inserer les donnees en base de donnees
					itemsOrdreReparationSaved = ordreReparationRepository.save((Iterable<OrdreReparation>)itemsOrdreReparation);
					if (itemsOrdreReparationSaved == null || itemsOrdreReparationSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("ordreReparation", locale));
						response.setHasError(true);
						return response;
					}
				}

				if (!itemsPanneDeclareeOrdreReparation.isEmpty()) {
					List<PanneDeclareeOrdreReparation> itemsPanneDeclareeOrdreReparationSaved = null;
					// inserer les donnees en base de donnees
					itemsPanneDeclareeOrdreReparationSaved = panneDeclareeOrdreReparationRepository.save((Iterable<PanneDeclareeOrdreReparation>) itemsPanneDeclareeOrdreReparation);
					if (itemsPanneDeclareeOrdreReparationSaved == null || itemsPanneDeclareeOrdreReparationSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("panneDeclareeOrdreReparation", locale));
						response.setHasError(true);
						return response;
					}
				}

				if (!itemsTypePrestationFicheReception.isEmpty()) {
					List<TypePrestationFicheReception> itemsTypePrestationFicheReceptionSaved = null;
					// inserer les donnees en base de donnees
					itemsTypePrestationFicheReceptionSaved = typePrestationFicheReceptionRepository.save((Iterable<TypePrestationFicheReception>) itemsTypePrestationFicheReception);
					if (itemsTypePrestationFicheReceptionSaved == null || itemsTypePrestationFicheReceptionSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("typePrestationFicheReception", locale));
						response.setHasError(true);
						return response;
					}
				}

				if (!itemsFicheReceptionPieceVehicule.isEmpty()) {
					List<FicheReceptionPieceVehicule> itemsFicheReceptionPieceVehiculeSaved = null;
					// inserer les donnees en base de donnees
					itemsFicheReceptionPieceVehiculeSaved = ficheReceptionPieceVehiculeRepository.save((Iterable<FicheReceptionPieceVehicule>) itemsFicheReceptionPieceVehicule);
					if (itemsFicheReceptionPieceVehiculeSaved == null || itemsFicheReceptionPieceVehiculeSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("ficheReceptionPieceVehicule", locale));
						response.setHasError(true);
						return response;
					}
				}

				List<FicheReceptionDto> itemsDto = new ArrayList<FicheReceptionDto>();
				for (FicheReception entity : itemsSaved) {
					FicheReceptionDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create FicheReception-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update FicheReception by using FicheReceptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<FicheReceptionDto> update(Request<FicheReceptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin update FicheReception-----");

		response = new Response<FicheReceptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<FicheReception> items = new ArrayList<FicheReception>();
			
			List<ElementEtatVehiculeFicheReception> itemsElementEtatVehiculeFicheReceptionCreate = new ArrayList<>();
			List<ElementEtatVehiculeFicheReception> itemsElementEtatVehiculeFicheReceptionDelete = new ArrayList<>();
			List<OrdreReparation> itemsOrdreReparationCreate = new ArrayList<>();
			List<OrdreReparation> itemsOrdreReparationDelete = new ArrayList<>();
			List<PanneDeclareeOrdreReparation> itemsPanneDeclareeOrdreReparationCreate = new ArrayList<>();
			List<PanneDeclareeOrdreReparation> itemsPanneDeclareeOrdreReparationDelete = new ArrayList<>();
			List<OrdreReparationDto> itemsOrdreReparationDtoCreate = new ArrayList<>();
			List<OrdreReparationDto> itemsOrdreReparationDtoDelete = new ArrayList<>();
			List<PanneDeclaree> itemsCouleurVehiculeCreate = new ArrayList<>();
			List<PanneDeclaree> itemsCouleurVehiculeDelete = new ArrayList<>();
			List<TypePrestationFicheReception> itemsTypePrestationFicheReceptionCreate = new ArrayList<>();
			List<TypePrestationFicheReception> itemsTypePrestationFicheReceptionDelete = new ArrayList<>();
			List<FicheReceptionPieceVehicule> itemsFicheReceptionPieceVehiculeCreate = new ArrayList<>();
			List<FicheReceptionPieceVehicule> itemsFicheReceptionPieceVehiculeDelete = new ArrayList<>();
			
			for (FicheReceptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la ficheReception existe
				FicheReception entityToSave = null;
				entityToSave = ficheReceptionRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if user exist
				if (dto.getUserId() != null && dto.getUserId() > 0){
					User existingUser = userRepository.findById(dto.getUserId(), false);
					if (existingUser == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUser(existingUser);
				}
				// Verify if couleurVehicule exist
				//				if (dto.getCouleurVehiculeId() != null && dto.getCouleurVehiculeId() > 0){
				//					CouleurVehicule existingCouleurVehicule = couleurVehiculeRepository.findById(dto.getCouleurVehiculeId(), false);
				//					if (existingCouleurVehicule == null) {
				//						response.setStatus(functionalError.DATA_NOT_EXIST("couleurVehicule -> " + dto.getCouleurVehiculeId(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					entityToSave.setCouleurVehicule(existingCouleurVehicule);
				//				}
				//				// Verify if etat exist
				//				if (dto.getEtatId() != null && dto.getEtatId() > 0){
				//					Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
				//					if (existingEtat == null) {
				//						response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					entityToSave.setEtat(existingEtat);
				//				}
				// Verify if etat exist
				if (Utilities.notBlank(dto.getEtatCode())){
					Etat existingEtat = etatRepository.findByCode(dto.getEtatCode(), false);
					if (existingEtat == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatCode(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEtat(existingEtat);
				}
				// Verify if vehicule exist
				if (dto.getVehiculeId() != null && dto.getVehiculeId() > 0){
					Vehicule existingVehicule = vehiculeRepository.findById(dto.getVehiculeId(), false);
					if (existingVehicule == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("vehicule -> " + dto.getVehiculeId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setVehicule(existingVehicule);
				}
				// Verify if niveauCarburant exist
				if (dto.getNiveauCarburantId() != null && dto.getNiveauCarburantId() > 0) {
					NiveauCarburant existingNiveauCarburant = niveauCarburantRepository.findById(dto.getNiveauCarburantId(), false);
					if (existingNiveauCarburant == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("niveauCarburant -> " + dto.getNiveauCarburantId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setNiveauCarburant(existingNiveauCarburant);
				}

				if (Utilities.isNotEmpty(dto.getDatasElementEtatVehicule())) {

					List<ElementEtatVehiculeFicheReception> elementEtatVehiculeFicheReceptions = elementEtatVehiculeFicheReceptionRepository.findByFicheReceptionId(entityToSaveId, false);

					if (Utilities.isNotEmpty(elementEtatVehiculeFicheReceptions)) {
						itemsElementEtatVehiculeFicheReceptionDelete.addAll(elementEtatVehiculeFicheReceptions);
					}
					
					List<ElementEtatVehicule> itemsElementEtatVehicule = new ArrayList<>();
					for (ElementEtatVehiculeDto elementEtatVehiculeDto : dto.getDatasElementEtatVehicule()) {

						//TypePrestation existingTypePrestation = typePrestationRepository.findByLibelle(typePrestationDto.getLibelle(), false);
						ElementEtatVehicule existingElementEtatVehicule = elementEtatVehiculeRepository.findById(elementEtatVehiculeDto.getId(), false);
						if (existingElementEtatVehicule == null) {
							//response.setStatus(functionalError.DATA_EXIST("typePrestation -> " + typePrestationDto.getLibelle(), locale));
							response.setStatus(functionalError.DATA_NOT_EXIST("elementEtatVehicule -> " + elementEtatVehiculeDto.getId(), locale));
							response.setHasError(true);
							return response;
						}
						//if (itemsTypePrestation.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(typePrestationDto.getLibelle()))) {
						if (itemsElementEtatVehicule.stream().anyMatch(a->a.getId().equals(elementEtatVehiculeDto.getId()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du id '" + elementEtatVehiculeDto.getId()+"' pour les ficheReceptions", locale));
							response.setHasError(true);
							return response;
						}
						itemsElementEtatVehicule.add(existingElementEtatVehicule);
						ElementEtatVehiculeFicheReception elementEtatVehiculeFicheReception = new ElementEtatVehiculeFicheReception();
						elementEtatVehiculeFicheReception.setElementEtatVehicule(existingElementEtatVehicule);
						elementEtatVehiculeFicheReception.setFicheReception(entityToSave);
						elementEtatVehiculeFicheReception.setIsDeleted(false);
						elementEtatVehiculeFicheReception.setIsGood(elementEtatVehiculeDto.getIsGood());
						elementEtatVehiculeFicheReception.setCreatedAt(Utilities.getCurrentDate());
						elementEtatVehiculeFicheReception.setCreatedBy(request.getUser());

						itemsElementEtatVehiculeFicheReceptionCreate.add(elementEtatVehiculeFicheReception);
					}

				}
				if (Utilities.isNotEmpty(dto.getDatasTypePrestation())) {

					List<TypePrestationFicheReception> typePrestationFicheReceptions = typePrestationFicheReceptionRepository.findByFicheReceptionId(entityToSaveId, false);

					if (Utilities.isNotEmpty(typePrestationFicheReceptions)) {
						itemsTypePrestationFicheReceptionDelete.addAll(typePrestationFicheReceptions);
					}

					List<TypePrestation> itemsTypePrestation = new ArrayList<TypePrestation>();
					for (TypePrestationDto typePrestationDto : dto.getDatasTypePrestation()) {

						//TypePrestation existingTypePrestation = typePrestationRepository.findByLibelle(typePrestationDto.getLibelle(), false);
						TypePrestation existingTypePrestation = typePrestationRepository.findById(typePrestationDto.getId(), false);
						if (existingTypePrestation == null) {
							//response.setStatus(functionalError.DATA_EXIST("typePrestation -> " + typePrestationDto.getLibelle(), locale));
							response.setStatus(functionalError.DATA_NOT_EXIST("typePrestation -> " + typePrestationDto.getId(), locale));
							response.setHasError(true);
							return response;
						}
						//if (itemsTypePrestation.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(typePrestationDto.getLibelle()))) {
						if (itemsTypePrestation.stream().anyMatch(a->a.getId().equals(typePrestationDto.getId()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du id '" + typePrestationDto.getId()+"' pour les ficheReceptions", locale));
							response.setHasError(true);
							return response;
						}
						itemsTypePrestation.add(existingTypePrestation);
						TypePrestationFicheReception typePrestationFicheReception = new TypePrestationFicheReception();
						typePrestationFicheReception.setTypePrestation(existingTypePrestation);
						typePrestationFicheReception.setFicheReception(entityToSave);
						typePrestationFicheReception.setIsDeleted(false);
						typePrestationFicheReception.setCreatedAt(Utilities.getCurrentDate());
						typePrestationFicheReception.setCreatedBy(request.getUser());

						itemsTypePrestationFicheReceptionCreate.add(typePrestationFicheReception);
					}


				}
				if (Utilities.isNotEmpty(dto.getDatasPieceVehicule())) {
					List<FicheReceptionPieceVehicule> ficheReceptionPieceVehicules = ficheReceptionPieceVehiculeRepository.findByFicheReceptionId(entityToSaveId, false);

					if (Utilities.isNotEmpty(ficheReceptionPieceVehicules)) {
						itemsFicheReceptionPieceVehiculeDelete.addAll(ficheReceptionPieceVehicules);
					}

					List<PieceVehicule> itemsPieceVehicule = new ArrayList<PieceVehicule>();
					for (PieceVehiculeDto pieceVehiculeDto : dto.getDatasPieceVehicule()) {

						//PieceVehicule existingPieceVehicule = pieceVehiculeRepository.findByLibelle(pieceVehiculeDto.getLibelle(), false);
						PieceVehicule existingPieceVehicule = pieceVehiculeRepository.findById(pieceVehiculeDto.getId(), false);
						if (existingPieceVehicule == null) {
							//response.setStatus(functionalError.DATA_EXIST("pieceVehicule -> " + pieceVehiculeDto.getLibelle(), locale));
							response.setStatus(functionalError.DATA_NOT_EXIST("pieceVehicule -> " + pieceVehiculeDto.getId(), locale));
							response.setHasError(true);
							return response;
						}
						//if (itemsPieceVehicule.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(pieceVehiculeDto.getLibelle()))) {
						if (itemsPieceVehicule.stream().anyMatch(a->a.getId().equals(pieceVehiculeDto.getId()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + pieceVehiculeDto.getLibelle()+"' pour les ficheReceptions", locale));
							response.setHasError(true);
							return response;
						}
						itemsPieceVehicule.add(existingPieceVehicule);
						FicheReceptionPieceVehicule ficheReceptionPieceVehicule = new FicheReceptionPieceVehicule();
						ficheReceptionPieceVehicule.setPieceVehicule(existingPieceVehicule);
						ficheReceptionPieceVehicule.setFicheReception(entityToSave);
						ficheReceptionPieceVehicule.setDetailAutres(pieceVehiculeDto.getDetailAutres());
						ficheReceptionPieceVehicule.setIsDeleted(false);
						ficheReceptionPieceVehicule.setCreatedAt(Utilities.getCurrentDate());
						ficheReceptionPieceVehicule.setCreatedBy(request.getUser());

						itemsFicheReceptionPieceVehiculeCreate.add(ficheReceptionPieceVehicule);
					}
				}
				if (Utilities.isNotEmpty(dto.getDatasPannesDeclare())) {

					List<PanneDeclareeOrdreReparation> panneDeclareeOrdreReparations = panneDeclareeOrdreReparationRepository.findByIsDeclaredByClientAndFicheReceptionId(entityToSaveId, Boolean.TRUE, false);

					OrdreReparation ordreReparation = null;
					if (Utilities.isNotEmpty(panneDeclareeOrdreReparations)) {
						ordreReparation = panneDeclareeOrdreReparations.get(0).getOrdreReparation();
						itemsPanneDeclareeOrdreReparationDelete.addAll(panneDeclareeOrdreReparations);
					}
					
					List<PanneDeclaree> itemsPanneDeclaree = new ArrayList<PanneDeclaree>();
					for (PanneDeclareeDto panneDeclareeDto : dto.getDatasPannesDeclare()) {

						
						PanneDeclaree existingPanneDeclaree = panneDeclareeRepository.findByLibelle(panneDeclareeDto.getLibelle(), false);
						if (existingPanneDeclaree == null) {
							existingPanneDeclaree = new PanneDeclaree();
							existingPanneDeclaree.setLibelle(panneDeclareeDto.getLibelle());
							existingPanneDeclaree.setIsDeleted(false);
							existingPanneDeclaree.setCreatedAt(Utilities.getCurrentDate());
							existingPanneDeclaree.setCreatedBy(request.getUser());
							panneDeclareeRepository.save(existingPanneDeclaree);
						}
						if (itemsPanneDeclaree.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(panneDeclareeDto.getLibelle()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + panneDeclareeDto.getLibelle()+"' pour les ficheReceptions", locale));
							response.setHasError(true);
							return response;
						}
						itemsPanneDeclaree.add(existingPanneDeclaree);
						PanneDeclareeOrdreReparation panneDeclareeOrdreReparation = new PanneDeclareeOrdreReparation();
						panneDeclareeOrdreReparation.setPanneDeclaree(existingPanneDeclaree);
						panneDeclareeOrdreReparation.setOrdreReparation(ordreReparation);
						panneDeclareeOrdreReparation.setIsDeclaredByClient(panneDeclareeDto.getIsDeclaredByClient());
						panneDeclareeOrdreReparation.setObservations(panneDeclareeDto.getObservations());
						panneDeclareeOrdreReparation.setIsDeleted(false);
						panneDeclareeOrdreReparation.setCreatedAt(Utilities.getCurrentDate());
						panneDeclareeOrdreReparation.setCreatedBy(request.getUser());

						itemsPanneDeclareeOrdreReparationCreate.add(panneDeclareeOrdreReparation);
					}
				}
				//				if (Utilities.notBlank(dto.getNumeroFiche())) {
				//					entityToSave.setNumeroFiche(dto.getNumeroFiche());
				//				}
				if (Utilities.notBlank(dto.getInterlocuteur())) {
					entityToSave.setInterlocuteur(dto.getInterlocuteur());
				}
				if (Utilities.notBlank(dto.getNomDeposeur())) {
					entityToSave.setNomDeposeur(dto.getNomDeposeur());
				}
				if (Utilities.notBlank(dto.getNomRecepteur())) {
					entityToSave.setNomRecepteur(dto.getNomRecepteur());
				}
				if (Utilities.notBlank(dto.getKilometrage())) {
					entityToSave.setKilometrage(dto.getKilometrage());
				}
				if (Utilities.notBlank(dto.getAutres())) {
					entityToSave.setAutres(dto.getAutres());
				}
				if (Utilities.notBlank(dto.getDateProchaineRevision())) {
					entityToSave.setDateProchaineRevision(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateProchaineRevision()));
				}
				if (Utilities.notBlank(dto.getDateReception())) {
					entityToSave.setDateReception(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateReception()));
				}
				if (Utilities.notBlank(dto.getDateSortie())) {
					entityToSave.setDateSortie(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateSortie()));
				}
				if (Utilities.notBlank(dto.getDateDepot())) {
					entityToSave.setDateDepot(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateDepot()));
				}
				if (Utilities.notBlank(dto.getDateVisiteTechnique())) {
					entityToSave.setDateVisiteTechnique(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateVisiteTechnique()));
				}
				if (dto.getIsDeposed() != null) {
					entityToSave.setIsDeposed(dto.getIsDeposed());
				}
				if (dto.getIsReceved() != null) {
					entityToSave.setIsReceved(dto.getIsReceved());
				}


				entityToSave.setDateVisiteTechnique(entityToSave.getVehicule().getDateVisiteTechnique());
				entityToSave.setDateProchaineRevision(entityToSave.getVehicule().getDateProchaineRevision());

				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<FicheReception> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = ficheReceptionRepository.save((Iterable<FicheReception>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("ficheReception", locale));
					response.setHasError(true);
					return response;
				}
				
				if (Utilities.isNotEmpty(itemsElementEtatVehiculeFicheReceptionCreate)) {
					
					if (Utilities.isNotEmpty(itemsElementEtatVehiculeFicheReceptionDelete)) {
						List<ElementEtatVehiculeFicheReception> itemsDeleteSaved = null;

						for (ElementEtatVehiculeFicheReception element : itemsElementEtatVehiculeFicheReceptionDelete) {
							element.setIsDeleted(true);
							element.setDeletedAt(Utilities.getCurrentDate());
							element.setDeletedBy(request.getUser());
						}
						// maj les donnees en base
						itemsDeleteSaved = elementEtatVehiculeFicheReceptionRepository.save((Iterable<ElementEtatVehiculeFicheReception>) itemsElementEtatVehiculeFicheReceptionDelete);
						if (itemsDeleteSaved == null || itemsDeleteSaved.isEmpty()) {
							response.setStatus(functionalError.SAVE_FAIL("", locale));
							response.setHasError(true);
							return response;
						}
					}
					List<ElementEtatVehiculeFicheReception> itemsCreateSaved = null;
					// maj les donnees en base
					itemsCreateSaved = elementEtatVehiculeFicheReceptionRepository.save((Iterable<ElementEtatVehiculeFicheReception>) itemsElementEtatVehiculeFicheReceptionCreate);
					if (itemsCreateSaved == null || itemsCreateSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("", locale));
						response.setHasError(true);
						return response;
					}
					
				}
				
				if (Utilities.isNotEmpty(itemsPanneDeclareeOrdreReparationCreate)) {

					if (Utilities.isNotEmpty(itemsPanneDeclareeOrdreReparationDelete)) {
						List<PanneDeclareeOrdreReparation> itemsDeleteSaved = null;

						for (PanneDeclareeOrdreReparation element : itemsPanneDeclareeOrdreReparationDelete) {
							element.setIsDeleted(true);
							element.setDeletedAt(Utilities.getCurrentDate());
							element.setDeletedBy(request.getUser());
						}
						// maj les donnees en base
						itemsDeleteSaved = panneDeclareeOrdreReparationRepository.save((Iterable<PanneDeclareeOrdreReparation>) itemsPanneDeclareeOrdreReparationDelete);
						if (itemsDeleteSaved == null || itemsDeleteSaved.isEmpty()) {
							response.setStatus(functionalError.SAVE_FAIL("", locale));
							response.setHasError(true);
							return response;
						}
					}
					List<PanneDeclareeOrdreReparation> itemsCreateSaved = null;
					// maj les donnees en base
					itemsCreateSaved = panneDeclareeOrdreReparationRepository.save((Iterable<PanneDeclareeOrdreReparation>) itemsPanneDeclareeOrdreReparationCreate);
					if (itemsCreateSaved == null || itemsCreateSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("", locale));
						response.setHasError(true);
						return response;
					}

				}
				if (Utilities.isNotEmpty(itemsTypePrestationFicheReceptionCreate)) {

					if (Utilities.isNotEmpty(itemsTypePrestationFicheReceptionDelete)) {
						List<TypePrestationFicheReception> itemsDeleteSaved = null;

						for (TypePrestationFicheReception element : itemsTypePrestationFicheReceptionDelete) {
							element.setIsDeleted(true);
							element.setDeletedAt(Utilities.getCurrentDate());
							element.setDeletedBy(request.getUser());
						}
						// maj les donnees en base
						itemsDeleteSaved = typePrestationFicheReceptionRepository.save((Iterable<TypePrestationFicheReception>) itemsTypePrestationFicheReceptionDelete);
						if (itemsDeleteSaved == null || itemsDeleteSaved.isEmpty()) {
							response.setStatus(functionalError.SAVE_FAIL("", locale));
							response.setHasError(true);
							return response;
						}
					}
					List<TypePrestationFicheReception> itemsCreateSaved = null;
					// maj les donnees en base
					itemsCreateSaved = typePrestationFicheReceptionRepository.save((Iterable<TypePrestationFicheReception>) itemsTypePrestationFicheReceptionCreate);
					if (itemsCreateSaved == null || itemsCreateSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("", locale));
						response.setHasError(true);
						return response;
					}

				}
				if (Utilities.isNotEmpty(itemsFicheReceptionPieceVehiculeCreate)) {

					if (Utilities.isNotEmpty(itemsFicheReceptionPieceVehiculeDelete)) {
						List<FicheReceptionPieceVehicule> itemsDeleteSaved = null;

						for (FicheReceptionPieceVehicule element : itemsFicheReceptionPieceVehiculeDelete) {
							element.setIsDeleted(true);
							element.setDeletedAt(Utilities.getCurrentDate());
							element.setDeletedBy(request.getUser());
						}
						// maj les donnees en base
						itemsDeleteSaved = ficheReceptionPieceVehiculeRepository.save((Iterable<FicheReceptionPieceVehicule>) itemsFicheReceptionPieceVehiculeDelete);
						if (itemsDeleteSaved == null || itemsDeleteSaved.isEmpty()) {
							response.setStatus(functionalError.SAVE_FAIL("", locale));
							response.setHasError(true);
							return response;
						}
					}
					List<FicheReceptionPieceVehicule> itemsCreateSaved = null;
					// maj les donnees en base
					itemsCreateSaved = ficheReceptionPieceVehiculeRepository.save((Iterable<FicheReceptionPieceVehicule>) itemsFicheReceptionPieceVehiculeCreate);
					if (itemsCreateSaved == null || itemsCreateSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("", locale));
						response.setHasError(true);
						return response;
					}

				}

				List<FicheReceptionDto> itemsDto = new ArrayList<FicheReceptionDto>();
				for (FicheReception entity : itemsSaved) {
					FicheReceptionDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update FicheReception-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete FicheReception by using FicheReceptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<FicheReceptionDto> delete(Request<FicheReceptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete FicheReception-----");

		response = new Response<FicheReceptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<FicheReception> items = new ArrayList<FicheReception>();

			for (FicheReceptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la ficheReception existe
				FicheReception existingEntity = null;
				existingEntity = ficheReceptionRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// typePrestationFicheReception
				List<TypePrestationFicheReception> listOfTypePrestationFicheReception = typePrestationFicheReceptionRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfTypePrestationFicheReception != null && !listOfTypePrestationFicheReception.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfTypePrestationFicheReception.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// elementEtatVehiculeFicheReception
				List<ElementEtatVehiculeFicheReception> listOfElementEtatVehiculeFicheReception = elementEtatVehiculeFicheReceptionRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfElementEtatVehiculeFicheReception != null && !listOfElementEtatVehiculeFicheReception.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfElementEtatVehiculeFicheReception.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// bonSortie
				List<BonSortie> listOfBonSortie = bonSortieRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfBonSortie != null && !listOfBonSortie.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfBonSortie.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation = ordreReparationRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfOrdreReparation != null && !listOfOrdreReparation.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfOrdreReparation.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// facture
				List<Facture> listOfFacture = factureRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfFacture != null && !listOfFacture.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFacture.size() + ")", locale));
					response.setHasError(true);
					return response;
				}

				// ficheReceptionPieceVehicule
				List<FicheReceptionPieceVehicule> listOfFicheReceptionPieceVehicule = ficheReceptionPieceVehiculeRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfFicheReceptionPieceVehicule != null && !listOfFicheReceptionPieceVehicule.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFicheReceptionPieceVehicule.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				ficheReceptionRepository.save((Iterable<FicheReception>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete FicheReception-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete FicheReception by using FicheReceptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<FicheReceptionDto> forceDelete(Request<FicheReceptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete FicheReception-----");

		response = new Response<FicheReceptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<FicheReception> items = new ArrayList<FicheReception>();

			for (FicheReceptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la ficheReception existe
				FicheReception existingEntity = null;
				existingEntity = ficheReceptionRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ficheReception -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// typePrestationFicheReception
				List<TypePrestationFicheReception> listOfTypePrestationFicheReception = typePrestationFicheReceptionRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfTypePrestationFicheReception != null && !listOfTypePrestationFicheReception.isEmpty()){
					Request<TypePrestationFicheReceptionDto> deleteRequest = new Request<TypePrestationFicheReceptionDto>();
					deleteRequest.setDatas(TypePrestationFicheReceptionTransformer.INSTANCE.toDtos(listOfTypePrestationFicheReception));
					deleteRequest.setUser(request.getUser());
					Response<TypePrestationFicheReceptionDto> deleteResponse = typePrestationFicheReceptionBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// elementEtatVehiculeFicheReception
				List<ElementEtatVehiculeFicheReception> listOfElementEtatVehiculeFicheReception = elementEtatVehiculeFicheReceptionRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfElementEtatVehiculeFicheReception != null && !listOfElementEtatVehiculeFicheReception.isEmpty()){
					Request<ElementEtatVehiculeFicheReceptionDto> deleteRequest = new Request<ElementEtatVehiculeFicheReceptionDto>();
					deleteRequest.setDatas(ElementEtatVehiculeFicheReceptionTransformer.INSTANCE.toDtos(listOfElementEtatVehiculeFicheReception));
					deleteRequest.setUser(request.getUser());
					Response<ElementEtatVehiculeFicheReceptionDto> deleteResponse = elementEtatVehiculeFicheReceptionBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// bonSortie
				List<BonSortie> listOfBonSortie = bonSortieRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfBonSortie != null && !listOfBonSortie.isEmpty()){
					Request<BonSortieDto> deleteRequest = new Request<BonSortieDto>();
					deleteRequest.setDatas(BonSortieTransformer.INSTANCE.toDtos(listOfBonSortie));
					deleteRequest.setUser(request.getUser());
					Response<BonSortieDto> deleteResponse = bonSortieBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation = ordreReparationRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfOrdreReparation != null && !listOfOrdreReparation.isEmpty()){
					Request<OrdreReparationDto> deleteRequest = new Request<OrdreReparationDto>();
					deleteRequest.setDatas(OrdreReparationTransformer.INSTANCE.toDtos(listOfOrdreReparation));
					deleteRequest.setUser(request.getUser());
					Response<OrdreReparationDto> deleteResponse = ordreReparationBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// facture
				List<Facture> listOfFacture = factureRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfFacture != null && !listOfFacture.isEmpty()){
					Request<FactureDto> deleteRequest = new Request<FactureDto>();
					deleteRequest.setDatas(FactureTransformer.INSTANCE.toDtos(listOfFacture));
					deleteRequest.setUser(request.getUser());
					Response<FactureDto> deleteResponse = factureBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				// ficheReceptionPieceVehicule
				List<FicheReceptionPieceVehicule> listOfFicheReceptionPieceVehicule = ficheReceptionPieceVehiculeRepository.findByFicheReceptionId(existingEntity.getId(), false);
				if (listOfFicheReceptionPieceVehicule != null && !listOfFicheReceptionPieceVehicule.isEmpty()){
					Request<FicheReceptionPieceVehiculeDto> deleteRequest = new Request<FicheReceptionPieceVehiculeDto>();
					deleteRequest.setDatas(FicheReceptionPieceVehiculeTransformer.INSTANCE.toDtos(listOfFicheReceptionPieceVehicule));
					deleteRequest.setUser(request.getUser());
					Response<FicheReceptionPieceVehiculeDto> deleteResponse = ficheReceptionPieceVehiculeBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				ficheReceptionRepository.save((Iterable<FicheReception>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete FicheReception-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get FicheReception by using FicheReceptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<FicheReceptionDto> getByCriteria(Request<FicheReceptionDto> request, Locale locale) {
		slf4jLogger.info("----begin get FicheReception-----");

		response = new Response<FicheReceptionDto>();

		try {
			List<FicheReception> items = null;
			items = ficheReceptionRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<FicheReceptionDto> itemsDto = new ArrayList<FicheReceptionDto>();
				for (FicheReception entity : items) {
					FicheReceptionDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(ficheReceptionRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("ficheReception", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get FicheReception-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full FicheReceptionDto by using FicheReception as object.
	 *
	 * @param entity, locale
	 * @return FicheReceptionDto
	 *
	 */
	private FicheReceptionDto getFullInfos(FicheReception entity, Integer size, Locale locale) throws Exception {
		FicheReceptionDto dto = FicheReceptionTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		
		Vehicule vehicule = vehiculeRepository.findById(dto.getVehiculeId(), false);
		if (vehicule != null) {
			dto.setImmatriculationVehicule(vehicule.getImmatriculation());
			dto.setMatriculeClient(vehicule.getClient().getMatricule());
		}
		if (size > 1) {
			return dto;
		}

		
		if (vehicule != null) {
			dto.setDataClient(ClientTransformer.INSTANCE.toDto(vehicule.getClient()));
			dto.setDataVehicule(VehiculeTransformer.INSTANCE.toDto(vehicule));;
		}

			
		// recuperation de tous les elements
		List<FicheReceptionPieceVehicule> ficheReceptionPieceVehicules = new ArrayList<>();
		List<TypePrestationFicheReception> typePrestationFicheReceptions = new ArrayList<>();
		List<PanneDeclareeOrdreReparation> panneDeclareeOrdreReparations = new ArrayList<>();
		List<ElementEtatVehiculeFicheReception> elementEtatVehiculeFicheReceptions = new ArrayList<>();
		
		
		ficheReceptionPieceVehicules = ficheReceptionPieceVehiculeRepository.findByFicheReceptionId(dto.getId(), false);
		if (Utilities.isNotEmpty(ficheReceptionPieceVehicules)) {
			List<PieceVehicule> pieceVehicules = new ArrayList<>();
			for (FicheReceptionPieceVehicule element : ficheReceptionPieceVehicules) {
				pieceVehicules.add(element.getPieceVehicule());
			}
			dto.setDatasPieceVehicule(PieceVehiculeTransformer.INSTANCE.toDtos(pieceVehicules));
		}
		typePrestationFicheReceptions = typePrestationFicheReceptionRepository.findByFicheReceptionId(dto.getId(), false);
		if (Utilities.isNotEmpty(typePrestationFicheReceptions)) {
			List<TypePrestation> typePrestations = new ArrayList<>();
			for (TypePrestationFicheReception element : typePrestationFicheReceptions) {
				typePrestations.add(element.getTypePrestation());
			}
			dto.setDatasTypePrestation(TypePrestationTransformer.INSTANCE.toDtos(typePrestations));
		}
		
		
		elementEtatVehiculeFicheReceptions = elementEtatVehiculeFicheReceptionRepository.findByFicheReceptionId(dto.getId(), false);
		if (Utilities.isNotEmpty(elementEtatVehiculeFicheReceptions)) {
			List<ElementEtatVehiculeDto> elementEtatVehicules = new ArrayList<>();
			for (ElementEtatVehiculeFicheReception element : elementEtatVehiculeFicheReceptions) {
				ElementEtatVehiculeDto elementEtatVehiculeDto = ElementEtatVehiculeTransformer.INSTANCE.toDto(element.getElementEtatVehicule());
				elementEtatVehiculeDto.setIsGood(element.getIsGood());
				elementEtatVehicules.add(elementEtatVehiculeDto);
			}
			dto.setDatasElementEtatVehicule(elementEtatVehicules);
		}
		
		panneDeclareeOrdreReparations = panneDeclareeOrdreReparationRepository.findByIsDeclaredByClientAndFicheReceptionId(dto.getId(), Boolean.TRUE, false);
		if (Utilities.isNotEmpty(panneDeclareeOrdreReparations)) {
			List<PanneDeclareeDto> panneDeclarees = new ArrayList<>();
			for (PanneDeclareeOrdreReparation element : panneDeclareeOrdreReparations) {
				PanneDeclareeDto panneDeclareeDto = PanneDeclareeTransformer.INSTANCE.toDto(element.getPanneDeclaree());
				panneDeclareeDto.setObservations(element.getObservations());
				panneDeclareeDto.setDiagnosticAtelier(element.getDiagnosticAtelier());
				panneDeclareeDto.setIsDeclaredByClient(element.getIsDeclaredByClient());
				panneDeclarees.add(panneDeclareeDto);
			}
			dto.setDatasPannesDeclare(panneDeclarees);
		}
		
		
		
		
		

		return dto;
	}
}
