

/*
 * Java transformer for entity table vehicule 
 * Created on 2019-09-10 ( Time 09:02:06 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "vehicule"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface VehiculeTransformer {

	VehiculeTransformer INSTANCE = Mappers.getMapper(VehiculeTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateProchaineRevision", dateFormat="dd/MM/yyyy",target="dateProchaineRevision"),

		@Mapping(source="entity.dateVisiteTechnique", dateFormat="dd/MM/yyyy",target="dateVisiteTechnique"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.typeVehicule.id", target="typeId"),
		@Mapping(source="entity.typeVehicule.libelle", target="typeVehiculeLibelle"),
		@Mapping(source="entity.client.id", target="clientId"),
		@Mapping(source="entity.marque.id", target="marqueId"),
		@Mapping(source="entity.marque.libelle", target="marqueLibelle"),
		@Mapping(source="entity.couleurVehicule.id", target="couleurVehiculeId"),
		@Mapping(source="entity.couleurVehicule.libelle", target="couleurVehiculeLibelle"),
		@Mapping(source="entity.energieVehicule.id", target="energieId"),
		@Mapping(source="entity.energieVehicule.libelle", target="energieVehiculeLibelle"),
	})
	VehiculeDto toDto(Vehicule entity) throws ParseException;

    List<VehiculeDto> toDtos(List<Vehicule> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.immatriculation", target="immatriculation"),
		@Mapping(source="dto.numeroChassis", target="numeroChassis"),
		@Mapping(source="dto.dateProchaineRevision", dateFormat="dd/MM/yyyy",target="dateProchaineRevision"),
		@Mapping(source="dto.dateVisiteTechnique", dateFormat="dd/MM/yyyy",target="dateVisiteTechnique"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="typeVehicule", target="typeVehicule"),
		@Mapping(source="client", target="client"),
		@Mapping(source="marque", target="marque"),
		@Mapping(source="couleurVehicule", target="couleurVehicule"),
		@Mapping(source="energieVehicule", target="energieVehicule"),
	})
    Vehicule toEntity(VehiculeDto dto, TypeVehicule typeVehicule, Client client, Marque marque, CouleurVehicule couleurVehicule, EnergieVehicule energieVehicule) throws ParseException;

    //List<Vehicule> toEntities(List<VehiculeDto> dtos) throws ParseException;

}
