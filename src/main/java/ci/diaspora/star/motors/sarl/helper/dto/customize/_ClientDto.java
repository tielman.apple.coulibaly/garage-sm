
/*
 * Java dto for entity table client 
 * Created on 2019-08-18 ( Time 17:15:06 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.diaspora.star.motors.sarl.helper.dto.VehiculeDto;

/**
 * DTO customize for table "client"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _ClientDto {

	
	
	List<VehiculeDto> datasVehicule ;

	public List<VehiculeDto> getDatasVehicule() {
		return datasVehicule;
	}

	public void setDatasVehicule(List<VehiculeDto> datasVehicule) {
		this.datasVehicule = datasVehicule;
	}
	
	
	
}
