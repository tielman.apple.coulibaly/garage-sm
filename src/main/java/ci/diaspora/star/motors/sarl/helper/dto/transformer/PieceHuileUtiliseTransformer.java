

/*
 * Java transformer for entity table piece_huile_utilise 
 * Created on 2019-08-18 ( Time 17:15:14 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "piece_huile_utilise"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface PieceHuileUtiliseTransformer {

	PieceHuileUtiliseTransformer INSTANCE = Mappers.getMapper(PieceHuileUtiliseTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.designationPieceHuileUtilise.id", target="designationPieceHuileUtiliseId"),
		@Mapping(source="entity.designationPieceHuileUtilise.libelle", target="designationPieceHuileUtiliseLibelle"),
		@Mapping(source="entity.ordreReparation.id", target="ordreReparationId"),
	})
	PieceHuileUtiliseDto toDto(PieceHuileUtilise entity) throws ParseException;

    List<PieceHuileUtiliseDto> toDtos(List<PieceHuileUtilise> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.quantite", target="quantite"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="designationPieceHuileUtilise", target="designationPieceHuileUtilise"),
		@Mapping(source="ordreReparation", target="ordreReparation"),
	})
    PieceHuileUtilise toEntity(PieceHuileUtiliseDto dto, DesignationPieceHuileUtilise designationPieceHuileUtilise, OrdreReparation ordreReparation) throws ParseException;

    //List<PieceHuileUtilise> toEntities(List<PieceHuileUtiliseDto> dtos) throws ParseException;

}
