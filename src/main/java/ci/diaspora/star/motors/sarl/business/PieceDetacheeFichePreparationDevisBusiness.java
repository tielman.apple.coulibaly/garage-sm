


                                                                                                        /*
 * Java transformer for entity table piece_detachee_fiche_preparation_devis
 * Created on 2019-08-18 ( Time 17:15:14 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "piece_detachee_fiche_preparation_devis"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class PieceDetacheeFichePreparationDevisBusiness implements IBasicBusiness<Request<PieceDetacheeFichePreparationDevisDto>, Response<PieceDetacheeFichePreparationDevisDto>> {

  private Response<PieceDetacheeFichePreparationDevisDto> response;
  @Autowired
  private PieceDetacheeFichePreparationDevisRepository pieceDetacheeFichePreparationDevisRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private PieceDetacheeRepository pieceDetacheeRepository;
  @Autowired
  private FournisseurRepository fournisseurRepository;
  @Autowired
  private FichePreparationDevisRepository fichePreparationDevisRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

              

  public PieceDetacheeFichePreparationDevisBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create PieceDetacheeFichePreparationDevis by using PieceDetacheeFichePreparationDevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PieceDetacheeFichePreparationDevisDto> create(Request<PieceDetacheeFichePreparationDevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin create PieceDetacheeFichePreparationDevis-----");

    response = new Response<PieceDetacheeFichePreparationDevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PieceDetacheeFichePreparationDevis> items = new ArrayList<PieceDetacheeFichePreparationDevis>();

      for (PieceDetacheeFichePreparationDevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("pieceDetacheeId", dto.getPieceDetacheeId());
        fieldsToVerify.put("fichePreparationDevisId", dto.getFichePreparationDevisId());
        fieldsToVerify.put("fournisseurId", dto.getFournisseurId());
        fieldsToVerify.put("quantite", dto.getQuantite());
        fieldsToVerify.put("prixUnitaire", dto.getPrixUnitaire());
        fieldsToVerify.put("total", dto.getTotal());
        fieldsToVerify.put("isValided", dto.getIsValided());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if pieceDetacheeFichePreparationDevis to insert do not exist
        PieceDetacheeFichePreparationDevis existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("pieceDetacheeFichePreparationDevis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if pieceDetachee exist
        PieceDetachee existingPieceDetachee = pieceDetacheeRepository.findById(dto.getPieceDetacheeId(), false);
        if (existingPieceDetachee == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pieceDetachee -> " + dto.getPieceDetacheeId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if fournisseur exist
        Fournisseur existingFournisseur = fournisseurRepository.findById(dto.getFournisseurId(), false);
        if (existingFournisseur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("fournisseur -> " + dto.getFournisseurId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if fichePreparationDevis exist
        FichePreparationDevis existingFichePreparationDevis = fichePreparationDevisRepository.findById(dto.getFichePreparationDevisId(), false);
        if (existingFichePreparationDevis == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("fichePreparationDevis -> " + dto.getFichePreparationDevisId(), locale));
          response.setHasError(true);
          return response;
        }
        PieceDetacheeFichePreparationDevis entityToSave = null;
        entityToSave = PieceDetacheeFichePreparationDevisTransformer.INSTANCE.toEntity(dto, existingPieceDetachee, existingFournisseur, existingFichePreparationDevis);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<PieceDetacheeFichePreparationDevis> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = pieceDetacheeFichePreparationDevisRepository.save((Iterable<PieceDetacheeFichePreparationDevis>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("pieceDetacheeFichePreparationDevis", locale));
          response.setHasError(true);
          return response;
        }
        List<PieceDetacheeFichePreparationDevisDto> itemsDto = new ArrayList<PieceDetacheeFichePreparationDevisDto>();
        for (PieceDetacheeFichePreparationDevis entity : itemsSaved) {
          PieceDetacheeFichePreparationDevisDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create PieceDetacheeFichePreparationDevis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update PieceDetacheeFichePreparationDevis by using PieceDetacheeFichePreparationDevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PieceDetacheeFichePreparationDevisDto> update(Request<PieceDetacheeFichePreparationDevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin update PieceDetacheeFichePreparationDevis-----");

    response = new Response<PieceDetacheeFichePreparationDevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PieceDetacheeFichePreparationDevis> items = new ArrayList<PieceDetacheeFichePreparationDevis>();

      for (PieceDetacheeFichePreparationDevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la pieceDetacheeFichePreparationDevis existe
        PieceDetacheeFichePreparationDevis entityToSave = null;
        entityToSave = pieceDetacheeFichePreparationDevisRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pieceDetacheeFichePreparationDevis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if pieceDetachee exist
        if (dto.getPieceDetacheeId() != null && dto.getPieceDetacheeId() > 0){
          PieceDetachee existingPieceDetachee = pieceDetacheeRepository.findById(dto.getPieceDetacheeId(), false);
          if (existingPieceDetachee == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("pieceDetachee -> " + dto.getPieceDetacheeId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setPieceDetachee(existingPieceDetachee);
        }
        // Verify if fournisseur exist
        if (dto.getFournisseurId() != null && dto.getFournisseurId() > 0){
          Fournisseur existingFournisseur = fournisseurRepository.findById(dto.getFournisseurId(), false);
          if (existingFournisseur == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("fournisseur -> " + dto.getFournisseurId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setFournisseur(existingFournisseur);
        }
        // Verify if fichePreparationDevis exist
        if (dto.getFichePreparationDevisId() != null && dto.getFichePreparationDevisId() > 0){
          FichePreparationDevis existingFichePreparationDevis = fichePreparationDevisRepository.findById(dto.getFichePreparationDevisId(), false);
          if (existingFichePreparationDevis == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("fichePreparationDevis -> " + dto.getFichePreparationDevisId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setFichePreparationDevis(existingFichePreparationDevis);
        }
        if (dto.getQuantite() != null && dto.getQuantite() > 0) {
          entityToSave.setQuantite(dto.getQuantite());
        }
        if (dto.getPrixUnitaire() != null && dto.getPrixUnitaire() > 0) {
          entityToSave.setPrixUnitaire(dto.getPrixUnitaire());
        }
        if (dto.getTotal() != null && dto.getTotal() > 0) {
          entityToSave.setTotal(dto.getTotal());
        }
        if (dto.getIsValided() != null) {
          entityToSave.setIsValided(dto.getIsValided());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<PieceDetacheeFichePreparationDevis> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = pieceDetacheeFichePreparationDevisRepository.save((Iterable<PieceDetacheeFichePreparationDevis>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("pieceDetacheeFichePreparationDevis", locale));
          response.setHasError(true);
          return response;
        }
        List<PieceDetacheeFichePreparationDevisDto> itemsDto = new ArrayList<PieceDetacheeFichePreparationDevisDto>();
        for (PieceDetacheeFichePreparationDevis entity : itemsSaved) {
          PieceDetacheeFichePreparationDevisDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update PieceDetacheeFichePreparationDevis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete PieceDetacheeFichePreparationDevis by using PieceDetacheeFichePreparationDevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PieceDetacheeFichePreparationDevisDto> delete(Request<PieceDetacheeFichePreparationDevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete PieceDetacheeFichePreparationDevis-----");

    response = new Response<PieceDetacheeFichePreparationDevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PieceDetacheeFichePreparationDevis> items = new ArrayList<PieceDetacheeFichePreparationDevis>();

      for (PieceDetacheeFichePreparationDevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la pieceDetacheeFichePreparationDevis existe
        PieceDetacheeFichePreparationDevis existingEntity = null;
        existingEntity = pieceDetacheeFichePreparationDevisRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pieceDetacheeFichePreparationDevis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        pieceDetacheeFichePreparationDevisRepository.save((Iterable<PieceDetacheeFichePreparationDevis>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete PieceDetacheeFichePreparationDevis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete PieceDetacheeFichePreparationDevis by using PieceDetacheeFichePreparationDevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<PieceDetacheeFichePreparationDevisDto> forceDelete(Request<PieceDetacheeFichePreparationDevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete PieceDetacheeFichePreparationDevis-----");

    response = new Response<PieceDetacheeFichePreparationDevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PieceDetacheeFichePreparationDevis> items = new ArrayList<PieceDetacheeFichePreparationDevis>();

      for (PieceDetacheeFichePreparationDevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la pieceDetacheeFichePreparationDevis existe
        PieceDetacheeFichePreparationDevis existingEntity = null;
          existingEntity = pieceDetacheeFichePreparationDevisRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pieceDetacheeFichePreparationDevis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

              

                                            existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          pieceDetacheeFichePreparationDevisRepository.save((Iterable<PieceDetacheeFichePreparationDevis>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete PieceDetacheeFichePreparationDevis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get PieceDetacheeFichePreparationDevis by using PieceDetacheeFichePreparationDevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<PieceDetacheeFichePreparationDevisDto> getByCriteria(Request<PieceDetacheeFichePreparationDevisDto> request, Locale locale) {
    slf4jLogger.info("----begin get PieceDetacheeFichePreparationDevis-----");

    response = new Response<PieceDetacheeFichePreparationDevisDto>();

    try {
      List<PieceDetacheeFichePreparationDevis> items = null;
      items = pieceDetacheeFichePreparationDevisRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<PieceDetacheeFichePreparationDevisDto> itemsDto = new ArrayList<PieceDetacheeFichePreparationDevisDto>();
        for (PieceDetacheeFichePreparationDevis entity : items) {
          PieceDetacheeFichePreparationDevisDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(pieceDetacheeFichePreparationDevisRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("pieceDetacheeFichePreparationDevis", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get PieceDetacheeFichePreparationDevis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full PieceDetacheeFichePreparationDevisDto by using PieceDetacheeFichePreparationDevis as object.
   *
   * @param entity, locale
   * @return PieceDetacheeFichePreparationDevisDto
   *
   */
  private PieceDetacheeFichePreparationDevisDto getFullInfos(PieceDetacheeFichePreparationDevis entity, Integer size, Locale locale) throws Exception {
    PieceDetacheeFichePreparationDevisDto dto = PieceDetacheeFichePreparationDevisTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
