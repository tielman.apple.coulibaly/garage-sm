


                                                                                                              /*
 * Java transformer for entity table fiche_preparation_devis
 * Created on 2019-08-18 ( Time 17:15:11 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "fiche_preparation_devis"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class FichePreparationDevisBusiness implements IBasicBusiness<Request<FichePreparationDevisDto>, Response<FichePreparationDevisDto>> {

  private Response<FichePreparationDevisDto> response;
  @Autowired
  private FichePreparationDevisRepository fichePreparationDevisRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private OrdreReparationRepository ordreReparationRepository;
  @Autowired
  private UserRepository userVerificateurRepository;
  @Autowired
  private PieceDetacheeFichePreparationDevisRepository pieceDetacheeFichePreparationDevisRepository;
  @Autowired
  private DevisRepository devisRepository;
  @Autowired
  private EtatRepository etatRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                                  
  @Autowired
  private PieceDetacheeFichePreparationDevisBusiness pieceDetacheeFichePreparationDevisBusiness;

                                        
  @Autowired
  private DevisBusiness devisBusiness;

            

  public FichePreparationDevisBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create FichePreparationDevis by using FichePreparationDevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FichePreparationDevisDto> create(Request<FichePreparationDevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin create FichePreparationDevis-----");

    response = new Response<FichePreparationDevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FichePreparationDevis> items = new ArrayList<FichePreparationDevis>();

      for (FichePreparationDevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("ordreReparationId", dto.getOrdreReparationId());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        fieldsToVerify.put("etatId", dto.getEtatId());
        fieldsToVerify.put("userVerificateurId", dto.getUserVerificateurId());
        fieldsToVerify.put("userValideurId", dto.getUserValideurId());
        fieldsToVerify.put("isValidedByVerificateur", dto.getIsValidedByVerificateur());
        fieldsToVerify.put("isValidedByValideur", dto.getIsValidedByValideur());
        fieldsToVerify.put("dateValidationVerificateur", dto.getDateValidationVerificateur());
        fieldsToVerify.put("dateValidationValideur", dto.getDateValidationValideur());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if fichePreparationDevis to insert do not exist
        FichePreparationDevis existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("fichePreparationDevis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if ordreReparation exist
        OrdreReparation existingOrdreReparation = ordreReparationRepository.findById(dto.getOrdreReparationId(), false);
        if (existingOrdreReparation == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getOrdreReparationId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if userVerificateur exist
        User existingUserVerificateur = userVerificateurRepository.findById(dto.getUserVerificateurId(), false);
        if (existingUserVerificateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("userVerificateur -> " + dto.getUserVerificateurId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if user exist
        User existingUser = userRepository.findById(dto.getUserValideurId(), false);
        if (existingUser == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserValideurId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if etat exist
        Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
        if (existingEtat == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
          response.setHasError(true);
          return response;
        }
        FichePreparationDevis entityToSave = null;
        entityToSave = FichePreparationDevisTransformer.INSTANCE.toEntity(dto, existingOrdreReparation, existingUserVerificateur, existingUser, existingEtat);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<FichePreparationDevis> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = fichePreparationDevisRepository.save((Iterable<FichePreparationDevis>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("fichePreparationDevis", locale));
          response.setHasError(true);
          return response;
        }
        List<FichePreparationDevisDto> itemsDto = new ArrayList<FichePreparationDevisDto>();
        for (FichePreparationDevis entity : itemsSaved) {
          FichePreparationDevisDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create FichePreparationDevis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update FichePreparationDevis by using FichePreparationDevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FichePreparationDevisDto> update(Request<FichePreparationDevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin update FichePreparationDevis-----");

    response = new Response<FichePreparationDevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FichePreparationDevis> items = new ArrayList<FichePreparationDevis>();

      for (FichePreparationDevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la fichePreparationDevis existe
        FichePreparationDevis entityToSave = null;
        entityToSave = fichePreparationDevisRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("fichePreparationDevis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if ordreReparation exist
        if (dto.getOrdreReparationId() != null && dto.getOrdreReparationId() > 0){
          OrdreReparation existingOrdreReparation = ordreReparationRepository.findById(dto.getOrdreReparationId(), false);
          if (existingOrdreReparation == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getOrdreReparationId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setOrdreReparation(existingOrdreReparation);
        }
        // Verify if userVerificateur exist
        if (dto.getUserVerificateurId() != null && dto.getUserVerificateurId() > 0){
          User existingUserVerificateur = userVerificateurRepository.findById(dto.getUserVerificateurId(), false);
          if (existingUserVerificateur == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("userVerificateur -> " + dto.getUserVerificateurId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserVerificateur(existingUserVerificateur);
        }
        // Verify if user exist
        if (dto.getUserValideurId() != null && dto.getUserValideurId() > 0){
          User existingUserValideur = userRepository.findById(dto.getUserValideurId(), false);
          if (existingUserValideur == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserValideurId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setUserValideur(existingUserValideur);
        }
        // Verify if etat exist
        if (dto.getEtatId() != null && dto.getEtatId() > 0){
          Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
          if (existingEtat == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setEtat(existingEtat);
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        if (dto.getIsValidedByVerificateur() != null) {
          entityToSave.setIsValidedByVerificateur(dto.getIsValidedByVerificateur());
        }
        if (dto.getIsValidedByValideur() != null) {
          entityToSave.setIsValidedByValideur(dto.getIsValidedByValideur());
        }
        if (Utilities.notBlank(dto.getDateValidationVerificateur())) {
          entityToSave.setDateValidationVerificateur(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateValidationVerificateur()));
        }
        if (Utilities.notBlank(dto.getDateValidationValideur())) {
          entityToSave.setDateValidationValideur(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateValidationValideur()));
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<FichePreparationDevis> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = fichePreparationDevisRepository.save((Iterable<FichePreparationDevis>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("fichePreparationDevis", locale));
          response.setHasError(true);
          return response;
        }
        List<FichePreparationDevisDto> itemsDto = new ArrayList<FichePreparationDevisDto>();
        for (FichePreparationDevis entity : itemsSaved) {
          FichePreparationDevisDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update FichePreparationDevis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete FichePreparationDevis by using FichePreparationDevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<FichePreparationDevisDto> delete(Request<FichePreparationDevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete FichePreparationDevis-----");

    response = new Response<FichePreparationDevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FichePreparationDevis> items = new ArrayList<FichePreparationDevis>();

      for (FichePreparationDevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la fichePreparationDevis existe
        FichePreparationDevis existingEntity = null;
        existingEntity = fichePreparationDevisRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("fichePreparationDevis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

        // pieceDetacheeFichePreparationDevis
        List<PieceDetacheeFichePreparationDevis> listOfPieceDetacheeFichePreparationDevis = pieceDetacheeFichePreparationDevisRepository.findByFichePreparationDevisId(existingEntity.getId(), false);
        if (listOfPieceDetacheeFichePreparationDevis != null && !listOfPieceDetacheeFichePreparationDevis.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPieceDetacheeFichePreparationDevis.size() + ")", locale));
          response.setHasError(true);
          return response;
        }
        // devis
        List<Devis> listOfDevis = devisRepository.findByFichePreparationId(existingEntity.getId(), false);
        if (listOfDevis != null && !listOfDevis.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDevis.size() + ")", locale));
          response.setHasError(true);
          return response;
        }


        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        fichePreparationDevisRepository.save((Iterable<FichePreparationDevis>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete FichePreparationDevis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete FichePreparationDevis by using FichePreparationDevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<FichePreparationDevisDto> forceDelete(Request<FichePreparationDevisDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete FichePreparationDevis-----");

    response = new Response<FichePreparationDevisDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<FichePreparationDevis> items = new ArrayList<FichePreparationDevis>();

      for (FichePreparationDevisDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la fichePreparationDevis existe
        FichePreparationDevis existingEntity = null;
          existingEntity = fichePreparationDevisRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("fichePreparationDevis -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                                          // pieceDetacheeFichePreparationDevis
        List<PieceDetacheeFichePreparationDevis> listOfPieceDetacheeFichePreparationDevis = pieceDetacheeFichePreparationDevisRepository.findByFichePreparationDevisId(existingEntity.getId(), false);
        if (listOfPieceDetacheeFichePreparationDevis != null && !listOfPieceDetacheeFichePreparationDevis.isEmpty()){
          Request<PieceDetacheeFichePreparationDevisDto> deleteRequest = new Request<PieceDetacheeFichePreparationDevisDto>();
          deleteRequest.setDatas(PieceDetacheeFichePreparationDevisTransformer.INSTANCE.toDtos(listOfPieceDetacheeFichePreparationDevis));
          deleteRequest.setUser(request.getUser());
          Response<PieceDetacheeFichePreparationDevisDto> deleteResponse = pieceDetacheeFichePreparationDevisBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
                                                // devis
        List<Devis> listOfDevis = devisRepository.findByFichePreparationId(existingEntity.getId(), false);
        if (listOfDevis != null && !listOfDevis.isEmpty()){
          Request<DevisDto> deleteRequest = new Request<DevisDto>();
          deleteRequest.setDatas(DevisTransformer.INSTANCE.toDtos(listOfDevis));
          deleteRequest.setUser(request.getUser());
          Response<DevisDto> deleteResponse = devisBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
            

            existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
                                            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          fichePreparationDevisRepository.save((Iterable<FichePreparationDevis>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete FichePreparationDevis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get FichePreparationDevis by using FichePreparationDevisDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<FichePreparationDevisDto> getByCriteria(Request<FichePreparationDevisDto> request, Locale locale) {
    slf4jLogger.info("----begin get FichePreparationDevis-----");

    response = new Response<FichePreparationDevisDto>();

    try {
      List<FichePreparationDevis> items = null;
      items = fichePreparationDevisRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<FichePreparationDevisDto> itemsDto = new ArrayList<FichePreparationDevisDto>();
        for (FichePreparationDevis entity : items) {
          FichePreparationDevisDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(fichePreparationDevisRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("fichePreparationDevis", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get FichePreparationDevis-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full FichePreparationDevisDto by using FichePreparationDevis as object.
   *
   * @param entity, locale
   * @return FichePreparationDevisDto
   *
   */
  private FichePreparationDevisDto getFullInfos(FichePreparationDevis entity, Integer size, Locale locale) throws Exception {
    FichePreparationDevisDto dto = FichePreparationDevisTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
