


/*
 * Java transformer for entity table etat
 * Created on 2019-08-18 ( Time 17:15:10 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "etat"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class EtatBusiness implements IBasicBusiness<Request<EtatDto>, Response<EtatDto>> {

	private Response<EtatDto> response;
	@Autowired
	private EtatRepository etatRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private BonSortieRepository bonSortieRepository;
	@Autowired
	private DevisRepository devisRepository;
	@Autowired
	private FicheControlFinalRepository ficheControlFinalRepository;
	@Autowired
	private FichePreparationDevisRepository fichePreparationDevisRepository;
	@Autowired
	private OrdreReparationRepository ordreReparationRepository;
	@Autowired
	private FicheReceptionRepository ficheReceptionRepository;
	@Autowired
	private HistorisationRepository historisationRepository;
	@Autowired
	private FactureRepository factureRepository;
	@Autowired
	private BonDecaissementRepository bonDecaissementRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private BonSortieBusiness bonSortieBusiness;


	@Autowired
	private DevisBusiness devisBusiness;


	@Autowired
	private FicheControlFinalBusiness ficheControlFinalBusiness;


	@Autowired
	private FichePreparationDevisBusiness fichePreparationDevisBusiness;


	@Autowired
	private OrdreReparationBusiness ordreReparationBusiness;


	@Autowired
	private FicheReceptionBusiness ficheReceptionBusiness;


	@Autowired
	private HistorisationBusiness historisationBusiness;


	@Autowired
	private FactureBusiness factureBusiness;



	@Autowired
	private BonDecaissementBusiness bonDecaissementBusiness;



	public EtatBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Etat by using EtatDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EtatDto> create(Request<EtatDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Etat-----");

		response = new Response<EtatDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Etat> items = new ArrayList<Etat>();

			for (EtatDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("code", dto.getCode());
				fieldsToVerify.put("libelle", dto.getLibelle());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if etat to insert do not exist
				Etat existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("etat -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = etatRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("etat -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les etats", locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = etatRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("etat -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les etats", locale));
					response.setHasError(true);
					return response;
				}

				Etat entityToSave = null;
				entityToSave = EtatTransformer.INSTANCE.toEntity(dto);
				entityToSave.setIsDeleted(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Etat> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = etatRepository.save((Iterable<Etat>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("etat", locale));
					response.setHasError(true);
					return response;
				}
				List<EtatDto> itemsDto = new ArrayList<EtatDto>();
				for (Etat entity : itemsSaved) {
					EtatDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create Etat-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Etat by using EtatDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EtatDto> update(Request<EtatDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Etat-----");

		response = new Response<EtatDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Etat> items = new ArrayList<Etat>();

			for (EtatDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la etat existe
				Etat entityToSave = null;
				entityToSave = etatRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				if (Utilities.notBlank(dto.getCode())) {
					Etat existingEntity = etatRepository.findByCode(dto.getCode(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("etat -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les etats", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					Etat existingEntity = etatRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("etat -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les etats", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Etat> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = etatRepository.save((Iterable<Etat>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("etat", locale));
					response.setHasError(true);
					return response;
				}
				List<EtatDto> itemsDto = new ArrayList<EtatDto>();
				for (Etat entity : itemsSaved) {
					EtatDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update Etat-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete Etat by using EtatDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EtatDto> delete(Request<EtatDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Etat-----");

		response = new Response<EtatDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Etat> items = new ArrayList<Etat>();

			for (EtatDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la etat existe
				Etat existingEntity = null;
				existingEntity = etatRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// bonSortie
				List<BonSortie> listOfBonSortie = bonSortieRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfBonSortie != null && !listOfBonSortie.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfBonSortie.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// devis
				List<Devis> listOfDevis = devisRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfDevis != null && !listOfDevis.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDevis.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ficheControlFinal
				List<FicheControlFinal> listOfFicheControlFinal = ficheControlFinalRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfFicheControlFinal != null && !listOfFicheControlFinal.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFicheControlFinal.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// fichePreparationDevis
				List<FichePreparationDevis> listOfFichePreparationDevis = fichePreparationDevisRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfFichePreparationDevis != null && !listOfFichePreparationDevis.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFichePreparationDevis.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation = ordreReparationRepository.findByEtatOrdreId(existingEntity.getId(), false);
				if (listOfOrdreReparation != null && !listOfOrdreReparation.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfOrdreReparation.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ficheReception
				List<FicheReception> listOfFicheReception = ficheReceptionRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfFicheReception != null && !listOfFicheReception.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFicheReception.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// historisation
				List<Historisation> listOfHistorisation = historisationRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfHistorisation != null && !listOfHistorisation.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistorisation.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// facture
				List<Facture> listOfFacture = factureRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfFacture != null && !listOfFacture.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFacture.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation2 = ordreReparationRepository.findByEtatOrdreId(existingEntity.getId(), false);
				if (listOfOrdreReparation2 != null && !listOfOrdreReparation2.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfOrdreReparation2.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// bonDecaissement
				List<BonDecaissement> listOfBonDecaissement = bonDecaissementRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfBonDecaissement != null && !listOfBonDecaissement.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfBonDecaissement.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				etatRepository.save((Iterable<Etat>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Etat-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Etat by using EtatDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<EtatDto> forceDelete(Request<EtatDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Etat-----");

		response = new Response<EtatDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Etat> items = new ArrayList<Etat>();

			for (EtatDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la etat existe
				Etat existingEntity = null;
				existingEntity = etatRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// bonSortie
				List<BonSortie> listOfBonSortie = bonSortieRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfBonSortie != null && !listOfBonSortie.isEmpty()){
					Request<BonSortieDto> deleteRequest = new Request<BonSortieDto>();
					deleteRequest.setDatas(BonSortieTransformer.INSTANCE.toDtos(listOfBonSortie));
					deleteRequest.setUser(request.getUser());
					Response<BonSortieDto> deleteResponse = bonSortieBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// devis
				List<Devis> listOfDevis = devisRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfDevis != null && !listOfDevis.isEmpty()){
					Request<DevisDto> deleteRequest = new Request<DevisDto>();
					deleteRequest.setDatas(DevisTransformer.INSTANCE.toDtos(listOfDevis));
					deleteRequest.setUser(request.getUser());
					Response<DevisDto> deleteResponse = devisBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ficheControlFinal
				List<FicheControlFinal> listOfFicheControlFinal = ficheControlFinalRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfFicheControlFinal != null && !listOfFicheControlFinal.isEmpty()){
					Request<FicheControlFinalDto> deleteRequest = new Request<FicheControlFinalDto>();
					deleteRequest.setDatas(FicheControlFinalTransformer.INSTANCE.toDtos(listOfFicheControlFinal));
					deleteRequest.setUser(request.getUser());
					Response<FicheControlFinalDto> deleteResponse = ficheControlFinalBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// fichePreparationDevis
				List<FichePreparationDevis> listOfFichePreparationDevis = fichePreparationDevisRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfFichePreparationDevis != null && !listOfFichePreparationDevis.isEmpty()){
					Request<FichePreparationDevisDto> deleteRequest = new Request<FichePreparationDevisDto>();
					deleteRequest.setDatas(FichePreparationDevisTransformer.INSTANCE.toDtos(listOfFichePreparationDevis));
					deleteRequest.setUser(request.getUser());
					Response<FichePreparationDevisDto> deleteResponse = fichePreparationDevisBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation = ordreReparationRepository.findByEtatOrdreId(existingEntity.getId(), false);
				if (listOfOrdreReparation != null && !listOfOrdreReparation.isEmpty()){
					Request<OrdreReparationDto> deleteRequest = new Request<OrdreReparationDto>();
					deleteRequest.setDatas(OrdreReparationTransformer.INSTANCE.toDtos(listOfOrdreReparation));
					deleteRequest.setUser(request.getUser());
					Response<OrdreReparationDto> deleteResponse = ordreReparationBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ficheReception
				List<FicheReception> listOfFicheReception = ficheReceptionRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfFicheReception != null && !listOfFicheReception.isEmpty()){
					Request<FicheReceptionDto> deleteRequest = new Request<FicheReceptionDto>();
					deleteRequest.setDatas(FicheReceptionTransformer.INSTANCE.toDtos(listOfFicheReception));
					deleteRequest.setUser(request.getUser());
					Response<FicheReceptionDto> deleteResponse = ficheReceptionBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// historisation
				List<Historisation> listOfHistorisation = historisationRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfHistorisation != null && !listOfHistorisation.isEmpty()){
					Request<HistorisationDto> deleteRequest = new Request<HistorisationDto>();
					deleteRequest.setDatas(HistorisationTransformer.INSTANCE.toDtos(listOfHistorisation));
					deleteRequest.setUser(request.getUser());
					Response<HistorisationDto> deleteResponse = historisationBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// facture
				List<Facture> listOfFacture = factureRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfFacture != null && !listOfFacture.isEmpty()){
					Request<FactureDto> deleteRequest = new Request<FactureDto>();
					deleteRequest.setDatas(FactureTransformer.INSTANCE.toDtos(listOfFacture));
					deleteRequest.setUser(request.getUser());
					Response<FactureDto> deleteResponse = factureBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// ordreReparation
				List<OrdreReparation> listOfOrdreReparation2 = ordreReparationRepository.findByEtatOrdreId(existingEntity.getId(), false);
				if (listOfOrdreReparation2 != null && !listOfOrdreReparation2.isEmpty()){
					Request<OrdreReparationDto> deleteRequest = new Request<OrdreReparationDto>();
					deleteRequest.setDatas(OrdreReparationTransformer.INSTANCE.toDtos(listOfOrdreReparation2));
					deleteRequest.setUser(request.getUser());
					Response<OrdreReparationDto> deleteResponse = ordreReparationBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// bonDecaissement
				List<BonDecaissement> listOfBonDecaissement = bonDecaissementRepository.findByEtatId(existingEntity.getId(), false);
				if (listOfBonDecaissement != null && !listOfBonDecaissement.isEmpty()){
					Request<BonDecaissementDto> deleteRequest = new Request<BonDecaissementDto>();
					deleteRequest.setDatas(BonDecaissementTransformer.INSTANCE.toDtos(listOfBonDecaissement));
					deleteRequest.setUser(request.getUser());
					Response<BonDecaissementDto> deleteResponse = bonDecaissementBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				etatRepository.save((Iterable<Etat>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Etat-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get Etat by using EtatDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<EtatDto> getByCriteria(Request<EtatDto> request, Locale locale) {
		slf4jLogger.info("----begin get Etat-----");

		response = new Response<EtatDto>();

		try {
			List<Etat> items = null;
			items = etatRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<EtatDto> itemsDto = new ArrayList<EtatDto>();
				for (Etat entity : items) {
					EtatDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(etatRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("etat", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Etat-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full EtatDto by using Etat as object.
	 *
	 * @param entity, locale
	 * @return EtatDto
	 *
	 */
	private EtatDto getFullInfos(Etat entity, Integer size, Locale locale) throws Exception {
		EtatDto dto = EtatTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
