
/*
 * Java dto for entity table element_etat_vehicule 
 * Created on 2019-08-19 ( Time 10:29:46 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * DTO customize for table "element_etat_vehicule"
 * 
 * @author SFL Back-End developper
 *
 */
@JsonInclude(Include.NON_NULL)
public class _ElementEtatVehiculeDto {
	
	protected Boolean isGood ;

	public Boolean getIsGood() {
		return isGood;
	}

	public void setIsGood(Boolean isGood) {
		this.isGood = isGood;
	}
	
	

}
