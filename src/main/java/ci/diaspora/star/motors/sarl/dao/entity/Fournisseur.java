/*
 * Created on 2019-08-18 ( Time 17:15:11 )
 * Generated by Telosys Tools Generator ( version 3.0.0 )
 */
// This Bean has a basic Primary Key (not composite) 

package ci.diaspora.star.motors.sarl.dao.entity;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "fournisseur"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="fournisseur" )
public class Fournisseur implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
	/*
	 * 
	 */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
	/*
	 * 
	 */
    @Column(name="nom", length=255)
    private String     nom          ;

	/*
	 * 
	 */
    @Column(name="prenom", length=255)
    private String     prenom       ;

	/*
	 * 
	 */
    @Column(name="matricule", length=255)
    private String     matricule    ;

	/*
	 * 
	 */
    @Column(name="telephone", length=255)
    private String     telephone    ;

	/*
	 * 
	 */
    @Column(name="email", length=255)
    private String     email        ;

	/*
	 * 
	 */
    @Column(name="is_deleted")
    private Boolean    isDeleted    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at")
    private Date       createdAt    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at")
    private Date       updatedAt    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="deleted_at")
    private Date       deletedAt    ;

	/*
	 * 
	 */
    @Column(name="created_by")
    private Integer    createdBy    ;

	/*
	 * 
	 */
    @Column(name="updated_by")
    private Integer    updatedBy    ;

	/*
	 * 
	 */
    @Column(name="deleted_by")
    private Integer    deletedBy    ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public Fournisseur() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Integer id ) {
        this.id = id ;
    }
    public Integer getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : nom ( VARCHAR ) 
    public void setNom( String nom ) {
        this.nom = nom;
    }
    public String getNom() {
        return this.nom;
    }

    //--- DATABASE MAPPING : prenom ( VARCHAR ) 
    public void setPrenom( String prenom ) {
        this.prenom = prenom;
    }
    public String getPrenom() {
        return this.prenom;
    }

    //--- DATABASE MAPPING : matricule ( VARCHAR ) 
    public void setMatricule( String matricule ) {
        this.matricule = matricule;
    }
    public String getMatricule() {
        return this.matricule;
    }

    //--- DATABASE MAPPING : telephone ( VARCHAR ) 
    public void setTelephone( String telephone ) {
        this.telephone = telephone;
    }
    public String getTelephone() {
        return this.telephone;
    }

    //--- DATABASE MAPPING : email ( VARCHAR ) 
    public void setEmail( String email ) {
        this.email = email;
    }
    public String getEmail() {
        return this.email;
    }

    //--- DATABASE MAPPING : is_deleted ( BIT ) 
    public void setIsDeleted( Boolean isDeleted ) {
        this.isDeleted = isDeleted;
    }
    public Boolean getIsDeleted() {
        return this.isDeleted;
    }

    //--- DATABASE MAPPING : created_at ( DATETIME ) 
    public void setCreatedAt( Date createdAt ) {
        this.createdAt = createdAt;
    }
    public Date getCreatedAt() {
        return this.createdAt;
    }

    //--- DATABASE MAPPING : updated_at ( DATETIME ) 
    public void setUpdatedAt( Date updatedAt ) {
        this.updatedAt = updatedAt;
    }
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    //--- DATABASE MAPPING : deleted_at ( DATETIME ) 
    public void setDeletedAt( Date deletedAt ) {
        this.deletedAt = deletedAt;
    }
    public Date getDeletedAt() {
        return this.deletedAt;
    }

    //--- DATABASE MAPPING : created_by ( INT ) 
    public void setCreatedBy( Integer createdBy ) {
        this.createdBy = createdBy;
    }
    public Integer getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : updated_by ( INT ) 
    public void setUpdatedBy( Integer updatedBy ) {
        this.updatedBy = updatedBy;
    }
    public Integer getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : deleted_by ( INT ) 
    public void setDeletedBy( Integer deletedBy ) {
        this.deletedBy = deletedBy;
    }
    public Integer getDeletedBy() {
        return this.deletedBy;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(nom);
        sb.append("|");
        sb.append(prenom);
        sb.append("|");
        sb.append(matricule);
        sb.append("|");
        sb.append(telephone);
        sb.append("|");
        sb.append(email);
        sb.append("|");
        sb.append(isDeleted);
        sb.append("|");
        sb.append(createdAt);
        sb.append("|");
        sb.append(updatedAt);
        sb.append("|");
        sb.append(deletedAt);
        sb.append("|");
        sb.append(createdBy);
        sb.append("|");
        sb.append(updatedBy);
        sb.append("|");
        sb.append(deletedBy);
        return sb.toString(); 
    } 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
