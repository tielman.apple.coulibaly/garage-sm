package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._ClientRepository;

/**
 * Repository : Client.
 */
@Repository
public interface ClientRepository extends JpaRepository<Client, Integer>, _ClientRepository {
	/**
	 * Finds Client by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Client whose id is equals to the given id. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.id = :id and e.isDeleted = :isDeleted")
	Client findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Client by using matricule as a search criteria.
	 *
	 * @param matricule
	 * @return An Object Client whose matricule is equals to the given matricule. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.matricule = :matricule and e.isDeleted = :isDeleted")
	List<Client> findByMatricule(@Param("matricule")String matricule, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using identificationProprietaire as a search criteria.
	 *
	 * @param identificationProprietaire
	 * @return An Object Client whose identificationProprietaire is equals to the given identificationProprietaire. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.identificationProprietaire = :identificationProprietaire and e.isDeleted = :isDeleted")
	List<Client> findByIdentificationProprietaire(@Param("identificationProprietaire")String identificationProprietaire, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using email as a search criteria.
	 *
	 * @param email
	 * @return An Object Client whose email is equals to the given email. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.email = :email and e.isDeleted = :isDeleted")
	Client findByEmail(@Param("email")String email, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using adresseGeographique as a search criteria.
	 *
	 * @param adresseGeographique
	 * @return An Object Client whose adresseGeographique is equals to the given adresseGeographique. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.adresseGeographique = :adresseGeographique and e.isDeleted = :isDeleted")
	List<Client> findByAdresseGeographique(@Param("adresseGeographique")String adresseGeographique, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using telephone as a search criteria.
	 *
	 * @param telephone
	 * @return An Object Client whose telephone is equals to the given telephone. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.telephone = :telephone and e.isDeleted = :isDeleted")
	List<Client> findByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using isOld as a search criteria.
	 *
	 * @param isOld
	 * @return An Object Client whose isOld is equals to the given isOld. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.isOld = :isOld and e.isDeleted = :isDeleted")
	List<Client> findByIsOld(@Param("isOld")Boolean isOld, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Client whose isDeleted is equals to the given isDeleted. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.isDeleted = :isDeleted")
	List<Client> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Client whose createdAt is equals to the given createdAt. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Client> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Client whose updatedAt is equals to the given updatedAt. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Client> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Client whose deletedAt is equals to the given deletedAt. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Client> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Client whose createdBy is equals to the given createdBy. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Client> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Client whose updatedBy is equals to the given updatedBy. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Client> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Client by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Client whose deletedBy is equals to the given deletedBy. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Client> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Client by using typeClientId as a search criteria.
	 *
	 * @param typeClientId
	 * @return A list of Object Client whose typeClientId is equals to the given typeClientId. If
	 *         no Client is found, this method returns null.
	 */
	@Query("select e from Client e where e.typeClient.id = :typeClientId and e.isDeleted = :isDeleted")
	List<Client> findByTypeClientId(@Param("typeClientId")Integer typeClientId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Client by using typeClientId as a search criteria.
   *
   * @param typeClientId
   * @return An Object Client whose typeClientId is equals to the given typeClientId. If
   *         no Client is found, this method returns null.
   */
  @Query("select e from Client e where e.typeClient.id = :typeClientId and e.isDeleted = :isDeleted")
  Client findClientByTypeClientId(@Param("typeClientId")Integer typeClientId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Client by using clientDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Client
	 * @throws DataAccessException,ParseException
	 */
	public default List<Client> getByCriteria(Request<ClientDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Client e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Client> query = em.createQuery(req, Client.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Client by using clientDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Client
	 *
	 */
	public default Long count(Request<ClientDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Client e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ClientDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ClientDto dto = request.getData() != null ? request.getData() : new ClientDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ClientDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ClientDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMatricule())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("matricule", dto.getMatricule(), "e.matricule", "String", dto.getMatriculeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getIdentificationProprietaire())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("identificationProprietaire", dto.getIdentificationProprietaire(), "e.identificationProprietaire", "String", dto.getIdentificationProprietaireParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmail())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("email", dto.getEmail(), "e.email", "String", dto.getEmailParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAdresseGeographique())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("adresseGeographique", dto.getAdresseGeographique(), "e.adresseGeographique", "String", dto.getAdresseGeographiqueParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephone())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephone", dto.getTelephone(), "e.telephone", "String", dto.getTelephoneParam(), param, index, locale));
			}
			if (dto.getIsOld()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isOld", dto.getIsOld(), "e.isOld", "Boolean", dto.getIsOldParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getTypeClientId()!= null && dto.getTypeClientId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeClientId", dto.getTypeClientId(), "e.typeClient.id", "Integer", dto.getTypeClientIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeClientLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeClientLibelle", dto.getTypeClientLibelle(), "e.typeClient.libelle", "String", dto.getTypeClientLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
