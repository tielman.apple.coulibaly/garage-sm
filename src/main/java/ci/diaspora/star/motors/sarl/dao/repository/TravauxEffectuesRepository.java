package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._TravauxEffectuesRepository;

/**
 * Repository : TravauxEffectues.
 */
@Repository
public interface TravauxEffectuesRepository extends JpaRepository<TravauxEffectues, Integer>, _TravauxEffectuesRepository {
	/**
	 * Finds TravauxEffectues by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object TravauxEffectues whose id is equals to the given id. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.id = :id and e.isDeleted = :isDeleted")
	TravauxEffectues findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds TravauxEffectues by using dateDebutTravaux as a search criteria.
	 *
	 * @param dateDebutTravaux
	 * @return An Object TravauxEffectues whose dateDebutTravaux is equals to the given dateDebutTravaux. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.dateDebutTravaux = :dateDebutTravaux and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByDateDebutTravaux(@Param("dateDebutTravaux")Date dateDebutTravaux, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TravauxEffectues by using dateFinTravaux as a search criteria.
	 *
	 * @param dateFinTravaux
	 * @return An Object TravauxEffectues whose dateFinTravaux is equals to the given dateFinTravaux. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.dateFinTravaux = :dateFinTravaux and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByDateFinTravaux(@Param("dateFinTravaux")Date dateFinTravaux, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TravauxEffectues by using isSigned as a search criteria.
	 *
	 * @param isSigned
	 * @return An Object TravauxEffectues whose isSigned is equals to the given isSigned. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.isSigned = :isSigned and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByIsSigned(@Param("isSigned")Boolean isSigned, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TravauxEffectues by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object TravauxEffectues whose isDeleted is equals to the given isDeleted. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TravauxEffectues by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object TravauxEffectues whose createdAt is equals to the given createdAt. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TravauxEffectues by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object TravauxEffectues whose updatedAt is equals to the given updatedAt. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TravauxEffectues by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object TravauxEffectues whose deletedAt is equals to the given deletedAt. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TravauxEffectues by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object TravauxEffectues whose createdBy is equals to the given createdBy. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TravauxEffectues by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object TravauxEffectues whose updatedBy is equals to the given updatedBy. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds TravauxEffectues by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object TravauxEffectues whose deletedBy is equals to the given deletedBy. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds TravauxEffectues by using designationTravauxEffectuesId as a search criteria.
	 *
	 * @param designationTravauxEffectuesId
	 * @return A list of Object TravauxEffectues whose designationTravauxEffectuesId is equals to the given designationTravauxEffectuesId. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.designationTravauxEffectues.id = :designationTravauxEffectuesId and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByDesignationTravauxEffectuesId(@Param("designationTravauxEffectuesId")Integer designationTravauxEffectuesId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one TravauxEffectues by using designationTravauxEffectuesId as a search criteria.
   *
   * @param designationTravauxEffectuesId
   * @return An Object TravauxEffectues whose designationTravauxEffectuesId is equals to the given designationTravauxEffectuesId. If
   *         no TravauxEffectues is found, this method returns null.
   */
  @Query("select e from TravauxEffectues e where e.designationTravauxEffectues.id = :designationTravauxEffectuesId and e.isDeleted = :isDeleted")
  TravauxEffectues findTravauxEffectuesByDesignationTravauxEffectuesId(@Param("designationTravauxEffectuesId")Integer designationTravauxEffectuesId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds TravauxEffectues by using userTechnicienId as a search criteria.
	 *
	 * @param userTechnicienId
	 * @return A list of Object TravauxEffectues whose userTechnicienId is equals to the given userTechnicienId. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.user.id = :userTechnicienId and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByUserTechnicienId(@Param("userTechnicienId")Integer userTechnicienId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one TravauxEffectues by using userTechnicienId as a search criteria.
   *
   * @param userTechnicienId
   * @return An Object TravauxEffectues whose userTechnicienId is equals to the given userTechnicienId. If
   *         no TravauxEffectues is found, this method returns null.
   */
  @Query("select e from TravauxEffectues e where e.user.id = :userTechnicienId and e.isDeleted = :isDeleted")
  TravauxEffectues findTravauxEffectuesByUserTechnicienId(@Param("userTechnicienId")Integer userTechnicienId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds TravauxEffectues by using ordreReparationId as a search criteria.
	 *
	 * @param ordreReparationId
	 * @return A list of Object TravauxEffectues whose ordreReparationId is equals to the given ordreReparationId. If
	 *         no TravauxEffectues is found, this method returns null.
	 */
	@Query("select e from TravauxEffectues e where e.ordreReparation.id = :ordreReparationId and e.isDeleted = :isDeleted")
	List<TravauxEffectues> findByOrdreReparationId(@Param("ordreReparationId")Integer ordreReparationId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one TravauxEffectues by using ordreReparationId as a search criteria.
   *
   * @param ordreReparationId
   * @return An Object TravauxEffectues whose ordreReparationId is equals to the given ordreReparationId. If
   *         no TravauxEffectues is found, this method returns null.
   */
  @Query("select e from TravauxEffectues e where e.ordreReparation.id = :ordreReparationId and e.isDeleted = :isDeleted")
  TravauxEffectues findTravauxEffectuesByOrdreReparationId(@Param("ordreReparationId")Integer ordreReparationId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of TravauxEffectues by using travauxEffectuesDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of TravauxEffectues
	 * @throws DataAccessException,ParseException
	 */
	public default List<TravauxEffectues> getByCriteria(Request<TravauxEffectuesDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from TravauxEffectues e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<TravauxEffectues> query = em.createQuery(req, TravauxEffectues.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of TravauxEffectues by using travauxEffectuesDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of TravauxEffectues
	 *
	 */
	public default Long count(Request<TravauxEffectuesDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from TravauxEffectues e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<TravauxEffectuesDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		TravauxEffectuesDto dto = request.getData() != null ? request.getData() : new TravauxEffectuesDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (TravauxEffectuesDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(TravauxEffectuesDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getDateDebutTravaux()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateDebutTravaux", dto.getDateDebutTravaux(), "e.dateDebutTravaux", "Date", dto.getDateDebutTravauxParam(), param, index, locale));
			}
			if (dto.getDateFinTravaux()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateFinTravaux", dto.getDateFinTravaux(), "e.dateFinTravaux", "Date", dto.getDateFinTravauxParam(), param, index, locale));
			}
			if (dto.getIsSigned()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isSigned", dto.getIsSigned(), "e.isSigned", "Boolean", dto.getIsSignedParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getDesignationTravauxEffectuesId()!= null && dto.getDesignationTravauxEffectuesId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("designationTravauxEffectuesId", dto.getDesignationTravauxEffectuesId(), "e.designationTravauxEffectues.id", "Integer", dto.getDesignationTravauxEffectuesIdParam(), param, index, locale));
			}
			if (dto.getUserTechnicienId()!= null && dto.getUserTechnicienId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userTechnicienId", dto.getUserTechnicienId(), "e.user.id", "Integer", dto.getUserTechnicienIdParam(), param, index, locale));
			}
			if (dto.getOrdreReparationId()!= null && dto.getOrdreReparationId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ordreReparationId", dto.getOrdreReparationId(), "e.ordreReparation.id", "Integer", dto.getOrdreReparationIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDesignationTravauxEffectuesLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("designationTravauxEffectuesLibelle", dto.getDesignationTravauxEffectuesLibelle(), "e.designationTravauxEffectues.libelle", "String", dto.getDesignationTravauxEffectuesLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenom", dto.getUserPrenom(), "e.user.prenom", "String", dto.getUserPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
