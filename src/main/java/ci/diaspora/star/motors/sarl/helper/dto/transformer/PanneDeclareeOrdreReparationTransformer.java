

/*
 * Java transformer for entity table panne_declaree_ordre_reparation 
 * Created on 2019-08-18 ( Time 17:15:13 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "panne_declaree_ordre_reparation"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface PanneDeclareeOrdreReparationTransformer {

	PanneDeclareeOrdreReparationTransformer INSTANCE = Mappers.getMapper(PanneDeclareeOrdreReparationTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.ordreReparation.id", target="ordreReparationId"),
		@Mapping(source="entity.panneDeclaree.id", target="panneDeclareeId"),
		@Mapping(source="entity.panneDeclaree.libelle", target="panneDeclareeLibelle"),
	})
	PanneDeclareeOrdreReparationDto toDto(PanneDeclareeOrdreReparation entity) throws ParseException;

    List<PanneDeclareeOrdreReparationDto> toDtos(List<PanneDeclareeOrdreReparation> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.observations", target="observations"),
		@Mapping(source="dto.diagnosticAtelier", target="diagnosticAtelier"),
		@Mapping(source="dto.isDeclaredByClient", target="isDeclaredByClient"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="ordreReparation", target="ordreReparation"),
		@Mapping(source="panneDeclaree", target="panneDeclaree"),
	})
    PanneDeclareeOrdreReparation toEntity(PanneDeclareeOrdreReparationDto dto, OrdreReparation ordreReparation, PanneDeclaree panneDeclaree) throws ParseException;

    //List<PanneDeclareeOrdreReparation> toEntities(List<PanneDeclareeOrdreReparationDto> dtos) throws ParseException;

}
