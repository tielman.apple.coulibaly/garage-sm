


                                                                                            /*
 * Java transformer for entity table panne_declaree_ordre_reparation
 * Created on 2019-08-18 ( Time 17:15:13 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "panne_declaree_ordre_reparation"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class PanneDeclareeOrdreReparationBusiness implements IBasicBusiness<Request<PanneDeclareeOrdreReparationDto>, Response<PanneDeclareeOrdreReparationDto>> {

  private Response<PanneDeclareeOrdreReparationDto> response;
  @Autowired
  private PanneDeclareeOrdreReparationRepository panneDeclareeOrdreReparationRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private OrdreReparationRepository ordreReparationRepository;
  @Autowired
  private PanneDeclareeRepository panneDeclareeRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

          

  public PanneDeclareeOrdreReparationBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create PanneDeclareeOrdreReparation by using PanneDeclareeOrdreReparationDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PanneDeclareeOrdreReparationDto> create(Request<PanneDeclareeOrdreReparationDto> request, Locale locale)  {
    slf4jLogger.info("----begin create PanneDeclareeOrdreReparation-----");

    response = new Response<PanneDeclareeOrdreReparationDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PanneDeclareeOrdreReparation> items = new ArrayList<PanneDeclareeOrdreReparation>();

      for (PanneDeclareeOrdreReparationDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("panneDeclareeId", dto.getPanneDeclareeId());
        fieldsToVerify.put("ordreReparationId", dto.getOrdreReparationId());
        fieldsToVerify.put("observations", dto.getObservations());
        fieldsToVerify.put("diagnosticAtelier", dto.getDiagnosticAtelier());
        fieldsToVerify.put("isDeclaredByClient", dto.getIsDeclaredByClient());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if panneDeclareeOrdreReparation to insert do not exist
        PanneDeclareeOrdreReparation existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("panneDeclareeOrdreReparation -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if ordreReparation exist
        OrdreReparation existingOrdreReparation = ordreReparationRepository.findById(dto.getOrdreReparationId(), false);
        if (existingOrdreReparation == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getOrdreReparationId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if panneDeclaree exist
        PanneDeclaree existingPanneDeclaree = panneDeclareeRepository.findById(dto.getPanneDeclareeId(), false);
        if (existingPanneDeclaree == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("panneDeclaree -> " + dto.getPanneDeclareeId(), locale));
          response.setHasError(true);
          return response;
        }
        PanneDeclareeOrdreReparation entityToSave = null;
        entityToSave = PanneDeclareeOrdreReparationTransformer.INSTANCE.toEntity(dto, existingOrdreReparation, existingPanneDeclaree);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<PanneDeclareeOrdreReparation> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = panneDeclareeOrdreReparationRepository.save((Iterable<PanneDeclareeOrdreReparation>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("panneDeclareeOrdreReparation", locale));
          response.setHasError(true);
          return response;
        }
        List<PanneDeclareeOrdreReparationDto> itemsDto = new ArrayList<PanneDeclareeOrdreReparationDto>();
        for (PanneDeclareeOrdreReparation entity : itemsSaved) {
          PanneDeclareeOrdreReparationDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create PanneDeclareeOrdreReparation-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update PanneDeclareeOrdreReparation by using PanneDeclareeOrdreReparationDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PanneDeclareeOrdreReparationDto> update(Request<PanneDeclareeOrdreReparationDto> request, Locale locale)  {
    slf4jLogger.info("----begin update PanneDeclareeOrdreReparation-----");

    response = new Response<PanneDeclareeOrdreReparationDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PanneDeclareeOrdreReparation> items = new ArrayList<PanneDeclareeOrdreReparation>();

      for (PanneDeclareeOrdreReparationDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la panneDeclareeOrdreReparation existe
        PanneDeclareeOrdreReparation entityToSave = null;
        entityToSave = panneDeclareeOrdreReparationRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("panneDeclareeOrdreReparation -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if ordreReparation exist
        if (dto.getOrdreReparationId() != null && dto.getOrdreReparationId() > 0){
          OrdreReparation existingOrdreReparation = ordreReparationRepository.findById(dto.getOrdreReparationId(), false);
          if (existingOrdreReparation == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("ordreReparation -> " + dto.getOrdreReparationId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setOrdreReparation(existingOrdreReparation);
        }
        // Verify if panneDeclaree exist
        if (dto.getPanneDeclareeId() != null && dto.getPanneDeclareeId() > 0){
          PanneDeclaree existingPanneDeclaree = panneDeclareeRepository.findById(dto.getPanneDeclareeId(), false);
          if (existingPanneDeclaree == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("panneDeclaree -> " + dto.getPanneDeclareeId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setPanneDeclaree(existingPanneDeclaree);
        }
        if (Utilities.notBlank(dto.getObservations())) {
                  entityToSave.setObservations(dto.getObservations());
        }
        if (Utilities.notBlank(dto.getDiagnosticAtelier())) {
                  entityToSave.setDiagnosticAtelier(dto.getDiagnosticAtelier());
        }
        if (dto.getIsDeclaredByClient() != null) {
          entityToSave.setIsDeclaredByClient(dto.getIsDeclaredByClient());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<PanneDeclareeOrdreReparation> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = panneDeclareeOrdreReparationRepository.save((Iterable<PanneDeclareeOrdreReparation>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("panneDeclareeOrdreReparation", locale));
          response.setHasError(true);
          return response;
        }
        List<PanneDeclareeOrdreReparationDto> itemsDto = new ArrayList<PanneDeclareeOrdreReparationDto>();
        for (PanneDeclareeOrdreReparation entity : itemsSaved) {
          PanneDeclareeOrdreReparationDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update PanneDeclareeOrdreReparation-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete PanneDeclareeOrdreReparation by using PanneDeclareeOrdreReparationDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PanneDeclareeOrdreReparationDto> delete(Request<PanneDeclareeOrdreReparationDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete PanneDeclareeOrdreReparation-----");

    response = new Response<PanneDeclareeOrdreReparationDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PanneDeclareeOrdreReparation> items = new ArrayList<PanneDeclareeOrdreReparation>();

      for (PanneDeclareeOrdreReparationDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la panneDeclareeOrdreReparation existe
        PanneDeclareeOrdreReparation existingEntity = null;
        existingEntity = panneDeclareeOrdreReparationRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("panneDeclareeOrdreReparation -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        panneDeclareeOrdreReparationRepository.save((Iterable<PanneDeclareeOrdreReparation>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete PanneDeclareeOrdreReparation-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete PanneDeclareeOrdreReparation by using PanneDeclareeOrdreReparationDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<PanneDeclareeOrdreReparationDto> forceDelete(Request<PanneDeclareeOrdreReparationDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete PanneDeclareeOrdreReparation-----");

    response = new Response<PanneDeclareeOrdreReparationDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PanneDeclareeOrdreReparation> items = new ArrayList<PanneDeclareeOrdreReparation>();

      for (PanneDeclareeOrdreReparationDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la panneDeclareeOrdreReparation existe
        PanneDeclareeOrdreReparation existingEntity = null;
          existingEntity = panneDeclareeOrdreReparationRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("panneDeclareeOrdreReparation -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

          

                                    existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          panneDeclareeOrdreReparationRepository.save((Iterable<PanneDeclareeOrdreReparation>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete PanneDeclareeOrdreReparation-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get PanneDeclareeOrdreReparation by using PanneDeclareeOrdreReparationDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<PanneDeclareeOrdreReparationDto> getByCriteria(Request<PanneDeclareeOrdreReparationDto> request, Locale locale) {
    slf4jLogger.info("----begin get PanneDeclareeOrdreReparation-----");

    response = new Response<PanneDeclareeOrdreReparationDto>();

    try {
      List<PanneDeclareeOrdreReparation> items = null;
      items = panneDeclareeOrdreReparationRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<PanneDeclareeOrdreReparationDto> itemsDto = new ArrayList<PanneDeclareeOrdreReparationDto>();
        for (PanneDeclareeOrdreReparation entity : items) {
          PanneDeclareeOrdreReparationDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(panneDeclareeOrdreReparationRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("panneDeclareeOrdreReparation", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get PanneDeclareeOrdreReparation-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full PanneDeclareeOrdreReparationDto by using PanneDeclareeOrdreReparation as object.
   *
   * @param entity, locale
   * @return PanneDeclareeOrdreReparationDto
   *
   */
  private PanneDeclareeOrdreReparationDto getFullInfos(PanneDeclareeOrdreReparation entity, Integer size, Locale locale) throws Exception {
    PanneDeclareeOrdreReparationDto dto = PanneDeclareeOrdreReparationTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
