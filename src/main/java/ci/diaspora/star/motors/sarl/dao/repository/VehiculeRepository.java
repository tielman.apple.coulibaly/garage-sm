package ci.diaspora.star.motors.sarl.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.dao.repository.customize._VehiculeRepository;

/**
 * Repository : Vehicule.
 */
@Repository
public interface VehiculeRepository extends JpaRepository<Vehicule, Integer>, _VehiculeRepository {
	/**
	 * Finds Vehicule by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Vehicule whose id is equals to the given id. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.id = :id and e.isDeleted = :isDeleted")
	Vehicule findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Vehicule by using immatriculation as a search criteria.
	 *
	 * @param immatriculation
	 * @return An Object Vehicule whose immatriculation is equals to the given immatriculation. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.immatriculation = :immatriculation and e.isDeleted = :isDeleted")
	List<Vehicule> findByImmatriculation(@Param("immatriculation")String immatriculation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using numeroChassis as a search criteria.
	 *
	 * @param numeroChassis
	 * @return An Object Vehicule whose numeroChassis is equals to the given numeroChassis. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.numeroChassis = :numeroChassis and e.isDeleted = :isDeleted")
	List<Vehicule> findByNumeroChassis(@Param("numeroChassis")String numeroChassis, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using dateProchaineRevision as a search criteria.
	 *
	 * @param dateProchaineRevision
	 * @return An Object Vehicule whose dateProchaineRevision is equals to the given dateProchaineRevision. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.dateProchaineRevision = :dateProchaineRevision and e.isDeleted = :isDeleted")
	List<Vehicule> findByDateProchaineRevision(@Param("dateProchaineRevision")Date dateProchaineRevision, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using dateVisiteTechnique as a search criteria.
	 *
	 * @param dateVisiteTechnique
	 * @return An Object Vehicule whose dateVisiteTechnique is equals to the given dateVisiteTechnique. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.dateVisiteTechnique = :dateVisiteTechnique and e.isDeleted = :isDeleted")
	List<Vehicule> findByDateVisiteTechnique(@Param("dateVisiteTechnique")Date dateVisiteTechnique, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Vehicule whose isDeleted is equals to the given isDeleted. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.isDeleted = :isDeleted")
	List<Vehicule> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Vehicule whose createdAt is equals to the given createdAt. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Vehicule> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Vehicule whose updatedAt is equals to the given updatedAt. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Vehicule> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Vehicule whose deletedAt is equals to the given deletedAt. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Vehicule> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Vehicule whose createdBy is equals to the given createdBy. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Vehicule> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Vehicule whose updatedBy is equals to the given updatedBy. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Vehicule> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Vehicule by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Vehicule whose deletedBy is equals to the given deletedBy. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Vehicule> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Vehicule by using typeId as a search criteria.
	 *
	 * @param typeId
	 * @return A list of Object Vehicule whose typeId is equals to the given typeId. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.typeVehicule.id = :typeId and e.isDeleted = :isDeleted")
	List<Vehicule> findByTypeId(@Param("typeId")Integer typeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Vehicule by using typeId as a search criteria.
   *
   * @param typeId
   * @return An Object Vehicule whose typeId is equals to the given typeId. If
   *         no Vehicule is found, this method returns null.
   */
  @Query("select e from Vehicule e where e.typeVehicule.id = :typeId and e.isDeleted = :isDeleted")
  Vehicule findVehiculeByTypeId(@Param("typeId")Integer typeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Vehicule by using clientId as a search criteria.
	 *
	 * @param clientId
	 * @return A list of Object Vehicule whose clientId is equals to the given clientId. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.client.id = :clientId and e.isDeleted = :isDeleted")
	List<Vehicule> findByClientId(@Param("clientId")Integer clientId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Vehicule by using clientId as a search criteria.
   *
   * @param clientId
   * @return An Object Vehicule whose clientId is equals to the given clientId. If
   *         no Vehicule is found, this method returns null.
   */
  @Query("select e from Vehicule e where e.client.id = :clientId and e.isDeleted = :isDeleted")
  Vehicule findVehiculeByClientId(@Param("clientId")Integer clientId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Vehicule by using marqueId as a search criteria.
	 *
	 * @param marqueId
	 * @return A list of Object Vehicule whose marqueId is equals to the given marqueId. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.marque.id = :marqueId and e.isDeleted = :isDeleted")
	List<Vehicule> findByMarqueId(@Param("marqueId")Integer marqueId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Vehicule by using marqueId as a search criteria.
   *
   * @param marqueId
   * @return An Object Vehicule whose marqueId is equals to the given marqueId. If
   *         no Vehicule is found, this method returns null.
   */
  @Query("select e from Vehicule e where e.marque.id = :marqueId and e.isDeleted = :isDeleted")
  Vehicule findVehiculeByMarqueId(@Param("marqueId")Integer marqueId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Vehicule by using couleurVehiculeId as a search criteria.
	 *
	 * @param couleurVehiculeId
	 * @return A list of Object Vehicule whose couleurVehiculeId is equals to the given couleurVehiculeId. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.couleurVehicule.id = :couleurVehiculeId and e.isDeleted = :isDeleted")
	List<Vehicule> findByCouleurVehiculeId(@Param("couleurVehiculeId")Integer couleurVehiculeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Vehicule by using couleurVehiculeId as a search criteria.
   *
   * @param couleurVehiculeId
   * @return An Object Vehicule whose couleurVehiculeId is equals to the given couleurVehiculeId. If
   *         no Vehicule is found, this method returns null.
   */
  @Query("select e from Vehicule e where e.couleurVehicule.id = :couleurVehiculeId and e.isDeleted = :isDeleted")
  Vehicule findVehiculeByCouleurVehiculeId(@Param("couleurVehiculeId")Integer couleurVehiculeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Vehicule by using energieId as a search criteria.
	 *
	 * @param energieId
	 * @return A list of Object Vehicule whose energieId is equals to the given energieId. If
	 *         no Vehicule is found, this method returns null.
	 */
	@Query("select e from Vehicule e where e.energieVehicule.id = :energieId and e.isDeleted = :isDeleted")
	List<Vehicule> findByEnergieId(@Param("energieId")Integer energieId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Vehicule by using energieId as a search criteria.
   *
   * @param energieId
   * @return An Object Vehicule whose energieId is equals to the given energieId. If
   *         no Vehicule is found, this method returns null.
   */
  @Query("select e from Vehicule e where e.energieVehicule.id = :energieId and e.isDeleted = :isDeleted")
  Vehicule findVehiculeByEnergieId(@Param("energieId")Integer energieId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Vehicule by using vehiculeDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Vehicule
	 * @throws DataAccessException,ParseException
	 */
	public default List<Vehicule> getByCriteria(Request<VehiculeDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Vehicule e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Vehicule> query = em.createQuery(req, Vehicule.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Vehicule by using vehiculeDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Vehicule
	 *
	 */
	public default Long count(Request<VehiculeDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Vehicule e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<VehiculeDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		VehiculeDto dto = request.getData() != null ? request.getData() : new VehiculeDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (VehiculeDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(VehiculeDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getImmatriculation())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("immatriculation", dto.getImmatriculation(), "e.immatriculation", "String", dto.getImmatriculationParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNumeroChassis())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numeroChassis", dto.getNumeroChassis(), "e.numeroChassis", "String", dto.getNumeroChassisParam(), param, index, locale));
			}
			if (dto.getDateProchaineRevision()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateProchaineRevision", dto.getDateProchaineRevision(), "e.dateProchaineRevision", "Date", dto.getDateProchaineRevisionParam(), param, index, locale));
			}
			if (dto.getDateVisiteTechnique()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateVisiteTechnique", dto.getDateVisiteTechnique(), "e.dateVisiteTechnique", "Date", dto.getDateVisiteTechniqueParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCreatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedAt()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getTypeId()!= null && dto.getTypeId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeId", dto.getTypeId(), "e.typeVehicule.id", "Integer", dto.getTypeIdParam(), param, index, locale));
			}
			if (dto.getClientId()!= null && dto.getClientId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("clientId", dto.getClientId(), "e.client.id", "Integer", dto.getClientIdParam(), param, index, locale));
			}
			if (dto.getMarqueId()!= null && dto.getMarqueId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("marqueId", dto.getMarqueId(), "e.marque.id", "Integer", dto.getMarqueIdParam(), param, index, locale));
			}
			if (dto.getCouleurVehiculeId()!= null && dto.getCouleurVehiculeId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("couleurVehiculeId", dto.getCouleurVehiculeId(), "e.couleurVehicule.id", "Integer", dto.getCouleurVehiculeIdParam(), param, index, locale));
			}
			if (dto.getEnergieId()!= null && dto.getEnergieId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("energieId", dto.getEnergieId(), "e.energieVehicule.id", "Integer", dto.getEnergieIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeVehiculeLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeVehiculeLibelle", dto.getTypeVehiculeLibelle(), "e.typeVehicule.libelle", "String", dto.getTypeVehiculeLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMarqueLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("marqueLibelle", dto.getMarqueLibelle(), "e.marque.libelle", "String", dto.getMarqueLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCouleurVehiculeLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("couleurVehiculeLibelle", dto.getCouleurVehiculeLibelle(), "e.couleurVehicule.libelle", "String", dto.getCouleurVehiculeLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnergieVehiculeLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("energieVehiculeLibelle", dto.getEnergieVehiculeLibelle(), "e.energieVehicule.libelle", "String", dto.getEnergieVehiculeLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
