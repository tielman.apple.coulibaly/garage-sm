package ci.diaspora.star.motors.sarl.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.diaspora.star.motors.sarl.dao.entity.DesignationElementFicheControle;
import ci.diaspora.star.motors.sarl.helper.dto.DesignationElementFicheControleDto;

/**
 * Repository customize : DesignationElementFicheControle.
 */
@Repository
public interface _DesignationElementFicheControleRepository {
	default List<String> _generateCriteria(DesignationElementFicheControleDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds DesignationElementFicheControle by using libelle and categorieId as a search criteria.
	 *
	 * @param libelle, categorieId
	 * @return An Object DesignationElementFicheControle whose libelle and categorieId are equals to the given libelle and categorieId. If
	 *         no DesignationElementFicheControle is found, this method returns null.
	 */
	@Query("select e from DesignationElementFicheControle e where e.libelle = :libelle and e.categorie.id = :categorieId and e.isDeleted = :isDeleted")
	DesignationElementFicheControle findByLibelleAndCategorieId(@Param("libelle")String libelle, @Param("categorieId")Integer categorieId, @Param("isDeleted")Boolean isDeleted);
	
}
