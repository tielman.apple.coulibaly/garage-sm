

/*
 * Java transformer for entity table bon_decaissement 
 * Created on 2019-08-18 ( Time 17:15:05 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.dao.entity.*;


/**
TRANSFORMER for table "bon_decaissement"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface BonDecaissementTransformer {

	BonDecaissementTransformer INSTANCE = Mappers.getMapper(BonDecaissementTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.userResponsableTechnique.id", target="userResponsableTechniqueId"),
		@Mapping(source="entity.userResponsableTechnique.nom", target="userResponsableTechniqueNom"),
		@Mapping(source="entity.userResponsableTechnique.prenom", target="userResponsableTechniquePrenom"),
		@Mapping(source="entity.userResponsableTechnique.login", target="userResponsableTechniqueLogin"),
		@Mapping(source="entity.userChefAtelier.id", target="userChefAtelierId"),
		@Mapping(source="entity.userChefAtelier.nom", target="userChefAtelierNom"),
		@Mapping(source="entity.userChefAtelier.prenom", target="userChefAtelierPrenom"),
		@Mapping(source="entity.userChefAtelier.login", target="userChefAtelierLogin"),
		@Mapping(source="entity.userResponsableDecaissement.id", target="userResponsableDecaissementId"),
		@Mapping(source="entity.userResponsableDecaissement.nom", target="userResponsableDecaissementNom"),
		@Mapping(source="entity.userResponsableDecaissement.prenom", target="userResponsableDecaissementPrenom"),
		@Mapping(source="entity.userResponsableDecaissement.login", target="userResponsableDecaissementLogin"),
		@Mapping(source="entity.devis.id", target="devisId"),
		@Mapping(source="entity.etat.id", target="etatId"),
		@Mapping(source="entity.etat.code", target="etatCode"),
		@Mapping(source="entity.etat.libelle", target="etatLibelle"),
		@Mapping(source="entity.userResponsableAchat.id", target="userResponsableAchatId"),
		@Mapping(source="entity.userResponsableAchat.nom", target="userResponsableAchatNom"),
		@Mapping(source="entity.userResponsableAchat.prenom", target="userResponsableAchatPrenom"),
		@Mapping(source="entity.userResponsableAchat.login", target="userResponsableAchatLogin"),
	})
	BonDecaissementDto toDto(BonDecaissement entity) throws ParseException;

    List<BonDecaissementDto> toDtos(List<BonDecaissement> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isValidedByChefAtelier", target="isValidedByChefAtelier"),
		@Mapping(source="dto.isValidedByResponsableTechnique", target="isValidedByResponsableTechnique"),
		@Mapping(source="dto.isValidedByResponsableDecaissement", target="isValidedByResponsableDecaissement"),
		@Mapping(source="dto.isValidedByResponsableAchat", target="isValidedByResponsableAchat"),
		@Mapping(source="userResponsableTechnique", target="userResponsableTechnique"),
		@Mapping(source="userChefAtelier", target="userChefAtelier"),
		@Mapping(source="userResponsableDecaissement", target="userResponsableDecaissement"),
		@Mapping(source="devis", target="devis"),
		@Mapping(source="etat", target="etat"),
		@Mapping(source="userResponsableAchat", target="userResponsableAchat"),
	})
    BonDecaissement toEntity(BonDecaissementDto dto, User userResponsableTechnique, User userChefAtelier, User userResponsableDecaissement, Devis devis, Etat etat, User userResponsableAchat) throws ParseException;

    //List<BonDecaissement> toEntities(List<BonDecaissementDto> dtos) throws ParseException;

}
