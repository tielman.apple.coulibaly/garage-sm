


/*
 * Java transformer for entity table vehicule
 * Created on 2019-08-18 ( Time 17:15:17 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.dao.entity.Client;
import ci.diaspora.star.motors.sarl.dao.entity.CouleurVehicule;
import ci.diaspora.star.motors.sarl.dao.entity.EnergieVehicule;
import ci.diaspora.star.motors.sarl.dao.entity.FicheReception;
import ci.diaspora.star.motors.sarl.dao.entity.Marque;
import ci.diaspora.star.motors.sarl.dao.entity.PanneDeclaree;
import ci.diaspora.star.motors.sarl.dao.entity.TypeVehicule;
import ci.diaspora.star.motors.sarl.dao.entity.User;
import ci.diaspora.star.motors.sarl.dao.entity.Vehicule;
import ci.diaspora.star.motors.sarl.dao.repository.ClientRepository;
import ci.diaspora.star.motors.sarl.dao.repository.CouleurVehiculeRepository;
import ci.diaspora.star.motors.sarl.dao.repository.EnergieVehiculeRepository;
import ci.diaspora.star.motors.sarl.dao.repository.FicheReceptionRepository;
import ci.diaspora.star.motors.sarl.dao.repository.MarqueRepository;
import ci.diaspora.star.motors.sarl.dao.repository.TypeVehiculeRepository;
import ci.diaspora.star.motors.sarl.dao.repository.UserRepository;
import ci.diaspora.star.motors.sarl.dao.repository.VehiculeRepository;
import ci.diaspora.star.motors.sarl.helper.ExceptionUtils;
import ci.diaspora.star.motors.sarl.helper.FunctionalError;
import ci.diaspora.star.motors.sarl.helper.TechnicalError;
import ci.diaspora.star.motors.sarl.helper.Utilities;
import ci.diaspora.star.motors.sarl.helper.Validate;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.helper.dto.FicheReceptionDto;
import ci.diaspora.star.motors.sarl.helper.dto.PanneDeclareeDto;
import ci.diaspora.star.motors.sarl.helper.dto.VehiculeDto;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.ClientTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.FicheReceptionTransformer;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.VehiculeTransformer;

/**
BUSINESS for table "vehicule"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class VehiculeBusiness implements IBasicBusiness<Request<VehiculeDto>, Response<VehiculeDto>> {

	private Response<VehiculeDto> response;
	@Autowired
	private VehiculeRepository vehiculeRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TypeVehiculeRepository typeVehiculeRepository;
	@Autowired
	private FicheReceptionRepository ficheReceptionRepository;
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private MarqueRepository marqueRepository;
	@Autowired
	private EnergieVehiculeRepository energieVehiculeRepository;
	@Autowired
	private CouleurVehiculeRepository couleurVehiculeRepository;
	
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	

	@Autowired
	private FicheReceptionBusiness ficheReceptionBusiness;



	public VehiculeBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Vehicule by using VehiculeDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<VehiculeDto> create(Request<VehiculeDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Vehicule-----");

		response = new Response<VehiculeDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Vehicule> items = new ArrayList<Vehicule>();
			List<Marque> itemsMarque = new ArrayList<>();
			List<CouleurVehicule> itemsCouleurVehicule = new ArrayList<>();
			List<TypeVehicule> itemsTypeVehicule = new ArrayList<>();
			List<EnergieVehicule> itemsEnergieVehicule = new ArrayList<>();


			for (VehiculeDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("clientId", dto.getClientId());
				fieldsToVerify.put("typeVehiculeLibelle", dto.getTypeVehiculeLibelle());
				fieldsToVerify.put("marqueLibelle", dto.getMarqueLibelle());
				fieldsToVerify.put("energieVehiculeLibelle", dto.getEnergieVehiculeLibelle());
				fieldsToVerify.put("couleurVehiculeLibelle", dto.getCouleurVehiculeLibelle());
				fieldsToVerify.put("immatriculation", dto.getImmatriculation());

				//fieldsToVerify.put("typeId", dto.getTypeId());
				//fieldsToVerify.put("marqueId", dto.getMarqueId());
				//fieldsToVerify.put("energieId", dto.getEnergieId());
				//fieldsToVerify.put("couleurVehiculeId", dto.getCouleurVehiculeId());
				//fieldsToVerify.put("numeroChassis", dto.getNumeroChassis());
				//fieldsToVerify.put("dateProchaineRevision", dto.getDateProchaineRevision());
				//fieldsToVerify.put("dateVisiteTechnique", dto.getDateVisiteTechnique());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if vehicule to insert do not exist
				Vehicule existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("vehicule -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				if (Utilities.notBlank(dto.getImmatriculation())) {
					existingEntity = vehiculeRepository.findVehiculeByImmatriculation(dto.getImmatriculation(), false);
					if (existingEntity != null ) {
						response.setStatus(functionalError.DATA_EXIST("vehicule -> " + dto.getImmatriculation(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getImmatriculation().equalsIgnoreCase(dto.getImmatriculation()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de l immatriculation '" + dto.getImmatriculation()+"' pour les vehicules", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (Utilities.notBlank(dto.getNumeroChassis())) {
					existingEntity = vehiculeRepository.findVehiculeByNumeroChassis(dto.getNumeroChassis(), false);
					if (existingEntity != null ) {
						response.setStatus(functionalError.DATA_EXIST("vehicule -> " + dto.getNumeroChassis(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNumeroChassis().equalsIgnoreCase(dto.getNumeroChassis()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numeroChassis '" + dto.getNumeroChassis()+"' pour les vehicules", locale));
						response.setHasError(true);
						return response;
					}					
				}

				// Verify if typeVehicule exist
				TypeVehicule existingTypeVehicule = typeVehiculeRepository.findByLibelle(dto.getTypeVehiculeLibelle(), false);
				if (existingTypeVehicule == null) {
					existingTypeVehicule = new TypeVehicule();
					existingTypeVehicule.setLibelle(dto.getTypeVehiculeLibelle());
					existingTypeVehicule.setIsDeleted(false);
					existingTypeVehicule.setCreatedAt(Utilities.getCurrentDate());
					existingTypeVehicule.setCreatedBy(request.getUser());
					typeVehiculeRepository.save(existingTypeVehicule);
					if (itemsTypeVehicule.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getTypeVehiculeLibelle()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getTypeVehiculeLibelle()+"' pour les vehicules", locale));
						response.setHasError(true);
						return response;
					}
					itemsTypeVehicule.add(existingTypeVehicule);
				}
				
				
				
				// Verify if client exist
				Client existingClient = clientRepository.findById(dto.getClientId(), false);
				if (existingClient == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("client -> " + dto.getClientId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if marque exist
				Marque existingMarque = marqueRepository.findByLibelle(dto.getMarqueLibelle(), false);
//				if (existingMarque == null) {
//					response.setStatus(functionalError.DATA_NOT_EXIST("marque -> " + dto.getMarqueLibelle(), locale));
//					response.setHasError(true);
//					return response;
//				}
				
				if (existingMarque == null) {
					existingMarque = new Marque();
					existingMarque.setLibelle(dto.getMarqueLibelle());
					existingMarque.setIsDeleted(false);
					existingMarque.setCreatedAt(Utilities.getCurrentDate());
					existingMarque.setCreatedBy(request.getUser());
					marqueRepository.save(existingMarque);
					if (itemsMarque.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getMarqueLibelle()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getMarqueLibelle()+"' pour les vehicules", locale));
						response.setHasError(true);
						return response;
					}
					itemsMarque.add(existingMarque);
				}
				// Verify if energieVehicule exist
				EnergieVehicule existingEnergieVehicule = energieVehiculeRepository.findByLibelle(dto.getEnergieVehiculeLibelle(), false);
//				if (existingEnergieVehicule == null) {
//					response.setStatus(functionalError.DATA_NOT_EXIST("energieVehicule -> " + dto.getEnergieId(), locale));
//					response.setHasError(true);
//					return response;
//				}
				if (existingEnergieVehicule == null) {
					existingEnergieVehicule = new EnergieVehicule();
					existingEnergieVehicule.setLibelle(dto.getEnergieVehiculeLibelle());
					existingEnergieVehicule.setIsDeleted(false);
					existingEnergieVehicule.setCreatedAt(Utilities.getCurrentDate());
					existingEnergieVehicule.setCreatedBy(request.getUser());
					energieVehiculeRepository.save(existingEnergieVehicule);
					if (itemsEnergieVehicule.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getEnergieVehiculeLibelle()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getEnergieVehiculeLibelle()+"' pour les vehicules", locale));
						response.setHasError(true);
						return response;
					}
					itemsEnergieVehicule.add(existingEnergieVehicule);
				}
				// Verify if couleurVehicule exist
				CouleurVehicule existingCouleurVehicule = couleurVehiculeRepository.findByLibelle(dto.getCouleurVehiculeLibelle(), false);
//				if (existingCouleurVehicule == null) {
//					response.setStatus(functionalError.DATA_NOT_EXIST("couleurVehicule -> " + dto.getCouleurVehiculeLibelle(), locale));
//					response.setHasError(true);
//					return response;
//				}
				if (existingCouleurVehicule == null) {
					existingCouleurVehicule = new CouleurVehicule();
					existingCouleurVehicule.setLibelle(dto.getCouleurVehiculeLibelle());
					existingCouleurVehicule.setIsDeleted(false);
					existingCouleurVehicule.setCreatedAt(Utilities.getCurrentDate());
					existingCouleurVehicule.setCreatedBy(request.getUser());
					couleurVehiculeRepository.save(existingCouleurVehicule);
					if (itemsCouleurVehicule.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getCouleurVehiculeLibelle()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getCouleurVehiculeLibelle()+"' pour les vehicules", locale));
						response.setHasError(true);
						return response;
					}
					itemsCouleurVehicule.add(existingCouleurVehicule);
				}
				
				Vehicule entityToSave = null;
				entityToSave = VehiculeTransformer.INSTANCE.toEntity(dto, existingTypeVehicule, existingClient, existingMarque, existingCouleurVehicule, existingEnergieVehicule);
				entityToSave.setIsDeleted(false);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Vehicule> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = vehiculeRepository.save((Iterable<Vehicule>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("vehicule", locale));
					response.setHasError(true);
					return response;
				}
				List<VehiculeDto> itemsDto = new ArrayList<VehiculeDto>();
				for (Vehicule entity : itemsSaved) {
					VehiculeDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create Vehicule-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Vehicule by using VehiculeDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<VehiculeDto> update(Request<VehiculeDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Vehicule-----");

		response = new Response<VehiculeDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Vehicule> items = new ArrayList<Vehicule>();
			List<Marque> itemsMarque = new ArrayList<>();
			List<CouleurVehicule> itemsCouleurVehicule = new ArrayList<>();
			List<TypeVehicule> itemsTypeVehicule = new ArrayList<>();
			List<EnergieVehicule> itemsEnergieVehicule = new ArrayList<>();



			for (VehiculeDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la vehicule existe
				Vehicule entityToSave = null;
				entityToSave = vehiculeRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("vehicule -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if typeVehicule exist
				if (Utilities.notBlank(dto.getTypeVehiculeLibelle())){
//				if (dto.getTypeId() != null && dto.getTypeId() > 0){
//					TypeVehicule existingTypeVehicule = typeVehiculeRepository.findById(dto.getTypeId(), false);
//					if (existingTypeVehicule == null) {
//						response.setStatus(functionalError.DATA_NOT_EXIST("typeVehicule -> " + dto.getTypeId(), locale));
//						response.setHasError(true);
//						return response;
//					}
					
					TypeVehicule existingTypeVehicule = typeVehiculeRepository.findByLibelle(dto.getTypeVehiculeLibelle(), false);
					if (existingTypeVehicule == null) {
						existingTypeVehicule = new TypeVehicule();
						existingTypeVehicule.setLibelle(dto.getTypeVehiculeLibelle());
						existingTypeVehicule.setIsDeleted(false);
						existingTypeVehicule.setCreatedAt(Utilities.getCurrentDate());
						existingTypeVehicule.setCreatedBy(request.getUser());
						typeVehiculeRepository.save(existingTypeVehicule);
						if (itemsTypeVehicule.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getTypeVehiculeLibelle()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getTypeVehiculeLibelle()+"' pour les vehicules", locale));
							response.setHasError(true);
							return response;
						}
						itemsTypeVehicule.add(existingTypeVehicule);
					}
					entityToSave.setTypeVehicule(existingTypeVehicule);
				}
				// Verify if client exist
				if (dto.getClientId() != null && dto.getClientId() > 0){
					Client existingClient = clientRepository.findById(dto.getClientId(), false);
					if (existingClient == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("client -> " + dto.getClientId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setClient(existingClient);
				}
				// Verify if marque exist
				if (Utilities.notBlank(dto.getMarqueLibelle())){
//				if (Utilities.notBlank(dto.getEnergieVehiculeLibelle())){
//					Marque existingMarque = marqueRepository.findById(dto.getMarqueId(), false);
//					if (existingMarque == null) {
//						response.setStatus(functionalError.DATA_NOT_EXIST("marque -> " + dto.getMarqueId(), locale));
//						response.setHasError(true);
//						return response;
//					}
					
					Marque existingMarque = marqueRepository.findByLibelle(dto.getMarqueLibelle(), false);
					if (existingMarque == null) {
						existingMarque = new Marque();
						existingMarque.setLibelle(dto.getMarqueLibelle());
						existingMarque.setIsDeleted(false);
						existingMarque.setCreatedAt(Utilities.getCurrentDate());
						existingMarque.setCreatedBy(request.getUser());
						marqueRepository.save(existingMarque);
						if (itemsMarque.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getMarqueLibelle()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getMarqueLibelle()+"' pour les vehicules", locale));
							response.setHasError(true);
							return response;
						}
						itemsMarque.add(existingMarque);
					}
					
					entityToSave.setMarque(existingMarque);
				}
				// Verify if energieVehicule exist
				if (Utilities.notBlank(dto.getEnergieVehiculeLibelle())){
					EnergieVehicule existingEnergieVehicule = energieVehiculeRepository.findByLibelle(dto.getEnergieVehiculeLibelle(), false);
//					EnergieVehicule existingEnergieVehicule = energieVehiculeRepository.findById(dto.getEnergieId(), false);
//					if (existingEnergieVehicule == null) {
//						response.setStatus(functionalError.DATA_NOT_EXIST("energieVehicule -> " + dto.getEnergieId(), locale));
//						response.setHasError(true);
//						return response;
//					}
					
					if (existingEnergieVehicule == null) {
						existingEnergieVehicule = new EnergieVehicule();
						existingEnergieVehicule.setLibelle(dto.getEnergieVehiculeLibelle());
						existingEnergieVehicule.setIsDeleted(false);
						existingEnergieVehicule.setCreatedAt(Utilities.getCurrentDate());
						existingEnergieVehicule.setCreatedBy(request.getUser());
						energieVehiculeRepository.save(existingEnergieVehicule);
						if (itemsEnergieVehicule.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getEnergieVehiculeLibelle()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getEnergieVehiculeLibelle()+"' pour les vehicules", locale));
							response.setHasError(true);
							return response;
						}
						itemsEnergieVehicule.add(existingEnergieVehicule);
					}
					entityToSave.setEnergieVehicule(existingEnergieVehicule);
				}
				// Verify if couleurVehicule exist
				//if (dto.getCouleurVehiculeId() != null && dto.getCouleurVehiculeId() > 0){
				if (Utilities.notBlank(dto.getCouleurVehiculeLibelle())){
//					CouleurVehicule existingCouleurVehicule = couleurVehiculeRepository.findById(dto.getCouleurVehiculeId(), false);
//					if (existingCouleurVehicule == null) {
//						response.setStatus(functionalError.DATA_NOT_EXIST("couleurVehicule -> " + dto.getCouleurVehiculeId(), locale));
//						response.setHasError(true);
//						return response;
//					}
					
					CouleurVehicule existingCouleurVehicule = couleurVehiculeRepository.findByLibelle(dto.getCouleurVehiculeLibelle(), false);
					if (existingCouleurVehicule == null) {
						existingCouleurVehicule = new CouleurVehicule();
						existingCouleurVehicule.setLibelle(dto.getCouleurVehiculeLibelle());
						existingCouleurVehicule.setIsDeleted(false);
						existingCouleurVehicule.setCreatedAt(Utilities.getCurrentDate());
						existingCouleurVehicule.setCreatedBy(request.getUser());
						couleurVehiculeRepository.save(existingCouleurVehicule);
						if (itemsCouleurVehicule.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getCouleurVehiculeLibelle()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getCouleurVehiculeLibelle()+"' pour les vehicules", locale));
							response.setHasError(true);
							return response;
						}
						itemsCouleurVehicule.add(existingCouleurVehicule);
					}
					
					entityToSave.setCouleurVehicule(existingCouleurVehicule);
				}

				if (Utilities.notBlank(dto.getImmatriculation())) {
					Vehicule existingEntity = vehiculeRepository.findVehiculeByImmatriculation(dto.getImmatriculation(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("vehicule -> " + dto.getImmatriculation(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getImmatriculation().equalsIgnoreCase(dto.getImmatriculation()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de l immatriculation '" + dto.getImmatriculation()+"' pour les vehicules", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setImmatriculation(dto.getImmatriculation());
				}
				if (Utilities.notBlank(dto.getNumeroChassis())) {
					Vehicule existingEntity = vehiculeRepository.findVehiculeByNumeroChassis(dto.getNumeroChassis(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("vehicule -> " + dto.getNumeroChassis(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNumeroChassis().equalsIgnoreCase(dto.getNumeroChassis()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numeroChassis '" + dto.getNumeroChassis()+"' pour les vehicules", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setNumeroChassis(dto.getNumeroChassis());

				}
				if (Utilities.notBlank(dto.getDateProchaineRevision())) {
					entityToSave.setDateProchaineRevision(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateProchaineRevision()));
				}
				if (Utilities.notBlank(dto.getDateVisiteTechnique())) {
					entityToSave.setDateVisiteTechnique(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateVisiteTechnique()));
				}

				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Vehicule> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = vehiculeRepository.save((Iterable<Vehicule>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("vehicule", locale));
					response.setHasError(true);
					return response;
				}
				List<VehiculeDto> itemsDto = new ArrayList<VehiculeDto>();
				for (Vehicule entity : itemsSaved) {
					VehiculeDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update Vehicule-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete Vehicule by using VehiculeDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<VehiculeDto> delete(Request<VehiculeDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Vehicule-----");

		response = new Response<VehiculeDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Vehicule> items = new ArrayList<Vehicule>();

			for (VehiculeDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la vehicule existe
				Vehicule existingEntity = null;
				existingEntity = vehiculeRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("vehicule -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				
				// ficheReception
				List<FicheReception> listOfFicheReception = ficheReceptionRepository.findByVehiculeId(existingEntity.getId(), false);
				if (listOfFicheReception != null && !listOfFicheReception.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFicheReception.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				vehiculeRepository.save((Iterable<Vehicule>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Vehicule-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Vehicule by using VehiculeDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<VehiculeDto> forceDelete(Request<VehiculeDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Vehicule-----");

		response = new Response<VehiculeDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Vehicule> items = new ArrayList<Vehicule>();

			for (VehiculeDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la vehicule existe
				Vehicule existingEntity = null;
				existingEntity = vehiculeRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("vehicule -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

//				// vehiculePieceVehicule
//				List<VehiculePieceVehicule> listOfVehiculePieceVehicule = vehiculePieceVehiculeRepository.findByVehiculeId(existingEntity.getId(), false);
//				if (listOfVehiculePieceVehicule != null && !listOfVehiculePieceVehicule.isEmpty()){
//					Request<VehiculePieceVehiculeDto> deleteRequest = new Request<VehiculePieceVehiculeDto>();
//					deleteRequest.setDatas(VehiculePieceVehiculeTransformer.INSTANCE.toDtos(listOfVehiculePieceVehicule));
//					deleteRequest.setUser(request.getUser());
//					Response<VehiculePieceVehiculeDto> deleteResponse = vehiculePieceVehiculeBusiness.delete(deleteRequest, locale);
//					if(deleteResponse.isHasError()){
//						response.setStatus(deleteResponse.getStatus());
//						response.setHasError(true);
//						return response;
//					}
//				}
				// ficheReception
				List<FicheReception> listOfFicheReception = ficheReceptionRepository.findByVehiculeId(existingEntity.getId(), false);
				if (listOfFicheReception != null && !listOfFicheReception.isEmpty()){
					Request<FicheReceptionDto> deleteRequest = new Request<FicheReceptionDto>();
					deleteRequest.setDatas(FicheReceptionTransformer.INSTANCE.toDtos(listOfFicheReception));
					deleteRequest.setUser(request.getUser());
					Response<FicheReceptionDto> deleteResponse = ficheReceptionBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				vehiculeRepository.save((Iterable<Vehicule>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Vehicule-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get Vehicule by using VehiculeDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<VehiculeDto> getByCriteria(Request<VehiculeDto> request, Locale locale) {
		slf4jLogger.info("----begin get Vehicule-----");

		response = new Response<VehiculeDto>();

		try {
			List<Vehicule> items = null;
			items = vehiculeRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<VehiculeDto> itemsDto = new ArrayList<VehiculeDto>();
				for (Vehicule entity : items) {
					VehiculeDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(vehiculeRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("vehicule", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Vehicule-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full VehiculeDto by using Vehicule as object.
	 *
	 * @param entity, locale
	 * @return VehiculeDto
	 *
	 */
	private VehiculeDto getFullInfos(Vehicule entity, Integer size, Locale locale) throws Exception {
		VehiculeDto dto = VehiculeTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		Client dataClient = clientRepository.findById(dto.getClientId(), false);
		if (dataClient != null) {
			dto.setDataClient(ClientTransformer.INSTANCE.toDto(dataClient));
		}

		return dto;
	}
}
