


                                                                          /*
 * Java transformer for entity table role_fonctionnalite
 * Created on 2019-08-18 ( Time 17:15:15 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.diaspora.star.motors.sarl.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.diaspora.star.motors.sarl.helper.dto.*;
import ci.diaspora.star.motors.sarl.helper.dto.transformer.*;
import ci.diaspora.star.motors.sarl.helper.contract.*;
import ci.diaspora.star.motors.sarl.helper.contract.IBasicBusiness;
import ci.diaspora.star.motors.sarl.helper.contract.Request;
import ci.diaspora.star.motors.sarl.helper.contract.Response;
import ci.diaspora.star.motors.sarl.dao.entity.*;
import ci.diaspora.star.motors.sarl.helper.*;
import ci.diaspora.star.motors.sarl.dao.repository.*;

/**
BUSINESS for table "role_fonctionnalite"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class RoleFonctionnaliteBusiness implements IBasicBusiness<Request<RoleFonctionnaliteDto>, Response<RoleFonctionnaliteDto>> {

  private Response<RoleFonctionnaliteDto> response;
  @Autowired
  private RoleFonctionnaliteRepository roleFonctionnaliteRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private RoleRepository roleRepository;
  @Autowired
  private FonctionnaliteRepository fonctionnaliteRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

          

  public RoleFonctionnaliteBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create RoleFonctionnalite by using RoleFonctionnaliteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<RoleFonctionnaliteDto> create(Request<RoleFonctionnaliteDto> request, Locale locale)  {
    slf4jLogger.info("----begin create RoleFonctionnalite-----");

    response = new Response<RoleFonctionnaliteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<RoleFonctionnalite> items = new ArrayList<RoleFonctionnalite>();

      for (RoleFonctionnaliteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("fonctionnaliteId", dto.getFonctionnaliteId());
        fieldsToVerify.put("roleId", dto.getRoleId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if roleFonctionnalite to insert do not exist
        RoleFonctionnalite existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("roleFonctionnalite -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if role exist
        Role existingRole = roleRepository.findById(dto.getRoleId(), false);
        if (existingRole == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if fonctionnalite exist
        Fonctionnalite existingFonctionnalite = fonctionnaliteRepository.findById(dto.getFonctionnaliteId(), false);
        if (existingFonctionnalite == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("fonctionnalite -> " + dto.getFonctionnaliteId(), locale));
          response.setHasError(true);
          return response;
        }
        RoleFonctionnalite entityToSave = null;
        entityToSave = RoleFonctionnaliteTransformer.INSTANCE.toEntity(dto, existingRole, existingFonctionnalite);
        entityToSave.setIsDeleted(false);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<RoleFonctionnalite> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = roleFonctionnaliteRepository.save((Iterable<RoleFonctionnalite>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("roleFonctionnalite", locale));
          response.setHasError(true);
          return response;
        }
        List<RoleFonctionnaliteDto> itemsDto = new ArrayList<RoleFonctionnaliteDto>();
        for (RoleFonctionnalite entity : itemsSaved) {
          RoleFonctionnaliteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create RoleFonctionnalite-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update RoleFonctionnalite by using RoleFonctionnaliteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<RoleFonctionnaliteDto> update(Request<RoleFonctionnaliteDto> request, Locale locale)  {
    slf4jLogger.info("----begin update RoleFonctionnalite-----");

    response = new Response<RoleFonctionnaliteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<RoleFonctionnalite> items = new ArrayList<RoleFonctionnalite>();

      for (RoleFonctionnaliteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la roleFonctionnalite existe
        RoleFonctionnalite entityToSave = null;
        entityToSave = roleFonctionnaliteRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("roleFonctionnalite -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if role exist
        if (dto.getRoleId() != null && dto.getRoleId() > 0){
          Role existingRole = roleRepository.findById(dto.getRoleId(), false);
          if (existingRole == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setRole(existingRole);
        }
        // Verify if fonctionnalite exist
        if (dto.getFonctionnaliteId() != null && dto.getFonctionnaliteId() > 0){
          Fonctionnalite existingFonctionnalite = fonctionnaliteRepository.findById(dto.getFonctionnaliteId(), false);
          if (existingFonctionnalite == null) {
            response.setStatus(functionalError.DATA_NOT_EXIST("fonctionnalite -> " + dto.getFonctionnaliteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setFonctionnalite(existingFonctionnalite);
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<RoleFonctionnalite> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = roleFonctionnaliteRepository.save((Iterable<RoleFonctionnalite>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("roleFonctionnalite", locale));
          response.setHasError(true);
          return response;
        }
        List<RoleFonctionnaliteDto> itemsDto = new ArrayList<RoleFonctionnaliteDto>();
        for (RoleFonctionnalite entity : itemsSaved) {
          RoleFonctionnaliteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update RoleFonctionnalite-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete RoleFonctionnalite by using RoleFonctionnaliteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<RoleFonctionnaliteDto> delete(Request<RoleFonctionnaliteDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete RoleFonctionnalite-----");

    response = new Response<RoleFonctionnaliteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<RoleFonctionnalite> items = new ArrayList<RoleFonctionnalite>();

      for (RoleFonctionnaliteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la roleFonctionnalite existe
        RoleFonctionnalite existingEntity = null;
        existingEntity = roleFonctionnaliteRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("roleFonctionnalite -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setIsDeleted(true);
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        roleFonctionnaliteRepository.save((Iterable<RoleFonctionnalite>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete RoleFonctionnalite-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete RoleFonctionnalite by using RoleFonctionnaliteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<RoleFonctionnaliteDto> forceDelete(Request<RoleFonctionnaliteDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete RoleFonctionnalite-----");

    response = new Response<RoleFonctionnaliteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<RoleFonctionnalite> items = new ArrayList<RoleFonctionnalite>();

      for (RoleFonctionnaliteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la roleFonctionnalite existe
        RoleFonctionnalite existingEntity = null;
          existingEntity = roleFonctionnaliteRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("roleFonctionnalite -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

          

            existingEntity.setIsDeleted(true);
                              existingEntity.setDeletedAt(Utilities.getCurrentDate());
                                    existingEntity.setDeletedBy(request.getUser());
            items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          roleFonctionnaliteRepository.save((Iterable<RoleFonctionnalite>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete RoleFonctionnalite-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get RoleFonctionnalite by using RoleFonctionnaliteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<RoleFonctionnaliteDto> getByCriteria(Request<RoleFonctionnaliteDto> request, Locale locale) {
    slf4jLogger.info("----begin get RoleFonctionnalite-----");

    response = new Response<RoleFonctionnaliteDto>();

    try {
      List<RoleFonctionnalite> items = null;
      items = roleFonctionnaliteRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<RoleFonctionnaliteDto> itemsDto = new ArrayList<RoleFonctionnaliteDto>();
        for (RoleFonctionnalite entity : items) {
          RoleFonctionnaliteDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(roleFonctionnaliteRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("roleFonctionnalite", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get RoleFonctionnalite-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full RoleFonctionnaliteDto by using RoleFonctionnalite as object.
   *
   * @param entity, locale
   * @return RoleFonctionnaliteDto
   *
   */
  private RoleFonctionnaliteDto getFullInfos(RoleFonctionnalite entity, Integer size, Locale locale) throws Exception {
    RoleFonctionnaliteDto dto = RoleFonctionnaliteTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
