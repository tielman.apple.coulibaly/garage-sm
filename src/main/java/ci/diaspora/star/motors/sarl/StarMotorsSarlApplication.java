package ci.diaspora.star.motors.sarl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarMotorsSarlApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarMotorsSarlApplication.class, args);
	}

}
